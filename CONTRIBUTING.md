# Farris UI Vue 贡献指南

下面是参与贡献 Farris UI 的共享指南，请在反馈`issue`和`Pull Request`之前，花费几分钟阅读以下内容。
你也可以根据自己的实践经验，自由的通过`Pull Reqeust`修改完善这个指南。

## 反馈Issue

如有你认为自己新发现了一个bug，或者提出一个新特性，请先确定之前没有人提出或者修复过这个bug或者特性。你可以在Issues列表和PR列表中搜索是否有人已经提出了类似bug或者特性。

接下来你可以在Issue列表界面创建一个新的Issue，我们为你提供了Issue模板，请在Issue模板中标记是要反馈「bug」还是「新特性」，并提供必须的上下文说明，以便于开发者可以清晰的理解你的意图。

## 领取Issue

如果你对修复某个bug，或者实现某个新特性敢兴趣，请在这个Issue下方通过评论告诉我们，我们可以进行充分沟通这个「bug」或者「新特性」的具体细节，然后会将Issue的负责人指定给你，将有你负责后续开发工作。
请务必首先认领Issue然后在在开启你的贡献工作。

## 贡献代码

如果你成功领取了项目Issue，请通过Gitee推荐的「fork + pull request」的方式贡献代码。
为了保证项目代码质量，我们指定了详细的编码风格指南。
为了你的PR可以顺利通过代码审查，请在编码前认真阅读以下**编码指南**：

- [Farris UI TypeScript 编码指南](./style-guide/typescript_style_guide.md)

- [Farris UI Vue 组件编码指南](./style-guide/vue_component_style_guide.md)

## 提交Pull Request

我们欢迎你通过提交PR参与项目贡献，在你计划提交PR前，请先阅读以下注意事项：

- 在你提交PR之前请确保已经开启了一个Issue并认领了它，我们只接收与认领Issue关联的PR。如果你打算实现一个比较大的特性，在开启新的Issue前最好先与项目管理者进行充分讨论。

- 在没有十足把握时，尽量提交小规格的PR。不要在一个PR中修复多于一个bug或实现多于一个新特性，以便于更容易被接受。提交两个小规模的PR，会比提交一个大规模修改的PR要好。

- 当你提交新特性，或者修改已有特性时，请包含相应的测试代码，以便于确认组件新的交互特性。

- 在提交PR前端请先执行Rebase以便于保持干净的历史提交记录。

- 我们提供了PR模板，请在提交PR时安装模板要求提供「修改的内容」、「管理的PR」、「测试用例」、「界面预览」等相关内容。


<a rel="license" href="https://creativecommons.org/licenses/by/3.0/"><img alt="Creative Commons License" style="border-width:0" src="https://i.creativecommons.org/l/by/3.0/88x31.png" /></a>
