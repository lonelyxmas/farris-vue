import { SetupContext, computed, defineComponent, ref, watch } from 'vue';
import { dynamicViewProps, DynamicViewProps } from './dynamic-view.props';
import dataGridPropsConverter from './props-converter/data-grid.converter';
import sectionPropsConverter from './props-converter/section.converter';
import { ButtonEdit, DataGrid, Section } from '@farris/ui-vue';

const componentMap: Record<string, any> = {
    'button-edit': ButtonEdit,
    'data-grid': DataGrid,
    section: Section
};

const componentPropsConverter: Record<string, any> = {
    'button-edit': () => { },
    'data-grid': dataGridPropsConverter,
    section: sectionPropsConverter
};

export default defineComponent({
    name: 'FDynamicView',
    props: dynamicViewProps,
    emits: ['update:modelValue'],
    setup(props: DynamicViewProps, context: SetupContext) {
        // const viewSchema = ref(props.schema);
        const modelValue = ref(props.modelValue);
        const hasContent = modelValue.value.content && !!modelValue.value.content.length;
        const componentKey = modelValue.value.type;
        const Component = componentMap[componentKey];

        const propsConverter = componentPropsConverter[componentKey];
        const viewProps = propsConverter ? propsConverter(modelValue) : {};

        function renderContent(content: any[]) {
            return content.map((contentSchema: any) => {
                return <f-dynamic-view v-model={contentSchema}></f-dynamic-view>;
            });
        }

        function render(viewSchem: any) {
            const hasContent = viewSchem.content && !!viewSchem.content.length;
            const componentKey = viewSchem.type;
            const Component = componentMap[componentKey];

            const propsConverter = componentPropsConverter[componentKey];
            const viewProps = propsConverter ? propsConverter(viewSchem) : {};
            return hasContent ? (
                <Component {...viewProps}>{renderContent(modelValue.value.content)}</Component>
            ) : (
                Component ? <Component {...viewProps}></Component> : <div></div>
            );
        }

        watch(
            () => props.modelValue,
            (value: any) => {
                modelValue.value = value;
                return render(modelValue.value);
            }
        );

        return () => {
            return render(modelValue.value);
        };
    }
});
