import { defineStore } from "pinia";
import{StorageThemeCSSUtils} from "@/utils/storage-theme-css";
/**
 * 存储样式给iframe调用
 * name可能的值：farris,framework。当样式重新生成需要更新到页面上时，
 * 需要知道更新的是哪个
 * 思路：样式存储到localstorage里，通过change响应变更 
 */
export const useBuildCSSStore = defineStore("buildCSS", {
  state: () => {
    return {
      change:0,
      name:''
    };
  },
  actions: {
    updateCSS(newCSS:string,themeName:string) {     
      const {saveCSS} =StorageThemeCSSUtils();
      this.change++;
      this.name=themeName;
      saveCSS(newCSS,themeName);
    }
  },
});
