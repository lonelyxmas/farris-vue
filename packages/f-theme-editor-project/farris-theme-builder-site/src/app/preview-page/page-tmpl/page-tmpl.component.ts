import { Component, OnInit } from '@angular/core';
import { PageService } from '../service/page.service';

@Component({
    selector: 'app-page-tmpl',
    templateUrl: './page-tmpl.component.html',
    styleUrls: ['./page-tmpl.component.css']
})
export class PageTmplComponent implements OnInit {
    // 当前选择标签页
    selectedTabId: string;

    constructor(private pgService: PageService) {
        this.pgService.getSelectedTabId().subscribe((tabId) => {
            this.selectedTabId = tabId;
        });
    }

    ngOnInit() {}
}
