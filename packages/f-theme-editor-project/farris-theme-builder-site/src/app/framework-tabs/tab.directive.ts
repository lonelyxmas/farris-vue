/* eslint-disable no-self-assign */
/* eslint-disable no-use-before-define */
import { Directive, EventEmitter, HostBinding, Input, Output, TemplateRef, OnInit, OnDestroy, ElementRef, Renderer2 } from '@angular/core';
import { TabsetComponent } from './tabset.component';

@Directive({
    selector: 'tab, [tab]'
})
export class TabDirective implements OnInit, OnDestroy {
    @Input() heading: string;

    @HostBinding('attr.id')
    @Input()
        id: string;

    @Input() appId: string;

    @Input() appEntrance: string;

    @Input() funcId: string;

    @Input() appType: string;

    @Input() appMode: string;

    @Input() disabled: boolean;

    @Input() removable: boolean;

    @Input() visible: boolean;

    @Input()
    get customClass(): string {
        return this._customClass;
    }

    set customClass(customClass: string) {
        if (this.customClass) {
            this.customClass.split(' ').forEach((cssClass: string) => {
                this.renderer.removeClass(this.elementRef.nativeElement, cssClass);
            });
        }

        this._customClass = customClass ? customClass.trim() : null;

        if (this.customClass) {
            this.customClass.split(' ').forEach((cssClass: string) => {
                this.renderer.addClass(this.elementRef.nativeElement, cssClass);
            });
        }
    }

    @HostBinding('class.isjquery')
    @Input()
    get isjquery(): boolean {
        return this._isjquery;
    }

    set isjquery(value: boolean) {
        this._isjquery = value;
    }

    @HostBinding('class.active')
    @Input()
    get active(): boolean {
        return this._active;
    }

    set active(active: boolean) {
        if (this._active === active) {
            return;
        }

        this.elementRef.nativeElement.style.display = active ? 'flex' : 'none';

        if ((this.disabled && active) || !active) {
            if (this._active && !active) {
                this.deselect.emit(this);
                this._active = active;
            }
            return;
        }

        this._active = active;
        if (active) {
            this.activate.emit(this);
        }

        this.tabset.tabs.forEach((tab: TabDirective) => {
            if (tab !== this && tab.active) {
                tab.active = false;
            }
        });
    }

    @Output() select: EventEmitter<TabDirective> = new EventEmitter();

    @Output() deselect: EventEmitter<TabDirective> = new EventEmitter();

    @Output() removed: EventEmitter<TabDirective> = new EventEmitter();

    @Output() activate: EventEmitter<TabDirective> = new EventEmitter();

    @HostBinding('class.tab-pane') addClass = true;

    headingRef: TemplateRef<any>;

    tabset: TabsetComponent;

    protected _active = false;

    protected _isjquery = false;

    protected _customClass: string;

    constructor(tabset: TabsetComponent, public elementRef: ElementRef, public renderer: Renderer2) {
        this.tabset = tabset;
        this.tabset.addTab(this);
    }

    ngOnInit(): void {
        this.removable = this.removable;
        this.elementRef.nativeElement.style.flex = 1;
    }

    ngOnDestroy(): void {
        this.tabset.removeTab(this, { reselect: false, emit: false });
    }
}
