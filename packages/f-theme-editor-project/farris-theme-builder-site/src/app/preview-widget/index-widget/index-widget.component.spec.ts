import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { IndexWidgetComponent } from './index-widget.component';

describe('IndexWidgetComponent', () => {
    let component: IndexWidgetComponent;
    let fixture: ComponentFixture<IndexWidgetComponent>;

    beforeEach(async(() => {
        TestBed.configureTestingModule({
            declarations: [IndexWidgetComponent]
        }).compileComponents();
    }));

    beforeEach(() => {
        fixture = TestBed.createComponent(IndexWidgetComponent);
        component = fixture.componentInstance;
        fixture.detectChanges();
    });

    it('should create', () => {
        expect(component).toBeTruthy();
    });
});
