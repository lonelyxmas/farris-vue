import { ChangeDetectorRef, Component, EventEmitter, Input, OnChanges, OnInit, Output, SimpleChanges } from '@angular/core';

/**
 * 宽高编辑器
 */
@Component({
    selector: 'app-size-editor',
    templateUrl: './size-editor.component.html',
    styleUrls: ['./size-editor.component.css']
})
export class SizeEditorComponent implements OnInit, OnChanges {

    @Input() originControlStyleList: string[];
    @Output() emitValueChange = new EventEmitter<string>();

    sizeType = [{ value: 'px', text: 'px' }, { value: '%', text: '%' }];

    width: number;
    height: number;


    /** 当前宽度类型 */
    currentWidthType = 'px';

    /** 当前高度类型 */
    currentHeightType = 'px';

    /** 宽度输入框的最大值，用于百分比类型下限制最高100% */
    maxWidth;

    /** 高度输入框的最大值，用于百分比类型下限制最高100% */
    maxHeight;

    constructor(private cd: ChangeDetectorRef) { }
    ngOnInit() {
        // this.initControlSize();
    }
    ngOnChanges(changes: SimpleChanges): void {
        this.initControlSize();
    }


    private initControlSize() {
        const widthStyle = this.getStyleValue('width');
        const heightStyle = this.getStyleValue('height');
        if (widthStyle) {
            this.width = parseInt(widthStyle.replace('px', '').replace('%', ''), 10);
            this.currentWidthType = widthStyle.includes('%') ? '%' : 'px';
            this.maxWidth = this.currentWidthType === '%' ? 100 : null;
        } else {
            this.width = null;
            // this.currentWidthType = 'px';
            this.maxWidth = null;
        }

        if (heightStyle) {
            this.height = parseInt(heightStyle.replace('px', '').replace('%', ''), 10);
            this.currentHeightType = heightStyle.includes('%') ? '%' : 'px';
            this.maxHeight = this.currentHeightType === '%' ? 100 : null;
        } else {
            this.height = null;
            // this.currentHeightType = 'px';
            this.maxHeight = null;
        }
    }

    onChangeWidth() {

        const otherstyles = this.originControlStyleList.filter(item => !item.includes('width'));
        let finalStyle = otherstyles.join(';');

        if (this.width) {
            const widthStyle = 'width:' + this.width + this.currentWidthType + ';';

            finalStyle = widthStyle + finalStyle;
        }


        this.emitValueChange.next(finalStyle);
    }
    onChangeHeight() {
        const otherstyles = this.originControlStyleList.filter(item => !item.includes('height'));
        let finalStyle = otherstyles.join(';');

        if (this.height) {
            const heightStyle = 'height:' + this.height + this.currentHeightType + ';';

            finalStyle = heightStyle + finalStyle;
        }


        this.emitValueChange.next(finalStyle);
    }
    onChangeWidthType() {
        this.maxWidth = this.currentWidthType === '%' ? 100 : null;

        // 若百分比格式下超出100%，就将值清空
        if (this.maxWidth && this.width > this.maxWidth) {
            this.width = null;
            this.cd.detectChanges();
        }
        this.onChangeWidth();
    }
    onChangeHeightType() {
        this.maxHeight = this.currentHeightType === '%' ? 100 : null;

        // 若百分比格式下超出100%，就将值清空
        if (this.maxHeight && this.height > this.maxHeight) {
            this.height = null;
            this.cd.detectChanges();
        }
        this.onChangeHeight();
    }

    /**
     * 根据样式key值，获取value值
     * @param styleKey key值
     */
    private getStyleValue(styleKey: string) {
        const style = this.originControlStyleList.find(item => item && item.includes(styleKey + ':'));

        if (!style) {
            return;
        }
        return style.replace(styleKey + ':', '');

    }
}
