import { Component, OnDestroy, ViewChild } from '@angular/core';
import { NotifyService } from '@farris/ui-notify';
// import { CodeViewComponent, SaveResult } from '@farris/ide-code-view';
import { FormEditorComponent } from './form-editor/form-editor.component';
import { WebCmdService } from '@farris/designer-services';
import { MessagerService } from '@farris/ui-messager';

@Component({
    selector: 'app-root',
    templateUrl: './app.component.html',
    styleUrls: ['./app.component.css']
})
export class AppComponent implements OnDestroy {
    title = 'form-designer';

    /** 界面展示类型：formEditor表单设计器；codeEditor构件编辑器 */
    // showType = 'formEditor';

    /** 元数据路径 */
    metadataPath: string;

    /** 代码视图组件实例 */
    // @ViewChild(CodeViewComponent) private codeViewComponent: CodeViewComponent;

    @ViewChild(FormEditorComponent) private formViewComponent: FormEditorComponent;

    constructor(private notifyService: NotifyService, private webCmdService: WebCmdService, private msgService: MessagerService) {

        // this.metadataPath = gsp.ide.getParam('id');
        // this.webCmdService.webCmpBuilderService.openCodeViewWithNewMethod.subscribe(data => {
        //     const { tsFilePathName: tsFilePath, methodCode, methodName } = data;
        //     if (tsFilePath) {

        //         // 触发刷新代码视图的文件树
        //         this.codeViewComponent.refreshNavTree(tsFilePath);

        //         // 触发打开新创建的ts代码编辑器
        //         this.codeViewComponent.open(tsFilePath);

        //         // 触发新增方法
        //         this.codeViewComponent.sendNotification(tsFilePath, { eventName: 'AddNewMethod', eventPayload: { methodCode, methodName } }).subscribe(result => {
        //             if (result && result.methodCode && result.methodName) {
        //                 this.webCmdService.bindNewMethodToControl(result.methodCode, result.methodName);
        //             }
        //         });

        //         // 切换到代码视图
        //         this.showType = 'codeEditor';
        //     }
        // });
    }

    ngOnDestroy(): void {
        this.webCmdService.webCmpBuilderService.openCodeViewWithNewMethod.unsubscribe();
    }

    /**
     * 切换视图类型
     * @param type  视图类型
     * @param tsFilePath ts文件路径，用于设计器中新建构件后触发打开ts代码
     */
    // changeViewType(type: string, tsFilePath?: string) {
    //     this.showType = type;
    // }
    /**
     * 表单保存后触发代码视图的保存
     */
    onFormSaved() {
        // this.codeViewComponent.throttledSaveAllFile();
    }

    /**
     * 全量保存代码视图
     */
    // onCodeEditorAllSaved(results: SaveResult[]) {
    //     if (results) {
    //         const successes = results.filter(r => r.success);
    //         if (successes.length === results.length) {
    //             this.notifyService.success('保存成功');

    //             if (window.saveFormAndCodeView) {
    //                 parent.IGIX.closeTabWithoutDirtyCheck(this.metadataPath);
    //             }
    //         } else {
    //             const failed = results.filter(r => !r.success);
    //             let message = '';
    //             failed.forEach(f => {
    //                 message += `<p>${f.name} ${f.tip}</p> `;
    //             });
    //             const modalOptions = {
    //                 title: '错误提示',
    //                 showMaxButton: false,
    //                 safeHtml: false
    //             };
    //             this.msgService.show('error', message, modalOptions);
    //         }
    //     }

    //     window.saveFormAndCodeView = false;

    //     // 保存代码视图后刷新web构件
    //     this.formViewComponent.refreshWebCmdAfterCodeViewSaved();
    // }
    // /**
    //  * 保存单个代码视图
    //  */
    // onCodeEditorSaved() {
    //     // 保存代码视图后刷新web构件
    //     this.formViewComponent.refreshWebCmdAfterCodeViewSaved();
    // }

    // /**
    //  * 代码视图发生变化，需要向IDE发送变更通知
    //  */
    // detectCodeFileDirty(flag: boolean) {

    //     window.isFormCodeViewChanged = flag;

    //     const hasChanges = window.isFormCodeViewChanged || window.isFormMetadataChanged;
    //     if (gsp.ide && gsp.ide.setDesignerStatus) {
    //         gsp.ide.setDesignerStatus(this.metadataPath, hasChanges);
    //     }

    // }
}
