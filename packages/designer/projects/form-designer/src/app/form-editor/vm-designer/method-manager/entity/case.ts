import { Extension } from './extension';
import { ITreeNode } from './tree-node';
import { OperationCollection } from './operation-collection';
import { ServiceRef } from './service-ref';
import { AnimationKeyframesSequenceMetadata } from '@angular/animations';

export class Case implements ITreeNode {
    get data(): { id: string; name: string; } {
        return { id: this.id, name: this.name };
    }
    get children(): ITreeNode[] {
        return this.handlers;
    }
    expanded?: boolean;

    id: string;
    name: string;
    condition: string;
    handlers: OperationCollection;

    belongedExt: Extension;

    root: ITreeNode;

    constructor(caseJson?: any, extension?: Extension) {
        // 如果有参数就解析。必须是两个参数都存在。
        if (caseJson) {
            this.parse(caseJson, extension);
        }
    }

    parse(caseJson: any, extension?: Extension) {
        this.id = caseJson.id;
        this.name = caseJson.name;
        this.condition = caseJson.condition;
        this.handlers = new OperationCollection(caseJson.handlers);

        if (extension) {
            this.belongedExt = extension;
        }
    }

    toJson() {
        const result = {
            id: this.id,
            name: this.name,
            condition: this.condition,
            handlers: []
        };
        for (const handler of this.handlers) {
            result.handlers.push(handler.toJson());
        }
        return result;
    }
}
