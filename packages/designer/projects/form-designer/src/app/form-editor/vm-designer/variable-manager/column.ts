import { TemplateRef } from '@angular/core';
import { FormVariableTypes } from '@farris/designer-services';
import { EditorTypes } from '@farris/ui-datagrid-editors';

/**
 * 初始列表的列配置
 */
export const createVariableDataGridColumn = (opCell: TemplateRef<any>) => {
    return [
        { field: 'code', title: '变量编号', editor: { type: EditorTypes.TEXTBOX }, template: opCell, validators: [{ type: 'required', message: '变量编号不能为空！' }] },
        { field: 'name', title: '变量名称', editor: { type: EditorTypes.TEXTBOX }, template: opCell, validators: [{ type: 'required', message: '变量名称不能为空！' }] },
        {
            field: 'type', title: '变量类型',
            editor: {
                type: EditorTypes.COMBOLIST,
                options: {
                    valueField: 'value',
                    textField: 'text',
                    idField: 'value',
                    data: FormVariableTypes,
                    editable: false,
                    showClear: false,
                    enableCancelSelected: false
                }
            },
            formatter: {
                type: 'enum',
                options: {
                    valueField: 'value',
                    textField: 'text',
                    data: FormVariableTypes,
                },
            },
            template: opCell
        },
        {
            field: 'category', title: '变量类别', readonly: true,
            formatter: {
                type: 'enum',
                options: {
                    valueField: 'value',
                    textField: 'text',
                    data: [{ text: '组件变量', value: 'locale' }, { text: '表单变量', value: 'remote' }]
                },
            },
            template: opCell
        },
        {
            field: 'sourceName',
            title: '来源',
            readonly: true

        }
    ];
};
