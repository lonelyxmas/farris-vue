export class ExtendProperty{
    FormCode:string;
    IsCommon:boolean;
}

export class ExtendsConvert{
    InitFromJobject(jsonObj: Object): ExtendProperty {
        if(jsonObj){
            let extendProperty = new ExtendProperty();
            extendProperty.FormCode = jsonObj["FormCode"];
            extendProperty.IsCommon = jsonObj["IsCommon"];
            return extendProperty;
        }
    }

    ConvertJObject(obj: ExtendProperty): Object {
        let extendProperty: ExtendProperty = obj as ExtendProperty;
        let jobj = new Object();
        jobj["FormCode"] = extendProperty.FormCode;
        jobj["IsCommon"] = extendProperty.IsCommon;
        return jobj;
    }
}