import { ICommandItem, CommandItemConvertor } from "./icommand-item"; 
import { CmdParameter } from "./cmd-parameter";

export class WebCommand{
    Id:string;
    Code:string;
    Name:string;
    Description:string;
    Parameters:Array<CmdParameter>;
    SourceCode:string;
    Items: Array<ICommandItem>;
}

export class WebCommandConvertor
{
    ConvertJObject(obj : WebCommand): Object {
        
        let cmpOp = obj as WebCommand;
        let jobj=new Object();
        jobj["Id"]=cmpOp.Id;
        jobj["Code"]=cmpOp.Code;
        jobj["Name"]=cmpOp.Name;
        jobj["Description"]=cmpOp.Description;
        jobj["SourceCode"]=cmpOp.SourceCode;
        jobj["Parameters"]=cmpOp.Parameters;
        let items=[];
        if(cmpOp.Items!=null)
        {
            let itemConvertor = new CommandItemConvertor();
            cmpOp.Items.forEach(element => {
                items.push(itemConvertor.ConvertJObject(element));
            }); 
        }
        jobj["Items"]=items;
        return jobj;
    }    
    
    InitFromJobject(jsonObj: Object): WebCommand {
        let cmpOp = new WebCommand();
        cmpOp.Id = jsonObj["Id"];
        cmpOp.Code = jsonObj["Code"];
        cmpOp.Name = jsonObj["Name"];
        cmpOp.Description = jsonObj["Description"];
        cmpOp.SourceCode = jsonObj["SourceCode"];
        cmpOp.Parameters=jsonObj["Parameters"];
        cmpOp.Items=new Array<ICommandItem>();
        if(jsonObj["Items"]!=null)
        {
            let itemConvertor = new CommandItemConvertor();
            jsonObj["Items"].forEach(element => {
                cmpOp.Items.push(itemConvertor.InitFromJobject(element));
            });
        }
        return cmpOp;
    }
}