import { Subject } from 'rxjs';
import { Observable } from 'rxjs/Observable';

/**
 * 统一的元数据服务抽象类。
 * 不同的场景（低代码、零代码、运行时定制、移动）需要使用不同的元数据服务。这里定义了一套统一的元数据服务抽象类，可以在不同的场景做实现，并注入到注入器（providers）中。
 */
export abstract class FarrisMetadataService {
  /** 根据元数据ID获取元数据 */
  abstract getMetadata(metadataId: string): Observable<FarrisMetadataDto>;
  /** 保存元数据 */
  abstract saveMetadata?(metadataDto: FarrisMetadataDto): Observable<any>;

  /** 根据类型获取元数据列表 */
  abstract getMetadataList?(typeName: string): Observable<any>;

  /** 查询是否有重名的元数据 */
  abstract validateRepeatName?(relativePath: string, fileName: string): Subject<any>;

  /** 根据名称查询元数据 */
  abstract loadMetadata?(fileName: string, relativePath: string): Subject<any>;

  /** 初始创建元数据 */
  abstract initializeMetadataEntity?(metadatadto: FarrisMetadataDto): Observable<any>;

  /** 创建元数据 */
  abstract createMetadata?(metadatadto: FarrisMetadataDto): Observable<any>;



}

/**
 * 元数据结构
 */
export class FarrisMetadataDto {
  id: string;
  nameSpace: string;
  code: string;
  name: string;
  fileName: string;
  type: string;
  bizobjectID: string;
  relativePath: string;
  extendProperty: string;
  content: string;
  extendable: boolean;

  constructor(id: string, nameSpace: string, code: string, name: string, fileName: string, type: string, bizobjectID: string, relativePath: string, extendProperty: string, content: string, extendable: boolean) {
    this.id = id;
    this.nameSpace = nameSpace;
    this.code = code;
    this.name = name;
    this.fileName = fileName;
    this.type = type;
    this.bizobjectID = bizobjectID;
    this.relativePath = relativePath;
    this.extendProperty = extendProperty;
    this.content = content;
    this.extendable = extendable;
  }
}


export class FarrisMetadata4RefDto {
  packageHeader: MetadataPackageHeader;
  metadata: FarrisMetadataDto;
}
export class MetadataPackageHeader {
  name: string;
  version: MetadataPackageVersion;
  location: string;
}
export class MetadataPackageVersion {
  versionString: string;
}