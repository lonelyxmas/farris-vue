import { Injectable } from '@angular/core';
import { Subject } from 'rxjs';

/**
 * 触发表单刷新的服务
 */
@Injectable({
    providedIn: 'root'
})
export class RefreshFormService {
    /** 刷新实体树 */
    public refreshSchamaTree = new Subject<any>();

    /** 刷新可视化区域、重绘页面 */
    public refreshFormDesigner = new Subject<any>();

    /** 刷新控件树 */
    public refreshControlTree = new Subject<any>();

    /** 刷新控件工具箱 */
    public refreshControlBox = new Subject<any>();

}
