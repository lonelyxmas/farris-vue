import { DataGrid, DataGridContextMenuItem, GridField } from '../components/collection/datagrid/export';
import { ListView } from '../components/collection/list-view/export';
import { Table, TableTd } from '../components/collection/table/export';
import { TreeGrid, TreeGridField } from '../components/collection/treegrid/export';
import { Button } from '../components/command/button/export';
import { NavTab } from '../components/command/nav-tab/export';
import { Scrollspy } from '../components/command/scrollspy/export';
import { Steps } from '../components/command/steps/export';
import { Tag } from '../components/command/tag/export';
import { ToolBarItem } from '../components/command/toolbar/component/toolbaritem/export';
import { ToolBar } from '../components/command/toolbar/export';
import { ViewChange } from '../components/command/view-change/export';
import { ComponentRef } from '../components/container/component-ref/export';
import { Component } from '../components/container/component/export';
import { ContentContainer } from '../components/container/content-container/export';
import { DynamicArea, DynamicAreaItem } from '../components/container/dynamic-area/export';
import { ExternalContainer } from '../components/container/external-container/export';
import { FieldSet } from '../components/container/field-set/export';
import { FormHeader } from '../components/container/form-header/export';
import { Form } from '../components/container/form/export';
import { Header, HeaderToolBar, HeaderToolBarItem } from '../components/container/header/export';
import { HiddenContainer } from '../components/container/hidden-container/export';
import { HtmlTemplate } from '../components/container/html-template/export';
import { ListNav } from '../components/container/list-nav/export';
import { LoopContainer } from '../components/container/loop-container/export';
import { ModalContainer } from '../components/container/modal-container/export';
import { ModalFooter, ModalFooterToolBar, ModalFooterToolBarItem } from '../components/container/modal-footer/export';
import { MultiViewContainer, MultiViewItem } from '../components/container/multi-view-container/export';
import { QueryForm } from '../components/container/query-form/export';
import { RouteContainer } from '../components/container/route-container/export';
import { ScrollCollapsibleArea } from '../components/container/scroll-collapsible-area/export';
import { Section, SectionToolbar, SectionToolbarItem } from '../components/container/section/export';
import { Sidebar } from '../components/container/sidebar/export';
import { Splitter, SplitterPane } from '../components/container/splitter/export';
import { Tab, TabPage, TabToolbar, TabToolbarItem } from '../components/container/tab/export';
import { DisplayField } from '../components/data/display-field/export';
import { MultiSelect } from '../components/data/multi-select/export';
import { ListFilter, ListFilterItem } from '../components/filter/list-filter/export';
import { QueryScheme, QuerySchemeItem } from '../components/filter/queryScheme/export';
import { Avatar } from '../components/input/avatar/export';
import { CheckGroup } from '../components/input/check-group/export';
import { CheckBox } from '../components/input/checkbox/export';
import { CitySelector } from '../components/input/city-selector/export';
import { Field } from '../components/input/common/field';
import { DateBox } from '../components/input/date-box/export';
import { DateRange } from '../components/input/date-range/export';
import { Image } from '../components/input/image/export';
import { ImageUpload } from '../components/input/image-upload/export';
import { InputGroup } from '../components/input/input-group/export';
import { LanguageTextBox } from '../components/input/language-text-box/export';
// import { LookupEdit } from '../components/input/lookup-edit/export';
import { MultiTextBox } from '../components/input/multi-text-box/export';
import { NumberRange } from '../components/input/number-range/export';
import { NumericBox } from '../components/input/numeric-box/export';
import { RadioGroup } from '../components/input/radio-group/export';
import { RichTextBox } from '../components/input/rich-text-box/export';
import { Search } from '../components/input/search/export';
import { EnumField } from '../components/input/select/export';
import { SwitchField } from '../components/input/switch/export';
import { Tags } from '../components/input/tags/export';
import { TextBox } from '../components/input/text-box/export';
import { TimePicker } from '../components/input/time-picker/export';
import { StaticText } from '../components/static-text/static-text/export';
import { Wizard, WizardDetail, WizardDetailContainer } from '../components/wizard/export';
import { ResponseLayout, ResponseLayoutItem } from '../components/container/response-layout/export';
import { AppointmentCalendar } from '../components/collection/appointment-calendar/export';

export const AllComponents: any = {
    // 命令类
    Button,
    ViewChange,
    ToolBar,
    ToolBarItem,
    Scrollspy,
    Steps,
    NavTab,
    Tag,

    // 输入类
    Field,
    TextBox,
    Avatar,
    CheckGroup,
    CheckBox,
    DateBox,
    Image,
    ImageUpload,
    RadioGroup,
    InputGroup,
    LanguageTextBox,
    // LookupEdit,
    MultiTextBox,
    NumberRange,
    NumericBox,
    RichTextBox,
    EnumField,
    TimePicker,
    SwitchField,
    Tags,
    CitySelector,

    // 容器类
    ContentContainer,
    Form,
    QueryForm,
    FormHeader,
    Component,
    Tab,
    TabPage,
    TabToolbar,
    TabToolbarItem,
    HtmlTemplate,
    Section,
    SectionToolbar,
    SectionToolbarItem,
    Splitter,
    SplitterPane,
    ComponentRef,
    FieldSet,
    ListNav,
    ExternalContainer,
    Sidebar,
    MultiViewContainer,
    MultiViewItem,
    Header,
    HeaderToolBar,
    HeaderToolBarItem,
    ModalFooterToolBar,
    ModalFooterToolBarItem,
    ModalFooter,
    HiddenContainer,
    ModalContainer,
    ScrollCollapsibleArea,
    RouteContainer,
    LoopContainer,
    DynamicArea,
    DynamicAreaItem,
    ResponseLayout,
    ResponseLayoutItem,

    // 数据集合类
    DataGrid,
    GridField,
    DataGridContextMenuItem,
    TreeGrid,
    TreeGridField,
    ListView,
    Table,
    TableTd,
    AppointmentCalendar,

    // 筛选类
    ListFilter,
    ListFilterItem,
    DateRange,
    Search,
    QueryScheme,
    QuerySchemeItem,

    // 向导
    Wizard,
    WizardDetail,
    WizardDetailContainer,

    // 数据类
    MultiSelect,
    DisplayField,

    // 其它
    StaticText
};
