import { PropertyChangeObject } from '@farris/ide-property-panel';

/**
 * 属性变更实体类
 */
export class FormPropertyChangeObject extends PropertyChangeObject {

    /** 属性变更后是否需要整体刷新表单 */
    needRefreshForm?: boolean;

    /** 属性变更后需要局部刷新的组件id */
    needRefreshedComponentId?: string;

    /** 是否需要刷新控件树 */
    needRefreshControlTree?: boolean;

    /** 是否需要更新控件树节点名称 */
    needUpdateControlTreeNodeName?: boolean;

    /** 关联变更的属性集合，用于更新表单DOM属性 */
    relateChangeProps?: Array<{
        propertyID: string,
        propertyValue: any
    }>;

    /** 属性变更后是否需要向schema字段同步 */
    needSyncToSchemaField?: boolean;

}
