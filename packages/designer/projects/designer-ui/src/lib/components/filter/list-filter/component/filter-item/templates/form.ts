export default (ctx: any) => {
  const require = ctx.filterConfig.control.required ? '<span class="farris-label-info text-danger">*</span>' : '';
  const autoLabel = ctx.autoLabel ? ' filter-input-list-autolabel' : '';

  const divCls = getStyle(ctx);
  const itemWrapCls = ctx.autoLabel ? 'filter-item-wrap' : '';
  const item = `
  <div class="filter-input-item filter-type-input">
    <div class="form-group farris-form-group ${itemWrapCls}">
      <label class="col-form-label" title="${ctx.filterConfig.name}">
        ${require}
        <span class="farris-label-text">${ctx.filterConfig.name}</span>
      </label>
      ${ctx.input}
    </div>
  </div>

  `;
  if (ctx.autoWidth) {
    return `
    <div class="${divCls}">
        ${item}
    </div>
    `;

  } else {
    return `
    <div class="${divCls}">
      <div class="filter-input-list farris-group-wrap ${autoLabel}">
        ${item}
      </div>
    </div>
      `;
  }
};


function getStyle(ctx: any) {
  let cls = '';
  if (ctx.autoWidth) {
    cls = 'filter-input-list farris-group-wrap';
    if (ctx.autoLabel) {
      cls += ' filter-input-list-autolabel';
    }
    if (ctx.component.alLeft) {
      cls += ' filter-input-list-left';
    }

  } else {
    const defaultCondtionStyle = 'col-12 col-md-6 col-xl-3 col-el-2';
    cls = (ctx.filterClass && !ctx.autoWidth) ? ctx.filterClass : defaultCondtionStyle;
  }

  return cls;
}
