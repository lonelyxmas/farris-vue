import { FarrisDesignBaseComponent } from '@farris/designer-element';
import { QuerySchemeControlMapping } from '../../control-mapping';

/**
 * 为了复用输入类控件的模板，这里将筛选项当做一个component
 */
export class QuerySchemeItemComponent extends FarrisDesignBaseComponent {

    parentQuerySchemeInstance: FarrisDesignBaseComponent;

    constructor(component: any, options: any, parentQuerySchemeInstance: any) {
        super(component, options);
        this.parentQuerySchemeInstance = parentQuerySchemeInstance;
    }

    /**
     * 渲染单个输入框
     * @param filterConfig 配置数据
     */
    renderItem(filterConfig: any): any {
        // const parentSchema = this.parentQuerySchemeInstance.component;
        const controlType = QuerySchemeControlMapping[this.component.controltype];
        const inputTemplate = this.renderTemplate(controlType, {
            component: this.component
        });
        return this.renderTemplate('QuerySchemeItem', {
            component: this.component,
            filterConfig,
            input: inputTemplate,
        });
    }
}
