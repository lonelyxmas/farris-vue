import { FarrisDesignBaseComponent } from '@farris/designer-element';
import { ListFilterControlMapping } from '../../control-mapping';

/**
 * 为了复用输入类控件的模板，这里将筛选项当做一个component
 */
export class ListFilterItemComponent extends FarrisDesignBaseComponent {

    parentListFilterInstance: FarrisDesignBaseComponent;

    constructor(component: any, options: any, parentListFilterInstance: any) {
        super(component, options);
        this.parentListFilterInstance = parentListFilterInstance;
    }

    /**
     * 渲染单个输入框
     * @param filterConfig 配置数据
     */
    renderItem(filterConfig: any): any {
        const parentSchema = this.parentListFilterInstance.component;
        const controlType = ListFilterControlMapping[this.component.controltype];
        const inputTemplate = this.renderTemplate(controlType, {
            component: this.component
        });
        return this.renderTemplate('ListFilterItem', {
            component: this.component,
            filterConfig,
            input: inputTemplate,
            autoLabel: parentSchema.autoLabel,
            autoWidth: parentSchema.autoWidth,
            filterClass: parentSchema.filterClass,
            showFilterListLength: parentSchema.filterList ? parentSchema.filterList.length - this.parentListFilterInstance['extendFilterList'].length : 0
        });
    }
}
