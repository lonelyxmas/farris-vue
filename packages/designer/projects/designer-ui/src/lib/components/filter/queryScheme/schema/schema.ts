export const QuerySchemeSchema = {
    id: 'queryScheme1',
    type: 'QueryScheme',
    fieldConfigs: [],
    presetFieldConfigs: [],
    formId: 'formId',
    isDisabled: false,
    presetQuerySolutionName: '默认筛选方案',
    isControlInline: true,
    visible: true,
    onQuery: '',
    binding: null,
    enableInitQuery: false,
    showCompleteLabel: false,
    expanded: true,
    enableHistory: false
};

