import { ElementPropertyConfig } from '@farris/ide-property-panel';
import { DisplayFieldProp } from '../property/property-config';
import { FarrisDesignBaseComponent } from '@farris/designer-element';
import { FormPropertyChangeObject } from '../../../../entity/property-change-entity';

export default class FdDisplayFieldComponent extends FarrisDesignBaseComponent {

    constructor(component: any, options: any) {
        super(component, options);
        this.category = 'data';
    }
    getStyles(): string {
        return 'display: inline-block;';
    }

    getTemplateName(): string {
        return 'DisplayField';
    }

    // 渲染模板
    render(): any {
        return super.render(this.renderTemplate('DisplayField', {
            component: this.component
        }));
    }


    /**
     * 组装属性面板配置数据
     */
    getPropertyConfig(): ElementPropertyConfig[] {
        const serviceHost = this.options.designerHost;
        const prop: DisplayFieldProp = new DisplayFieldProp(serviceHost, this.viewModelId, this.componentId);
        const propertyConfig: ElementPropertyConfig[] = prop.getPropConfig(this.component);
        return propertyConfig;
    }

    /**
     * 属性变更后事件：默认监听样式类属性变更，并触发模板重绘
     * @param changeObject 变更集
     */
    onPropertyChanged(changeObject: FormPropertyChangeObject): void {
        if (changeObject.propertyID !== 'visible') {
            this.triggerRedraw();
        }
    }
}
