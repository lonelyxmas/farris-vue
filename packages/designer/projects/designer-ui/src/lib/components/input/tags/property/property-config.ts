import { ElementPropertyConfig } from '@farris/ide-property-panel';
import { FormPropertyChangeObject } from '../../../../entity/property-change-entity';
import { InputProps } from '../../common/property/input-property-config';

export class TagsProp extends InputProps {

    propertyConfig: ElementPropertyConfig[];

    getPropConfig(propertyData: any): ElementPropertyConfig[] {
        this.propertyConfig = [];

        // 基本信息属性
        const basicPropConfig = this.getBasicPropConfig(propertyData, this.viewModelId);
        this.propertyConfig.push(basicPropConfig);

        // 外观属性
        const appearanceProperties = this.getAppearancePropConfig(propertyData, this.viewModelId);
        this.propertyConfig.push(appearanceProperties);

        // 行为属性
        const behaviorPropConfig = this.getBehaviorPropConfig(propertyData, this.viewModelId);
        this.propertyConfig.push(behaviorPropConfig);

        // 事件属性
        const eventPropConfig = this.getEventPropConfig(propertyData, this.viewModelId);
        this.propertyConfig.push(eventPropConfig);


        return this.propertyConfig;
    }

    private getAppearancePropConfig(propertyData: any, viewModelId: string, showPosition = 'card'): ElementPropertyConfig {

        const self = this;
        let appearanceProperties = [];
        if (showPosition === 'card' || showPosition === 'tableTdEditor') {
            const commonProps = this.getAppearanceCommonPropConfig(propertyData, viewModelId, showPosition);
            appearanceProperties = appearanceProperties.concat(commonProps);
        }
        appearanceProperties.push(
            {
                propertyID: 'className',
                propertyName: '自定义样式',
                propertyType: 'string',
                description: '自定义样式类名。'
            },
            {
                propertyID: 'color',
                propertyName: '自定义背景色',
                propertyType: 'string',
                description: '标记的背景色值，请使用十六进制颜色码，例如#ff0000'
            },
            {
                propertyID: 'tagType',
                propertyName: '标记类型',
                propertyType: 'select',
                iterator: [
                    { key: 'none', value: '无' },
                    { key: 'success', value: '成功类' },
                    { key: 'info', value: '提示类' },
                    { key: 'error', value: '错误类' },
                    { key: 'warning', value: '警告类' }
                ],
                description: '预置的标记类型，优先级低于自定义背景色。'
            }
        );

        const config = {
            categoryId: 'appearance',
            categoryName: '外观',
            properties: appearanceProperties,
            setPropertyRelates(changeObject: FormPropertyChangeObject, prop, parameters) {
                if (!changeObject) {
                    return;
                }
                self.changeAppearancePropertyRelates(this.properties, changeObject, propertyData, parameters);
            },

        };

        if (showPosition !== 'card') {
            Object.assign(config, {
                categoryId: showPosition + '_' + config.categoryId,
                propertyData,
                enableCascade: true,
                parentPropertyID: 'editor',
                tabId: showPosition,
                tabName: '编辑器'
            });
        }
        return config;
    }

    private getBehaviorPropConfig(propertyData: any, viewModelId: string, showPosition = 'card'): ElementPropertyConfig {

        const behaviorProperties = this.getBehaviorCommonPropConfig(propertyData, viewModelId);
        const self = this;
        const config = {
            categoryId: 'behavior',
            categoryName: '行为',
            properties: behaviorProperties,
            setPropertyRelates(changeObject: FormPropertyChangeObject, prop, parameters) {
                self.changeBehaviorPropertyRelates(this.properties, changeObject, propertyData, parameters, showPosition);
            }
        };

        if (showPosition !== 'card') {
            Object.assign(config, {
                categoryId: showPosition + '_' + config.categoryId,
                propertyData,
                enableCascade: true,
                parentPropertyID: 'editor',
                tabId: showPosition,
                tabName: '编辑器'
            });
        }
        return config;
    }

    private getEventPropConfig(propertyData: any, viewModelId: string, showPosition = 'card'): ElementPropertyConfig {

        const eventList = [
            {
                label: 'click',
                name: '点击事件'
            },
            {
                label: 'dataChanged',
                name: '标记数量变更事件'
            },
            {
                label: 'closed',
                name: '关闭标记事件'
            }
        ];
        return this.getEventPropertyConfig(propertyData, viewModelId, showPosition, { customEventList: eventList });
    }

    /**
     * table单元格编辑器属性
     * @param tdData 单元格数据
     * @param viewModelId viewModelId
     * @returns 属性配置
     */
    getTableTdEdtiorPropConfig(tdData: any, viewModelId: string): ElementPropertyConfig[] {
        const propertyData = tdData.editor;

        this.propertyConfig = [];

        // 外观属性
        const appearanceConfig = this.getAppearancePropConfig(propertyData, viewModelId, 'tableTdEditor');
        this.propertyConfig.push(appearanceConfig);

        // 行为属性
        const behaviorConfig = this.getBehaviorPropConfig(propertyData, viewModelId, 'tableTdEditor');
        behaviorConfig.properties = behaviorConfig.properties.filter(p => !'binding,visible'.includes(p.propertyID));
        this.propertyConfig.push(behaviorConfig);

        // 事件属性
        const eventPropConfig = this.getEventPropConfig(propertyData, this.viewModelId, 'tableTdEditor');
        this.propertyConfig.push(eventPropConfig);

        // 表达式属性
        const exprPropConfig = this.getExpressionPropConfig(propertyData, viewModelId, 'tableTdEditor');
        if (exprPropConfig) {
            this.propertyConfig.push(exprPropConfig);
        }
        return this.propertyConfig;
    }

}
