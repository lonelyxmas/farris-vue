import FdImageUploadComponent from './component/fd-img-upload';
import { ImageUploadSchema } from './schema/schema';

import { ComponentExportEntity } from '@farris/designer-element';
import { cloneDeep } from 'lodash-es';
import FdImageUploadTemplates from './templates';

export const ImageUpload: ComponentExportEntity = {
    type: 'ImageUpload',
    component: FdImageUploadComponent,
    template: FdImageUploadTemplates,
    metadata: cloneDeep(ImageUploadSchema)
};
