
export default (ctx: any) => {
  return `
<div class="farris-input-wrap">
  <div class="f-cmp-inputgroup">
    <div class="input-group">
      <input class="form-control text-left  " name="input-group-value" title="" type="text" placeholder="${ctx.component.placeHolder || ''}"
        autocomplete="off" readonly="">
      <div class="input-group-append">
        <span class="input-group-text"><span class="f-icon f-icon-search"></span></span>

      </div>
    </div>
  </div>
</div>
      `;
};
