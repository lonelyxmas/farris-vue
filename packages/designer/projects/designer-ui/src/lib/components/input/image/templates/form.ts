import { ControlCssLoaderUtils } from '../../../../utils/control-css-loader';

export default (ctx: any) => {
  const imgAlt = ctx.component.alt ? ctx.component.alt : '图片';
  const imgStr = `<div class="f-imgcontainer-in-form ide-image"><img src="${ControlCssLoaderUtils.getAssetsUrl()}/images/input/image/img-input.jpg" alt="${imgAlt}" /></div>`;
  if (ctx.component.showInTable) {
    return ` <div class="farris-input-wrap f-utils-hcenter-vcenter">
        ${imgStr}
    </div>`;
  } else {
    return imgStr;
  }
};



