import { TypeConverter } from '@farris/ide-property-panel';

export class ProgressStepsConverter implements TypeConverter {

    constructor() { }
    convertTo(data): string {
        if (data && data.length > 0) {
            return '共 ' + data.length + ' 步';
        }
        return '共 0 步';
    }
}
