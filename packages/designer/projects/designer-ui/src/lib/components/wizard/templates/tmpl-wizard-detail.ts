export default (ctx: any) => {
    let headerTmplStr = '';
    if (ctx.component.headerTemplate) {
        headerTmplStr = `<div class="f-component-wizard-page-detail-header">${ctx.component.headerTemplate}</div>`;
    }
    let contentTmplStr = '';
    if (ctx.component.contentTemplate) {
        contentTmplStr = `<div class="f-component-wizard-page-detail-content">${ctx.component.contentTemplate}</div>`;
    } else {
        contentTmplStr = `<div class="f-component-wizard-page-detail-content">
        <div class="drag-container" id = "${ctx.component.id}" ref="${ctx.nestedKey}" dragref="${ctx.component.id}-container">
            ${ctx.children}
        </div></div>`;
    }
    let footerTmplStr = '';
    if (ctx.component.footerTemplate) {
        footerTmplStr = `<div class="f-component-wizard-page-detail-footer">${ctx.component.footerTemplate}</div>`;
    }
    // fill属性传自wizard组件
    return `<div class="f-component-wizard-page-detail ${ctx.wizardFill ? 'f-component-wizard-page-detail-fill' : ''}" dragref="${ctx.component.id}-container">
    ${headerTmplStr}${contentTmplStr}${footerTmplStr}
    </div>`;
};
