export const contextMenu = [
  {
    id: 'changeToTab',
    title: '切换为标签页'
  },
  {
    id: 'moveToTab',
    title: '移动至标签页'
  }
];
