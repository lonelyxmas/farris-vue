import { ElementPropertyConfig } from '@farris/ide-property-panel';
import { CodeEditorComponent } from '@farris/designer-devkit';
import { ContainerUsualProp } from '../../../container/common/property/container-property-config';
import { FormPropertyChangeObject } from '../../../../entity/property-change-entity';

export class TabPageProp extends ContainerUsualProp {

    propertyConfig: ElementPropertyConfig[];

    getPropConfig(propertyData: any): ElementPropertyConfig[] {
        this.propertyConfig = [];

        // 基本信息属性
        const basicPropConfig = this.getBasicPropConfig();
        this.propertyConfig.push(basicPropConfig);

        // 外观属性
        const appearancePropConfig = this.getAppearancePropConfig();
        this.propertyConfig.push(appearancePropConfig);

        // 行为属性
        const behaviorPropConfig = this.getBehaviorPropConfig(propertyData, this.viewModelId);
        this.propertyConfig.push(behaviorPropConfig);

        // 工具栏位置属性
        const toolbarPropConfig = this.getToolbarPropConfig(propertyData, this.viewModelId);
        this.propertyConfig.push(toolbarPropConfig);

        return this.propertyConfig;
    }

    getBasicPropConfig(): ElementPropertyConfig {
        return {
            categoryId: 'basic',
            categoryName: '基本信息',
            properties: [
                {
                    propertyID: 'id',
                    propertyName: '标识',
                    propertyType: 'string',
                    description: '工具栏按钮的标识',
                    readonly: true
                },
                {
                    propertyID: 'title',
                    propertyName: '标题',
                    propertyType: 'string',
                    description: '工具栏按钮的标签'
                }
            ],
            setPropertyRelates(changeObject: FormPropertyChangeObject) {
                if (!changeObject) {
                    return;
                }
                if (changeObject.propertyID === 'title') {
                    changeObject.needUpdateControlTreeNodeName = true;
                }
            }
        };
    }


    private getAppearancePropConfig(): ElementPropertyConfig {
        const baseAppearanceInfo = this.getCommonAppearanceProperties();

        return {
            categoryId: 'appearance',
            categoryName: '外观',
            properties: [
                ...baseAppearanceInfo,
                {
                    propertyID: 'size',
                    propertyName: '尺寸',
                    propertyType: 'cascade',
                    cascadeConfig: [
                        {
                            propertyID: 'width',
                            propertyName: '宽度（px）',
                            propertyType: 'number',
                            description: '组件的宽度',
                            min: 0,
                            decimals: 0
                        },
                        {
                            propertyID: 'height',
                            propertyName: '高度（px）',
                            propertyType: 'number',
                            description: '组件的高度',
                            min: 0,
                            decimals: 0
                        }
                    ]
                }],
        };
    }

    private getBehaviorPropConfig(propertyData: any, viewModelId: string): ElementPropertyConfig {
        const visibleProp = this.getVisiblePropertyEntity(propertyData, viewModelId);

        return {
            categoryId: 'behavior',
            categoryName: '行为',
            properties: [
                visibleProp,
                {
                    propertyID: 'removeable',
                    propertyName: '是否可移除',
                    propertyType: 'select',
                    iterator: [
                      {key: true, value: '是'},
                      {key: false, value: '否'}
                    ],
                    description: '是否可移除'
                },
                {
                    propertyID: 'headerTemplate',
                    propertyName: '标签页模板',
                    propertyType: 'modal',
                    description: '标签页模板',
                    editor: CodeEditorComponent,
                    editorParams: {
                        language: 'html'
                    }
                },
            ]
        };
    }

    private getToolbarPropConfig(propertyData: any, viewModelId: string): ElementPropertyConfig {
        return {
            categoryId: 'toolbar',
            categoryName: '工具栏',
            propertyData: propertyData.toolbar,
            enableCascade: true,
            parentPropertyID: 'toolbar',
            properties: [
                {
                    propertyID: 'position',
                    propertyName: '工具栏位置',
                    propertyType: 'select',
                    description: '工具栏位置设置',
                    iterator: [{ key: 'inHead', value: '标题区域' }, { key: 'inContent', value: '内容区域' }]
                }
            ]
        };
    }
}
