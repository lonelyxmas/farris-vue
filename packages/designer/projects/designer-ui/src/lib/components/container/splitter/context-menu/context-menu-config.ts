export const contextMenu = [
  {
    id: 'paneDirection',
    title: '布局方向',
    children: [
      {
        id: 'horizontal',
        title: '横向'
      },
      {
        id: 'vertical',
        title: '纵向'
      }
    ]
  },
  {
    id: 'paneProportion',
    title: '分配比例',
    children: [
      {
        id: 'paneProportion11',
        title: '1:1'
      },
      {
        id: 'paneProportion12',
        title: '1:2'
      }
    ]
  }
];

export const splitterPaneContextMenu = [
  {
    title: '添加从表区域',
    id: 'createDetailContainer'
  },
  {
    title: '添加从从表区域',
    id: 'createGrandContainer'
  },
  {
      title: '从表填充',
      id: 'childContainerFill'
  }
];
