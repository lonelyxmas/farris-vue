import { ComponentExportEntity } from '@farris/designer-element';
import FdDynamicAreaComponent from './component/fd-dynamic-area';
import { DynamicAreaItemSchema, DynamicAreaSchema } from './schema/schema';

import FdDynamicAreaTemplates from './templates';

/**
 * 动态区域
 */
export const DynamicArea: ComponentExportEntity = {
    type: 'DynamicArea',
    component: FdDynamicAreaComponent,
    template: FdDynamicAreaTemplates,
    metadata: DynamicAreaSchema
};

/**
 * 动态区域项
 */
export const DynamicAreaItem: ComponentExportEntity = {
    type: 'DynamicAreaItem',
    metadata: DynamicAreaItemSchema
};
