export const contextMenu = [
    {
        id: 'addSiblingContent',
        title: '添加同级',
        children: [
            {
                id: 'Form',
                parentMenuId: 'addSiblingContent',
                title: '卡片'
            }
        ]
    },
    {
        id: 'splitFormComponent',
        title: '拆分组件'
    },
    {
        id: 'changeFormToTableComponent',
        title: '切换为表格'
    }
];
