import { DomService } from '@farris/designer-services';
import { SectionToolbarItemSchema } from './../schema/schema';
import FdToolBarItemComponent from '../../../command/toolbar/component/toolbaritem/component/fd-toolbar-item';
import { FormPropertyChangeObject } from '../../../../entity/property-change-entity';
import { SectionToolbarItemProp } from '../property/toolbar-item-property-config';
import { ElementPropertyConfig } from '@farris/ide-property-panel';
import { DgControl } from '../../../../utils/dg-control';

export default class FdSectionToolBarItemComponent extends FdToolBarItemComponent {
    constructor(component: any, options: any) {
        super(component, options);
        this.viewModelId = '';
        this.viewModelId = this.parent.viewModelId;
        this.componentId = this.parent.componnentId;
    }
    getDefaultSchema() {
        return SectionToolbarItemSchema;
    }

    getPropertyConfig(): ElementPropertyConfig[] {
        const serviceHost = this.options && this.options.designerHost;
        const prop = new SectionToolbarItemProp(serviceHost, this.viewModelId, this.componentId);
        const propertyConfig: ElementPropertyConfig[] = prop.getPropConfig(this.viewModelId, this.component);
        return propertyConfig;
    }

    onPropertyChanged(changeObject: FormPropertyChangeObject): void {
        this.parent.onPropertyChanged(changeObject);
    }

    /**
     * 设置section按钮的展示名称和路径，用于交互面板已绑定事件的窗口
     */
    setComponentBasicInfoMap() {
        if (!this.component) {
            return;
        }
        const btnTitle = this.component['title'];
        let sectionTitle = '';
        const domService = this.options.designerHost.getService('DomService') as DomService;

        const section = this.parent && this.parent.parent;
        if (section && section.type === DgControl.Section.type && section.component) {
            sectionTitle = section.component.mainTitle;
        }
        domService.controlBasicInfoMap.set(this.component.id, {
            showName: btnTitle,
            parentPathName: sectionTitle ? `${sectionTitle} > ${btnTitle}` : btnTitle
        });


    }
}
