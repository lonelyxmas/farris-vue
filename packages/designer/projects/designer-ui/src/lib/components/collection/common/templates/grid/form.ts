
export default (ctx: any) => {

  // 列表模板
  const gridTpl = getGridTpl(ctx);

  // 卡片式编辑相关模板
  const cardEditTpl = getCardEditTpl(ctx);

  return `
    ${gridTpl}
    ${cardEditTpl}
  `;
};


/**
 * 组装列表模板
 */
function getGridTpl(ctx) {

  const headers = ctx.gridFieldInstances.map((gridFieldInstance) => gridFieldInstance.render());
  const minWith = getDataGridHeaderMinWidth(ctx.component.fields || []);
  const displayNoneCls = ctx.isEnableCardEdit && ctx.component.currentViewShowType !== 'grid' ? 'none !important' : '';

  return `
    <div class="ide-dataGrid table table-bordered h-100 f-utils-fill-flex-column f-utils-overflow-auto" style="display:${displayNoneCls};min-height:190px;" ref="${ctx.gridViewPanelKey}">
        <div class="f-datagrid-header d-flex" style="min-width:${minWith}px;">
          ${headers.join('')}
      </div>

      <div style="position:absolute;top:35px;bottom:25px;left:0;right:0;z-index:100" id = "${ctx.component.id}" ref="${ctx.nestedKey}" dragref="${ctx.component.id}-container">  </div>

      <div class="flex-fill">
        <div class="f-datagrid-norecords-content" style="width: 100%; left: 0; z-index: 99;">暂无数据</div>
      </div>

    </div>
`;
}

function getDataGridHeaderMinWidth(fields: any[]) {
  let minWidth = 0;
  fields.forEach(field => {
    let fieldWidth = 120;
    if (field && field.size && field.size.width) {
      fieldWidth = field.size.width;
    }
    minWidth += fieldWidth;
  })

  return minWidth;
}
/**
 * 组装卡片式编辑需要的切换按钮和卡片视图
 */
function getCardEditTpl(ctx: any) {

  if (!ctx.isEnableCardEdit) {
    return '';
  }

  let iconPanel = '';
  ctx.toolbarIconData.forEach(iconItem => {
    const activeCls = iconItem.type === ctx.component.currentViewShowType ? 'tile-btn-active' : '';

    iconPanel += `
        <div class="f-view-change-tile-btn ${activeCls}" title=${iconItem.title} ref=${ctx.gridViewChangeBtnKey} key=${iconItem.type}>
              <span class="tile-btn-icon ${iconItem.icon}"></span>
          </div>
        `;
  });

  const displayNoneCls = ctx.component.currentViewShowType === 'grid' ? 'none !important' : 'flex';
  return `
<div class="pl-1" style="position: absolute;top: 50px;z-index: 110;">
  <div class="f-view-change">
      <div class="f-view-change-tile">
          ${iconPanel}
      </div>
  </div>
</div>
<div ref="${ctx.cardViewPanelKey}" class="cardViewPanel h-100 f-utils-fill-flex-row f-utils-fill" style="display:${displayNoneCls}">${ctx.cardEditTemplates}</div>
  `;
}
