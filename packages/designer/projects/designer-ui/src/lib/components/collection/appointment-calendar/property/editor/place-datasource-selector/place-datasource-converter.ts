import { TypeConverter } from '@farris/ide-property-panel';

export class PlaceDataSourceConverter implements TypeConverter {

    constructor() { }
    convertTo(data): string {

        if (data && data.voId && data.voName) {
            return data.voName;
        }
        if (data && data.formId && data.formName) {
            return data.formName;
        }
        return data;
    }
}
