export const ButtonSchema = {
    id: 'button',
    type: 'Button',
    dataSourceType: 'static',
    text: '按钮',
    appearance: {
        class: 'btn btn-secondary'
    },
    usageMode: 'button',
    disable: false,
    visible: true
};

