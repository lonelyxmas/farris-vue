export const ViewChangeSchema =  {
    id: '',
    type: 'ViewChange',
    defaultType: '',
    changeMethod: 'display',
    viewType: 'dropdown',
    toolbarData: [],
    toolTypeChange: null,
    visible: true,
};

