import FdButtonComponent from './component/fd-button';
import { ButtonSchema } from './schema/schema';
import FdButtonTemplates from './templates';
import { ComponentExportEntity } from '@farris/designer-element';


export const Button: ComponentExportEntity = {
    type: 'Button',
    component: FdButtonComponent,
    template: FdButtonTemplates,
    metadata: ButtonSchema
};
