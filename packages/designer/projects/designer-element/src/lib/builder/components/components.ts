import { FarrisDesignBaseComponent } from '../../base-component/component';
import { FarrisDesignBaseNestedComponent } from '../../base-component/nested-component';
import { assign } from 'lodash-es';

export default class UIComponents {

    static components: any = {};

    static setComponents(uiComponents) {
        const baseComponents = {
            FarrisDesignBaseComponent,
            FarrisDesignBaseNestedComponent
        };
        const allComponents = Object.assign(baseComponents, uiComponents);
        assign(UIComponents.components, allComponents);
    }
    /**
     * 根据控件schema创建组件实例
     * @param component 控件json schema
     * @param options 
     */
    static create(component, options): any {
        let comp = null;

        const fdCmp = UIComponents.components[component.type];

        if (fdCmp && fdCmp['component']) {
            comp = new fdCmp['component'](component, options);
            return comp;
        }

        const baseCmp = UIComponents.components.hasOwnProperty('FarrisDesignBase' + component.type);
        if (baseCmp) {
            if (typeof (fdCmp) === 'object') {
                comp = new baseCmp['component'](component, options);
            } else {
                comp = new baseCmp(component, options);
            }
            return comp;
        }

        return new FarrisDesignBaseComponent(component, options);
    }


}
