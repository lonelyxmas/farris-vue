const fieldConfigs = [
    {
        "id": "e605c9f9-a252-48ca-9c65-0bffb3427e86",
        "labelCode": "ID",
        "code": "ID",
        "name": "主键啊",
        "placeHolder": "",
        "control": { "id": "e605c9f9-a252-48ca-9c65-0bffb3427e86", "controltype": "text", "require": false, "className": "", "modalConfig": { "modalCmp": null, "mapFields": null, "showHeader": true, "title": "", "showCloseButton": true, "showMaxButton": true, "width": 800, "height": 600, "showFooterButtons": true, "footerButtons": [] } }
    },
    {
        "id": "2691b91b-1b5e-4eca-b802-ff5cc2ee7856",
        "labelCode": "Version",
        "code": "Version",
        "name": "版本",
        "placeHolder": "",
        "control": { "id": "2691b91b-1b5e-4eca-b802-ff5cc2ee7856", "controltype": "date", "require": false, "format": "yyyy-MM-dd", "weekSelect": false, "startFieldCode": "Version", "endFieldCode": "Version" }
    },
    {
        "id": "2e6a0b5c-dd36-4bfd-ab3c-e3035f3a8e5a",
        "labelCode": "EmployeeID.EmployeeID",
        "code": "EmployeeID",
        "name": "报销人员下拉帮助",
        "placeHolder": "",
        "control": {
            "controltype": "combolist-help",
            "require": false,
            "uri": "shaixuanceshialltype.employeeID",
            "textField": "name",
            "valueField": "id",
            "idField": "id",
            "helpId": "915a0b20-975a-4df1-8cfd-888c3dda0009",
            "displayType": "List",
            "loadTreeDataType": 'default',
            "enableFullTree": false,
            "editable": false,
            "mapFields": { 'code': 'employeeID.employeeID_code', 'name': 'employeeID.employeeID_name' },
            "singleSelect": true,
            "pageSize": 20,
            "pageList": "10,20,30,50",
            "expandLevel": -1,
            "context": {
                "enableExtendLoadMethod": true
            }
        }
    },
    {
        "id": "387679f1-4f61-40f3-8286-b3b55219a7aa",
        "labelCode": "EmployeeID.EmployeeID_code",
        "code": "code",
        "name": "编号",
        "placeHolder": "",
        "control": { "id": "387679f1-4f61-40f3-8286-b3b55219a7aa", "controltype": "text", "require": false, "className": "", "modalConfig": { "modalCmp": null, "mapFields": null, "showHeader": true, "title": "", "showCloseButton": true, "showMaxButton": true, "width": 800, "height": 600, "showFooterButtons": true, "footerButtons": [] } }
    },
    {
        "id": "f2878e6d-99a2-45e7-8678-3635b1e0ec2a",
        "labelCode": "EmployeeID.EmployeeID_name",
        "code": "name",
        "name": "名称",
        "placeHolder": "",
        "control": { "id": "f2878e6d-99a2-45e7-8678-3635b1e0ec2a", "controltype": "text", "require": false, "className": "", "modalConfig": { "modalCmp": null, "mapFields": null, "showHeader": true, "title": "", "showCloseButton": true, "showMaxButton": true, "width": 800, "height": 600, "showFooterButtons": true, "footerButtons": [] } }
    },
    {
        "id": "393c7ac7-64b2-4888-83a9-c1d8b2755339",
        "labelCode": "DomainID.DomainID",
        "code": "DomainID",
        "name": "所属部门弹出帮助",
        "placeHolder": "",
        "control": {
            "controltype": "help",
            "require": false,
            "uri": "shaixuanceshialltype.domainID",
            "textField": "name",
            "valueField": "id",
            "idField": "id",
            "helpId": "b524a702-7323-4d46-998e-5ba0c6abcd49",
            "displayType": "TreeList",
            "loadTreeDataType": 'default',
            "enableFullTree": false,
            "editable": false,
            "dialogTitle": "",
            "singleSelect": true,
            "enableCascade": false,
            "cascadeStatus": "enable",
            "pageSize": 20,
            "pageList": "10,20,30,50",
            "nosearch": false,
            "expandLevel": -1,
            "context": {
                "enableExtendLoadMethod": true
            },
            "quickSelect": {
                "enable": false,
                "showMore": true,
                "showItemsCount": 10
            }
        }
    },
    {
        "id": "138082d4-9b07-4da2-9858-73861e0ac127",
        "labelCode": "DomainID.DomainID_code",
        "code": "code",
        "name": "编号",
        "placeHolder": "",
        "control": { "id": "138082d4-9b07-4da2-9858-73861e0ac127", "controltype": "text", "require": false, "className": "", "modalConfig": { "modalCmp": null, "mapFields": null, "showHeader": true, "title": "", "showCloseButton": true, "showMaxButton": true, "width": 800, "height": 600, "showFooterButtons": true, "footerButtons": [] } }
    },
    {
        "id": "99587da0-8c9d-4efb-a196-f5762ba6000e",
        "labelCode": "DomainID.DomainID_name",
        "code": "name",
        "name": "名称",
        "placeHolder": "",
        "control": { "id": "99587da0-8c9d-4efb-a196-f5762ba6000e", "controltype": "text", "require": false, "className": "", "modalConfig": { "modalCmp": null, "mapFields": null, "showHeader": true, "title": "", "showCloseButton": true, "showMaxButton": true, "width": 800, "height": 600, "showFooterButtons": true, "footerButtons": [] } }
    },
    {
        "id": "d9ace6f2-a77a-4b2b-9b3c-61154bb40848",
        "labelCode": "BillCode",
        "code": "BillCode",
        "name": "单据编号inputgroup",
        "placeHolder": "",
        "control": { "id": "d9ace6f2-a77a-4b2b-9b3c-61154bb40848", "controltype": "input-group", "require": false, "className": "", "editable": true, "groupText": "", "usageMode": "text", "modalConfig": { "modalCmp": null, "mapFields": null, "showHeader": true, "title": "", "showCloseButton": true, "showMaxButton": true, "width": 800, "height": 600, "showFooterButtons": true, "footerButtons": [] }, "groupTextUseLang": true }
    },
    {
        "id": "3a8403c1-aa75-4091-947e-53f1d2bb8d4d",
        "labelCode": "TotalSum",
        "code": "TotalSum",
        "name": "报帐金额",
        "placeHolder": "",
        "control": {"id":"3a8403c1-aa75-4091-947e-53f1d2bb8d4d","controltype":"single-number","require":false,"textAlign":"left","precision":2,"isBigNumber":false,"className":"","maxValue":100000,"minValue":-100000}
    },
    {
        "id": "c7dbe607-bddc-4391-92e2-16f0221765df",
        "labelCode": "BillType",
        "code": "BillType",
        "name": "报销类型多选",
        "placeHolder": "",
        "control": {
            "controltype": "dropdown",
            "require": false,
            "valueType": "1",
            "multiSelect": true,
            "enumValues": [
                {
                    "value": "1",
                    "name": "1"
                },
                {
                    "value": "2",
                    "name": "2"
                },
                {
                    "value": "3",
                    "name": "3"
                }
            ]
        }
    },
    {
        "id": "d152e48d-13d1-4553-94fa-525fa67d4f2b",
        "labelCode": "BillDate",
        "code": "BillDate",
        "name": "制单日期单日期",
        "placeHolder": "",
        "control": { "id": "d152e48d-13d1-4553-94fa-525fa67d4f2b", "controltype": "date", "require": false, "format": "yyyy-MM-dd", "weekSelect": false, "startFieldCode": "BillDate", "endFieldCode": "BillDate" }
    },
    {
        "id": "abb7ef8e-ef19-4d83-99cc-1b7ff4f05345",
        "labelCode": "SecID",
        "code": "SecID",
        "name": "密级ID下拉选择",
        "placeHolder": "",
        "control": {
            "controltype": "dropdown",
            "require": false,
            "valueType": "1",
            "multiSelect": false,
            "enumValues": [
                {
                    "value": "a",
                    "name": "a"
                },
                {
                    "value": "b",
                    "name": "b"
                },
                {
                    "value": "c",
                    "name": "c"
                },
                {
                    "value": "d",
                    "name": "d"
                },
                {
                    "value": "e",
                    "name": "e"
                }
            ]
        }
    },
    {
        "id": "219c9c45-3bfb-4482-8755-1ea0d3c9475c",
        "labelCode": "SecLevel",
        "code": "SecLevel",
        "name": "密级",
        "placeHolder": "",
        "control": { "id": "219c9c45-3bfb-4482-8755-1ea0d3c9475c", "controltype": "number", "require": false, "className": "", "textAlign": "left", "precision": 0, "isBigNumber": false }
    },
    {
        "id": "01ca22ef-fbd4-4322-8e88-503edb9e071c",
        "labelCode": "ProjectID",
        "code": "ProjectID",
        "name": "所属项目",
        "placeHolder": "",
        "control": { "id": "01ca22ef-fbd4-4322-8e88-503edb9e071c", "controltype": "text", "require": false, "className": "", "modalConfig": { "modalCmp": null, "mapFields": null, "showHeader": true, "title": "", "showCloseButton": true, "showMaxButton": true, "width": 800, "height": 600, "showFooterButtons": true, "footerButtons": [] } }
    },
    {
        "id": "d9b196a3-2bf0-46ba-8807-22f6ac62ad78",
        "labelCode": "ProjectMrg",
        "code": "ProjectMrg",
        "name": "项目经理",
        "placeHolder": "",
        "control": { "id": "d9b196a3-2bf0-46ba-8807-22f6ac62ad78", "controltype": "text", "require": false, "className": "", "modalConfig": { "modalCmp": null, "mapFields": null, "showHeader": true, "title": "", "showCloseButton": true, "showMaxButton": true, "width": 800, "height": 600, "showFooterButtons": true, "footerButtons": [] } }
    },
    {
        "id": "6f1bf2f5-7afc-42c5-81ee-b547db370be9",
        "labelCode": "AuditStatus",
        "code": "AuditStatus",
        "name": "稽核状态单选",
        "placeHolder": "",
        "control": {
            "controltype": "radio",
            "require": false,
            "valueType": "1",
            "horizontal": true,
            "showLabel": false,
            "enumValues": [
                {
                    "value": "1",
                    "name": "1"
                },
                {
                    "value": "2",
                    "name": "2"
                }
            ]
        }
    }
];

const querySolutionList = [
    // {
    //     "id": "3a491eb1-018e-e03e-d430-f05fccd894f3",
    //     "belongId": "shaixuanalltype",
    //     "code": "xxx",
    //     "isSystem": false,
    //     "name": null,
    //     "queryConditionString": "[{\"ID\":\"2e6a0b5c-dd36-4bfd-ab3c-e3035f3a8e5a\",\"FieldCode\":\"EmployeeID.EmployeeID\",\"FieldName\":\"报销人员下拉帮助\",\"ValueType\":0,\"placeHolder\":\"\",\"beginPlaceHolder\":\"\",\"endPlaceHolder\":\"\",\"visible\":true,\"Value\":{\"Type\":13,\"Content\":{\"valueField\":\"\",\"value\":[],\"textValue\":\"help-text-value-null\"}},\"Control\":{\"Type\":13,\"Content\":{\"uri\":\"shaixuanceshialltype.employeeID\",\"idField\":\"id\",\"valueField\":\"id\",\"textField\":\"name\",\"mapFields\":{\"code\":\"employeeID.employeeID_code\",\"name\":\"employeeID.employeeID_name\"},\"displayType\":\"LOOKUPLIST\",\"singleSelect\":false,\"enableFullTree\":false,\"loadTreeDataType\":\"default\",\"expandLevel\":-1,\"require\":false,\"context\":{\"enableExtendLoadMethod\":true}}}},{\"ID\":\"393c7ac7-64b2-4888-83a9-c1d8b2755339\",\"FieldCode\":\"DomainID.DomainID\",\"FieldName\":\"所属部门弹出帮助\",\"ValueType\":0,\"placeHolder\":\"\",\"beginPlaceHolder\":\"\",\"endPlaceHolder\":\"\",\"visible\":true,\"Value\":{\"Type\":2,\"Content\":{\"value\":[],\"valueField\":\"\",\"textValue\":\"help-text-value-null\",\"isInputText\":false}},\"Control\":{\"Type\":2,\"Content\":{\"uri\":\"shaixuanceshialltype.domainID\",\"textField\":\"name\",\"valueField\":\"id\",\"displayType\":\"TreeList\",\"idField\":\"id\",\"context\":{\"enableExtendLoadMethod\":true},\"enableFullTree\":false,\"loadTreeDataType\":\"default\",\"singleSelect\":false,\"expandLevel\":-1,\"enableCascade\":false,\"cascadeStatus\":\"enable\",\"require\":false,\"nosearch\":false,\"editable\":false,\"dialogTitle\":\"\",\"pageSize\":20,\"pageList\":\"10,20,30,50\",\"quickSelect\":{\"enable\":false,\"showItemsCount\":10,\"showMore\":true}}}},{\"ID\":\"d9ace6f2-a77a-4b2b-9b3c-61154bb40848\",\"FieldCode\":\"BillCode\",\"FieldName\":\"单据编号inputgroup\",\"ValueType\":0,\"placeHolder\":\"\",\"beginPlaceHolder\":\"\",\"endPlaceHolder\":\"\",\"visible\":true,\"Value\":{\"Type\":15,\"Content\":{\"value\":[],\"textValue\":\"\"}},\"Control\":{\"Type\":15,\"Content\":{\"className\":\"\",\"require\":false,\"editable\":true,\"groupText\":\"\",\"usageMode\":\"text\"}}},{\"ID\":\"3a8403c1-aa75-4091-947e-53f1d2bb8d4d\",\"FieldCode\":\"TotalSum\",\"FieldName\":\"报帐金额\",\"ValueType\":0,\"placeHolder\":\"\",\"beginPlaceHolder\":\"\",\"endPlaceHolder\":\"\",\"visible\":true,\"Value\":{\"Type\":5,\"Content\":{\"startValue\":null,\"endValue\":null}},\"Control\":{\"Type\":5,\"Content\":{\"className\":\"\",\"placeholder\":\"\",\"require\":false,\"precision\":2,\"textAlign\":\"left\",\"bigNumber\":false}}},{\"ID\":\"c7dbe607-bddc-4391-92e2-16f0221765df\",\"FieldCode\":\"BillType\",\"FieldName\":\"报销类型多选\",\"ValueType\":0,\"placeHolder\":\"\",\"beginPlaceHolder\":\"\",\"endPlaceHolder\":\"\",\"visible\":true,\"Value\":{\"Type\":3,\"Content\":{\"value\":[]}},\"Control\":{\"Type\":3,\"Content\":{\"valueType\":\"1\",\"enumValues\":[{\"Type\":1,\"Content\":{\"value\":\"1\",\"name\":\"1\"}},{\"Type\":1,\"Content\":{\"value\":\"2\",\"name\":\"2\"}},{\"Type\":1,\"Content\":{\"value\":\"3\",\"name\":\"3\"}}],\"multiSelect\":true,\"require\":false}}},{\"ID\":\"d152e48d-13d1-4553-94fa-525fa67d4f2b\",\"FieldCode\":\"BillDate\",\"FieldName\":\"制单日期单日期\",\"ValueType\":0,\"placeHolder\":\"\",\"beginPlaceHolder\":\"\",\"endPlaceHolder\":\"\",\"visible\":true,\"Value\":{\"Type\":1,\"Content\":{\"dateValue\":\"\"}},\"Control\":{\"Type\":1,\"Content\":{\"format\":\"yyyy-MM-dd\",\"returnFormat\":\"yyyy-MM-dd\",\"require\":false,\"isDynamicDate\":true}}},{\"ID\":\"abb7ef8e-ef19-4d83-99cc-1b7ff4f05345\",\"FieldCode\":\"SecID\",\"FieldName\":\"密级ID下拉选择\",\"ValueType\":0,\"placeHolder\":\"\",\"beginPlaceHolder\":\"\",\"endPlaceHolder\":\"\",\"visible\":true,\"Value\":{\"Type\":3,\"Content\":{\"value\":[]}},\"Control\":{\"Type\":3,\"Content\":{\"valueType\":\"1\",\"enumValues\":[{\"Type\":1,\"Content\":{\"value\":\"a\",\"name\":\"a\"}},{\"Type\":1,\"Content\":{\"value\":\"b\",\"name\":\"b\"}},{\"Type\":1,\"Content\":{\"value\":\"c\",\"name\":\"c\"}},{\"Type\":1,\"Content\":{\"value\":\"d\",\"name\":\"d\"}},{\"Type\":1,\"Content\":{\"value\":\"e\",\"name\":\"e\"}}],\"multiSelect\":true,\"require\":false}}},{\"ID\":\"219c9c45-3bfb-4482-8755-1ea0d3c9475c\",\"FieldCode\":\"SecLevel\",\"FieldName\":\"密级\",\"ValueType\":0,\"placeHolder\":\"\",\"beginPlaceHolder\":\"\",\"endPlaceHolder\":\"\",\"visible\":true,\"Value\":{\"Type\":5,\"Content\":{\"startValue\":null,\"endValue\":null}},\"Control\":{\"Type\":5,\"Content\":{\"className\":\"\",\"placeholder\":\"\",\"require\":false,\"precision\":0,\"textAlign\":\"left\",\"bigNumber\":false}}},{\"ID\":\"6f1bf2f5-7afc-42c5-81ee-b547db370be9\",\"FieldCode\":\"AuditStatus\",\"FieldName\":\"稽核状态单选\",\"ValueType\":0,\"placeHolder\":\"\",\"beginPlaceHolder\":\"\",\"endPlaceHolder\":\"\",\"visible\":true,\"Value\":{\"Type\":14,\"Content\":{}},\"Control\":{\"Type\":14,\"Content\":{\"valueType\":\"1\",\"enumValues\":[{\"Type\":1,\"Content\":{\"value\":\"1\",\"name\":\"1\"}},{\"Type\":1,\"Content\":{\"value\":\"2\",\"name\":\"2\"}}],\"horizontal\":true,\"showLabel\":false,\"require\":false}}},{\"ID\":\"01ca22ef-fbd4-4322-8e88-503edb9e071c\",\"FieldCode\":\"ProjectID\",\"FieldName\":\"所属项目\",\"ValueType\":0,\"placeHolder\":\"\",\"beginPlaceHolder\":\"\",\"endPlaceHolder\":\"\",\"visible\":true,\"Value\":{\"Type\":0,\"Content\":{\"value\":\"\"}},\"Control\":{\"Type\":0,\"Content\":{\"className\":\"\",\"require\":false}}}]",
    //     "advancedQueryConditionsString": null,
    //     "mode": "1",
    //     "userCode": "wangjinzhe",
    //     "userId": "9e35953a-40fd-4a9a-922f-b9f59bf2a517",
    //     "userName": "wangjinzhe",
    //     "isDefault": false,
    //     "type": "private",
    //     "extendId": "query-scheme-1",
    //     "extendInfo": null,
    //     "code_chs": null,
    //     "code_en": null,
    //     "code_cht": null
    // },
    // {
    //     "id": "2120021b-df0c-0eef-d1d5-feaa78b01d7e",
    //     "belongId": "shaixuanalltype",
    //     "code": "aaa",
    //     "isSystem": false,
    //     "name": null,
    //     "queryConditionString": "[{\"ID\":\"e605c9f9-a252-48ca-9c65-0bffb3427e86\",\"FieldCode\":\"ID\",\"FieldName\":\"主键啊\",\"ValueType\":0,\"placeHolder\":\"\",\"beginPlaceHolder\":\"\",\"endPlaceHolder\":\"\",\"visible\":true,\"conditionid\":9,\"compareType\":0,\"relation\":1,\"Value\":{\"Type\":0,\"Content\":{\"value\":\"123\"}},\"Control\":{\"Type\":0,\"Content\":{\"className\":\"\",\"require\":false}}},{\"ID\":\"6f1bf2f5-7afc-42c5-81ee-b547db370be9\",\"FieldCode\":\"AuditStatus\",\"FieldName\":\"稽核状态单选\",\"ValueType\":0,\"placeHolder\":\"\",\"beginPlaceHolder\":\"\",\"endPlaceHolder\":\"\",\"visible\":true,\"conditionid\":6,\"compareType\":0,\"relation\":1,\"Lbracket\":\"(\",\"Value\":{\"Type\":14,\"Content\":{\"value\":\"2\"}},\"Control\":{\"Type\":14,\"Content\":{\"valueType\":\"1\",\"enumValues\":[{\"Type\":1,\"Content\":{\"value\":\"1\",\"name\":\"1\"}},{\"Type\":1,\"Content\":{\"value\":\"2\",\"name\":\"2\"}}],\"horizontal\":true,\"showLabel\":false,\"require\":false}}},{\"ID\":\"2e6a0b5c-dd36-4bfd-ab3c-e3035f3a8e5a\",\"FieldCode\":\"EmployeeID.EmployeeID\",\"FieldName\":\"报销人员下拉帮助\",\"ValueType\":0,\"placeHolder\":\"\",\"beginPlaceHolder\":\"\",\"endPlaceHolder\":\"\",\"visible\":true,\"conditionid\":1,\"compareType\":0,\"relation\":1,\"Lbracket\":\"((\",\"Value\":{\"Type\":13,\"Content\":{\"valueField\":\"id\",\"value\":[{\"id\":\"9e35953a-40fd-4a9a-922f-b9f59bf2a517\",\"code\":\"wangjinzhe\",\"name\":\"wangjinzhe\",\"userGroup\":\"0\",\"sysOrgId\":\"001\",\"tenantId\":10000,\"secLevel\":\"PUBLIC\",\"userType\":3,\"note\":null},{\"id\":\"7e9676e7-5683-4d56-a85e-45d32e2d996c\",\"code\":\"wangjinzhe2\",\"name\":\"wangjinzhe2\",\"userGroup\":\"0\",\"sysOrgId\":\"2d5b79a4-3a86-3fd2-7f71-02c9dc68df6e\",\"tenantId\":10000,\"secLevel\":\"PUBLIC\",\"userType\":3,\"note\":null}],\"textValue\":\"wangjinzhe,wangjinzhe2\"}},\"Control\":{\"Type\":13,\"Content\":{\"uri\":\"shaixuanceshialltype.employeeID\",\"idField\":\"id\",\"valueField\":\"id\",\"textField\":\"name\",\"mapFields\":{\"code\":\"employeeID.employeeID_code\",\"name\":\"employeeID.employeeID_name\"},\"displayType\":\"LOOKUPLIST\",\"singleSelect\":false,\"enableFullTree\":false,\"loadTreeDataType\":\"default\",\"expandLevel\":-1,\"require\":false,\"context\":{\"enableExtendLoadMethod\":true}}}},{\"ID\":\"393c7ac7-64b2-4888-83a9-c1d8b2755339\",\"FieldCode\":\"DomainID.DomainID\",\"FieldName\":\"所属部门弹出帮助\",\"ValueType\":0,\"placeHolder\":\"\",\"beginPlaceHolder\":\"\",\"endPlaceHolder\":\"\",\"visible\":true,\"conditionid\":2,\"compareType\":0,\"relation\":1,\"Rbracket\":\")\",\"Value\":{\"Type\":2,\"Content\":{\"value\":[{\"id\":\"1640d027-7531-2388-e5eb-6fe0ae8c9734\",\"code\":\"SCM\",\"name\":\"供应链产品部\",\"treeinfo\":{\"parentElement\":\"001\",\"sequence\":2,\"layer\":2,\"isDetail\":true}},{\"id\":\"001\",\"code\":\"001\",\"name\":\"浪潮国际\",\"treeinfo\":{\"parentElement\":\"\",\"sequence\":1,\"layer\":1,\"isDetail\":false}}],\"valueField\":\"id\",\"textValue\":\"供应链产品部,浪潮国际\",\"isInputText\":false}},\"Control\":{\"Type\":2,\"Content\":{\"uri\":\"shaixuanceshialltype.domainID\",\"textField\":\"name\",\"valueField\":\"id\",\"displayType\":\"TreeList\",\"idField\":\"id\",\"context\":{\"enableExtendLoadMethod\":true},\"enableFullTree\":false,\"loadTreeDataType\":\"default\",\"singleSelect\":false,\"expandLevel\":-1,\"enableCascade\":false,\"cascadeStatus\":\"enable\",\"require\":false,\"nosearch\":false,\"editable\":false,\"dialogTitle\":\"\",\"pageSize\":20,\"pageList\":\"10,20,30,50\",\"quickSelect\":{\"enable\":false,\"showItemsCount\":10,\"showMore\":true}}}},{\"ID\":\"d9ace6f2-a77a-4b2b-9b3c-61154bb40848\",\"FieldCode\":\"BillCode\",\"FieldName\":\"单据编号inputgroup\",\"ValueType\":0,\"placeHolder\":\"\",\"beginPlaceHolder\":\"\",\"endPlaceHolder\":\"\",\"visible\":true,\"conditionid\":7,\"compareType\":0,\"relation\":1,\"Lbracket\":\"(\",\"Value\":{\"Type\":15,\"Content\":{\"value\":[],\"textValue\":\"123\",\"isInputText\":true}},\"Control\":{\"Type\":15,\"Content\":{\"className\":\"\",\"require\":false,\"editable\":true,\"groupText\":\"\",\"usageMode\":\"text\"}}},{\"ID\":\"3a8403c1-aa75-4091-947e-53f1d2bb8d4d\",\"FieldCode\":\"TotalSum\",\"FieldName\":\"报帐金额\",\"ValueType\":0,\"placeHolder\":\"\",\"beginPlaceHolder\":\"\",\"endPlaceHolder\":\"\",\"visible\":true,\"conditionid\":8,\"compareType\":0,\"relation\":1,\"Rbracket\":\")))\",\"Value\":{\"Type\":6,\"Content\":{\"numValue\":5}},\"Control\":{\"Type\":6,\"Content\":{\"className\":\"\",\"placeholder\":\"\",\"precision\":2,\"require\":false,\"textAlign\":\"left\",\"bigNumber\":false}}},{\"ID\":\"c7dbe607-bddc-4391-92e2-16f0221765df\",\"FieldCode\":\"BillType\",\"FieldName\":\"报销类型多选\",\"ValueType\":0,\"placeHolder\":\"\",\"beginPlaceHolder\":\"\",\"endPlaceHolder\":\"\",\"visible\":true,\"conditionid\":3,\"compareType\":0,\"relation\":1,\"Lbracket\":\"(\",\"Value\":{\"Type\":3,\"Content\":{\"value\":[{\"value\":\"1\",\"name\":\"1\"}],\"key\":\"1\"}},\"Control\":{\"Type\":3,\"Content\":{\"valueType\":\"1\",\"enumValues\":[{\"Type\":1,\"Content\":{\"value\":\"1\",\"name\":\"1\"}},{\"Type\":1,\"Content\":{\"value\":\"2\",\"name\":\"2\"}},{\"Type\":1,\"Content\":{\"value\":\"3\",\"name\":\"3\"}}],\"multiSelect\":true,\"require\":false}}},{\"ID\":\"d152e48d-13d1-4553-94fa-525fa67d4f2b\",\"FieldCode\":\"BillDate\",\"FieldName\":\"制单日期单日期\",\"ValueType\":0,\"placeHolder\":\"\",\"beginPlaceHolder\":\"\",\"endPlaceHolder\":\"\",\"visible\":true,\"conditionid\":4,\"compareType\":0,\"relation\":1,\"Lbracket\":\"(\",\"Value\":{\"Type\":1,\"Content\":{\"dateValue\":\"2023-09-01\"}},\"Control\":{\"Type\":1,\"Content\":{\"format\":\"yyyy-MM-dd\",\"returnFormat\":\"yyyy-MM-dd\",\"require\":false,\"isDynamicDate\":false}}},{\"ID\":\"abb7ef8e-ef19-4d83-99cc-1b7ff4f05345\",\"FieldCode\":\"SecID\",\"FieldName\":\"密级ID下拉选择\",\"ValueType\":0,\"placeHolder\":\"\",\"beginPlaceHolder\":\"\",\"endPlaceHolder\":\"\",\"visible\":true,\"conditionid\":5,\"compareType\":0,\"relation\":1,\"Rbracket\":\"))\",\"Value\":{\"Type\":3,\"Content\":{\"value\":[{\"value\":\"a\",\"name\":\"a\"},{\"value\":\"b\",\"name\":\"b\"},{\"value\":\"c\",\"name\":\"c\"}],\"key\":\"a,b,c\"}},\"Control\":{\"Type\":3,\"Content\":{\"valueType\":\"1\",\"enumValues\":[{\"Type\":1,\"Content\":{\"value\":\"a\",\"name\":\"a\"}},{\"Type\":1,\"Content\":{\"value\":\"b\",\"name\":\"b\"}},{\"Type\":1,\"Content\":{\"value\":\"c\",\"name\":\"c\"}},{\"Type\":1,\"Content\":{\"value\":\"d\",\"name\":\"d\"}},{\"Type\":1,\"Content\":{\"value\":\"e\",\"name\":\"e\"}}],\"multiSelect\":true,\"require\":false}}}]",
    //     "advancedQueryConditionsString": "{\"relation\":1,\"items\":[{\"conditionid\":9}],\"children\":[{\"groupid\":4,\"relation\":1,\"items\":[{\"conditionid\":6}],\"children\":[{\"groupid\":5,\"relation\":1,\"items\":[],\"children\":[{\"groupid\":2,\"relation\":1,\"items\":[{\"conditionid\":1},{\"conditionid\":2}],\"children\":[],\"path\":[1,4,5,2]},{\"groupid\":3,\"relation\":1,\"items\":[{\"conditionid\":7},{\"conditionid\":8}],\"children\":[],\"path\":[1,4,5,3]}],\"path\":[1,4,5]}],\"path\":[1,4]},{\"groupid\":7,\"relation\":1,\"items\":[{\"conditionid\":3}],\"children\":[{\"groupid\":6,\"relation\":1,\"items\":[{\"conditionid\":4},{\"conditionid\":5}],\"children\":[],\"path\":[1,7,6]}],\"path\":[1,7]}],\"path\":[1],\"groupid\":1}",
    //     "mode": "2",
    //     "userCode": "wangjinzhe",
    //     "userId": "9e35953a-40fd-4a9a-922f-b9f59bf2a517",
    //     "userName": "wangjinzhe",
    //     "isDefault": false,
    //     "type": "private",
    //     "extendId": "query-scheme-1",
    //     "extendInfo": null,
    //     "code_chs": null,
    //     "code_en": null,
    //     "code_cht": null
    // },
    {
        "id": "c5df9edf-0e74-04b2-efb5-54572e1ab2e9",
        "belongId": "shaixuanalltype",
        "code": "上次筛选",
        "isSystem": true,
        "name": null,
        // "queryConditionString": "[{\"ID\":\"2e6a0b5c-dd36-4bfd-ab3c-e3035f3a8e5a\",\"FieldCode\":\"EmployeeID.EmployeeID\",\"FieldName\":\"报销人员下拉帮助\",\"ValueType\":0,\"placeHolder\":\"\",\"beginPlaceHolder\":\"\",\"endPlaceHolder\":\"\",\"visible\":true,\"Value\":{\"Type\":13,\"Content\":{\"valueField\":\"id\",\"value\":[{\"id\":\"05701277-d0cf-bae1-a056-1d5ebe8ea9fa\",\"code\":\"liuzhihao\",\"name\":\"柳志浩\",\"userGroup\":\"0\",\"sysOrgId\":\"001\",\"tenantId\":10000,\"secLevel\":\"0\",\"userType\":0,\"note\":null}],\"textValue\":\"柳志浩\"}},\"Control\":{\"Type\":13,\"Content\":{\"uri\":\"shaixuanceshialltype.employeeID\",\"idField\":\"id\",\"valueField\":\"id\",\"textField\":\"name\",\"mapFields\":{\"code\":\"employeeID.employeeID_code\",\"name\":\"employeeID.employeeID_name\"},\"displayType\":\"LOOKUPLIST\",\"singleSelect\":false,\"enableFullTree\":false,\"loadTreeDataType\":\"default\",\"expandLevel\":-1,\"require\":false,\"context\":{\"enableExtendLoadMethod\":true}}}},{\"ID\":\"393c7ac7-64b2-4888-83a9-c1d8b2755339\",\"FieldCode\":\"DomainID.DomainID\",\"FieldName\":\"所属部门弹出帮助\",\"ValueType\":0,\"placeHolder\":\"\",\"beginPlaceHolder\":\"\",\"endPlaceHolder\":\"\",\"visible\":true,\"Value\":{\"Type\":2,\"Content\":{\"value\":[],\"valueField\":\"\",\"textValue\":\"help-text-value-null\",\"isInputText\":false}},\"Control\":{\"Type\":2,\"Content\":{\"uri\":\"shaixuanceshialltype.domainID\",\"textField\":\"name\",\"valueField\":\"id\",\"displayType\":\"TreeList\",\"idField\":\"id\",\"context\":{\"enableExtendLoadMethod\":true},\"enableFullTree\":false,\"loadTreeDataType\":\"default\",\"singleSelect\":false,\"expandLevel\":-1,\"enableCascade\":false,\"cascadeStatus\":\"enable\",\"require\":false,\"nosearch\":false,\"editable\":false,\"dialogTitle\":\"\",\"pageSize\":20,\"pageList\":\"10,20,30,50\",\"quickSelect\":{\"enable\":false,\"showItemsCount\":10,\"showMore\":true}}}},{\"ID\":\"d9ace6f2-a77a-4b2b-9b3c-61154bb40848\",\"FieldCode\":\"BillCode\",\"FieldName\":\"单据编号inputgroup\",\"ValueType\":0,\"placeHolder\":\"\",\"beginPlaceHolder\":\"\",\"endPlaceHolder\":\"\",\"visible\":true,\"Value\":{\"Type\":15,\"Content\":{\"value\":[],\"textValue\":\"123\",\"isInputText\":true}},\"Control\":{\"Type\":15,\"Content\":{\"className\":\"\",\"require\":false,\"editable\":true,\"groupText\":\"\",\"usageMode\":\"text\"}}},{\"ID\":\"3a8403c1-aa75-4091-947e-53f1d2bb8d4d\",\"FieldCode\":\"TotalSum\",\"FieldName\":\"报帐金额\",\"ValueType\":0,\"placeHolder\":\"\",\"beginPlaceHolder\":\"\",\"endPlaceHolder\":\"\",\"visible\":true,\"Value\":{\"Type\":5,\"Content\":{\"startValue\":123,\"endValue\":234}},\"Control\":{\"Type\":5,\"Content\":{\"className\":\"\",\"placeholder\":\"\",\"require\":false,\"precision\":2,\"textAlign\":\"left\",\"bigNumber\":false}}},{\"ID\":\"c7dbe607-bddc-4391-92e2-16f0221765df\",\"FieldCode\":\"BillType\",\"FieldName\":\"报销类型多选\",\"ValueType\":0,\"placeHolder\":\"\",\"beginPlaceHolder\":\"\",\"endPlaceHolder\":\"\",\"visible\":true,\"Value\":{\"Type\":3,\"Content\":{\"value\":[{\"value\":\"1\",\"name\":\"1\"}],\"key\":\"1\"}},\"Control\":{\"Type\":3,\"Content\":{\"valueType\":\"1\",\"enumValues\":[{\"Type\":1,\"Content\":{\"value\":\"1\",\"name\":\"1\"}},{\"Type\":1,\"Content\":{\"value\":\"2\",\"name\":\"2\"}},{\"Type\":1,\"Content\":{\"value\":\"3\",\"name\":\"3\"}}],\"multiSelect\":true,\"require\":false}}},{\"ID\":\"d152e48d-13d1-4553-94fa-525fa67d4f2b\",\"FieldCode\":\"BillDate\",\"FieldName\":\"制单日期单日期\",\"ValueType\":0,\"placeHolder\":\"\",\"beginPlaceHolder\":\"\",\"endPlaceHolder\":\"\",\"visible\":true,\"Value\":{\"Type\":1,\"Content\":{\"dateValue\":\"2023-08-31\"}},\"Control\":{\"Type\":1,\"Content\":{\"format\":\"yyyy-MM-dd\",\"returnFormat\":\"yyyy-MM-dd\",\"require\":false,\"isDynamicDate\":true}}},{\"ID\":\"abb7ef8e-ef19-4d83-99cc-1b7ff4f05345\",\"FieldCode\":\"SecID\",\"FieldName\":\"密级ID下拉选择\",\"ValueType\":0,\"placeHolder\":\"\",\"beginPlaceHolder\":\"\",\"endPlaceHolder\":\"\",\"visible\":true,\"Value\":{\"Type\":3,\"Content\":{\"value\":[{\"value\":\"b\",\"name\":\"b\"},{\"value\":\"c\",\"name\":\"c\"}],\"key\":\"b,c\"}},\"Control\":{\"Type\":3,\"Content\":{\"valueType\":\"1\",\"enumValues\":[{\"Type\":1,\"Content\":{\"value\":\"a\",\"name\":\"a\"}},{\"Type\":1,\"Content\":{\"value\":\"b\",\"name\":\"b\"}},{\"Type\":1,\"Content\":{\"value\":\"c\",\"name\":\"c\"}},{\"Type\":1,\"Content\":{\"value\":\"d\",\"name\":\"d\"}},{\"Type\":1,\"Content\":{\"value\":\"e\",\"name\":\"e\"}}],\"multiSelect\":true,\"require\":false}}},{\"ID\":\"219c9c45-3bfb-4482-8755-1ea0d3c9475c\",\"FieldCode\":\"SecLevel\",\"FieldName\":\"密级\",\"ValueType\":0,\"placeHolder\":\"\",\"beginPlaceHolder\":\"\",\"endPlaceHolder\":\"\",\"visible\":true,\"Value\":{\"Type\":5,\"Content\":{\"startValue\":23,\"endValue\":453}},\"Control\":{\"Type\":5,\"Content\":{\"className\":\"\",\"placeholder\":\"\",\"require\":false,\"precision\":0,\"textAlign\":\"left\",\"bigNumber\":false}}},{\"ID\":\"6f1bf2f5-7afc-42c5-81ee-b547db370be9\",\"FieldCode\":\"AuditStatus\",\"FieldName\":\"稽核状态单选\",\"ValueType\":0,\"placeHolder\":\"\",\"beginPlaceHolder\":\"\",\"endPlaceHolder\":\"\",\"visible\":true,\"Value\":{\"Type\":14,\"Content\":{\"value\":\"2\"}},\"Control\":{\"Type\":14,\"Content\":{\"valueType\":\"1\",\"enumValues\":[{\"Type\":1,\"Content\":{\"value\":\"1\",\"name\":\"1\"}},{\"Type\":1,\"Content\":{\"value\":\"2\",\"name\":\"2\"}}],\"horizontal\":true,\"showLabel\":false,\"require\":false}}},{\"ID\":\"01ca22ef-fbd4-4322-8e88-503edb9e071c\",\"FieldCode\":\"ProjectID\",\"FieldName\":\"所属项目\",\"ValueType\":0,\"placeHolder\":\"\",\"beginPlaceHolder\":\"\",\"endPlaceHolder\":\"\",\"visible\":true,\"Value\":{\"Type\":0,\"Content\":{\"value\":\"gf\"}},\"Control\":{\"Type\":0,\"Content\":{\"className\":\"\",\"require\":false}}},{\"ID\":\"e605c9f9-a252-48ca-9c65-0bffb3427e86\",\"FieldCode\":\"ID\",\"FieldName\":\"主键啊\",\"ValueType\":0,\"placeHolder\":\"\",\"beginPlaceHolder\":\"\",\"endPlaceHolder\":\"\",\"visible\":true,\"Value\":{\"Type\":0,\"Content\":{\"value\":\"dgf\"}},\"Control\":{\"Type\":0,\"Content\":{\"className\":\"\",\"require\":false}}},{\"ID\":\"c4af57b9-b025-4f33-82f6-be9fbdaa960f\",\"FieldCode\":\"boolFlag\",\"FieldName\":\"布尔选择\",\"ValueType\":0,\"placeHolder\":\"\",\"beginPlaceHolder\":\"\",\"endPlaceHolder\":\"\",\"visible\":true,\"Value\":{\"Type\":8,\"Content\":{\"value\":[true]}},\"Control\":{\"Type\":8,\"Content\":{\"data\":[{\"value\":\"true\",\"name\":\"布尔选择\"}],\"separator\":\",\",\"isStringValue\":false,\"className\":\"\",\"require\":false}}}]",
        "queryConditionString": "[{\"ID\":\"2e6a0b5c-dd36-4bfd-ab3c-e3035f3a8e5a\",\"FieldCode\":\"EmployeeID.EmployeeID\",\"FieldName\":\"报销人员下拉帮助\",\"ValueType\":0,\"placeHolder\":\"\",\"beginPlaceHolder\":\"\",\"endPlaceHolder\":\"\",\"visible\":true,\"Value\":{\"Type\":13,\"Content\":{\"valueField\":\"id\",\"value\":[{\"id\":\"05701277-d0cf-bae1-a056-1d5ebe8ea9fa\",\"code\":\"liuzhihao\",\"name\":\"柳志浩\",\"userGroup\":\"0\",\"sysOrgId\":\"001\",\"tenantId\":10000,\"secLevel\":\"0\",\"userType\":0,\"note\":null}],\"textValue\":\"柳志浩\"}},\"Control\":{\"Type\":13,\"Content\":{\"uri\":\"shaixuanceshialltype.employeeID\",\"idField\":\"id\",\"valueField\":\"id\",\"textField\":\"name\",\"mapFields\":{\"code\":\"employeeID.employeeID_code\",\"name\":\"employeeID.employeeID_name\"},\"displayType\":\"LOOKUPLIST\",\"singleSelect\":false,\"enableFullTree\":false,\"loadTreeDataType\":\"default\",\"expandLevel\":-1,\"require\":false,\"context\":{\"enableExtendLoadMethod\":true}}}},{\"ID\":\"393c7ac7-64b2-4888-83a9-c1d8b2755339\",\"FieldCode\":\"DomainID.DomainID\",\"FieldName\":\"所属部门弹出帮助\",\"ValueType\":0,\"placeHolder\":\"\",\"beginPlaceHolder\":\"\",\"endPlaceHolder\":\"\",\"visible\":true,\"Value\":{\"Type\":2,\"Content\":{\"value\":[{\"id\":\"c4427472-95c6-e2c5-c3aa-ef6d537a1b33\",\"code\":\"Inspur\",\"name\":\"浪潮集团\",\"treeinfo\":{\"parentElement\":\"\",\"sequence\":2,\"layer\":1,\"isDetail\":false}}],\"valueField\":\"id\",\"textValue\":\"浪潮集团\",\"isInputText\":false}},\"Control\":{\"Type\":2,\"Content\":{\"uri\":\"shaixuanceshialltype.domainID\",\"textField\":\"name\",\"valueField\":\"id\",\"displayType\":\"TreeList\",\"idField\":\"id\",\"context\":{\"enableExtendLoadMethod\":true},\"enableFullTree\":false,\"loadTreeDataType\":\"default\",\"singleSelect\":false,\"expandLevel\":-1,\"enableCascade\":false,\"cascadeStatus\":\"enable\",\"require\":false,\"nosearch\":false,\"editable\":false,\"dialogTitle\":\"\",\"pageSize\":20,\"pageList\":\"10,20,30,50\",\"quickSelect\":{\"enable\":false,\"showItemsCount\":10,\"showMore\":true}}}},{\"ID\":\"d9ace6f2-a77a-4b2b-9b3c-61154bb40848\",\"FieldCode\":\"BillCode\",\"FieldName\":\"单据编号inputgroup\",\"ValueType\":0,\"placeHolder\":\"\",\"beginPlaceHolder\":\"\",\"endPlaceHolder\":\"\",\"visible\":true,\"Value\":{\"Type\":15,\"Content\":{\"value\":[],\"textValue\":\"单据编号inputgroup\",\"isInputText\":true}},\"Control\":{\"Type\":15,\"Content\":{\"className\":\"\",\"require\":false,\"editable\":true,\"groupText\":\"\",\"usageMode\":\"text\"}}},{\"ID\":\"c7dbe607-bddc-4391-92e2-16f0221765df\",\"FieldCode\":\"BillType\",\"FieldName\":\"报销类型多选\",\"ValueType\":0,\"placeHolder\":\"\",\"beginPlaceHolder\":\"\",\"endPlaceHolder\":\"\",\"visible\":true,\"Value\":{\"Type\":3,\"Content\":{\"value\":[{\"value\":\"2\",\"name\":\"2\"}],\"key\":\"2\"}},\"Control\":{\"Type\":3,\"Content\":{\"valueType\":\"1\",\"enumValues\":[{\"Type\":1,\"Content\":{\"value\":\"1\",\"name\":\"1\"}},{\"Type\":1,\"Content\":{\"value\":\"2\",\"name\":\"2\"}},{\"Type\":1,\"Content\":{\"value\":\"3\",\"name\":\"3\"}}],\"multiSelect\":true,\"require\":false}}},{\"ID\":\"d152e48d-13d1-4553-94fa-525fa67d4f2b\",\"FieldCode\":\"BillDate\",\"FieldName\":\"制单日期单日期\",\"ValueType\":0,\"placeHolder\":\"\",\"beginPlaceHolder\":\"\",\"endPlaceHolder\":\"\",\"visible\":true,\"Value\":{\"Type\":1,\"Content\":{\"dateValue\":\"2023-09-01\"}},\"Control\":{\"Type\":1,\"Content\":{\"format\":\"yyyy-MM-dd\",\"returnFormat\":\"yyyy-MM-dd\",\"require\":false,\"isDynamicDate\":true}}},{\"ID\":\"abb7ef8e-ef19-4d83-99cc-1b7ff4f05345\",\"FieldCode\":\"SecID\",\"FieldName\":\"密级ID下拉选择\",\"ValueType\":0,\"placeHolder\":\"\",\"beginPlaceHolder\":\"\",\"endPlaceHolder\":\"\",\"visible\":true,\"Value\":{\"Type\":3,\"Content\":{\"value\":[{\"value\":\"a\",\"name\":\"a\"},{\"value\":\"b\",\"name\":\"b\"}],\"key\":\"a,b\"}},\"Control\":{\"Type\":3,\"Content\":{\"valueType\":\"1\",\"enumValues\":[{\"Type\":1,\"Content\":{\"value\":\"a\",\"name\":\"a\"}},{\"Type\":1,\"Content\":{\"value\":\"b\",\"name\":\"b\"}},{\"Type\":1,\"Content\":{\"value\":\"c\",\"name\":\"c\"}},{\"Type\":1,\"Content\":{\"value\":\"d\",\"name\":\"d\"}},{\"Type\":1,\"Content\":{\"value\":\"e\",\"name\":\"e\"}}],\"multiSelect\":true,\"require\":false}}},{\"ID\":\"219c9c45-3bfb-4482-8755-1ea0d3c9475c\",\"FieldCode\":\"SecLevel\",\"FieldName\":\"密级\",\"ValueType\":0,\"placeHolder\":\"\",\"beginPlaceHolder\":\"\",\"endPlaceHolder\":\"\",\"visible\":true,\"Value\":{\"Type\":5,\"Content\":{\"startValue\":123,\"endValue\":213}},\"Control\":{\"Type\":5,\"Content\":{\"className\":\"\",\"placeholder\":\"\",\"require\":false,\"precision\":0,\"textAlign\":\"left\",\"bigNumber\":false}}},{\"ID\":\"6f1bf2f5-7afc-42c5-81ee-b547db370be9\",\"FieldCode\":\"AuditStatus\",\"FieldName\":\"稽核状态单选\",\"ValueType\":0,\"placeHolder\":\"\",\"beginPlaceHolder\":\"\",\"endPlaceHolder\":\"\",\"visible\":true,\"Value\":{\"Type\":14,\"Content\":{\"value\":\"2\"}},\"Control\":{\"Type\":14,\"Content\":{\"valueType\":\"1\",\"enumValues\":[{\"Type\":1,\"Content\":{\"value\":\"1\",\"name\":\"1\"}},{\"Type\":1,\"Content\":{\"value\":\"2\",\"name\":\"2\"}}],\"horizontal\":true,\"showLabel\":false,\"require\":false}}},{\"ID\":\"01ca22ef-fbd4-4322-8e88-503edb9e071c\",\"FieldCode\":\"ProjectID\",\"FieldName\":\"所属项目\",\"ValueType\":0,\"placeHolder\":\"\",\"beginPlaceHolder\":\"\",\"endPlaceHolder\":\"\",\"visible\":true,\"Value\":{\"Type\":0,\"Content\":{\"value\":\"dsfd \"}},\"Control\":{\"Type\":0,\"Content\":{\"className\":\"\",\"require\":false}}},{\"ID\":\"3a8403c1-aa75-4091-947e-53f1d2bb8d4d\",\"FieldCode\":\"TotalSum\",\"FieldName\":\"报帐金额\",\"ValueType\":0,\"placeHolder\":\"\",\"beginPlaceHolder\":\"\",\"endPlaceHolder\":\"\",\"visible\":true,\"Value\":{\"Type\":6,\"Content\":{\"numValue\":21333}},\"Control\":{\"Type\":6,\"Content\":{\"className\":\"\",\"placeholder\":\"\",\"precision\":2,\"require\":false,\"textAlign\":\"left\",\"bigNumber\":false,\"min\":-100000,\"max\":100000}}},{\"ID\":\"c4af57b9-b025-4f33-82f6-be9fbdaa960f\",\"FieldCode\":\"boolFlag\",\"FieldName\":\"布尔选择\",\"ValueType\":0,\"placeHolder\":\"\",\"beginPlaceHolder\":\"\",\"endPlaceHolder\":\"\",\"visible\":true,\"Value\":{\"Type\":8,\"Content\":{\"value\":[]}},\"Control\":{\"Type\":8,\"Content\":{\"data\":[{\"value\":\"true\",\"name\":\"布尔选择\"}],\"separator\":\",\",\"isStringValue\":false,\"className\":\"\",\"require\":false}}},{\"ID\":\"d9b196a3-2bf0-46ba-8807-22f6ac62ad78\",\"FieldCode\":\"ProjectMrg\",\"FieldName\":\"项目经理\",\"ValueType\":0,\"placeHolder\":\"\",\"beginPlaceHolder\":\"\",\"endPlaceHolder\":\"\",\"visible\":true,\"Value\":{\"Type\":0,\"Content\":{}},\"Control\":{\"Type\":0,\"Content\":{\"className\":\"\",\"require\":false}}},{\"ID\":\"2691b91b-1b5e-4eca-b802-ff5cc2ee7856\",\"FieldCode\":\"Version\",\"FieldName\":\"版本\",\"ValueType\":0,\"placeHolder\":\"\",\"beginPlaceHolder\":\"\",\"endPlaceHolder\":\"\",\"visible\":true,\"Value\":{\"Type\":4,\"Content\":{\"startTime\":\"2023-09-01\",\"endTime\":\"2023-09-15\"}},\"Control\":{\"Type\":4,\"Content\":{\"format\":\"yyyy-MM-dd\",\"require\":false,\"weekSelect\":false,\"startFieldCode\":\"Version\",\"endFieldCode\":\"Version\"}}}]",
        "advancedQueryConditionsString": null,
        "mode": null,
        "userCode": "wangjinzhe",
        "userId": "9e35953a-40fd-4a9a-922f-b9f59bf2a517",
        "userName": "wangjinzhe",
        "isDefault": false,
        "type": "pre",
        "extendId": "query-scheme-1",
        "extendInfo": null,
        "code_chs": null,
        "code_en": null,
        "code_cht": null
    }
]



export {
    fieldConfigs,
    querySolutionList
}