# Radio Group 单选组

Radio Group 是一个单选一组枚举值的输入组件。

## 基本用法

:::demo

```vue
{demos/radio-group/basic.vue}
```

:::

## 禁用状态

:::demo

```vue
{demos/radio-group/disable.vue}
```

:::

## 排列方式

:::demo

```vue
{demos/radio-group/alignment.vue}
```

:::

## 类型

```typescript
export interface Radio {
    /**
     * 枚举值
     */
    value: ComputedRef<any>;
    /**
     * 枚举展示文本
     */
    name: ComputedRef<any>;
}

export interface ChangeRadio {
    enumData: ComputedRef<Array<Radio>>;

    /**
     * 获取枚举值
     */
    getValue(item: Radio): any;
    /**
     * 获取枚举文本
     */
    getText(item: Radio): any;

    /**
     * 切换单选按钮事件
     */
    onClickRadio: (item: Radio, $event: Event) => void;
}
```

## 属性

| 属性名     | 类型                  | 默认值  | 说明               |
| :--------- | :-------------------- | :------ | :----------------- |
| id         | `string`              | --      | 组件标识           |
| name       | `string`              | --      | 组件名称           |
| enumData   | `object[] as Radio[]` | []      | 单选组数据源       |
| textField  | `string`              | 'name'  | 显示文本字段       |
| valueField | `string`              | 'value' | 值字段             |
| horizontal | `boolean`             | false   | 是否横向显示枚举项 |
| disabled   | `boolean`             | false   | 是否禁用组件       |
| tabIndex   | `number`              | --      | Tab 键索引         |

## 插槽

::: tip
暂无内容
:::
