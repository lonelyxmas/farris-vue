import type { App } from 'vue';
import Dropdown from './src/dropdown.component';
// import fDropdownToggleDirective from "./src/composition/function/f-dropdown-toggle.directive";
// import fDropdownMenuDirective from "./src/composition/function/f-dropdown-menu.directive";

export * from './src/types';
export * from './src/composition/props/dropdown.props';

export { Dropdown };

export default {
    install(app: App): void {
        app.component(Dropdown.name, Dropdown);
        // app.directive('fDropdownToggle', fDropdownToggleDirective);
        // app.directive('fDropdownMenu', fDropdownMenuDirective);
    }
};
