/**
 * Copyright (c) 2020 - present, Inspur Genersoft Co., Ltd.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *       http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
export interface DropdownItem {
    label: string;
    value: string | number;
    disabled?: boolean;
    active?: boolean;
    divide?: boolean;
}

export interface DropdownProps {
    width: string | number;
    /** 下拉按钮对应文字 */
    title: string;
    /** 是否禁用该按钮 */
    disabled: boolean;
    hover: boolean;
    size: string;
    /** 下拉框类型 */
    type: string;
    hideOnClick: boolean;
    /** 下拉按钮是否分开展示 */
    splitButton: boolean;
    position: string;
    /** 下拉框内容 */
    model: DropdownItem[];
    show: boolean;
    nest: boolean;
    /** 下拉框内容是否被选中 */
    active: boolean;
    /** 图标样式 */
    iconClass: string;
}
