/**
 * Copyright (c) 2020 - present, Inspur Genersoft Co., Ltd.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *       http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
export interface UseDropdownDirective {
    toggleElement: any;

    issubdp: boolean;

    internalOpen: any;

    getNativeElement: (this: any) => any;

    close: (this: any) => any;

    _getClsName: (position: string) => any;

    toggle: (this: any) => any;

    open: (this: any) => any;

    closeSiblingDropdowns: (this: any) => any;

    compatibleDispatchEvent: (eventEl: any, eventName: any) => any;

    bindDocumentEvents: (this: any) => any;

    unbindDocumentEvents: (this: any) => any;

    isEventFromToggle: (event: MouseEvent) => any;

    appendMenuEl: (menuEl: any) => any;

    getOpenState: (this: any) => any;
}
