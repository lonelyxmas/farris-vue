/**
 * Copyright (c) 2020 - present, Inspur Genersoft Co., Ltd.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *       http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
import { ExtractPropTypes } from 'vue';

export const dropdownDirectiveProps = {

    // toggle相关
    _disabled: { type: Boolean, default: false },

    _internalOpen: { type: Boolean, default: false },

    _placement: { type: String, default: 'bottom' },

    issubdp: { type: Boolean, default: false },

    _seflEl: { type: HTMLElement },

    placement: { type: String },

    _calculateMenu: { type: Boolean, default: false },

    dpIsOpen: { type: Boolean, default: false },

    autoRectify: { type: Boolean, default: false },

    forceState: {type:  Array<boolean>, default:[]},

    open: { type: Boolean }

};

export type DropdownDirectiveProps = ExtractPropTypes<typeof dropdownDirectiveProps>;
