/* eslint-disable no-script-url */
/**
 * Copyright (c) 2020 - present, Inspur Genersoft Co., Ltd.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *       http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

import { defineComponent, SetupContext, ref } from 'vue';
import { DropdownProps } from './types';
import { dropdownProps } from './composition/props/dropdown.props';
import { useDropdown } from './composition/function/use-dropdown';
import FDropdownItem from './dropdown.item.component';

export default defineComponent({
    name: 'FDropdown',
    props: dropdownProps,
    setup(props: DropdownProps, context: SetupContext) {
        const model = ref(props.model);
        const { show, leftButtonClick, showDropMenu, hoverDropdown } = useDropdown(props, context);
        return () => (
            <div>
                <div
                    class={[
                        'farris-dropdown',
                        'btn-group',
                        { 'dropup': props.position === 'top' },
                        { 'dropdown': props.position === 'bottom' },
                        { 'dropleft': props.position === 'left' },
                        { 'dropright': props.position === 'right' },
                    ]}
                    style={{ width: props.width }}
                >
                    {props.splitButton && (
                        <span
                            class={[
                                'btn',
                                { 'dropdown-item': props.nest },
                                { 'btn-lg': props.size === 'large' },
                                { 'btn-sm': props.size === 'small' },
                                { 'btn-primary': props.type === 'primary' },
                                { 'btn-success': props.type === 'success' },
                                { 'btn-warning': props.type === 'warning' },
                                { 'btn-danger': props.type === 'danger' },
                                { 'btn-info': props.type === 'info' },
                            ]}
                            style="width:100%"
                            onClick={leftButtonClick}
                        >
                            {props.title}
                        </span>
                    )}
                    <span
                        class={[
                            'dropdown-toggle',
                            { btn: !props.nest },
                            { 'dropdown-item': props.nest },
                            { 'dropdown-toggle-split': props.splitButton },
                            { 'btn-lg': props.size === 'large' },
                            { 'btn-sm': props.size === 'small' },
                            { 'btn-primary': props.type === 'primary' },
                            { 'btn-success': props.type === 'success' },
                            { 'btn-warning': props.type === 'warning' },
                            { 'btn-danger': props.type === 'danger' },
                            { 'btn-info': props.type === 'info' },
                            { 'btn-secondary': props.type === 'secondary' },
                            { 'btn-link': props.type === 'link' },
                            { 'disabled': props.disabled },
                        ]}
                        style="width:100%"
                        onClick={showDropMenu}
                        onMouseenter={hoverDropdown}
                        onMouseleave={hoverDropdown}
                    >
                        {/* 分行线 */}
                        <span class="sr-only" v-show={props.splitButton}></span>
                        {props.splitButton ? '' : props.title}
                        {/* 图标下拉 */}
                        <span class={`f-icon ${props.iconClass.toString()}`}
                            v-show={props.iconClass}></span>
                    </span>
                    <div class={`dropdown-menu${show.value ? ' show' : ''}`}>
                        <div>
                            {model.value.map(({ label, value, disabled, active, divide }) => (
                                <FDropdownItem
                                    value={value.toString()}
                                    label={label}
                                    disabled={disabled}
                                    active={active}
                                    divide={divide}
                                // select={selectItem}
                                ></FDropdownItem>
                            ))}
                        </div>
                    </div>
                </div>
            </div >
        );
    },
});
