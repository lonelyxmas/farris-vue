/* eslint-disable no-script-url */
/**
 * Copyright (c) 2020 - present, Inspur Genersoft Co., Ltd.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *       http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

// component define
import { defineComponent, ref } from 'vue';
import { DropdownItem } from './types';
import { dropdownItemProps } from './composition/props/dropdown.item.props';

export default defineComponent({
    name: 'FDropdownItem',
    props: dropdownItemProps,
    emits: ['select'] as (string[] & ThisType<void>) | undefined,
    setup(props: DropdownItem, context) {
        const value = ref(props.value);
        const label = ref(props.label);
        const disabled = ref(props.disabled);
        const active = ref(props.active);
        const divide = ref(props.divide);

        const selectItem = () => {
            if (disabled.value) {
                return;
            }
            context.emit('select', props);
        };

        return () => (
            <div>
                <div class="dropdown-divider" v-show={divide.value}></div>
                <li
                    class={`dropdown-item${active.value ? ' active' : ''} ${disabled.value ? ' disabled' : ''}`}
                    onClick={selectItem}
                    title={value.value.toString()}
                >
                    {label.value}
                </li>
            </div>
        );
    },
});
