/**
 * Copyright (c) 2020 - present, Inspur Genersoft Co., Ltd.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *       http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
import { reactive, createApp, onUnmounted, ref } from 'vue';
import type { App } from 'vue';
import FMessageBox from './message-box.component';
import { FModal } from '../../modal';
import { ExceptionInfo } from './message-box.props';

export interface MessageBoxOption {
    type: string;
    title?: string;
    detail?: string;
    okButtonText?: string;
    cancelButtonText?: string;
    exceptionInfo?: ExceptionInfo;
    acceptCallback?: () => void;
    rejectCallback?: () => void;
}

function initInstance(props: MessageBoxOption, content?: string): App {
    const container = document.createElement('div');
    container.style.display = 'contents';
    const app: App = createApp({
        setup() {
            onUnmounted(() => {
                document.body.removeChild(container);
            });
            const showModal = ref(true);
            const showHeader = props.type === 'error' || props.type === 'prompt';
            const modalTitle = props.type === 'error' ? '错误提示' : props.type === 'prompt' ? props.title : '';
            const acceptCallback = props.acceptCallback || (() => {});
            const rejectCallback = props.rejectCallback || (() => {});
            return () => (
                <FModal
                    class="modal-message modal-message-type-info"
                    title={modalTitle}
                    v-model={showModal.value}
                    show-header={showHeader}
                    show-buttons={false}>
                    <FMessageBox {...props} onAccept={acceptCallback} onReject={rejectCallback} onClose={app.unmount}></FMessageBox>
                </FModal>
            );
        }
    });
    document.body.appendChild(container);
    app.mount(container);
    return app;
}

export default class MessageBoxService {
    static show(options: MessageBoxOption): void {
        const props: MessageBoxOption = reactive({
            ...options
        });
        initInstance(props);
    }

    static info(message: string, detail: string) {
        const props: MessageBoxOption = reactive({
            type: 'info',
            title: message,
            detail,
            okButtonText: '知道了',
            cancelButtonText: ''
        });
        this.show(props);
    }

    static warning(message: string, detail: string) {
        const props: MessageBoxOption = reactive({
            type: 'warning',
            title: message,
            detail,
            okButtonText: '知道了',
            cancelButtonText: ''
        });
        this.show(props);
    }

    static success(message: string, detail: string) {
        const props: MessageBoxOption = reactive({
            type: 'success',
            title: message,
            detail,
            okButtonText: '关闭',
            cancelButtonText: ''
        });
        this.show(props);
    }

    static error(message: string, detail: string, date?: string): any {
        const props: MessageBoxOption = reactive({
            type: 'error',
            okButtonText: '关闭',
            cancelButtonText: '',
            exceptionInfo: { date, message, detail } as ExceptionInfo
        });
        this.show(props);
    }

    static prompt(message: string, detail: string) {
        const props: MessageBoxOption = reactive({
            type: 'prompt',
            title: message,
            detail,
            okButtonText: '确定',
            cancelButtonText: '取消'
        });
        this.show(props);
    }

    static question(message: string, detail: string, acceptCallback: () => void, rejectCallback: () => void) {
        const props: MessageBoxOption = reactive({
            type: 'question',
            title: message,
            detail,
            okButtonText: '确定',
            cancelButtonText: '取消',
            acceptCallback,
            rejectCallback
        });
        this.show(props);
    }
}
