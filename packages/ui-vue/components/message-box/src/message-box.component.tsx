/**
 * Copyright (c) 2020 - present, Inspur Genersoft Co., Ltd.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *       http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
import { computed, defineComponent, ref, SetupContext } from 'vue';
import { ExceptionInfo, MessageBoxProps, messageBoxProps } from './message-box.props';
import { type WebkitLineClampProperty, type BoxOrientProperty } from 'csstype';
import { ModalButton } from '../../modal';
import { useCopy } from './composition/use-copy';

export default defineComponent({
    name: 'FMessageBox',
    props: messageBoxProps,
    emits: ['accept', 'reject', 'close'] as (string[] & ThisType<void>) | undefined,
    setup(props: MessageBoxProps, context: SetupContext) {
        const messageType = ref(props.type);
        const messageTitle = ref(props.title);
        const messageDetail = ref(props.detail);
        const exception = ref<ExceptionInfo>(props.exceptionInfo);
        const promptInputType = ref('');
        const promptFontSize = ref(14);
        const promptMaxLength = ref(140);
        const promptInputPlaceholder = ref('');
        const messageText = ref('');
        const enableWordCount = ref(false);
        const wordsTotalTips = ref('');
        const wordsTotal = ref(messageDetail.value.length);
        const buttons = ref<ModalButton[]>([]);
        const okButtonText = ref(props.okButtonText);
        const cancelButtonText = ref(props.cancelButtonText);
        const showFontSize = ref(true);
        const showCancelButton = ref(true);
        const showOkButton = ref(true);

        const copyButtonText = ref('复制详细信息');
        const showLines = ref(3);
        const exceptionMessageMaxHeight = ref(480);

        const messageBoxContainerClass = computed(() => {
            const classObject = {
                'modal-tips': true,
                'd-flex': true,
                'flex-row': true
            } as Record<string, boolean>;
            const messageBoxTypeClass = `messager-type-${messageType.value}`;
            classObject[messageBoxTypeClass] = true;
            return classObject;
        });

        const messageBoxContainerStyle = computed(() => {
            const styleObject = {} as Record<string, any>;
            if (messageType.value === 'prompt') {
                styleObject.padding = '0.5rem 0.5rem 1rem 1.5rem';
            } else if (messageType.value === 'error') {
                styleObject.padding = '0.5rem 1.5rem 1rem 1.5rem';
            }
            return styleObject;
        });

        const messageBoxIconClass = computed(() => {
            const classObject = { 'f-icon': true } as Record<string, boolean>;
            const iconClass = `f-icon-${messageType.value}`;
            classObject[iconClass] = true;
            return classObject;
        });

        const safeMessageTitle = computed(() => {
            return messageTitle.value || (exception.value && exception.value.message);
        });

        const safeMessageDetail = computed(() => {
            return messageDetail.value;
        });

        const shouldShowMessageDetail = computed(() => {
            return !!messageDetail.value;
        });

        const isExceptionMessage = computed(() => {
            return !!exception.value;
        });

        const shouldShowExceptionDate = computed(() => {
            return !!exception.value && !!exception.value.date;
        });

        const exceptionDateMessage = computed(() => {
            const exceptionDate = (exception.value && exception.value.date) || '';
            return `发生时间 : ${exceptionDate}`;
        });

        const shouldShowExceptionMessage = computed(() => {
            return !!exception.value && !!exception.value.detail;
        });

        const exceptionMessageStyle = computed(() => {
            const maxHeight = `${exceptionMessageMaxHeight.value}px`;
            const styleObject = {
                overflow: 'hidden',
                'text-overflow': 'ellipsis',
                display: '-webkit-box',
                '-webkit-box-orient': 'vertical' as BoxOrientProperty,
                '-webkit-line-clamp': showLines.value as WebkitLineClampProperty,
                'max-height': maxHeight
            };
            return styleObject;
        });

        const safeExceptionMessage = computed(() => {
            const safeMessage = (exception.value && exception.value.detail) || '';
            return safeMessage;
        });

        const shouldShowExpandHandle = computed(() => {
            return true;
        });

        const expandExceptionMessage = ref(false);
        const expandText = ref('展开');
        const collapseText = ref('收起');
        const expandHandleStyle = computed(() => {
            const styleObject = {
                display: 'block',
                color: '#2A87FF'
            } as Record<string, any>;
            styleObject['text-align'] = expandExceptionMessage.value ? '' : 'right';
            return styleObject;
        });

        function toggalExceptionMessage(expand: boolean, $event: Event) {
            expandExceptionMessage.value = !expandExceptionMessage.value;
            showLines.value = expandExceptionMessage.value ? 20 : 3;
        }

        function onClickExpand(payload: MouseEvent) {
            return toggalExceptionMessage(true, payload);
        }

        function onClickCollapse(payload: MouseEvent) {
            return toggalExceptionMessage(true, payload);
        }

        const promptTextAreaStyle = computed(() => {
            const fontSize = `${promptFontSize.value}px`;
            const styleObject = {
                'font-size': fontSize,
                height: '100%'
            };
            return styleObject;
        });

        function onTextChange($event: InputEvent) {
            if ($event.currentTarget) {
                const textVaue = (($event.currentTarget as any).value || '') as string;
                wordsTotal.value = textVaue.length;
            }
        }

        const shouldShowWordCount = computed(() => {
            return enableWordCount.value && promptMaxLength.value;
        });

        const shouldShowOkCancelButtons = computed(() => {
            return !(buttons.value && buttons.value.length) && (okButtonText.value || cancelButtonText.value);
        });

        const shouldShowFontSize = computed(() => {
            return messageType.value === 'prompt' && showFontSize.value;
        });

        function onFontSizeChange($event: Event) {}

        const shouldShowCancelButton = computed(() => {
            return showCancelButton.value && cancelButtonText.value;
        });

        const shouldShowOkButton = computed(() => {
            return showOkButton.value && okButtonText.value;
        });

        function onClickCancelButton($event: MouseEvent) {
            if (messageType.value === 'question') {
                context.emit('reject');
            }
            context.emit('close');
        }

        function onClickOkButton($event: MouseEvent) {
            if (messageType.value === 'question') {
                context.emit('accept');
            }
            context.emit('close');
        }

        const shouldShowButtons = computed(() => {
            return !!(buttons.value && buttons.value.length);
        });

        const shouldShowCopyButton = computed(() => {
            return exception.value && exception.value.date && exception.value.message && exception.value.detail;
        });

        const { onCopy } = useCopy(props, context);
        const copyFeedback = ref('复制成功');
        const showCopyFeedback = ref(false);
        function onClickCopyButton($event: MouseEvent) {
            onCopy(safeExceptionMessage.value)
                .catch((reason: any) => {
                    copyFeedback.value = '复制失败';
                })
                .finally(() => {
                    showCopyFeedback.value = true;
                    setTimeout(() => {
                        showCopyFeedback.value = false;
                    }, 700);
                });
        }
        const copyFeedbackStyle = computed(() => {
            const styleObject = {
                position: 'absolute',
                left: '50%',
                top: '50%',
                width: '100px',
                height: '40px',
                background: '#303C53',
                'line-height': '40px',
                'text-align': 'center',
                'margin-left': '-30px',
                'margin-top': '-50px',
                'border-radius': '10px',
                'box-shadow': '0px 2px 8px 0px',
                color: '#fff',
                transition: 'all .3s ease'
            } as Record<string, any>;
            styleObject.opacity = showCopyFeedback.value ? '0.8' : '0';
            styleObject.display = showCopyFeedback.value ? '' : 'none';
            return styleObject;
        });

        return () => {
            return (
                <div class="farris-messager">
                    {messageType.value !== 'prompt' && (
                        <section class={messageBoxContainerClass.value} style={messageBoxContainerStyle.value}>
                            <div class="float-left modal-tips-iconwrap">
                                <span class={messageBoxIconClass.value}></span>
                            </div>
                            <div class="modal-tips-content">
                                <p class="toast-msg-title" v-html={safeMessageTitle.value}></p>
                                {shouldShowMessageDetail.value && <p class="toast-msg-detail" v-html={safeMessageDetail.value}></p>}
                                {isExceptionMessage.value && (
                                    <div class="toast-msg-detail">
                                        {shouldShowExceptionDate.value && <div>{exceptionDateMessage.value}</div>}
                                        {shouldShowExceptionMessage.value && (
                                            <div id="exception_error_msg" ref="exceptionMessageRef" style={exceptionMessageStyle.value}>
                                                详细信息 : <span v-html={safeExceptionMessage.value}></span>
                                            </div>
                                        )}
                                        {shouldShowExpandHandle.value && (
                                            <span style={expandHandleStyle.value}>
                                                {expandExceptionMessage.value && (
                                                    <span onClick={onClickExpand} style="cursor: pointer;">
                                                        {collapseText.value}
                                                    </span>
                                                )}
                                                {!expandExceptionMessage.value && (
                                                    <span onClick={onClickCollapse} style="cursor: pointer;">
                                                        {expandText.value}
                                                    </span>
                                                )}
                                            </span>
                                        )}
                                    </div>
                                )}
                            </div>
                        </section>
                    )}
                    {messageType.value === 'prompt' && (
                        <>
                            <section class={messageBoxContainerClass.value} style={messageBoxContainerStyle.value}>
                                <div class="flex-fill fixdiv mb-1 mr-3">
                                    <textarea
                                        title="promptMessage"
                                        name="promptMessage"
                                        class="form-control"
                                        style={promptTextAreaStyle.value}
                                        rows="4"
                                        maxlength={promptMaxLength.value}
                                        onInput={(payload: Event) => onTextChange(payload as InputEvent)}>
                                        {safeMessageDetail.value}
                                    </textarea>
                                </div>
                            </section>
                            <span
                                class="textarea-wordcount"
                                title={wordsTotalTips.value}
                                style="position: absolute; bottom: 76px; right: 32px; cursor: pointer; text-align: right;">
                                {wordsTotal.value + ' / ' + promptMaxLength.value}
                            </span>
                        </>
                    )}
                    {shouldShowOkCancelButtons.value && (
                        <div class="modal-footer">
                            {shouldShowCopyButton.value && (
                                <span style="width: 100%;color: #2A87FF;padding-left: 37px;">
                                    <span onClick={onClickCopyButton} style="cursor: pointer;">
                                        {copyButtonText.value}
                                    </span>
                                </span>
                            )}
                            {shouldShowCancelButton.value && (
                                <button type="button" class="btn btn-secondary btn-lg" onClick={onClickCancelButton}>
                                    {cancelButtonText.value}
                                </button>
                            )}
                            {shouldShowOkButton.value && (
                                <button type="button" class="btn btn-primary btn-lg" onClick={onClickOkButton}>
                                    {okButtonText.value}
                                </button>
                            )}
                        </div>
                    )}
                    {shouldShowButtons.value && (
                        <div class="modal-footer">
                            {shouldShowCopyButton.value && (
                                <span style="width: 100%;color: #2A87FF;padding-left: 37px;">
                                    <span onClick={onClickCopyButton} style="cursor: pointer;">
                                        {copyButtonText.value}
                                    </span>
                                </span>
                            )}
                            {buttons.value.length &&
                                buttons.value.map((button) => {
                                    return (
                                        <button type="button" onClick={button.handle} class={button.class}>
                                            {button.iconClass && <span class={button.iconClass}></span>}
                                            {button.text}
                                        </button>
                                    );
                                })}
                        </div>
                    )}
                    <div style={copyFeedbackStyle.value}>{copyFeedback.value}</div>
                </div>
            );
        };
    }
});
