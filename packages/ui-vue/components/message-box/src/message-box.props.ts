/**
 * Copyright (c) 2020 - present, Inspur Genersoft Co., Ltd.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *       http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
import { ExtractPropTypes, PropType } from 'vue';

export interface ExceptionInfo {
    date: string;
    message: string;
    detail: string;
}

export const messageBoxProps = {
    type: { Type: String, default: 'info' },
    title: { Type: String, default: '' },
    detail: { Type: String, default: '' },
    okButtonText: { Type: String, default: '确定' },
    cancelButtonText: { Type: String, default: '取消' },
    exceptionInfo: { Type: Object as PropType<ExceptionInfo>, default: { date: '', message: '', detail: '' } }
};
export type MessageBoxProps = ExtractPropTypes<typeof messageBoxProps>;
