/**
 * Copyright (c) 2020 - present, Inspur Genersoft Co., Ltd.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *       http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
import { SetupContext } from 'vue';
import { UseCopy } from './types';

// https://github.com/feross/clipboard-copy
function makeError() {
    return new DOMException('The request is not allowed', 'NotAllowedError');
}

async function copyClipboardApi(text: string) {
    // Use the Async Clipboard API when available. Requires a secure browsing
    // context (i.e. HTTPS)
    if (!navigator.clipboard) {
        throw makeError();
    }
    return navigator.clipboard.writeText(text);
}

async function copyExecCommand(text: string) {
    // Put the text to copy into a <span>
    const span = document.createElement('span');
    span.textContent = text;

    // Preserve consecutive spaces and newlines
    span.style.whiteSpace = 'pre';
    span.style.webkitUserSelect = 'auto';
    span.style.userSelect = 'all';

    // Add the <span> to the page
    document.body.appendChild(span);

    // Make a selection object representing the range of text selected by the user
    const selection = window.getSelection();
    const range = window.document.createRange();
    selection?.removeAllRanges();
    range.selectNode(span);
    selection?.addRange(range);

    // Copy text to the clipboard
    let success = false;
    try {
        success = window.document.execCommand('copy');
    } finally {
        // Cleanup
        selection?.removeAllRanges();
        window.document.body.removeChild(span);
    }

    if (!success) throw makeError();
}

async function clipboardCopy(text: string) {
    try {
        await copyClipboardApi(text);
    } catch (err) {
        // ...Otherwise, use document.execCommand() fallback
        try {
            await copyExecCommand(text);
        } catch (err2) {
            throw err2 || err || makeError();
        }
    }
}
export function useCopy(props: any, context: SetupContext): UseCopy {
    async function onCopy(text: string) {
        await clipboardCopy(text);
    }

    return { onCopy };
}
