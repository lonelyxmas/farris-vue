/**
 * Copyright (c) 2020 - present, Inspur Genersoft Co., Ltd.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *       http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
import { defineComponent, ref, onMounted, watch } from 'vue';
import type { SetupContext } from 'vue';
import { numberRangeProps, NumberRangeProps } from './number-range.props';
import { UseData } from './composition/use-data';
import { UseUtil } from './composition/use-util';
import './index.scss';

export default defineComponent({
    name: 'FNumberRange',
    props: numberRangeProps,
    emits: ['valueChange', 'blur', 'focus', 'click', 'input', 'beginValueChange',
        'endValueChange'] as (string[] & ThisType<void>) | undefined,
    setup(props: NumberRangeProps, context: SetupContext) {
        const realValue: any = ref(null); // 真实值 数字
        const beginValue: any = ref(null); // 真实值 数字
        const endValue: any = ref(null); // 真实值 数字
        const targetValue = ref(''); // 活跃的输入框
        const isFocus = ref(false);
        const inputValue = ref('');
        const inputBeginValue = ref('');
        const inputEndValue = ref('');
        const formatOptions = ref({});
        const { isDisableOfBtn, format, buildFormatOptions, getRealValue } = UseUtil(
            props,
            context,
            realValue,
            formatOptions,
            targetValue,
            beginValue,
            endValue
        );
        const { onFocusTextBox, onBlurTextBox, onInput, up, down, onKeyDown } = UseData(
            props,
            context,
            realValue,
            inputValue,
            isFocus,
            formatOptions,
            targetValue,
            beginValue,
            endValue,
            inputBeginValue,
            inputEndValue
        );

        onMounted(() => {
            console.log('111');
            const _beginValue = getRealValue(props.beginValue);
            beginValue.value = _beginValue;
            inputBeginValue.value = format(_beginValue);
            const _endValue = getRealValue(props.endValue);
            endValue.value = _endValue;
            inputEndValue.value = format(_endValue);
        });

        watch(
            () => [props.precision, props.useThousands, props.prefix, props.suffix],
            ([newPrecision, newUseThousands, newPrefix, newSuffix]) => {
                formatOptions.value = buildFormatOptions();
                inputBeginValue.value = format(beginValue.value);
                inputEndValue.value = format(endValue.value);
            }
        );
        watch(
            () => [props.beginValue, props.endValue],
            ([newBeginValue, newEndValue]) => {
                const _beginValue = getRealValue(newBeginValue);
                beginValue.value = _beginValue;
                inputBeginValue.value = format(_beginValue);
                const _endValue = getRealValue(newEndValue);
                endValue.value = _endValue;
                inputEndValue.value = format(_endValue);
            }
        );

        return () => (
            <div>
                <div class="input-group  number-range f-cmp-number-spinner">
                    <div class="form-control input-container">
                        <div class="sub-input-group">
                            <input
                                class="sub-input form-contro"
                                style={{ 'text-align': props.textAlign }}
                                type="text"
                                value={inputBeginValue.value}
                                onBlur={(e) => onBlurTextBox(e, 'beginValue')}
                                onFocus={(e) => onFocusTextBox(e, 'beginValue')}
                                onInput={(e) => onInput(e, 'change')}
                                disabled={props.disabled}
                                readonly={props.readonly || !props.editable}
                                placeholder={props.disabled || props.readonly || !props.editable ? '' : props.beginPlaceHolder}
                                onKeydown={(e) => {
                                    onKeyDown(e);
                                }}
                            />
                            {!props.disabled && !props.readonly && props.showButton && (
                                <div class="input-group-append btn-group btn-group-number sub-btn-group">
                                    <button
                                        title="up-button"
                                        class="btn btn-secondary btn-number-flag"
                                        style={{ cursor: isDisableOfBtn('up', beginValue.value) ? 'pointer' : 'not-allowed' }}
                                        onClick={(e) => {
                                            up(e, 'beginValue');
                                        }}
                                        disabled={!isDisableOfBtn('up', beginValue.value)}>
                                        <span class="icon k-i-arrow-chevron-up number-arrow-chevron"></span>
                                    </button>
                                    <button
                                        title="down-button"
                                        class="btn btn-secondary btn-number-flag"
                                        style={{ cursor: isDisableOfBtn('down', beginValue.value) ? 'pointer' : 'not-allowed' }}
                                        onClick={(e) => {
                                            down(e, 'beginValue');
                                        }}
                                        disabled={!isDisableOfBtn('down', beginValue.value)}>
                                        <span class="icon k-i-arrow-chevron-down number-arrow-chevron"></span>
                                    </button>
                                </div>
                            )}
                        </div>
                        <span class="spliter">~</span>
                        <div class="sub-input-group">
                            <input
                                class="sub-input form-contro"
                                style={{ 'text-align': props.textAlign }}
                                type="text"
                                value={inputEndValue.value}
                                onBlur={(e) => onBlurTextBox(e, 'endValue')}
                                onFocus={(e) => onFocusTextBox(e, 'endValue')}
                                onInput={(e) => onInput(e, 'change')}
                                disabled={props.disabled}
                                readonly={props.readonly || !props.editable}
                                placeholder={props.disabled || props.readonly || !props.editable ? '' : props.endPlaceHolder}
                                onKeydown={(e) => {
                                    onKeyDown(e);
                                }}
                            />
                            {!props.disabled && !props.readonly && props.showButton && (
                                <div class="input-group-append btn-group btn-group-number sub-btn-group">
                                    <button
                                        title="up-button"
                                        class="btn btn-secondary btn-number-flag"
                                        style={{ cursor: isDisableOfBtn('up', endValue.value) ? 'pointer' : 'not-allowed' }}
                                        onClick={(e) => {
                                            up(e, 'endValue');
                                        }}
                                        disabled={!isDisableOfBtn('up', endValue.value)}>
                                        <span class="icon k-i-arrow-chevron-up number-arrow-chevron"></span>
                                    </button>
                                    <button
                                        title="down-button"
                                        class="btn btn-secondary btn-number-flag"
                                        style={{ cursor: isDisableOfBtn('down', endValue.value) ? 'pointer' : 'not-allowed' }}
                                        onClick={(e) => {
                                            down(e, 'endValue');
                                        }}
                                        disabled={!isDisableOfBtn('down', endValue.value)}>
                                        <span class="icon k-i-arrow-chevron-down number-arrow-chevron"></span>
                                    </button>
                                </div>
                            )}
                        </div>
                    </div>
                </div>
            </div>
        );
    }
});
