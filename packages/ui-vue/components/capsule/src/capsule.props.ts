import { ComputedRef, ExtractPropTypes } from 'vue';

export interface CapsuleItem {
    [key: string]: any;
    /**
     * 枚举值
     */
    value: any;
    /**
     * 枚举展示文本
     */
    name: string;
}

export const capsuleProps = {
    items: { Type: Array<CapsuleItem>, default: [] },
    /**
     * 组件值
     */
    modelValue: { type: Object, default: null }
};

export type CapsuleProps = ExtractPropTypes<typeof capsuleProps>;
