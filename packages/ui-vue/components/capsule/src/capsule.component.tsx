import { Ref, SetupContext, computed, defineComponent, nextTick, onMounted, ref, watch } from "vue";
import { CapsuleItem, CapsuleProps, capsuleProps } from "./capsule.props";
import './capsule.css';

export default defineComponent({
    name: 'FCapsule',
    props: capsuleProps,
    emits: ['change', 'update:modelValue'],
    setup(props: CapsuleProps, context: SetupContext) {
        const items = ref(props.items as CapsuleItem[]);
        const capsuleContainerRef = ref<any>();
        const paddingSpace = 2;
        const modelValue = ref(props.modelValue);
        const transition = ref('none');

        const capsuleContainerClass = computed(() => {
            const classObject = {
                'f-capsule-container': true
            } as Record<string, boolean>;
            return classObject;
        });

        const capsuleContainerStyle = computed(() => {
            const styleObject = {

            } as Record<string, any>;
            return styleObject;
        });

        const capsuleItemClass = function (item: CapsuleItem) {
            const classObject = {
                'f-capsule-item': true,
                'f-capsule-active-item': item.value === modelValue.value
            } as Record<string, boolean>;
            return classObject;
        };

        const capusleClass = computed(() => {
            const classObject = {
                'f-capsule': true
            } as Record<string, boolean>;
            return classObject;
        });

        const capsulePostion = ref(paddingSpace);
        const capsuleWidth = ref(0);

        const capsuleStyle = computed(() => {
            const styleObject = {
                'left': `${capsulePostion.value}px`,
                'width': `${capsuleWidth.value}px`,
                'transition': transition.value
            } as Record<string, any>;
            return styleObject;
        });

        function resetCapsuleItemRefMap() {
            const refMap = {};
            items.value.reduce((refMap: any, item: CapsuleItem, index: number) => {
                refMap[index] = ref<any>();
                return refMap;
            }, refMap);
            return refMap;
        }

        const caspuleItemRefMap: Record<number, Ref<any>> = resetCapsuleItemRefMap();

        function moveCapsuleToActiveItem(item: CapsuleItem) {
            const capsuleItemValue = item.value;
            const matchedItemIndex = items.value.findIndex((item: CapsuleItem) => item.value === capsuleItemValue);
            const activeItemIndex = matchedItemIndex > -1 ? matchedItemIndex : 0;
            const activeItemElement = caspuleItemRefMap[activeItemIndex].value as HTMLElement;
            if (activeItemElement) {
                const activeItemRect = activeItemElement.getBoundingClientRect();
                const hostRect = capsuleContainerRef.value.getBoundingClientRect();
                capsulePostion.value = activeItemRect.left - hostRect.left + paddingSpace;
                capsuleWidth.value = activeItemRect.width - 2 * paddingSpace;
            }
        }

        function updateActiveItem(item: CapsuleItem, emitChangeEvent?: boolean) {
            const capsuleItemValue = item.value;
            modelValue.value = capsuleItemValue;
            moveCapsuleToActiveItem(item);
            if (emitChangeEvent) {
                context.emit('change', capsuleItemValue);
            }
        }

        watch(
            () => props.modelValue,
            (value: any) => {
                modelValue.value = value;
            }
        );

        onMounted(async () => {
            const matchedItemIndex = items.value.findIndex((item: CapsuleItem) => item.value === modelValue.value);
            const activeItemIndex = matchedItemIndex > -1 ? matchedItemIndex : 0;
            updateActiveItem(items.value[activeItemIndex], false);
            await nextTick();
            transition.value = '0.1s ease-out all';
        });

        function onClickCapsuleItem($event: MouseEvent, item: CapsuleItem, itemIndex: number) {
            updateActiveItem(item, true);
        }

        return () => {
            return (
                <span ref={capsuleContainerRef} class={capsuleContainerClass.value} style={capsuleContainerStyle.value}>
                    <span class="f-capsule-pane">
                        {
                            items.value.map((item: CapsuleItem, index: number) => {
                                return (
                                    <div ref={caspuleItemRefMap[index]} class={capsuleItemClass(item)}
                                        onClick={(paylaod: MouseEvent) => onClickCapsuleItem(paylaod, item, index)}>
                                        <i class={item.icon}></i>
                                        {item.name}
                                    </div>
                                );
                            })
                        }
                    </span>
                    <small class={capusleClass.value} style={capsuleStyle.value}>
                    </small>
                </span>
            );
        };
    }
});
