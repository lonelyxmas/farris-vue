import { ExtractPropTypes } from "vue";

export const transferProps = {
    enableSearch:{type:Boolean,default:true},
    placeholder:{type:String,default:''},
    displayType:{type:String,default:'List'},
    dataSource:{type:Array<object>,default:[]}
};

export type TransferProps = ExtractPropTypes<typeof transferProps>;
