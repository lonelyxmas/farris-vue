import { SetupContext, computed, defineComponent, ref } from "vue";
import { TransferProps, transferProps } from "./transfer.props";
import './transfer.css';

export default defineComponent({
    name: 'FTransfer',
    props: transferProps,
    emits: [],
    setup(props: TransferProps, context: SetupContext) {
        const leftTitle = ref('');
        const selectionTitle = ref('已选：');
        const selectionItemTitle = ref('项数据');
        const selectionTitleAction = ref('移动至');
        const canCheckLength = ref(0);
        const leftCount = ref(0);
        const shouldShowSearchBox = ref(props.enableSearch);
        const placeholder = ref(props.placeholder);
        const searchValue = ref('');
        const emptyData = ref('');
        const displayType = ref(props.displayType);
        const shouldShowCheckbox = ref(false);
        const fillToParent = ref(false);

        const shouldShowSearchIcon = computed(() => !searchValue.value);
        const shouldShowClearIcon = computed(() => !!searchValue.value);

        const dataSource = ref(props.dataSource);
        const selections = ref([]);

        function clearSearchValue($event: MouseEvent) {
            searchValue.value = '';
        }

        function renderSearchBox() {
            return (
                <div class="form-group farris-form-group transfer-search-box">
                    <div class="farris-input-wrap">
                        <div class="f-cmp-inputgroup">
                            <div class="input-group f-state-editable">
                                <input class="form-control f-utils-fill text-left" v-model={searchValue.value}
                                    name="input-group-value" type="text"
                                    placeholder={placeholder.value} autocomplete="off" ></input>
                                <div class="input-group-append" >
                                    {shouldShowClearIcon.value &&
                                        <span class="input-group-text input-group-clear"
                                            onClick={(payload: MouseEvent) => clearSearchValue(payload)}>
                                            <i class="f-icon modal_close"></i>
                                        </span>}
                                    {shouldShowSearchIcon.value && <span class="input-group-text">
                                        <span class="k-icon k-i-search"></span>
                                    </span>}

                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            );
        }

        const optionListClass = computed(() => {
            const classObject = {
                'option-pane-group': true,
                'multi-select-list-group-flush': true,
                'option-pane-group--left': true,
                'f-multi-select-norecords': !dataSource.value || !dataSource.value.length
            } as Record<string, boolean>;
            return classObject;
        });

        function canChecked(optionItem: any) {
            return true;
        }

        function isChecked(optionItem: any, position: string) {
            return false;
        }

        const optionListItemClass = function (optionItem: any) {
            const classObject = {
                'option-pane-group-item': true,
                'f-state-disabled': !canChecked(optionItem),
                'active': isChecked(optionItem, 'L')
            } as Record<string, boolean>;
            return classObject;
        };

        function leftDataChecked(optionItems: any[]) {

        }

        function rightDataChecked(optionItems: any[]) {

        }

        const shouldShowEmptyList = computed(() => !dataSource.value || !dataSource.value.length);

        const shouldShowOptionList = computed(() => displayType.value === 'List');
        const shouldShowOptionTree = computed(() => displayType.value === 'Tree');

        function renderOptionList() {
            return <ul class={optionListClass.value}>
                {shouldShowEmptyList.value && <li class="f-multi-select-norecords-content"> {emptyData.value} </li>}
                {
                    dataSource.value && dataSource.value.length && dataSource.value.map((optionItem: any) => {
                        return <li class={optionListItemClass(optionItem)}>
                            <div class="custom-control custom-checkbox  custom-control-inline"
                                onClick={() => leftDataChecked([optionItem])}>
                                <input title="checkbox" type="checkbox" class="custom-control-input" checked={isChecked(optionItem, 'L')}
                                    disabled={!canChecked(optionItem)} />
                                <label class="custom-control-label">
                                    {optionItem}
                                </label>
                            </div>
                        </li>;
                    })
                }
            </ul>;
        }

        function renderOptionTree() {
            return <></>;
        }

        function renderOptionPane() {
            return <div class="f-utils-fill option-pane d-flex flex-column">
                <div class="f-utils-flex-column option-pane-content f-utils-fill">
                    {shouldShowSearchBox.value && renderSearchBox()}
                    <div class="container columns-box f-utils-fill">
                        {shouldShowOptionList.value && renderOptionList()}
                        {shouldShowOptionTree.value && renderOptionTree()}
                    </div>
                </div>
            </div>;
        }

        const selectionListClass = computed(() => {
            const classObject = {
                'f-multi-select-list-group': true,
                'multi-select-list-group-flush': true,
                'f-multi-select-list-group--right': true
            } as Record<string, boolean>;
            return classObject;
        });

        const shouldShowCheckboxInSelectionList = function (selectionItem: any) {
            return shouldShowCheckbox.value && selectionItem.showCheckbox !== false;
        };

        const selectionListItemClass = function (selectionItem: any) {
            const classObject = {
                'f-multi-select-list-group-item': true,
                'item--show-checkbox': shouldShowCheckboxInSelectionList(selectionItem),
                'active': isChecked(selectionItem, 'R')
            } as Record<string, boolean>;
            return classObject;
        };

        function renderSelectionList() {
            return <ul class={selectionListClass.value}>
                {
                    selections.value && !!selections.value.length &&
                    selections.value.map((selectionItem: any) => {
                        return <li class={selectionListItemClass(selectionItem)}
                            onClick={() => rightDataChecked(selectionItem)}>
                            {
                                shouldShowCheckboxInSelectionList(selectionItem) &&
                                <div class="custom-control custom-checkbox f-checkradio-single">
                                    <input title="selection" type="checkbox"
                                        class="custom-control-input" checked={isChecked(selectionItem, 'R')}></input>
                                    <label class="custom-control-label"></label>
                                </div>
                            }
                            {selectionItem}
                        </li >;
                    })
                }
            </ul>;
        }

        function renderSelectionPane() {
            return <div class="f-utils-fill selection-pane d-flex flex-column">
                <div class="f-utils-flex-column selection-pane-content f-utils-fill">
                    <div class="selection-pane-title">
                        <span class="selection-title">{selectionTitle.value}</span>
                        <span class="selection-count">{selections.value && selections.value.length}</span>
                        <span class="selection-item-title">{selectionItemTitle.value}</span>
                        <span class="selection-title-action">
                            <i class="f-icon f-icon-exhale-discount"></i>
                            {selectionTitleAction.value}
                        </span>
                    </div>
                    <div class="container columns-box f-utils-fill">
                        {renderSelectionList()}
                    </div>
                </div>
            </div>;
        }

        const transferClass = computed(() => {
            const classObject = {
                'transfer': true,
                'row': true,
                'f-utils-fill': fillToParent.value
            } as Record<string, boolean>;
            return classObject;
        });

        return () => {
            return (
                <div class={transferClass.value}>
                    {renderOptionPane()}
                    {renderSelectionPane()}
                </div>
            );
        };
    }
});
