/**
 * Copyright (c) 2020 - present, Inspur Genersoft Co., Ltd.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *       http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
import { defineComponent, computed, ref, EmitsOptions } from 'vue';
import type { SetupContext } from 'vue';
import { buttonEditProps, ButtonEditProps } from './button-edit.props';
import { useButton } from './composition/use-button';
import { useClear } from './composition/use-clear';
import { useTextBox } from './composition/use-text-box';
import Overlay from '../../overlay/src/overlay.component';

export default defineComponent({
    name: 'FButtonEdit',
    props: buttonEditProps,
    emits: [
        'updateExtendInfo',
        'clear',
        'change',
        'click',
        'clickButton',
        'blur',
        'focus',
        'mouseEnterIcon',
        'mouseLeaveIcon',
        'keyup',
        'keydown',
        'inputClick',
        'input',
        'update:modelValue'
    ]  as (string[] & ThisType<void>) | undefined,
    setup(props: ButtonEditProps, context: SetupContext) {
        const buttonEditRef = ref<any>();
        const modelValue = ref(props.modelValue);
        const shouldShowConent = ref(false);
        const { buttonClass, onClickButton, onMouseEnterButton, onMouseLeaveButton } = useButton(props, context, shouldShowConent);
        const displayText = ref('');
        const {
            hasFocusedTextBox,
            isTextBoxReadonly,
            textBoxClass,
            textBoxPlaceholder,
            textBoxTitle,
            onBlurTextBox,
            onClickTextBox,
            onFocusTextBox,
            onInput,
            onKeyDownTextBox,
            onKeyUpTextBox,
            onMouseDownTextBox,
            onTextBoxValueChange
        } = useTextBox(props, context, modelValue, displayText);

        const { enableClearButton, showClearButton, onClearValue, onMouseEnterTextBox, onMouseLeaveTextBox } = useClear(
            props,
            context,
            modelValue,
            hasFocusedTextBox,
            displayText
        );

        const inputGroupClass = computed(() => ({
            'input-group': true,
            'f-state-disable': props.disable,
            'f-state-editable': props.editable && !props.disable && !props.readonly,
            'f-state-readonly': props.readonly && !props.disable,
            'f-state-focus': hasFocusedTextBox
        }));

        function onClickOverlay() {
            shouldShowConent.value = false;
        }

        const showOverlayContent = computed(() => shouldShowConent.value);

        const popupPosition = () => {
            if (buttonEditRef.value) {
                const buttonEditClientRect = buttonEditRef.value.getBoundingClientRect();
                const { left, top, height } = buttonEditClientRect;
                return {
                    left,
                    top: top + height
                };
            }
            return { left: 0, top: 0 };
        };

        function commitValue(value: any) {
            shouldShowConent.value = false;
            // modelValue.value = value;
        }

        context.expose({ commitValue });

        return () => {
            return (
                <>
                    <div {...context.attrs} ref={buttonEditRef} class="f-cmp-inputgroup" id={props.id}>
                        <div
                            class={[props.customClass, inputGroupClass.value]}
                            onMouseenter={onMouseEnterTextBox}
                            onMouseleave={onMouseLeaveTextBox}>
                            <input
                                name="input-group-value"
                                autocomplete={'' + props.autoComplete}
                                class={textBoxClass.value}
                                disabled={props.disable}
                                maxlength={props.maxLength}
                                minlength={props.minLength}
                                placeholder={textBoxPlaceholder.value}
                                readonly={isTextBoxReadonly.value}
                                tabindex={props.tabIndex}
                                title={textBoxTitle.value}
                                type={props.inputType}
                                value={modelValue.value}
                                onBlur={onBlurTextBox}
                                onChange={onTextBoxValueChange}
                                onClick={onClickTextBox}
                                onFocus={onFocusTextBox}
                                onInput={onInput}
                                onKeydown={onKeyDownTextBox}
                                onKeyup={onKeyUpTextBox}
                                onMousedown={onMouseDownTextBox}
                            />
                            <div class={buttonClass.value}>
                                {enableClearButton.value && (
                                    <span class="input-group-text input-group-clear" v-show={showClearButton.value} onClick={onClearValue}>
                                        <i class="f-icon modal_close"></i>
                                    </span>
                                )}
                                {props.buttonContent && (
                                    <span
                                        class="input-group-text input-group-append-button"
                                        onClick={onClickButton}
                                        onMouseenter={onMouseEnterButton}
                                        onMouseleave={onMouseLeaveButton}
                                        v-html={props.buttonContent}></span>
                                )}
                            </div>
                        </div>
                    </div>
                    {showOverlayContent.value && (
                        <Overlay popup-content-position={popupPosition()} onClick={onClickOverlay}>
                            {context.slots.default?.()}
                        </Overlay>
                    )}
                </>
            );
        };
    }
});
