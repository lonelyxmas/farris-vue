/**
 * Copyright (c) 2020 - present, Inspur Genersoft Co., Ltd.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *       http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
import { ComputedRef, ref, Ref, defineComponent, SetupContext, onMounted, watch } from 'vue';
import FQcCheckbox from '../condition-item/checkbox/checkbox.component'
import FQcComboLookup from '../condition-item/combolookup/combolookup.component'
import FQcDatePicker from '../condition-item/datepicker/datepicker.component'
import FQcDropdown from '../condition-item/dropdown/dropdown.component'
import FQcInput from '../condition-item/input/input.component'
import FQcInputGroup from '../condition-item/inputgroup/inputgroup.component'
import FQcNumber from '../condition-item/number/number.component'
import FQcNumberRange from '../condition-item/numberrange/numberrange.component'
import FQcRadio from '../condition-item/radio/radio.component'
import FQcSmarHelp from '../condition-item/smarthelp/smarthelp.component'

// import getCheckBox from '../condition-item/checkbox.component'
// import getComboLookup from '../condition-item/combolookup.component'
// import getDatePicker from '../condition-item/datepicker.component'
// import getDropdown from '../condition-item/dropdown.component'
// import getInput from '../condition-item/input.component'
// import getInputGroup from '../condition-item/inputgroup.component'
// import getNumber from '../condition-item/number.component'
// import getNumberRange from '../condition-item/numberrange.component'
// import getRadio from '../condition-item/radio.component'
// import getSmartHelp from '../condition-item/smarthelp.component'

import { QueryConditionProps, queryConditionProps } from './querycondition.props';
import { QuerySolutionVo } from '../../entity/querysolutionvo'
import { QueryCondition } from '../../entity/querycondition'
import { ControlType } from '../../entity/controltype';
import { toRefs } from '@vueuse/core';
//  import './index.scss';

export default defineComponent({
    name: 'FQueryCondition',
    props: queryConditionProps,
    emits: ['valueChange', 'blur', 'focus', 'click', 'input'] as (string[] & ThisType<void>) | undefined,
    setup(props: QueryConditionProps, context: SetupContext) {

        const {solutionChange} = props
        const { selectSolution } = toRefs(props)
        function renderCondition(solution: QuerySolutionVo) {
            console.log('执行renderCondition')
            if (!solution) return
            let conditionList = solution.queryConditions
            console.log('conditionList', solution)
            let list = conditionList && conditionList.map((condition: QueryCondition) => {
                let result = null
                console.log(condition.control.getControlType(), condition.fieldName, condition)
                switch (condition.control.getControlType()) {

                    case ControlType.Text:
                        // const renderText = getInput(condition, valueChange, true)
                        // return renderText.renderInput()
                        return <FQcInput item={condition} conditionChange={valueChange} hideLabel={false}></FQcInput>
                        break;
                    case ControlType.InputGroup:
                        // const renderInputGroup = getInputGroup()
                        // return renderInputGroup.renderInputGroup()
                        return <FQcInputGroup></FQcInputGroup>
                        break;
                    case ControlType.DropDownList:
                        // const renderDropDown = getDropdown()
                        // return renderDropDown.renderDropdown()

                        return <FQcDropdown></FQcDropdown>
                        break;
                    case ControlType.SmartHelp:
                        // const renderSmartHelp = getSmartHelp()
                        // return renderSmartHelp.renderSmartHelp()
                        return <FQcSmarHelp></FQcSmarHelp>
                        break;
                    case ControlType.SingleNumber:
                        // const renderSingleNumber = getNumber()
                        // return renderSingleNumber.renderNumber()
                        return <FQcNumber item={condition} conditionChange={valueChange} hideLabel={false}></FQcNumber>
                        break;
                    case ControlType.NumberRange:
                        // const renderNumberRange = getNumberRange()
                        // return renderNumberRange.renderNumberRange()
                        return <FQcNumberRange item={condition} conditionChange={valueChange} hideLabel={false}></FQcNumberRange>
                        break;
                    case ControlType.SingleDate:
                        // const renderSingleDate = getDatePicker()
                        // return renderSingleDate.renderDatePicker()
                        return <FQcDatePicker></FQcDatePicker>

                        break;
                    case ControlType.DateRange:
                        // const renderDateRange = getDatePicker()
                        // return renderDateRange.renderDatePicker()
                        return <FQcDatePicker></FQcDatePicker>
                        break;
                    case ControlType.SingleYear:
                        // const renderSingleYear = getDatePicker()
                        // return renderSingleYear.renderDatePicker()
                        return <FQcDatePicker></FQcDatePicker>
                        break;
                    case ControlType.SingleMonth:
                        // const renderSingleMonth = getDatePicker()
                        // return renderSingleMonth.renderDatePicker()
                        return <FQcDatePicker></FQcDatePicker>
                        break;
                    case ControlType.MonthRange:
                        // const renderMonthRange = getDatePicker()
                        // return renderMonthRange.renderDatePicker()
                        return <FQcDatePicker></FQcDatePicker>
                        break;
                    case ControlType.SingleDateTime:
                        // const renderSingleDateTime = getDatePicker()
                        // return renderSingleDateTime.renderDatePicker()
                        return <FQcDatePicker></FQcDatePicker>
                        break;
                    case ControlType.DateTimeRange:
                        // const renderDateTimeRange = getDatePicker()
                        // return renderDateTimeRange.renderDatePicker()
                        return <FQcDatePicker></FQcDatePicker>
                        break;
                    case ControlType.ComboLookUp:
                        // const renderComboLookUp = getComboLookup()
                        // return renderComboLookUp.renderComboLookup()
                        return <FQcComboLookup></FQcComboLookup>
                        break;
                    case ControlType.BoolCheck:
                        // const renderBoolCheck = getCheckBox()
                        // return renderBoolCheck.renderCheckBox()
                        return <FQcCheckbox></FQcCheckbox>
                        break;
                    case ControlType.Radio:
                        // const renderRadio = getRadio()
                        // return renderRadio.renderRadio()
                        return <FQcRadio></FQcRadio>
                        break;
                    default:
                        // const renderText2 = getInput()
                        return 'default'


                }

            })
            console.log('list', list)
            return list

        }

        function valueChange(data) {
            // let target = selectSolution.value.queryConditions.find(val => val.id === data.id)
            console.log('change', data, selectSolution.value)
            // solutionChange(data)
            
        }

        onMounted(() => {
            console.log(selectSolution.value)
            // renderCondition()
            // console.log(1, props.selectSolution)

        });
        //  watch(
        //     () => [props.selectSolution],
        //     ([newSelectSolution]) => {
        //         console.log(123, newSelectSolution)
        //         if(!newSelectSolution) return
        //         renderCondition(newSelectSolution)
        //     }
        // );



        function showdata2() {
            console.log(selectSolution.value)
        }

        return () => (
            <div class="row f-utils-flex-row-wrap farris-form farris-form-controls-inline">
                <div onClick={showdata2}>showdata</div>
                {renderCondition(selectSolution.value)}
                {/* <div onClick={clicktest}>clicksssssssssssssssssss</div> */}
            </div>
        );
    }
});
