/**
 * Copyright (c) 2020 - present, Inspur Genersoft Co., Ltd.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *       http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
import { defineComponent, computed, ref, onMounted, onBeforeUpdate, watch, reactive } from 'vue';
import type { SetupContext } from 'vue';
import { QueryCondition } from '../../../entity/querycondition';
import { QcInputProps, qcInputProps } from './input.props';
import { getGuid, getConditionItemTitle } from '../../../util/util';
//  import './index.scss';

export default defineComponent({
    name: 'FQcInput',
    props: qcInputProps,
    emits: ['conditionChange'] as (string[] & ThisType<void>) | undefined,
    setup(props: QcInputProps, context: SetupContext) {

        const { item, conditionChange, hideLabel } = props
        const config = reactive({

            text: '',
            placeholder: '',
            maxLength: undefined,
            testId: '',
            hasError: false,
        })


        onMounted(() => {
            if(item) {
                let _control:any = item.control
                config.testId = getGuid()
                config.placeholder = item && (item.placeHolder ? item.placeHolder : _control.placeholder)
                config.text = item.value.value
            }
           

        });

    

        function onBlur(e: any) {

            //好像没什么必要 多余
            console.log('onBulr', item, e)
            // return
            // if (config.hasError) {
            //     return;
            // }
            // conditionChange(item);
        }
        /**
         * 是否有错误信息
         */
        function hasErrorInfo(textControl: any) {
            if (textControl.invalid && (textControl.dirty || textControl.touched) && textControl.errors.maxlength) {
                config.hasError = true;
                return true;
            }
            config.hasError = false;
            return false;
        }

        function valueChange(e: any) {
            console.log('valueChange', e)
            if(e) {
                item?.value.setOriginalValue(e?.target.value)
            }
        }
        function claerValue() {
            console.log('claerValue')
            item?.value.setOriginalValue('')
        }



        return () => (
            <div class="col-12 col-md-6 col-xl-3 col-el-2 ng-star-inserted">
                <div class="farris-group-wrap">
                    <div class={'form-group farris-form-group  common-group' + (item?.readonly ? 'q-state-readonly' : '')}>
                        {
                            item && !hideLabel && <label class="col-form-label ng-star-inserted" title={getConditionItemTitle(item)}>
                                {item.control.isRequired() && <span class="farris-label-info text-danger" >*</span>}
                                <span class="farris-label-text">{item.fieldName}</span>
                            </label>
                        }
                       { item && <div class="farris-input-wrap">
                            <f-button-edit modelValue={config.text} id={config.testId} name={item.fieldName} onBlur={onBlur}
                            placeHolder={config.placeholder} readonly={item.readonly} maxLength={config.maxLength} onClear={claerValue}
                            onchange={valueChange}  buttonContent={''} enableClear={true}
                            ></f-button-edit>


                            {config.maxLength && <div class={'farris-feedback' + (hasErrorInfo(config.text) ? 'f-state-invalid' : "")} >
                                <span class="f-feedback-message">请输入有效格式</span>
                            </div>}
                        </div>}
                    </div>
                </div>
            </div>

        );
    }
});
