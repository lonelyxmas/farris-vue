/**
 * Copyright (c) 2020 - present, Inspur Genersoft Co., Ltd.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *       http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
import { defineComponent, computed, ref, onMounted, onBeforeUpdate, watch, reactive } from 'vue';
import type { SetupContext } from 'vue';
import { QcNumberProps, qcNumberProps } from './number.props';
//  import './index.scss';
import { getGuid, getConditionItemTitle } from '../../../util/util';
import { SingleNumberValue } from '../../../entity/conditionvalue/single-number-value';

export default defineComponent({
    name: 'FQcNumber',
    props: qcNumberProps,
    emits: ['valueChange', 'blur', 'focus', 'click', 'input'] as (string[] & ThisType<void>) | undefined,
    setup(props: QcNumberProps, context: SetupContext) {




        const { item, conditionChange, hideLabel } = props
        const config = reactive({

            number: 0,
            placeholder: '',
            precision: 0,
            textAlign: '',
            testId: '',
            canNull: false,
            bigNumber: true,
            min: null,
            max: null
        })


        onMounted(() => {
            if (item) {
                let _control: any = item.control
                console.log('2jx')
                config.number = item.value.getOriginalValue();
                config.testId = getGuid()
                config.canNull = true;
                config.placeholder = item.placeHolder ? item.placeHolder : _control.placeholder
                config.precision = _control.precision;
                config.textAlign = _control.textAlign;
                config.bigNumber = _control.bigNumber ? _control.bigNumber : false;
                if (!config.bigNumber) {
                    config.min = (_control.min === undefined || _control.min === null) ? -2147483648 : (_control.min);
                    config.max = (_control.max === undefined || _control.max === null) ? 2147483647 : (_control.max);
                }

                config.placeholder = item && (item.placeHolder ? item.placeHolder : _control.placeholder)
            }


        });

        

        


        function valueChange(data: any) {
            console.log('valueChange', data)
            const numberValue = new SingleNumberValue();
            
            item?.value.setOriginalValue(data)
            // item?.value.setOriginalValue('')
            // conditionChange.emit();
        }
        function claerValue() {
            item?.value.setOriginalValue('')
        }





        return () => (
            <div class="col-12 col-md-6 col-xl-3 col-el-2 ng-star-inserted">
                <div class="farris-group-wrap">
                    <div class={'form-group farris-form-group  common-group' + (item?.readonly ? 'q-state-readonly' : '')}>
                        {
                            item && !hideLabel && <label class="col-form-label ng-star-inserted" title={getConditionItemTitle(item)}>
                                {item.control.isRequired() && <span class="farris-label-info text-danger" >*</span>}
                                <span class="farris-label-text">{item.fieldName}</span>
                            </label>
                        }
                        {item && <f-number-spinner
                            value={config.number}
                            onValueChange={valueChange}
                            id={config.testId}
                            min={config.min}
                            max={config.max}
                            canNull={config.canNull}
                            bigNumber={config.bigNumber}
                            placeholder={config.placeholder}
                            precision={config.precision}
                            textAlign={config.textAlign}
                            readonly={item.readonly}
                        ></f-number-spinner>}
                    </div>

                </div>
            </div>

        );
    }
});
