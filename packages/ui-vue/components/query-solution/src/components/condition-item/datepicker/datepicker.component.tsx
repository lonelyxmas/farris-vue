/**
 * Copyright (c) 2020 - present, Inspur Genersoft Co., Ltd.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *       http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
import { defineComponent, computed, ref, onMounted, onBeforeUpdate, watch } from 'vue';
import type { SetupContext } from 'vue';
import { QcDatePickerProps, qcDatePikcerProps } from './datepicker.props';
//  import './index.scss';

export default defineComponent({
    name: 'FQcDatePicker',
    props: qcDatePikcerProps,
    emits: ['valueChange', 'blur', 'focus', 'click', 'input'] as (string[] & ThisType<void>) | undefined,
    setup(props: QcDatePickerProps, context: SetupContext) {




        onMounted(() => {
        });



        return () => (
            <div class="col-12 col-md-6 col-xl-3 col-el-2 ng-star-inserted">
                <div class="farris-group-wrap">
                FQcDatePicker
                </div>
            </div>

        );
    }
});
