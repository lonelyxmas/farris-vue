function getGuid() {
    return (s4() + s4() + "-" + s4() + "-" + s4() + "-" + s4() + "-" + s4() + s4() + s4());
}

function s4() {
    return (((1 + Math.random()) * 0x10000) | 0).toString(16).substring(1);
}
function getConditionItemTitle(field: any) {
    if (field.id && field.id !== '') {
        return field.fieldName;
    }
}

export {
    getGuid,
    s4,
    getConditionItemTitle
}