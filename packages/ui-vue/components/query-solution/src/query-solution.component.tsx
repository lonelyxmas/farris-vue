/**
 * Copyright (c) 2020 - present, Inspur Genersoft Co., Ltd.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *       http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
import { defineComponent, computed, ref, onMounted, onBeforeUpdate, watch } from 'vue';
import type { SetupContext } from 'vue';
import { cloneDeep } from 'lodash-es';
import { querySolutionProps, QuerySolutionProps } from './query-solution.props';
import { FieldConfig } from './entity/fieldconfig/fieldconfig'
import { QuerySolutionVo, QuerySolutionJSON } from './entity/querysolutionvo'
import { QueryCondition } from './entity/querycondition';
import { useData } from '../composition/use-data'
import { useUtil } from '../composition/use-util'
import { useInit } from '../composition/use-init'
import FQueryCondition from './components/query-condition/querycondition.component';

import './index.scss';


export default defineComponent({
    name: 'FQuerySolution',
    props: querySolutionProps,
    emits: ['valueChange', 'blur', 'focus', 'click', 'input'] as (string[] & ThisType<void>) | undefined,
    
    setup(props: QuerySolutionProps, context: SetupContext) {
        console.log(111)
        const fieldConfigs = ref<Array<QueryCondition>>([])
        const fieldConfigsAdvanced = ref<Array<QueryCondition>>([])
        const querySolutionVos = ref<Array<QuerySolutionVo>>([])
        const querySolutionAdvancedVos = ref<Array<QuerySolutionVo>>([])
        const selectSolution = ref<QuerySolutionVo | null>(null)


        const {  } = useData(props, context)
        const { convertToQueryConditions } = useInit(props, context)
        const { initQuerySolutionList, rangeToSimpleControlType } = useUtil(props, context)

        onMounted(() => {
            console.log('mount', props.fieldConfigs)
            let _fieldConfgs = convertToQueryConditions(props.fieldConfigs)
            console.log('_fieldConfgs', _fieldConfgs)
            fieldConfigs.value = _fieldConfgs
            let _fieldConfigsAdvanced = convertToQueryConditions(rangeToSimpleControlType(props.fieldConfigs))
            fieldConfigsAdvanced.value = _fieldConfigsAdvanced
            let list = initQuerySolutionList(props.querySolutionList, _fieldConfgs)
            querySolutionVos.value = list
            console.log(list)
            if(list.length) {
                selectSolution.value = list[0]
            }


            
           
        });


        function showdata() {
            console.log(selectSolution.value)
        }

        function changeSelectSolution(solution: QuerySolutionVo) {

            console.log('changeSelectSolution', solution)
            selectSolution.value = solution
        }

        return () => (
            <div class="farris-panel position-relative query-solution" tabindex="1" style="outline: none;">
                {/* { console.log(fieldConfigs.value, fieldConfigsAdvanced.value, querySolutionVos.value)} */}
                <div class="solution-header">
                    <div>
                        这里是标题
                    </div>
                    <div>
                        这里是条件概括
                    </div>
                    <div>这里是按钮组</div>
                </div>
                <div onClick={showdata}>showdata</div>
                <FQueryCondition selectSolution={selectSolution.value} solutionChange={changeSelectSolution}></FQueryCondition>

                
            </div>
        );
    }
});
