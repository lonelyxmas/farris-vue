import { IValue } from "./ivalue";
import { ControlType } from "../controltype";

export class SingleYearValue implements IValue{
    setOriginalValue(value: any): void {
        this.yearValue = value.formatted;
    }
    getOriginalValue() {
        return this.yearValue;
    }
    getControlType(): ControlType {
        return ControlType.SingleYear;
    }
    clearValue(): void {
        this.yearValue = undefined;
    }
    yearValue: string;
    constructor(year: string = ''){
        this.yearValue = year;
    }
    isEmpty(): boolean {
        return !this.yearValue;
    }
}