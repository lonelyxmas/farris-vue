import { IValue } from "./ivalue";
import { ControlType } from "../controltype";
import { IEnumValue, EnumValueConvert } from "../controltype/dropdownlist/ienumvalue";

export class DropDownListValue implements IValue{
    clearValue(): void {
        this.value = [];
        this.key = undefined;
    }
    setOriginalValue(data): void {}
    getOriginalValue() {
        return this.key;
    }
    getControlType(): ControlType {
        return ControlType.DropDownList;
    }
    value: any = [];//旧结构{Type: '', Content: {value: string, name: string}} 新结构Array[{value: string, name: string}]
    key: string;//旧结构无，新结构为选中的value值，多选是以,分割的字符串
    constructor(data: {value: any, key: string} = {value: [], key: undefined}){
        if(data.value && data.value.length > 0){//已有初始值[]
            if(data.hasOwnProperty('key')){//新结构
                this.value = data.value;
                this.key = data.key;
            }else{//旧结构
                this.value = data.value['Content'] && data.value['Content'].value ? [data.value['Content']] : [];
                this.key = data.value['Content'] && data.value['Content'].value ? data.value['Content'].value: undefined;
            }
        }else{
            this.clearValue();
        }
    }
    isEmpty(): boolean {
        return !this.key;
    }
}