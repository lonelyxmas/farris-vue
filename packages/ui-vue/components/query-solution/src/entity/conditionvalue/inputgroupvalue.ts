import { IValue } from './ivalue';
import { ControlType } from '../controltype';
import { cloneDeep } from 'lodash-es';

export class InputGroupValue implements IValue {

    getControlType(): ControlType {
        return ControlType.InputGroup;
    }

    setOriginalValue(value: any) {
        throw new Error("Method not implemented.");
    }

    getOriginalValue(): string {
        const args = this.textField.split('.');
        const textArr = this.value.map(item => this.getPropValue(item, cloneDeep(args)));
        return textArr && textArr.length ? textArr.join(','): '';
    }

    clearValue(): void {
        this.value = [];
        this.textValue = undefined;
        this.textField = undefined;
        this.isInputText = undefined;
    }

    isEmpty(): boolean {
        return !this.textValue;
    }

    //通过弹窗返回的若干个值对象构成的数组
    value: any[];
    //控件内显示的值
    textValue: string;
    //弹窗模式下，取列表中哪个字段的值映射到当前字段
    textField: string;
    //是否是手动输入的值
    isInputText: boolean;

    constructor(data: {
        value: any,
        textValue: string,
        textField: string,
        isInputText: boolean
    } = {value: [], textValue: undefined, textField: undefined, isInputText: undefined}){
        this.value = data.value;
        // this.textValue = data.textValue ? data.textValue.trim() : '';
        this.textValue = data.textValue ? data.textValue : '';
        this.textField = data.textField;
        this.isInputText = data.isInputText;
    }

    getTextValue(targetField: string): string {
        const args = targetField.split('.');
        const textArr = this.value.map(item => this.getPropValue(item, cloneDeep(args)));
        return textArr && textArr.length ? textArr.join(','): '';
    }

    getPropValue(item: any, args: Array<string>): any{
        if(args.length > 1){
            const arg = args.shift();
            return item[arg] ? this.getPropValue(item[arg], args) : '';
        }
        return item[args[0]];
    }
}