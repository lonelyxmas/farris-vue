import { IValue } from "./ivalue";
import { ControlType } from "../controltype";
import { cloneDeep } from 'lodash-es';

export class ComboLookUpValue implements IValue{
    clearValue(): void {
        this.value = [];
        this.valueField = undefined;
        this.textValue = undefined;
    }
    setOriginalValue(value: any): void {
        throw new Error("Method not implemented.");
    }
    getOriginalValue(): string {
        const args = this.valueField.split('.');
        const valueArr = this.value.map(helpItem => this.getPropValue(helpItem, cloneDeep(args)));
        return valueArr && valueArr.length ? valueArr.join(',') : '';
    }
    getControlType(): ControlType {
        return ControlType.ComboLookUp;
    }

    valueField:string;
    value:any[];
    textValue: string;

    constructor(data: {
        value: any, 
        valueField: string,
        textValue: string;
    } = {value: [], valueField: undefined, textValue: undefined}){
        if(data.textValue){//新结构，value是数组
            this.valueField = data.valueField;
            this.value = data.value;
            this.textValue = data.textValue;
        }else{//旧结构，value是对象
            this.valueField = data.valueField ? data.valueField : '';
            this.value = (data.value && data.value.data) ? [data.value.data] : [];
            this.textValue = 'help-text-value-null'; //旧结构无textValue属性
        }

    }
    isEmpty(): boolean {
        return !this.valueField;
    }

    getPropValue(helpItem: any, args: Array<string>): any{
        if(args.length > 1){
            const arg = args.shift();
            return helpItem[arg] ? this.getPropValue(helpItem[arg], args) : null;
        }
        return helpItem[args[0]];
    }

    getTextValue(textField: string): string {
        const args = textField.split('.');
        const textArr = this.value.map(helpItem => this.getPropValue(helpItem, cloneDeep(args)));
        return textArr && textArr.length ? textArr.join(',') : '';
    }

}