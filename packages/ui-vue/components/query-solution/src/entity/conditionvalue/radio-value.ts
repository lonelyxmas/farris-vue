import { IValue } from "./ivalue";
import { ControlType } from "../controltype";

export class RadioValue implements IValue {
    clearValue(): void {
        this.value = undefined;
    }
    setOriginalValue(value: any): void {
        this.value = value;
    }
    getOriginalValue(): any {
        return this.value;
    }
    getControlType(): ControlType {
        return ControlType.Radio;
    }
    value: any;
    constructor(value: any = undefined) {
        this.value = value;
    }
    isEmpty(): boolean {
        return !this.value;
    }
}