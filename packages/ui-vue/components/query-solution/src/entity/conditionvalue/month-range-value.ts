import { IValue } from "./ivalue";
import { ControlType } from "../controltype";

export class MonthRangeValue implements IValue {
  setOriginalValue(value: { formatted: string, delimiter: string }): void {
    if (value.formatted) {
      this.startTime = value.formatted.split(value.delimiter)[0];
      this.endTime = value.formatted.split(value.delimiter)[1];
    } else {
      this.clearValue();
    }
  }
  getOriginalValue() {
    if(!this.startTime||!this.endTime){
      return '';
     }
    return `${this.startTime}~${this.endTime}`;
  }
  getControlType(): ControlType {
    return ControlType.MonthRange;
  }
  clearValue(): void {
    this.startTime = undefined;
    this.endTime = undefined;
  }
  startTime: string;
  endTime: string;

  constructor(value: { startTime: string, endTime: string } = { startTime: '', endTime: '' }) {
    this.startTime = value.startTime;
    this.endTime = value.endTime;
  }
  isEmpty(): boolean {
    return !this.startTime || !this.endTime;
  }
}
