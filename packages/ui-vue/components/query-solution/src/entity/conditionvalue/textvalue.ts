import { IValue } from "./ivalue";
import { ControlType } from "../controltype";

export class TextValue implements IValue{
    clearValue(): void {
        this.value = undefined;
    }
    setOriginalValue(value: any): void {
        this.value = value;
    }
    getOriginalValue() {
        return this.value;
    }
    getControlType(): ControlType {
        return ControlType.Text;
    }
    value:string;
    constructor(data: {value: string} = {value : ''}){
        // this.value = data.value ? data.value.trim() : '';
        this.value = data.value ? data.value : '';
    }
    isEmpty(): boolean {
        return !this.value;
    }
}