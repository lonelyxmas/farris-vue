import { IValue } from "./ivalue";
import { ControlType } from "../controltype";

export class SingleMonthValue implements IValue{
    setOriginalValue(value: any): void {
        this.monthValue = value.formatted;
    }
    getOriginalValue() {
        return this.monthValue;
    }
    getControlType(): ControlType {
        return ControlType.SingleMonth;
    }
    clearValue(): void {
        this.monthValue = undefined;
    }
    monthValue: string;
    constructor(month: string = ''){
        this.monthValue = month;
    }
    isEmpty(): boolean {
        return !this.monthValue;
    }
}