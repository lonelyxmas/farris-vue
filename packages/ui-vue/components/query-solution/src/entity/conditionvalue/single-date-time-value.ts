import { IValue } from "./ivalue";
import { ControlType } from "../controltype";

export class SingleDateTimeValue implements IValue{
    setOriginalValue(value: any): void {
        this.datetimeValue = value.formatted;
    }
    getOriginalValue() {
        return this.datetimeValue;
    }
    getControlType(): ControlType {
        return ControlType.SingleDateTime;
    }
    datetimeValue: string;
    
    constructor(datetime: string = ''){
        this.datetimeValue = datetime;
    }
    isEmpty(): boolean {
        return !this.datetimeValue;
    }
    clearValue(): void {
        this.datetimeValue = undefined;
    }
}