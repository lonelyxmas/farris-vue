import { ControlType } from "../controltype";
import { TextValue } from "./textvalue";
import { NumberRangeValueConvertor, NumberRangeValue } from "./numberrangevaue";
import { DateRangeValue } from "./daterangevalue";
import { SmartHelpValue } from "./smarthelpvalue";
import { SingleDateValue } from './single-date-value';
import { SingleNumberValue } from './single-number-value';
import { SingleYearValue } from './single-year-value';
import { CheckBoxValue } from './checkbox-value';
import { DropDownListValue } from './dropdownlistvalue';
import { SingleMonthValue } from './single-month-value';
import { MonthRangeValue } from './month-range-value';
import { SingleDateTimeValue } from './single-date-time-value';
import { ComboLookUpValue} from './combolookup-value';
import { RadioValue } from './radio-value';
import { InputGroupValue } from './inputgroupvalue';

export interface IValue {
    getControlType(): ControlType;
    getOriginalValue(): any;
    setOriginalValue(value: any): void;
    clearValue(): void;
    isEmpty(): boolean;
}

export class ConditionValueConvert {
    /**
     * 序列化
     * 为了兼容C#产生的旧数据，向server传递的JSON object属性名全改为首字母大写
     */
    convertToObject(obj: IValue): Object {
        let conditionValue = obj as IValue;
        let controlType = conditionValue.getControlType();
        let jobj = new Object();
        jobj["Type"] = controlType;
        if (controlType == ControlType.NumberRange) {
            let convertor = new NumberRangeValueConvertor();
            jobj["Content"] = convertor.convertToObject(conditionValue);
        }
        else {
            jobj["Content"] = conditionValue;
        }
        return jobj;
    }


    initFromObject(jobj: Object): IValue {
        let content: IValue = jobj["Content"];
        
        switch(jobj["Type"]){
            case ControlType.Text:
                return new TextValue(jobj["Content"]);
            case ControlType.NumberRange:
                return new NumberRangeValue(jobj["Content"]);
            case ControlType.DateRange:
            case ControlType.DateTimeRange:
                return new DateRangeValue(jobj["Content"]);
            case ControlType.DropDownList:
                return new DropDownListValue(jobj["Content"]);
            case ControlType.SmartHelp:
                return new SmartHelpValue(jobj["Content"]);
            case ControlType.ComboLookUp:
                return new ComboLookUpValue(jobj["Content"]);
            case ControlType.SingleDate:
                return Object.assign(new SingleDateValue(), content as SingleDateValue);
            case ControlType.SingleNumber:
                return Object.assign(new SingleNumberValue(), content as SingleNumberValue);
            case ControlType.SingleYear:
                return Object.assign(new SingleYearValue(), content as SingleYearValue);
            case ControlType.BoolCheck:
                return Object.assign(new CheckBoxValue(), content as CheckBoxValue);
            case ControlType.Radio:
                return Object.assign(new RadioValue(), content as RadioValue);
            case ControlType.SingleMonth:
                return Object.assign(new SingleMonthValue(), content as SingleMonthValue);
            case ControlType.MonthRange:
                return Object.assign(new MonthRangeValue(), content as MonthRangeValue);
            case ControlType.SingleDateTime:
                return Object.assign(new SingleDateTimeValue(), content as SingleDateTimeValue);
            case ControlType.InputGroup:
                return Object.assign(new InputGroupValue(), content as InputGroupValue);
        }
    }
}