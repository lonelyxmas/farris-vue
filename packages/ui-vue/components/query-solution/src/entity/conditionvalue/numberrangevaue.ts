import { IValue } from "./ivalue";
import { ControlType } from "../controltype";

export class NumberRangeValue implements IValue {
    isEmpty(): boolean {
        return (this.startValue == null && this.endValue == null);
    }
    clearValue(): void {
        this.startValue = undefined;
        this.endValue = undefined;
    }
    setOriginalValue(value: any): void {
        throw new Error("Method not implemented.");
    }
    getOriginalValue() {
        return {
            begin: this.startValue,
            end: this.endValue
        };
    }
    getControlType(): ControlType {
        return ControlType.NumberRange;
    }
    startValue: number;
    endValue: number;
    constructor(data: {startValue: string, endValue: string} = {startValue: null, endValue: null}){
        this.startValue = data.startValue == null ? null : parseFloat(data.startValue);
        this.endValue = data.endValue == null ? null : parseFloat(data.endValue);
    }
}

export class NumberRangeValueConvertor {
    convertToObject(obj: IValue): Object {
        let numberRangeValue = obj as NumberRangeValue;
        let jobj = new Object();
        jobj["startValue"] = numberRangeValue.startValue;
        jobj["endValue"] = numberRangeValue.endValue;
        return jobj;
    }

    initFromObject(jobj: Object): NumberRangeValue {
        let numberRangeValue = new NumberRangeValue();
        if (jobj["startValue"]) {
            //key point！as Number并不会转化格式
            numberRangeValue.startValue = parseFloat(jobj["startValue"]);
        }
        if (jobj["endValue"]) {
            numberRangeValue.endValue = parseFloat(jobj["endValue"]);
        }
        return numberRangeValue;
    }
}