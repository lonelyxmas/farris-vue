import { IValue } from "./ivalue";
import { ControlType } from "../controltype";

type propsDateValue = {
    count: null | number,
    dateType: string,
    isFirstDay: boolean,
    isLastDay: boolean,
    name: string,
    period: string,
    text: string
}
export class SingleDateValue implements IValue{
    setOriginalValue(value: any): void {
        this.dateValue = value.formatted;
    }
    getOriginalValue() {
        return this.dateValue;
    }
    getControlType(): ControlType {
        return ControlType.SingleDate;
    }
    dateValue: string | propsDateValue;
    
    constructor(date: string | propsDateValue = ''){
        this.dateValue = date;
    }
    isEmpty(): boolean {
        return !this.dateValue;
    }
    clearValue(): void {
        this.dateValue = undefined;
    }
}