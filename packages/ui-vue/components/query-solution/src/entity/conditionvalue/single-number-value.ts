import { IValue } from "./ivalue";
import { ControlType } from "../controltype";

export class SingleNumberValue implements IValue{
    isEmpty(): boolean {
        return this.numValue == null;
    }
    clearValue(): void {
        this.numValue = undefined;
    }
    setOriginalValue(value: any): void {
        // throw new Error("Method not implemented.");
        this.numValue = isNaN(parseFloat(value)) ?   null :  value;
    }
    getOriginalValue() {
        return this.numValue;
    }
    getControlType(): ControlType {
        return ControlType.SingleNumber;
    }
    numValue: number;

    constructor(data: string = null){
        this.numValue = data == null ? null : parseFloat(data);
    }
}