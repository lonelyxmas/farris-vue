import { IValue } from "./ivalue";
import { ControlType } from "../controltype";
// CheckBoxValue初始化，与'app-solution-checkbox'组件初始化是分离的
// CheckBoxValue 要求value是布尔类型，组件要求value是true时，值应为true字符串
export class CheckBoxValue implements IValue{
    clearValue(): void {
        this.value = [];
    }
    // 如果组件的返回值是字符串，转化为布尔值
    setOriginalValue(value: any[]): void {
        if(value.length){
            this.value = value.map(v => {
               if(typeof v == 'string'){
                 return true;
               }else {
                 return v;
               }
            });
         }else{
            this.value = value;
         }
    }
    // 把CheckBoxValue的true布尔值，转化为组件可识别的字符串
    getOriginalValue() {
        if(this.value.length){
          return this.value.map(v => {
            if(v === true){
              return "true";
            }else{
              return v;
            }
          });
        }
        return this.value;
    }
    getControlType(): ControlType {
        return ControlType.BoolCheck;
    }
    value: any[];
    // CheckBoxValue 初始化，传入的值未做任何修改
    constructor(value: any[] = []){
        this.value = value;
    }
    isEmpty(): boolean {
        return this.value.length == 0;
    }
}