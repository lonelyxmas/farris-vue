import { IValue } from "./ivalue";
import { ControlType } from "../controltype";
import { cloneDeep } from 'lodash-es';

export class SmartHelpValue implements IValue{

    getControlType(): ControlType {
        return ControlType.SmartHelp;
    }

    setOriginalValue(value: any): void {
        throw new Error("Method not implemented.");
    }

    getOriginalValue(): string {
        const args = this.valueField.split('.');
        const valueArr = this.value.map(helpItem => this.getPropValue(helpItem, cloneDeep(args)));
        return valueArr && valueArr.length ? valueArr.join(',') : '';
    }

    clearValue(): void {
        this.value = [];
        this.valueField = undefined;
        this.textValue = undefined;
        this.isInputText = undefined;
    }
    
    isEmpty(): boolean {
        return !this.value.length;
    }

    value: any[];
    valueField: string;
    textValue: string;
    //帮助的值是否为手动输入的任意值，对应帮助的任意输入属性nosearch
    isInputText: boolean; 

    constructor(data: {
        value: any, 
        valueField: string,
        textValue: string,
        isInputText: boolean
    } = {value: [], valueField: undefined, textValue: undefined, isInputText: undefined}){
        if(data.textValue){
            this.value = data.value;
            this.valueField = data.valueField;
            this.textValue = data.textValue;
            this.isInputText = data.isInputText;
        }else{
            //帮助未支持多选时，没有textValue属性，所有值都是通过选择产生的
            this.value = data.value;
            this.valueField = data.valueField ? data.valueField : '';
            this.textValue = 'help-text-value-null'; 
            this.isInputText = false;
        }
    }

    getTextValue(textField: string): string {
        const args = textField.split('.');
        const textArr = this.value.map(helpItem => this.getPropValue(helpItem, cloneDeep(args)));
        return textArr && textArr.length ? textArr.join(',') : '';
    }

    getPropValue(helpItem: any, args: Array<string>): any{
        if(args.length > 1){
            const arg = args.shift();
            return helpItem[arg] ? this.getPropValue(helpItem[arg], args) : null;
        }
        return helpItem[args[0]];
    }
}