export enum ValueType{

    /**
     *值类型
     */
    Value = 0,

    /**
     * 帮助类型
     */
    SmartHelp=1,

    /**
     * 枚举类型
     */
    Enum=2,

    /**
     * 表达式
     */
    Express=3
}