import { CompareType } from "./comparetype";
import { ValueType } from "./valuetype";
import { IControl, ControlConvert } from "./controltype/icontrol";
import { IValue, ConditionValueConvert } from "./conditionvalue/ivalue";

/**
 * 查询条件实体类
 */
export class QueryCondition {

  /**
   * 唯一性标识
   */
  id: string;

  /**
   * 字段编号
   */
  fieldCode: string;

  /**
   * 字段名称
   */
  fieldName: string;

  /**
   * 比较符
   */
  // compareType:CompareType;

  /**
   * 值类型
   */
  valueType: ValueType;

  /**
   * 为空时提示信息
   */
  placeHolder: string;
  /**
   * 开始
   */
  beginPlaceHolder: string;
  /**
   * 结束
   */
  endPlaceHolder: string;
  /**
   * 值
   * 该属性为IValue接口，根据不同的控件类型有不同的实现类：具体如下：
   * 1. 文本类型：TextValue
   * 2. 下拉列表：DropDownListValue
   * 3. 日期区间：DateRangeValue
   * 4. 数字区间：NumberRangeValue
   * 5. 帮助类型：SmartHelpValue
   * 6. 复选框: CheckBoxValue
   * 7. 单数值: SingleNumberValue
   * 8. 单年度: SingleYearValue
   * 9. 单月度: SingleMonthValue
   * 10. 月度区间: MonthRangeValue
   * 11. 单日期: SingleDateValue
   * 12. 单日期时间: SingleDateTimeValue
   * 13. 下拉帮助： ComboLookUpValue
   * 14. 单选框： RadioValue
   * 15. 智能输入框： InputGroupValue
   */
  value: IValue;

  /**
   * 控件类型，目前包括文本类型，下拉列表，日期区间，数字区间，帮助类型
   * 控件类型为IControl接口，根据不同的控件类型对应不同的实现类，具体如下：
   * 1. 文本类型：TextControl
   * 2. 下拉列表：DropDownListControl
   * 3. 日期区间：DateRangeControl
   * 4. 数字区间：NumberRangeControl
   * 5. 帮助类型：SmartHelpControl
   * 6. 单日期： SingleDateControl
   * 7. 单数值：SingleNumberControl
   * 8. 单年度：SingleYearControl
   * 9. 单月度：SingleMonthControl
   * 10. 月度区间：MonthRangeControl
   * 11. 单日期时间：SingleDateTimeControl
   * 12. 日期时间区间：DateTimeRangeControl
   * 13. 复选框：CheckBoxControl
   * 14. 下拉帮助：ComboLookUpControl
   * 15. 单选框： RadioControl
   * 16. 智能输入框： InputGroupControl
   */
  control: IControl;
  readonly: boolean;
  visible?: boolean = true;
  /**
   * 高级模式下的条件id
   */
  conditionid?: number;
  /**
   * 高级模式下的值关系
   */
  compareType?: number;
  /**
   * 高级模式下的与下个条件的关系
   */
  relation?: number;
  /**
   * 高级模式下的值右括号
   */
  Rbracket?: string;
  /**
   * 高级模式下的值左括号
   */
  Lbracket?: string;

}

/**
 * 查询条件序列化与反序列化器
 */
export class QueryConditionConvert {

  /**
   * 序列化
   * @param obj 查询条件实体
   * 为了兼容C#产生的旧数据，向server传递的JSON object属性名全改为首字母大写
   */
  convertJObject(obj: QueryCondition): Object {
    let queryCondition = obj as QueryCondition;
    let jobj = new Object();
    jobj["ID"] = queryCondition.id;
    jobj["FieldCode"] = queryCondition.fieldCode;
    jobj["FieldName"] = queryCondition.fieldName;
    // jobj["compareType"]=queryCondition.compareType;
    jobj["ValueType"] = queryCondition.valueType;
    jobj["placeHolder"] = queryCondition.placeHolder;
    // 无论是否区间都追加
    jobj["beginPlaceHolder"] = queryCondition.hasOwnProperty('beginPlaceHolder') ? queryCondition['beginPlaceHolder'] : '';
    jobj["endPlaceHolder"] = queryCondition.hasOwnProperty('endPlaceHolder') ? queryCondition['endPlaceHolder'] : '';
    jobj["visible"] = queryCondition.hasOwnProperty('visible') ? queryCondition['visible'] : true;
    jobj["conditionid"] = queryCondition.hasOwnProperty('conditionid') ? queryCondition['conditionid'] : undefined;
    jobj["compareType"] = queryCondition.hasOwnProperty('compareType') ? queryCondition['compareType'] : undefined;
    jobj["relation"] = queryCondition.hasOwnProperty('relation') ? queryCondition['relation'] : undefined;
    jobj["Lbracket"] = queryCondition.hasOwnProperty('Lbracket') ? queryCondition['Lbracket'] : undefined;
    jobj["Rbracket"] = queryCondition.hasOwnProperty('Rbracket') ? queryCondition['Rbracket'] : undefined;
    let valueConvert = new ConditionValueConvert();
    if (queryCondition.value) {
      jobj["Value"] = valueConvert.convertToObject(queryCondition.value);
    }
    let controlConvertor = new ControlConvert();
    if (queryCondition.control) {
      jobj["Control"] = controlConvertor.convertJObject(queryCondition.control);
    }
    return jobj;
  }

  /**
   * 反序列化
   * @param jobj 查询条件实体序列化后的JSON对象
   * server端存储的是前端定义好格式的字符串，所以client接收时以约定好的格式反序列化
   */
  initFromJobject(jobj: Object): QueryCondition {
    let queryCondition = new QueryCondition();
    queryCondition.id = jobj["ID"];
    queryCondition.fieldCode = jobj["FieldCode"];
    queryCondition.fieldName = jobj["FieldName"];
    // queryCondition.compareType=jobj["compareType"] as CompareType;
    queryCondition.valueType = (jobj["ValueType"]) as ValueType;
    queryCondition.placeHolder = jobj["placeHolder"];
    queryCondition.beginPlaceHolder = jobj.hasOwnProperty('beginPlaceHolder') ? jobj['beginPlaceHolder'] : '';
    queryCondition.endPlaceHolder = jobj.hasOwnProperty('endPlaceHolder') ? jobj['endPlaceHolder'] : '';
    queryCondition.visible = jobj.hasOwnProperty('visible') ? jobj['visible'] : true;
    queryCondition.conditionid = jobj.hasOwnProperty('conditionid') ? jobj['conditionid'] : undefined;
    queryCondition.compareType = jobj.hasOwnProperty('compareType') ? jobj['compareType'] : undefined;
    queryCondition.relation = jobj.hasOwnProperty('relation') ? jobj['relation'] : undefined;
    queryCondition.Rbracket = jobj.hasOwnProperty('Rbracket') ? jobj['Rbracket'] : undefined;
    queryCondition.Lbracket = jobj.hasOwnProperty('Lbracket') ? jobj['Lbracket'] : undefined;
    const conditonValue = jobj["Value"];
    if (conditonValue) {
      let valueConvert = new ConditionValueConvert();
      queryCondition.value = valueConvert.initFromObject(conditonValue);
    }
    const conditionControl = jobj["Control"];
    if (conditionControl) {
      let controlConvertor = new ControlConvert();
      queryCondition.control = controlConvertor.initFromJobject(conditionControl);
    }
    return queryCondition;
  }
}
