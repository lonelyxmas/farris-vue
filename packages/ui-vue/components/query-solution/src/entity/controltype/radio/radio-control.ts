import { IControl } from "../icontrol";
import { ControlType } from "../../controltype";
import { EnumType } from "../dropdownlist/enumtype";
import { IEnumValue, EnumValueConvert } from "../dropdownlist/ienumvalue";
import { EnumStringValue } from "../dropdownlist/stringenumvalue";
import { EnumBoolValue } from "../dropdownlist/boolenumvalue";
import { EnumIntegerValue } from "../dropdownlist/integerenumvalue";

/**
 * 单选组控件
 */
export class RadioControl implements IControl {
    
    /**
     * 获取控件类型
     */
    getControlType(): ControlType {
        return ControlType.Radio;
    }
    isRequired(): boolean {
        return !!this.require;
    }
    /**
     * 单选框值类型
     */
    valueType: EnumType;

    /**
     * 单选框数据源列表
     * 该属性为IEnumValue接口类型，具体实现类如下：
     * 1.布尔类型：EnumBoolValue
     * 2.字符串类型：EnumStringValue
     * 3.整型类型：EnumIntegerValue 例如Radio Button类型需要使用EnumIntegerValue实现类
     */
    enumValues: Array<IEnumValue>;

    /**
    * 自定义样式
    */
    className: string;

    /**
     * 是否显示label
     */
    showLabel: boolean;
    
    /* 是否水平分布 */
    horizontal: boolean;
    
    /* 是否禁用 */
    disabled: boolean;
    
    require: boolean;
}

/**
* 单选组控件序列化与反序列化器
*/
export class RadioControlConvert{
    /**
     * 序列化
     * @param obj 单选组控件实体
     */
    convertToObject(obj: RadioControl){
        let radioControl = obj as RadioControl;
        let jobj = new Object();
        jobj["valueType"] = radioControl.valueType;
        if(radioControl.enumValues != null){
            let jarray = jobj["enumValues"] = [];
            let convertor = new EnumValueConvert();
            radioControl.enumValues.forEach(element => {
                if(radioControl.valueType == EnumType.BoolType){
                    jarray.push(convertor.convertJObject(Object.assign(new EnumBoolValue(),element)));
                }
                else if(radioControl.valueType == EnumType.IntType){
                    jarray.push(convertor.convertJObject(Object.assign(new EnumIntegerValue(),element)));
                }
                else{
                    jarray.push(convertor.convertJObject(Object.assign(new EnumStringValue(),element)));
                }
            });
        }
        jobj["horizontal"] = radioControl.horizontal;
        jobj["showLabel"] = radioControl.showLabel;
        jobj["disabled"] = radioControl.disabled;
        jobj["className"] = radioControl.className;
        jobj["require"] = radioControl.require;
        return jobj;
    }

    /**
     * 反序列化
     * @param jobj 单选组控件实体序列化后的JSON对象
     */
    initFromObject(jobj: Object): RadioControl{
        let control = new RadioControl();
        if(jobj["valueType"] != null){
            control.valueType = jobj["valueType"] as number;
        }
        if(jobj["enumValues"] != null){
            control.enumValues = new Array<IEnumValue>();
            jobj["enumValues"].forEach(element => {
                let convertor = new EnumValueConvert();
                control.enumValues.push(convertor.initFromJobject(element));
            });
        }
        if(jobj["horizontal"] != null){
            control.horizontal = jobj["horizontal"];
        }
        if(jobj["showLabel"] != null){
            control.showLabel = jobj["showLabel"];
        }
        if(jobj["disabled"] != null){
            control.disabled = jobj["disabled"];
        }
        if(jobj["className"] != null){
            control.className = jobj["className"];
        }
        if(jobj["require"] != null){
            control.require = jobj["require"];
        }
        return control;
    }
}