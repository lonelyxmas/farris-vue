import { IEnumValue } from "./ienumvalue";
import { EnumType } from "./enumtype";

/**
 * 下拉列表String类型
 */
export class EnumStringValue implements IEnumValue{
    getEnumType(): EnumType {
        return  EnumType.StringType;
    }

    getEnumValueName():string{
        return this.name;
    }

    getEnumValue():any{
        return this.value;
    }
    
    /**
     * 文本显示
     */
    name:string;

    /**
     * 值显示
     */
    value:string;
}