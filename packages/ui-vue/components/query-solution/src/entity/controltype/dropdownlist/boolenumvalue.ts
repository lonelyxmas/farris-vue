import { IEnumValue } from "./ienumvalue";
import { EnumType } from "./enumtype";

/**
 * 下拉列表Bool类型
 */
export class EnumBoolValue implements IEnumValue{
    
    getEnumType(): EnumType {
        return  EnumType.BoolType;
    }

    getEnumValueName():string{
        return this.name;
    }

    getEnumValue():any{
        return this.value;
    }

    /**
     * 文本显示
     */
    name:string;

    /**
     * 值显示
     */
    value:boolean;
}