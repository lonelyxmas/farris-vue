import { IControl } from "../icontrol";
import { ControlType } from "../../controltype";
import { EnumType } from "./enumtype";
import { IEnumValue, EnumValueConvert } from "./ienumvalue";
import { EnumStringValue } from "./stringenumvalue";
import { EnumBoolValue } from "./boolenumvalue";
import { EnumIntegerValue } from "./integerenumvalue";

/**
 * 下拉列表控件
 */
export class DropDownListControl implements IControl{

    /**
     * 获取控件类型
     */
    getControlType(): ControlType {
        return ControlType.DropDownList;
    }
    isRequired(): boolean {
        return !!this.require;
    }
    /**
     * 下拉值类型
     */
    valueType: EnumType;

    /**
     * 下拉数据源列表
     * 该属性为IEnumValue接口类型，具体实现类如下：
     * 1.布尔类型：EnumBoolValue,布尔类型的控件会转成下拉列表，实现类使用EnumBoolValue
     * 2.字符串类型：EnumStringValue
     * 3.整型类型：EnumIntegerValue 例如Radio Button类型需要使用EnumIntegerValue实现类
     */
    enumValues: Array<IEnumValue>;

    /**
     * 服务器端API
     */
    uri: string;

    /**
     * 面板显示前事件
     */
    beforeShow: any;

    /**
     * 面板隐藏前事件
     */
    beforeHide: any;

    idField: string;

    textField: string;

    /**
     * 是否多选
    */
    multiSelect: boolean;

    /**
     * 自定义样式
    */
    className: string;

    placeholder: string;

    panelHeight: number | string;

    require: boolean;
}

/**
 * 下拉列表控件序列化与反序列化器
 */
export class DropDownListControlConvert{

    /**
     * 序列化
     * @param obj 下拉列表控件实体
     */
    convertToObject(obj: DropDownListControl){
        let dropDownListControl = obj as DropDownListControl;
        let jobj = new Object();
        jobj["valueType"] = dropDownListControl.valueType;
        if(dropDownListControl.enumValues != null){
            let jarray = jobj["enumValues"] = [];
            let convertor = new EnumValueConvert();
            dropDownListControl.enumValues.forEach(element => {
                if(dropDownListControl.valueType == EnumType.BoolType){
                    jarray.push(convertor.convertJObject(Object.assign(new EnumBoolValue(),element)));
                }
                else if(dropDownListControl.valueType == EnumType.IntType){
                    jarray.push(convertor.convertJObject(Object.assign(new EnumIntegerValue(),element)));
                }
                else{
                    jarray.push(convertor.convertJObject(Object.assign(new EnumStringValue(),element)));
                }
            });
        }
        jobj["uri"] = dropDownListControl.uri;
        jobj["beforeShow"] = dropDownListControl.beforeShow;
        jobj["beforeHide"] = dropDownListControl.beforeHide;
        jobj["idField"] = dropDownListControl.idField;
        jobj["textField"] = dropDownListControl.textField;
        jobj["multiSelect"] = dropDownListControl.multiSelect;
        jobj["panelHeight"] = dropDownListControl.panelHeight;
        jobj["placeholder"] = dropDownListControl.placeholder;
        jobj["className"] = dropDownListControl.className;
        jobj["require"] = dropDownListControl.require;
        return jobj;
    }

    /**
     * 反序列化
     * @param jobj 下拉列表控件实体序列化后的JSON对象
     */
    initFromObject(jobj: Object): DropDownListControl{
        let control = new DropDownListControl();
        if(jobj["valueType"] != null){
            control.valueType = jobj["valueType"] as number;
        }
        if(jobj["enumValues"] != null){
            control.enumValues = new Array<IEnumValue>();
            jobj["enumValues"].forEach(element => {
                let convertor = new EnumValueConvert();
                control.enumValues.push(convertor.initFromJobject(element));
            });
        }
        if(jobj["uri"] != null){
            control.uri = jobj["uri"];
        }
        if(jobj["beforeShow"] != null){
            control.beforeShow = jobj["beforeShow"];
        }
        if(jobj["beforeHide"] != null){
            control.beforeHide = jobj["beforeHide"];
        }
        if(jobj["idField"] != null){
            control.idField = jobj["idField"];
        }
        if(jobj["textField"] != null){
            control.textField = jobj["textField"];
        }
        if(jobj["multiSelect"] != null){
            control.multiSelect = jobj["multiSelect"];
        }
        if(jobj["panelHeight"] != null){
            control.panelHeight = jobj["panelHeight"];
        }
        if(jobj["placeholder"] != null){
            control.placeholder = jobj["placeholder"];
        }
        if(jobj["className"] != null){
            control.className = jobj["className"];
        }
        if(jobj["require"] != null){
            control.require = jobj["require"];
        }
        return control;
    }
}