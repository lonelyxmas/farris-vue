import { EnumType } from "./enumtype";
import { EnumIntegerValue } from "./integerenumvalue";
import { EnumStringValue } from "./stringenumvalue";
import { EnumBoolValue } from "./boolenumvalue";

export interface IEnumValue{
    getEnumType():EnumType;
    getEnumValueName():string;
    getEnumValue():any;
}

export class EnumValueConvert{

    convertJObject(obj:IEnumValue):Object{
        let enumValue=obj as IEnumValue;
        let enumType=enumValue.getEnumType();
        let jobj=new Object();
        jobj["Type"]=enumType;
        jobj["Content"]=enumValue;
        return jobj;
    }

    initFromJobject(jobj:Object):IEnumValue{
        let enumType=jobj["Type"] as EnumType;
        let content=jobj["Content"] as IEnumValue;
        if(enumType==EnumType.IntType){
            return Object.assign(new EnumIntegerValue(),content) ;
        }
        else if(enumType==EnumType.BoolType){
            return Object.assign(new EnumBoolValue(),content) ;
        }
        else{
            return Object.assign(new EnumStringValue(),content) ;
        }
    }
}