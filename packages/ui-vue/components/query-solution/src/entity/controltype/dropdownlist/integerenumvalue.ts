import { IEnumValue } from "./ienumvalue";
import { EnumType } from "./enumtype";

/**
 * 下拉列表Integer类型
 */
export class EnumIntegerValue implements IEnumValue{
    getEnumType(): EnumType {
        return EnumType.IntType;
    }
    
    getEnumValueName():string{
        return this.name;
    }

    getEnumValue():any{
        return this.value;
    }

    /**
     * 文本显示
     */
    name:string;

    /**
     * 值显示
     */
    value:number;
}