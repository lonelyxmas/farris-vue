import { IControl } from "../icontrol";
import { ControlType } from "../../controltype";

/**
 * 帮助控件
 */
export class SmartHelpContol implements IControl {

    /**
     * 获取控件类型
     */
    getControlType(): ControlType {
        return ControlType.SmartHelp;
    }
    isRequired(): boolean {
        return !!this.require;
    }

    /**
     * 帮助数据源
     */
    uri: string;

    /**
     * 帮助的文本显示字段
     */
    textField: string;

    /**
     * 帮助值字段
     */
    valueField: string;

    /**
     * 字段映射信息
     */
    mapFields: any;

    /**
     * 主键
    */
    idField: any;

    /**
     * 帮助展示形式：普通列表，树结构
    */
    displayType: any;

    /**
     * 运行时参数配置传递
    */
    context: any;

    /**
     * 帮助前事件
    */
    preEventCmd: any;

    /**
    * 帮助后事件
    */
    postEventCmd: any;

    /**
     * 自定义样式
    */
    className: string;

    /**
     * 构造完整树
     */
    enableFullTree: boolean;

    /**
     * 数据加载方式
     */
    loadTreeDataType: string;

    /**
     * 是否多选
     */
    singleSelect: boolean;

    /**展开到指定级数 */
    expandLevel: number;

    /**
     * 启用级联选择
     */
    enableCascade: boolean;

    /**
     * 级联控制默认值： enable: 同步选择, up：包含上级, down：包含下级, disable：仅选择自身
     */
    cascadeStatus: "enable" | "up" | "down" | "disable";

    /**
     * 控件为空时提示信息，兼容旧表单
     */
    placeholder: string;

    /**
     * 控件是否必填
     */
    require: boolean;

    //期望手工输入、选择两用时，nosearch设为true
    nosearch: boolean;

    /**
     * 选择数据后自定义显示的字段，默认为空，为空时显示帮助设置的文本字段
     */
    displayFields: string;

    /**
     * 自定义显示字段时，不同字段间的分隔符
     */
    displayTextSeparator: string;

    /**
     * 是否允许编辑
     */
    editable: boolean;

    /**
     * 值变化时，需要清空哪些字段
     */
    clearFields: string;

    /**
     * 帮助清空事件
     */
    clear: any;
    /**
     * 默认分页
     */
    pageSize: number;
    /**
     * 分页
     */
    pageList: string;
   // 标题
   dialogTitle: string;
   // 宽度
   panelWidth: number;
   // 高度
   panelHeight: number;
   // 快捷选择
   quickSelect: QuickSelectOption;
}

export class QuickSelectOption {
    enable: boolean;
    showItemsCount?: number;
    formatter?: (data: any) => string;
    showMore?: boolean;
}