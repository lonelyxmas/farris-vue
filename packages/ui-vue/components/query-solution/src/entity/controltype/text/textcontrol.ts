import { IControl } from "../icontrol";
import { ControlType } from "../../controltype";

/**
 * 文本控件
 */
export class TextControl implements IControl {

    /**
     * 获取控件类型，返回值：文本控件类型
     */
    getControlType(): ControlType {
        return ControlType.Text;
    }
    isRequired(): boolean {
        return !!this.require;
    }
    /**
     * 字符串长度
     */
    length: number;
    /**
     * 自定义样式
    */
    className: string;

    placeholder: string;

    require: boolean;
}