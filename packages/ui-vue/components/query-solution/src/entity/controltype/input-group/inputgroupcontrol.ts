import { IControl } from "../icontrol";
import { ControlType } from "../../controltype";

/**
 * 智能输入框控件
 */
export class InputGroupControl implements IControl {

    /**
     * 获取控件类型，返回值：智能输入框控件类型
     */
    getControlType(): ControlType {
        return ControlType.InputGroup;
    }
    isRequired(): boolean {
        return !!this.require;
    }
    
    /**
     * 自定义样式
    */
    className: string;

    /**
     * 是否必填
     */
    require: boolean;

    /**
     * 是否允许输入
     */
    editable: boolean;

    /**
     * 按钮显示文本
     */
    groupText: string;

    /**
     * 使用模式（文本、弹出表单）
     */
    usageMode: string;

    /**
     * 弹出表单相关配置
     */
    dialogOptions: DialogConfig;

    /**
     * 按钮点击事件
     */
    click: any;
}

export class DialogConfig {
    modalId: string;
    mapFields: any;
    title: string;
    width: number;
    height: number;
    showCloseButton:boolean;
    showMaxButton:boolean;
    showHeader:boolean;
}