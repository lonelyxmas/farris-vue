import { IControl } from "../icontrol";
import { ControlType } from "../../controltype";

/**
 * 日期区间控件
 */
export class MonthRangeControl implements IControl {

    /**
     * 获取控件类型
     */
    getControlType(): ControlType {
        return ControlType.MonthRange;
    }

    isRequired(): boolean {
        return !!this.require;
    }

    /**
     * 日期格式
     */
    format: string;
    
    /**
     * 返回服务器端日期格式
     */
    returnFormat: string;

    /**
     * 日期变更后事件
     */
    dateChangedCmd: any;
    
    /*
     *起始提示
     */
    //beginPlaceHolder: string;
 
    /**
     * 结束提示
     */
    // endPlaceHolder: string;

    /**
     * 自定义样式
     */
    className: string;
    placeholder: string;
    require: boolean;
}