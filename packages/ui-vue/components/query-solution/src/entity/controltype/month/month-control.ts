import { IControl } from "../icontrol";
import { ControlType } from "../../controltype";

/**
 * 日期控件
 */
export class SingleMonthControl implements IControl {

    /**
     * 获取控件类型
     */
    getControlType(): ControlType {
        return ControlType.SingleMonth;
    }

    isRequired(): boolean {
        return !!this.require;
    }

    /**
     * 日期格式
     */
    format: string;
    
    /**
     * 返回服务器端日期格式
     */
    returnFormat:string;
    
    /**
     * 日期变更后事件
     */
    dateChangedCmd: any;
    
    /**
     * 自定义样式
     */
    className: string;
    placeholder: string;
    require: boolean;
}