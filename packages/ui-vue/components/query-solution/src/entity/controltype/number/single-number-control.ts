import { IControl } from "../icontrol";
import { ControlType } from "../../controltype";

/**
 * 数字区间控件
 */
export class SingleNumberControl implements IControl {

    /**
     * 获取控件类型
     */
    getControlType(): ControlType {
        return ControlType.SingleNumber;
    }
    isRequired(): boolean {
        return !!this.require;
    }
    /**
     * 长度
     */
    length: number;

    /**
     * 精度
     */
    precision: number;
    /**
    * 自定义样式
    */
    className: string;

    placeholder: string;

    require: boolean;

    textAlign: string;

    bigNumber: boolean;

    min: number;
    max: number;
}