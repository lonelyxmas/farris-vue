import { ControlType } from "../controltype";
import { DropDownListControl, DropDownListControlConvert } from "./dropdownlist/dropdownlistcontrol";
import { TextControl } from "./text/textcontrol";
import { DateRangeControl } from "./daterange/date-range-control";
import { SingleDateControl } from "./date/single-date-control";
import { SmartHelpContol } from "./smarthelp/smarthelpcontrol";
import { NumberRangeControl } from "./numberrange/numberrangecontrol";
import { SingleNumberControl } from './number/single-number-control';
import { SingleYearControl } from './year/year-control';
import { CheckBoxControl } from './checkbox/checkbox-control';
import { DateTimeRangeControl } from './daterange/datertime-range-control';
import { SingleMonthControl } from './month/month-control';
import { MonthRangeControl } from './monthrange/month-range-control';
import { SingleDateTimeControl } from './date/single-date-time-control';
import { ComboLookUpControl } from './combolookup/combolookup-control';
import { RadioControl, RadioControlConvert } from './radio/radio-control';
import { InputGroupControl } from './input-group/inputgroupcontrol';

/**
 * 控件接口
 */
export interface IControl {

    /**
     * 获取控件类型
     */
    getControlType(): ControlType;

    /**
     * 控件是否必填
     */
    isRequired(): boolean;
}

export class ControlConvert {
    convertJObject(obj: IControl): Object {
        let control = obj as IControl;
        let controlType = control.getControlType();
        let jobj = new Object();
        jobj["Type"] = controlType;

        if(controlType == ControlType.DropDownList) { 
            let convertor = new DropDownListControlConvert();
            jobj["Content"] = convertor.convertToObject(control as DropDownListControl);
        }
        else if(controlType == ControlType.Radio) {
            let convertor = new RadioControlConvert();
            jobj["Content"] = convertor.convertToObject(control as RadioControl);
        }
        else {
            jobj["Content"] = control;
        }
        return jobj;
    }

    initFromJobject(jobj: Object): IControl {
        let controlType = jobj["Type"] as ControlType;
        let content: IControl = jobj["Content"];

        if(controlType==ControlType.Text)
            return Object.assign(new TextControl(), content as TextControl);
        
        if(controlType == ControlType.InputGroup)
            return Object.assign(new InputGroupControl(), content as InputGroupControl);
        
        if(controlType == ControlType.DateRange)
            return Object.assign(new DateRangeControl(), content as DateRangeControl);

        if(controlType == ControlType.NumberRange)
            return Object.assign(new NumberRangeControl(), content as NumberRangeControl);

        if(controlType == ControlType.SingleDate)
            return Object.assign(new SingleDateControl(), content as SingleDateControl);

        if(controlType == ControlType.SingleNumber)
            return Object.assign(new SingleNumberControl(), content as SingleNumberControl);

        if(controlType == ControlType.DropDownList)
            return new DropDownListControlConvert().initFromObject(content);

        if(controlType == ControlType.SmartHelp)
            return Object.assign(new SmartHelpContol(), content as SmartHelpContol);

        if(controlType == ControlType.ComboLookUp)
            return Object.assign(new ComboLookUpControl(), content as ComboLookUpControl);

        if(controlType == ControlType.SingleYear)
            return Object.assign(new SingleYearControl(), content as SingleYearControl);

        if(controlType == ControlType.SingleMonth)
            return Object.assign(new SingleMonthControl(), content as SingleMonthControl);

        if(controlType == ControlType.BoolCheck)
            return Object.assign(new CheckBoxControl(), content as CheckBoxControl);

        if(controlType == ControlType.Radio)
            return new RadioControlConvert().initFromObject(content);

        if(controlType == ControlType.DateTimeRange)
            return Object.assign(new DateTimeRangeControl(), content as DateTimeRangeControl);

        if(controlType == ControlType.MonthRange)
            return Object.assign(new MonthRangeControl(), content as MonthRangeControl);

        if(controlType == ControlType.SingleDateTime)
            return Object.assign(new SingleDateTimeControl(), content as SingleDateTimeControl);
    }
}