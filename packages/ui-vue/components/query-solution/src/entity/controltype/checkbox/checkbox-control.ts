import { IControl } from "../icontrol";
import { ControlType } from "../../controltype";
interface Checkbox {
    value: any;
    name: any;
}
/**
 * 日期控件
 */
export class CheckBoxControl implements IControl {
    
    /**
     * 获取控件类型
     */
    getControlType(): ControlType {
        return ControlType.BoolCheck;
    }
    isRequired(): boolean {
        return !!this.require;
    }
    /**
    * 自定义样式
    */
    className: string;
    /* radio 数组 */
    data: Checkbox[] = [];
    /* 复选框name */
    name: string;
    /* 是否水平分布 */
    horizontal: boolean;
    /* 禁用 */
    disable: boolean;
    /* 复选框组的值 */
    value: any;
    /* 分隔符 默认逗号*/
    separator = ',';
    /* 值类型是否是string */
    isStringValue = true;

    require: boolean;
}