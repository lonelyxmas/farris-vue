import { IControl } from "../icontrol";
import { ControlType } from "../../controltype";

/**
 * 日期区间控件
 */
export class DateRangeControl implements IControl {

    /**
     * 获取控件类型
     */
    getControlType(): ControlType {
        return ControlType.DateRange;
    }
    isRequired(): boolean {
        return !!this.require;
    }
    
    /**
     * 日期格式
     */
    format: string;
    
    /**
     * 返回服务器端日期格式
     */
    returnFormat: string;

    /**
     * 日期变更后事件
     */
    dateChangedCmd: any;

    /**
     * 自定义样式
     */
    className: string;
    
    /**
     * 结束提示
     */
    // beginPlaceHolder: string;

    /**
     * 起始提示
     */
    // endPlaceHolder: string;
    
    placeholder: string;
    require: boolean;
    weekSelect: boolean;
    /**
     * 区间起始属性值
     */
    startFieldCode: string;
    /**
   * 区间结束属性值
   */
    endFieldCode: string;
}