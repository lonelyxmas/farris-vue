import { IControl } from "../icontrol";
import { ControlType } from "../../controltype";

/**
 * 帮助控件
 */
export class ComboLookUpControl implements IControl {

    /**
     * 获取控件类型
     */
    getControlType(): ControlType {
        return ControlType.ComboLookUp;
    }
    isRequired(): boolean {
        return !!this.require;
    }
    /**
     * 下拉帮助数据源
     */
    uri: string;

    /**
     * 主键
     */
    idField: any;

    /**
     * 下拉帮助值字段
     */
    valueField: string;

    /**
     * 下拉帮助文本显示字段
     */
    textField: string;

    /**
     * 字段映射信息
     */
    mapFields: any;

    /**
     * 下拉帮助展示形式：下拉列表帮助，下拉树列表帮助
     */
    displayType: any;

    /**
     * 帮助前事件
     */
    preEventCmd: any;

    /**
     * 帮助后事件
     */
    postEventCmd: any;

    /**
     * 是否多选
     */
    singleSelect: boolean;

    /**
     * 自定义样式
     */
    className: string;

    placeholder: string;

    panelWidth: number;

    panelHeight: number;

    require: boolean;
       /**
     * 构造完整树
     */
    enableFullTree: boolean;
    /**
     * 数据加载方式
     */
     loadTreeDataType: string;
     /**
     * 运行时参数配置传递
    */
    context: any;
}