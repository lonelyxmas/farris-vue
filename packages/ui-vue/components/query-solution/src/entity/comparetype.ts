/**
 * 此类没有被使用
 * 比较符 
 */
export enum CompareType{

    /**
     * 等于
     */
    // Equal=1,
    Equal = 0,

    /**
     * 大于
     */
    GreaterThan = 2,

    /**
     * 大于或等于
     */
    GreaterThanOrEqual = 3,

    /**
     * 小于
     */
    LessThan = 4,

    /**
     * 小于或等于
     */
    LessThanOrEqual = 5, 

    /**
     * 之间
     */
    // BetWeen=6
    Like = 6
}