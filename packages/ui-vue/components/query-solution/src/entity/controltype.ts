export enum ControlType{
    Text = 0,
    SingleDate = 1,
    SmartHelp = 2,
    DropDownList = 3,
    DateRange = 4,
    NumberRange = 5,
    SingleNumber = 6,
    SingleYear = 7,
    BoolCheck = 8,
    DateTimeRange = 9,
    SingleMonth = 10,
    MonthRange = 11,
    SingleDateTime = 12,
    ComboLookUp = 13,
    Radio = 14,
    InputGroup = 15
}