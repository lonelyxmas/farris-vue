/**
 * 查询方案展示配置
 */
export class QuerySolutionOptions{

    /**
     * 隐藏保存按钮
     */
    hiddenSaveButton:boolean;

    /**
     * 隐藏另存为按钮
     */
    hiddenSaveAsButton:boolean;

    /**
     * 隐藏下拉按钮
     */
    hiddenDropDownButton:boolean;

    /**
     * 隐藏筛选按钮
     */
    hiddenFilterButton:boolean;
}