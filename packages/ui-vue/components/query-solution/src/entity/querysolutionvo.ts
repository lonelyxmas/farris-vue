import { QueryCondition, QueryConditionConvert } from "./querycondition";
import { QuerySolutionExtendVo } from './querysolutionextendvo';
import {QueryconditionAdvancedData} from '../querycondition-advanced/type/type'
import { cloneDeep } from 'lodash-es';
/**
 * 查询方案实体类 
 */
export class QuerySolutionVo {
    /**
     * 唯一性标识
     */
    id: string;

    /**
     * 所属ID，例如所属表单ID
     */
    belongId: string;

    /**
     * 查询方案编号
     */
    code: string;

    /**
     * 查询方案名称
     */
    name: string;

    /**
     * 查询条件
     */
    queryConditions: Array<QueryCondition>;

    /**
     * 是否系统预置查询方案
     */
    isSystem: boolean;

    /**
     * 是否默认查询方案
     * 初始时，为false
     */
    isDefault: boolean = false;

    isChanged: boolean = false;
    /**
     * 类型： pre是上次查询
     */
    type: string;

    extendId: string = 'query-scheme-1';

    extendInfo: Array<QuerySolutionExtendVo>;

    code_chs: string;

    code_en: string;

    code_cht: string;
    /**
     * 高级模式新增字段，1为标准模式，2为高级模式
     */
    mode: string;
    /**
     * 高级模式新增辅助字段，用来展示condition的树结构
     */
    queryConditionsAdvanced: QueryconditionAdvancedData;
}

/**
 * 查询方案实体类序列化与反序列化器
 */
export class QuerySolutionVoConvertor {

    /**
     * 序列化
     * @param obj 查询方案实体
     */
    convertJObject(obj: QuerySolutionVo): Object {
        let querySolutionVo = obj as QuerySolutionVo;
        let jobj = new Object();
        jobj["id"] = querySolutionVo.id;
        jobj["belongId"] = querySolutionVo.belongId;
        jobj["code"] = querySolutionVo.code;
        jobj["name"] = querySolutionVo.name;
        jobj["isSystem"] = querySolutionVo.isSystem;
        jobj["isDefault"] = querySolutionVo.isDefault;
        jobj["type"] = querySolutionVo.type;
        jobj["extendId"] = querySolutionVo.extendId;
        jobj["extendInfo"] = querySolutionVo.extendInfo;
        jobj["code_chs"] = querySolutionVo.code_chs;
        jobj["code_en"] = querySolutionVo.code_en;
        jobj["code_cht"] = querySolutionVo.code_cht;
        jobj["mode"] = querySolutionVo.mode || '1';
        //针对树数据中做简化处理，仅保留对应条件的conditionid

        if(querySolutionVo.mode === '2') {
            // let _queryConditionsAdvanced = querySolutionVo.queryConditionsAdvanced
            // let _advancedFlatConditions = this.getFlatConditions(_queryConditionsAdvanced)
            // _advancedFlatConditions.forEach(val => val.conditionid = val.conditionid *2)
            let _queryConditionsAdvanced = this.simplifyTreeData(cloneDeep(querySolutionVo.queryConditionsAdvanced))
            jobj["advancedQueryConditionsString"] = JSON.stringify(_queryConditionsAdvanced)
        }
        
        if (querySolutionVo.queryConditions && querySolutionVo.queryConditions.length > 0) {
            let jArray = [];
            let convertor = new QueryConditionConvert();
            querySolutionVo.queryConditions.forEach(element => {
                jArray.push(convertor.convertJObject(element));
            });
            //向服务端发送数据时，发送query condition list的json string
            jobj["queryConditionString"] = JSON.stringify(jArray);
        }
        return jobj;
    }

    /**
     * 反序列化(初始化)
     * @param jobj 查询方案序列化后的Object
     */
    initFromJobject(jobj: Object): QuerySolutionVo {
        let querySolutionVo = new QuerySolutionVo();
        querySolutionVo.id = jobj["id"];
        querySolutionVo.code = jobj["code"];
        querySolutionVo.name = jobj["name"];
        querySolutionVo.belongId = jobj["belongId"];
        querySolutionVo.isSystem = jobj["isSystem"];
        querySolutionVo.isDefault = jobj["isDefault"];
        querySolutionVo.type = jobj.hasOwnProperty('type') ? jobj["type"] : '';
        querySolutionVo.extendId = jobj.hasOwnProperty('extendId') ? jobj["extendId"] : "";
        querySolutionVo.queryConditions = new Array<QueryCondition>();
        //高级模式新增mode,1为传统模式，2为高级模式
        querySolutionVo.mode = jobj["mode"] || 1;
        
        //服务器端返回的query condition list实际是 json string
        const queryConditionString = jobj["queryConditionString"];
        if (queryConditionString) {
            const conditionJarry = JSON.parse(queryConditionString);
            let convertor = new QueryConditionConvert();
            conditionJarry.forEach(element => {
                querySolutionVo.queryConditions.push(convertor.initFromJobject(element));
            });
        }
        let _queryConditionsAdvanced = jobj["advancedQueryConditionsString"] ? JSON.parse(jobj["advancedQueryConditionsString"]) : null;

        if(_queryConditionsAdvanced) {
            _queryConditionsAdvanced = this.complicateTreeData(_queryConditionsAdvanced, querySolutionVo.queryConditions)
            // let complicateTreeData = (treeData) => {

            //     if (treeData.items && treeData.items.length) {
            //         treeData.items.forEach(val => {
            //             let target = querySolutionVo.queryConditions.find(item => item.conditionid === val.conditionid)
            //             if (target) {
            //                 val = cloneDeep(target)
            //                 val.groupid = undefined
            //                 val.Lbracket = undefined
            //                 val.Rbracket = undefined
            //             }
            //         })
            //     }
            //     if(treeData.children && treeData.children.length) {
            //         treeData.children.forEach(val => {
            //             complicateTreeData(val)
            //         })
            //     }
            // }
            // complicateTreeData(_queryConditionsAdvanced)
            querySolutionVo.queryConditionsAdvanced = cloneDeep(_queryConditionsAdvanced)
        }
        return querySolutionVo;
    }
    /**
     * 从缓存中取回的JSON数据需要转化
     * 从大写转换到小写条件
     */
    formatFromJobject(jobj: Array<any> | string): Array<QueryCondition> {
        let queryConditions = new Array<QueryCondition>();
        let conditionJarry;
        if (typeof jobj == 'string') {
            conditionJarry = JSON.parse(jobj);
        } else {
            conditionJarry = jobj;
        }
        let convertor = new QueryConditionConvert();
        conditionJarry.forEach(element => {
            queryConditions.push(convertor.initFromJobject(element));
        });
        return queryConditions;
    }
    /**
     * 存入缓存中
     * 从大写转换到小写条件
     */
    formatConvertJObject(jobj: Array<QueryCondition>):Array<any>{
        let queryConditions =[];
        let convertor = new QueryConditionConvert();
        jobj.forEach(element => {
            queryConditions.push(convertor.convertJObject(element));
        });
        return queryConditions;
    }

    /**
     * 将高级模式下树结构condition扁平化
     * @param data 
     * @returns 
     */
    getFlatConditions(data) {
        if (data.children) {
          return [].concat(data.items, ...data.children.map(val => this.getFlatConditions(val)))
    
        }
        return [].concat(data.items)
    
      }
      simplifyTreeData(data) {
        if(data.items && data.items.length) {
            let _items = data.items.map(val =>  {return {conditionid: val.conditionid}})
            data.items = _items
        }
        if(data.children) {
            data.children.forEach(val => this.simplifyTreeData(val))
        }
        return data
    }
    complicateTreeData(originData, conditionList) {
        
        let handleFunc = (treeData) => {

            if (treeData.items && treeData.items.length) {
                treeData.items = treeData.items.map(val => {
                    let target = conditionList.find(item => item.conditionid === val.conditionid)
                    if (target) {
                        let result = cloneDeep(target)
                        result.groupid = undefined
                        result.Lbracket = undefined
                        result.Rbracket = undefined
                        return result
                    }
                    return null
                })
            }
            if(treeData.children && treeData.children.length) {
                treeData.children.forEach(val => {
                    handleFunc(val)
                })
            }
            //仅供返回顶层数据，遍历过程使用不到
            return treeData
        }
        return handleFunc(originData)

    }
}

export interface QuerySolutionJSON {
    id: string,
    belongId: string,
    code: string,
    isSystem: boolean,
    name: null | string,
    queryConditionString: string,
    advancedQueryConditionsString: string,
    mode?: '1' | '2' | null,
    userCode: string,
    userId: string,
    userName: string,
    isDefault: boolean,
    type: string,
    extendId: string,
    extendInfo?: null | string,
    code_chs?: null | string,
    code_en?: null | string,
    code_cht?: null | string
}