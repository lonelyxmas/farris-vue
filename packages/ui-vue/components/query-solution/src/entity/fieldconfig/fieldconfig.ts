import { QueryCondition } from '../querycondition';

/**
 * 字段配置
 */
export class FieldConfig {

  /**
   * 字段ID
   */
  id?: string;

  /**
   * 字段标签，字段编号的唯一标识
   */
  labelCode: string;

  /**
   * 字段名称
   */
  name?: string;

  /**
   * 是否被选中
   */
  isChecked?: boolean;

  /**
   * 是否被禁用
   */
  isDisabled?: boolean;

  /**
   * 字段为空时提示信息
   */
  placeHolder?: string;
  beginPlaceHolder?:string;
  endPlaceHolder?:string;

  control?: any;

  value?: any;

  visible?: boolean = true;

  constructor(qc: QueryCondition) {
    this.id = qc.id;
    this.labelCode = qc.fieldCode;
    this.name = qc.fieldName;
  }


}
