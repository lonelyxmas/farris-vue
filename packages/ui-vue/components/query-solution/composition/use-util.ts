

import { ref, onMounted, SetupContext } from 'vue';
import { QuerySolutionVo, QuerySolutionJSON } from '../src/entity/querysolutionvo'
import { QuerySolutionVoConvertor } from '../src/entity/querysolutionvo'
import { QueryCondition } from '../src/entity/querycondition'
import { FieldConfig } from '../src/entity/fieldconfig/fieldconfig'
import { SmartHelpContol } from '../src/entity/controltype/smarthelp/smarthelpcontrol';
import { InputGroupControl } from '../src/entity/controltype/input-group/inputgroupcontrol';
import { DropDownListControl } from '../src/entity/controltype/dropdownlist/dropdownlistcontrol';
import { RadioControl } from '../src/entity/controltype/radio/radio-control';
import { CheckBoxControl } from '../src/entity/controltype/checkbox/checkbox-control';
import { cloneDeep } from 'lodash-es';
export function useUtil(props: any, context: SetupContext) {

    function initQuerySolutionList(querySolutionList: Array<QuerySolutionJSON>, fieldConfigs:Array<QueryCondition>) {
        let qslist: Array<QuerySolutionVo> = []
        const convertor = new QuerySolutionVoConvertor();
        querySolutionList.forEach((element: QuerySolutionJSON) => {
            let querysolutionVO = formatQuerySolutionVO(convertor, element, fieldConfigs, true);
            qslist.push(querysolutionVO)

        })
        return qslist
    }

    function formatQuerySolutionVO(tConvertor: QuerySolutionVoConvertor, element: QuerySolutionJSON | QuerySolutionVo, fieldConfigs:Array<QueryCondition>, fromJobject = false): QuerySolutionVo {
        let querysolutionVO: QuerySolutionVo;
        let sourceData: any;
        let _fieldCodeArr: Array<string> = [];
        let _tQueryConditions: Array<QueryCondition>;
        if (fromJobject) {
            querysolutionVO = tConvertor.initFromJobject(element as QuerySolutionJSON);
        } else {
            querysolutionVO = element as QuerySolutionVo;
        }
        fieldConfigs.forEach((item: QueryCondition) => { _fieldCodeArr.push(item.fieldCode) });
        if (querysolutionVO && querysolutionVO.queryConditions && querysolutionVO.queryConditions.length > 0) {
            //若表单上删除了某个字段，但已保存的自定义方案中包含该字段时，在加载方案过程中将该字段剔除
            _tQueryConditions = querysolutionVO.queryConditions.filter(function (element) {
                return _fieldCodeArr.indexOf(element.fieldCode) > -1;
            });
            querysolutionVO.queryConditions = _tQueryConditions;

            //对自定义方案的国际化信息、帮助控件的帮助前及帮助后事件进行动态赋值
            querysolutionVO.queryConditions.forEach(qcelement => {
                sourceData = fieldConfigs.find(val => val.fieldCode == qcelement.fieldCode);
                qcelement.fieldName = sourceData.fieldName;
                qcelement.placeHolder = sourceData.placeHolder;
                qcelement.beginPlaceHolder = sourceData.hasOwnProperty('beginPlaceHolder') ? sourceData['beginPlaceHolder'] : '';
                qcelement.endPlaceHolder = sourceData.hasOwnProperty('endPlaceHolder') ? sourceData['endPlaceHolder'] : '';
                if (sourceData.control.preEventCmd) {
                    (qcelement.control as SmartHelpContol).preEventCmd = sourceData.control.preEventCmd;
                }
                if (sourceData.control.postEventCmd) {
                    (qcelement.control as SmartHelpContol).postEventCmd = sourceData.control.postEventCmd;
                }
                if (sourceData.control.clear) {
                    (qcelement.control as SmartHelpContol).clear = sourceData.control.clear;
                }
                if (sourceData.control.click) {
                    (qcelement.control as InputGroupControl).click = sourceData.control.click;
                }
                if (sourceData.control.beforeShow) {
                    (qcelement.control as DropDownListControl).beforeShow = sourceData.control.beforeShow;
                }
                if (sourceData.control.beforeHide) {
                    (qcelement.control as DropDownListControl).beforeHide = sourceData.control.beforeHide;
                }
                if (sourceData.control.dateChangedCmd) {
                    (qcelement.control as any).dateChangedCmd = sourceData.control.dateChangedCmd;
                }
                if (sourceData.control.enumValues && sourceData.control.enumValues.length > 0) {
                    (qcelement.control as DropDownListControl).enumValues = sourceData.control.enumValues;
                    (qcelement.control as RadioControl).enumValues = sourceData.control.enumValues;
                }
                if (sourceData.control.data && sourceData.control.data.length > 0) {
                    (qcelement.control as CheckBoxControl).data = sourceData.control.data;
                }
            });
        }
        return querysolutionVO;
    }


    function rangeToSimpleControlType(configs:Array<FieldConfig>):Array<FieldConfig> {
        let unformatConfigs = cloneDeep(configs)
        unformatConfigs.forEach(val => {
            if (val.control && val.control.controltype) {

                val.control.controltype = rangeToSimpleTypeModify(val.control.controltype)
            }

        })
        return unformatConfigs
    }
    function rangeToSimpleTypeModify(controlType:string) {

        let result = ''
        switch (controlType) {
    
          case 'date':
            result = 'single-date'
            break
          case 'date-time':
            result = 'single-date-time'
            break
          case 'month':
            result = 'single-month'
            break
          case 'number':
            result = 'single-number'
            break
          default:
            result = controlType
        }
        return result
      }

      
      
    return {
        initQuerySolutionList,
        rangeToSimpleControlType
    };
}
