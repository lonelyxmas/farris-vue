

import { ref, onMounted, SetupContext } from 'vue';
import { QuerySolutionVo, QuerySolutionJSON } from '../src/entity/querysolutionvo'
import { QuerySolutionVoConvertor } from '../src/entity/querysolutionvo'
import { QueryCondition } from '../src/entity/querycondition'
import { FieldConfig } from '../src/entity/fieldconfig/fieldconfig'
import { QuickSelectOption, SmartHelpContol } from '../src/entity/controltype/smarthelp/smarthelpcontrol';
import { DialogConfig, InputGroupControl } from '../src/entity/controltype/input-group/inputgroupcontrol';
import { DropDownListControl } from '../src/entity/controltype/dropdownlist/dropdownlistcontrol';
import { RadioControl } from '../src/entity/controltype/radio/radio-control';
import { CheckBoxControl } from '../src/entity/controltype/checkbox/checkbox-control';
import { cloneDeep } from 'lodash-es';
import { TextControl } from '../src/entity/controltype/text/textcontrol';
import { TextValue } from '../src/entity/conditionvalue/textvalue';
import { InputGroupValue } from '../src/entity/conditionvalue/inputgroupvalue';
import { DateRangeControl } from '../src/entity/controltype/daterange/date-range-control';
import { DateRangeValue } from '../src/entity/conditionvalue/daterangevalue';
import { DateTimeRangeControl } from '../src/entity/controltype/daterange/datertime-range-control';
import { MonthRangeControl } from '../src/entity/controltype/monthrange/month-range-control';
import { MonthRangeValue } from '../src/entity/conditionvalue/month-range-value';
import { NumberRangeControl } from '../src/entity/controltype/numberrange/numberrangecontrol';
import { NumberRangeValue } from '../src/entity/conditionvalue/numberrangevaue';
import { DropDownListValue } from '../src/entity/conditionvalue/dropdownlistvalue';
import { SmartHelpValue } from '../src/entity/conditionvalue/smarthelpvalue';
import { ComboLookUpControl } from '../src/entity/controltype/combolookup/combolookup-control';
import { ComboLookUpValue } from '../src/entity/conditionvalue/combolookup-value';
import { SingleDateControl } from '../src/entity/controltype/date/single-date-control';
import { SingleDateValue } from '../src/entity/conditionvalue/single-date-value';
import { SingleDateTimeControl } from '../src/entity/controltype/date/single-date-time-control';
import { SingleDateTimeValue } from '../src/entity/conditionvalue/single-date-time-value';
import { SingleNumberControl } from '../src/entity/controltype/number/single-number-control';
import { SingleNumberValue } from '../src/entity/conditionvalue/single-number-value';
import { SingleYearControl } from '../src/entity/controltype/year/year-control';
import { SingleYearValue } from '../src/entity/conditionvalue/single-year-value';
import { SingleMonthControl } from '../src/entity/controltype/month/month-control';
import { SingleMonthValue } from '../src/entity/conditionvalue/single-month-value';
import { CheckBoxValue } from '../src/entity/conditionvalue/checkbox-value';
import { RadioValue } from '../src/entity/conditionvalue/radio-value';
import { ValueType } from '../src/entity/valuetype';
export function useInit(props: any, context: SetupContext) {
    function convertToQueryConditions(fields: Array<FieldConfig>): Array<QueryCondition> {
        let queryCondition: QueryCondition;
        return fields.map(field => {
            queryCondition = new QueryCondition();
            queryCondition.id = field.id;
            queryCondition.fieldCode = field.labelCode;
            // queryCondition.code = field.code; 无用属性
            queryCondition.fieldName = field.name;
            queryCondition.valueType = ValueType.Value;
            queryCondition.placeHolder = field.placeHolder;
            queryCondition.beginPlaceHolder = field.hasOwnProperty('beginPlaceHolder') ? field.beginPlaceHolder : '';
            queryCondition.endPlaceHolder = field.hasOwnProperty('endPlaceHolder') ? field.endPlaceHolder : '';
            let { control, value } = getControlAndValue(field);
            queryCondition.control = control;
            queryCondition.value = value;
            return queryCondition;
        });
    }
    /**
     * 根据控件类型分别创建筛选条件的control对象和value对象
     * @param field 字段配置信息
     * @returns 筛选条件的control对象和value对象
     */
    function getControlAndValue(field: FieldConfig) {
        const controlData = field.control;
        const valueData = field.value;
        let control, value: any;
        switch (controlData.controltype) {
            case 'text':
                control = new TextControl();
                control.className = controlData.className;
                control.placeholder = controlData.placeholder;
                control.require = controlData.require;
                //plain string
                value = new TextValue({ value: valueData });
                break;
            case 'input-group':
                control = new InputGroupControl();
                control.className = controlData.className;
                control.require = controlData.require;
                control.editable = controlData.editable;
                control.groupText = controlData.groupText;
                control.usageMode = controlData.usageMode;
                control.dialogOptions = setInputGroupDialog(field);
                control.click = controlData.click;
                value = new InputGroupValue(valueData);
                break;
            case 'date':
                control = new DateRangeControl();
                control.format = controlData.format;
                control.returnFormat = controlData.returnFormat;
                control.dateChangedCmd = controlData.valueChangedCmd ? controlData.valueChangedCmd : controlData.dateChangedCmd;
                control.className = controlData.className;
                control.placeholder = controlData.placeholder;
                control.require = controlData.require;
                control.weekSelect = controlData.weekSelect;
                control.showWeekNumbers = controlData.showWeekNumbers;
                control.startFieldCode = controlData.startFieldCode || field.labelCode;
                control.endFieldCode = controlData.endFieldCode || field.labelCode;
                //control.beginPlaceHolder=controlData.beginPlaceHolder;
                //control.endPlaceHolder=controlData.endPlaceHolder;
                //{startTime: valueData.startTime, endTime: valueData.endTime}
                value = new DateRangeValue(valueData);
                break;
            case 'date-time':
                control = new DateTimeRangeControl();
                control.format = controlData.format;
                control.returnFormat = controlData.returnFormat;
                control.dateChangedCmd = controlData.valueChangedCmd ? controlData.valueChangedCmd : controlData.dateChangedCmd;
                control.className = controlData.className;
                control.placeholder = controlData.placeholder;
                control.require = controlData.require;
                control.enableCustomFormat = controlData.enableCustomFormat;
                // control.beginPlaceHolder=controlData.beginPlaceHolder;
                //control.endPlaceHolder=controlData.endPlaceHolder;
                //control.weekSelect = controlData.weekSelect;
                //control.showWeekNumbers = controlData.showWeekNumbers;
                //{startTime: valueData.startTime, endTime: valueData.endTime}
                value = new DateRangeValue(valueData);
                break;
            case 'month':
                control = new MonthRangeControl();
                control.format = controlData.format;
                control.returnFormat = controlData.returnFormat;
                control.dateChangedCmd = controlData.valueChangedCmd ? controlData.valueChangedCmd : controlData.dateChangedCmd;
                control.className = controlData.className;
                control.placeholder = controlData.placeholder;
                control.require = controlData.require;
                //control.beginPlaceHolder=controlData.beginPlaceHolder;
                //control.endPlaceHolder=controlData.endPlaceHolder;
                value = new MonthRangeValue(valueData);
                break;
            case 'number':
                control = new NumberRangeControl();
                control.className = controlData.className;
                control.placeholder = emptyString(controlData.placeholder);
                control.require = controlData.require;
                control.precision = controlData.precision;
                control.textAlign = controlData.textAlign;
                control.bigNumber = controlData.isBigNumber;
                control.min = controlData.minValue;
                control.max = controlData.maxValue;
                //control.beginPlaceHolder=controlData.beginPlaceHolder;
                //control.endPlaceHolder=controlData.endPlaceHolder;
                //{startValue: valueData.startValue, endValue: valueData.endValue}
                value = new NumberRangeValue(valueData);
                break;
            case 'dropdown':
                control = new DropDownListControl();
                control.valueType = controlData.valueType;
                control.enumValues = controlData.enumValues;
                control.uri = controlData.uri;
                control.beforeShow = controlData.beforeShow;
                control.beforeHide = controlData.beforeHide;
                control.idField = controlData.idField;
                control.textField = controlData.textField;
                control.multiSelect = controlData.multiSelect;
                control.className = controlData.className;
                control.placeholder = controlData.placeholder;
                control.panelHeight = controlData.panelHeight;
                control.require = controlData.require;
                //{value: [enumValues], key: 以，号分割的字符串})
                value = new DropDownListValue(valueData);
                break;
            case 'help':
                control = new SmartHelpContol();
                control.uri = controlData.uri;
                control.textField = controlData.textField;
                control.valueField = controlData.valueField;
                control.displayType = controlData.displayType;
                control.idField = controlData.idField;
                control.mapFields = controlData.mapFields;
                control.preEventCmd = controlData.preEventCmd;
                control.postEventCmd = controlData.postEventCmd;
                control.context = controlData.context;
                control.className = controlData.className;
                control.enableFullTree = controlData.enableFullTree;
                control.loadTreeDataType = controlData.loadTreeDataType;
                control.singleSelect = controlData.singleSelect;
                control.expandLevel = controlData.expandLevel;
                control.enableCascade = controlData.enableCascade;
                control.cascadeStatus = controlData.cascadeStatus;
                control.placeholder = controlData.placeholder;
                control.require = controlData.require;
                control.nosearch = controlData.nosearch;
                control.displayFields = controlData.displayFields;
                control.displayTextSeparator = controlData.displayTextSeparator;
                control.editable = controlData.editable;
                control.clearFields = controlData.clearFields;
                control.clear = controlData.clear;
                control.dialogTitle = controlData.dialogTitle;
                control.panelHeight = controlData.panelHeight;
                control.panelWidth = controlData.panelWidth;
                if (controlData.hasOwnProperty('pageSize')) {
                    control.pageSize = controlData.pageSize;
                }
                if (controlData.hasOwnProperty('pageList')) {
                    control.pageList = controlData.pageList;
                }
                if (controlData.hasOwnProperty('quickSelect')) {
                    let quickSelectControl = new QuickSelectOption();
                    quickSelectControl.enable = controlData.quickSelect['enable'];
                    quickSelectControl.showItemsCount = controlData.quickSelect['showItemsCount'];
                    quickSelectControl.formatter = controlData.quickSelect['formatter'];
                    quickSelectControl.showMore = controlData.quickSelect['showMore'];
                    control.quickSelect = quickSelectControl;
                }
                //{value: [{}], valueField: string, textValue: string}
                value = new SmartHelpValue(valueData);
                break;
            case 'combolist-help':
                control = new ComboLookUpControl();
                control.uri = controlData.uri;
                control.idField = controlData.idField;
                control.valueField = controlData.valueField;
                control.textField = controlData.textField;
                control.mapFields = controlData.mapFields;
                control.displayType = displayTypeTransform(controlData.displayType);
                control.singleSelect = controlData.singleSelect;
                control.preEventCmd = controlData.preEventCmd;
                control.postEventCmd = controlData.postEventCmd;
                control.enableFullTree = controlData.enableFullTree;
                control.loadTreeDataType = controlData.loadTreeDataType;
                control.expandLevel = controlData.expandLevel;
                control.className = controlData.className;
                control.placeholder = controlData.placeholder;
                control.panelWidth = controlData.panelWidth;
                control.panelHeight = controlData.panelHeight;
                control.require = controlData.require;
                control.context = controlData.context;
                //{value: [{}], valueField: string, textValue: string}
                value = new ComboLookUpValue(valueData);
                break;
            case 'single-date':
                control = new SingleDateControl();
                control.format = controlData.format;
                control.returnFormat = controlData.returnFormat;
                control.dateChangedCmd = controlData.valueChangedCmd ? controlData.valueChangedCmd : controlData.dateChangedCmd;
                control.className = controlData.className;
                control.placeholder = controlData.placeholder;
                control.require = controlData.require;
                control.isDynamicDate = controlData.isDynamicDate ? controlData.isDynamicDate : false;
                //plain string
                value = new SingleDateValue(valueData);
                break;
            case 'single-date-time':
                control = new SingleDateTimeControl();
                control.format = controlData.format;
                control.returnFormat = controlData.returnFormat;
                control.dateChangedCmd = controlData.valueChangedCmd ? controlData.valueChangedCmd : controlData.dateChangedCmd;
                control.className = controlData.className;
                control.placeholder = controlData.placeholder;
                control.require = controlData.require;
                control.enableCustomFormat = controlData.enableCustomFormat;
                value = new SingleDateTimeValue(valueData);
                break;
            case 'single-number':
                control = new SingleNumberControl();
                control.className = controlData.className;
                control.placeholder = emptyString(controlData.placeholder);
                control.precision = controlData.precision;
                control.require = controlData.require;
                control.textAlign = controlData.textAlign;
                control.bigNumber = controlData.isBigNumber;
                control.min = controlData.minValue;
                control.max = controlData.maxValue;
                //{numValue: xxxx}
                value = new SingleNumberValue(valueData);
                break;
            case 'single-year':
                control = new SingleYearControl();
                control.format = controlData.format;
                control.returnFormat = controlData.returnFormat;
                control.dateChangedCmd = controlData.valueChangedCmd ? controlData.valueChangedCmd : controlData.dateChangedCmd;
                control.className = controlData.className;
                control.placeholder = controlData.placeholder;
                control.require = controlData.require;
                control.maxDate = controlData.maxDate;
                control.minDate = controlData.minDate;
                //yearValue : plain string
                value = new SingleYearValue(valueData);
                break;
            case 'single-month':
                control = new SingleMonthControl();
                control.format = controlData.format;
                control.returnFormat = controlData.returnFormat;
                control.dateChangedCmd = controlData.valueChangedCmd ? controlData.valueChangedCmd : controlData.dateChangedCmd;
                control.className = controlData.className;
                control.placeholder = controlData.placeholder;
                control.require = controlData.require;
                value = new SingleMonthValue(valueData);
                break;
            case 'bool-check':
                control = new CheckBoxControl();
                control.className = controlData.className;
                control.data = [{ value: "true", name: field.name }];
                control.horizontal = controlData.horizontal;
                control.disable = controlData.disable;
                control.isStringValue = false;
                control.require = controlData.require;
                //[true] or [false]
                value = new CheckBoxValue(valueData);
                break;
            case 'radio':
                control = new RadioControl();
                control.valueType = controlData.valueType;
                control.enumValues = controlData.enumValues;
                control.className = controlData.className;
                control.showLabel = controlData.showLabel;
                control.horizontal = controlData.horizontal;
                control.disabled = controlData.disabled;
                control.require = controlData.require;
                value = new RadioValue(valueData);
                break;
        }
        return { control, value };
    }

    /**
     * 智能输入框控件需要弹出表单时，将表单配置JSON描述转换为弹出表单命令可接收的弹窗参数
     * @param field 配置为智能输入框控件的字段的JSON描述
     * @returns 弹窗参数
     */
    function setInputGroupDialog(field: any): DialogConfig | undefined {
        let inputGroupControlData = field.control;
        let modalConfigData = inputGroupControlData.modalConfig;
        if (!field.id || inputGroupControlData.usageMode == 'text') {
            return undefined;
        }

        let dialogConfig = new DialogConfig();
        dialogConfig.modalId = field.id;
        if (modalConfigData.mapFields) {
            dialogConfig.mapFields = JSON.parse(modalConfigData.mapFields.replace(/'/g, '"'));
        }
        dialogConfig.showHeader = modalConfigData.hasOwnProperty('showHeader') ? modalConfigData.showHeader : true;
        dialogConfig.showCloseButton = modalConfigData.hasOwnProperty('showCloseButton') ? modalConfigData.showCloseButton : true;
        dialogConfig.showMaxButton = modalConfigData.hasOwnProperty('showMaxButton') ? modalConfigData.showMaxButton : true;
        dialogConfig.title = modalConfigData.hasOwnProperty('title') ? modalConfigData.title : '';
        dialogConfig.width = modalConfigData.width;
        dialogConfig.height = modalConfigData.height;
        return dialogConfig;
    }

    /**
     * 解决数值控件placeholder显示为undefined的问题
     * @param s 数值控件JSON描述中的placeholder配置信息
     */
    function emptyString(data: any) {
        if (data === null || data === undefined) {
            return '';
        }
        return data;
    }

    /**
     * 解决jit生成代码后下拉帮助控件的displayType属性与组件api不一致的问题
     * @param data 下拉帮助控件JSON描述中的displayType配置信息
     */
    function displayTypeTransform(data: any) {
        if (data === 'List') {
            return 'LOOKUPLIST';
        }
        return 'LOOKUPTREELIST';
    }


    return {
        convertToQueryConditions

    };
}
