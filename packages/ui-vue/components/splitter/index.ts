import type { App } from 'vue';
import Splitter from './src/splitter.component';

export * from './src/splitter.props';
export { Splitter };

export default {
    install(app: App): void {
        app.component(Splitter.name, Splitter);
    }
};
