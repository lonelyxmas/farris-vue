import { Ref, SetupContext, computed, defineComponent, ref } from "vue";
import { SplitterPane, SplitterPropsType, splitterProps } from "./splitter.props";
import { useResize } from "./composition/use-resize";
import './splitter.css';

export default defineComponent({
    name: 'FSplitter',
    props: splitterProps,
    emits: [],
    setup(props: SplitterPropsType, context: SetupContext) {
        // const splitterPanes = ref(new Map<string, SplitterPane>());
        const leftPane: Ref<SplitterPane> = ref({ actualWidth: 200 });
        const rightPane: Ref<SplitterPane> = ref({ actualWidth: 200 });
        const topPane: Ref<SplitterPane> = ref({ actualHeight: 200 });
        const bottomPane: Ref<SplitterPane> = ref({ actualHeight: 200 });

        const { onClickSplitterHorizontalResizeBar, onClickSplitterVerticalResizeBar, horizontalResizeHandleStyle,
            verticalResizeHandleStyle, resizeOverlayStyle } = useResize(leftPane, rightPane, topPane, bottomPane);

        const splitterStyle = computed(() => {
            const styleObject = {
                'flex-direction': props.direction === 'row' ? 'column' : 'row'
            } as Record<string, any>;
            return styleObject;
        });

        const leftSplitterPaneStyle = computed(() => {
            const styleObject = {
                width: `${leftPane.value.actualWidth}px`
            } as Record<string, any>;
            return styleObject;
        });
        const rightSplitterPaneStyle = computed(() => {
            const styleObject = {
                width: `${rightPane.value.actualWidth}px`
            } as Record<string, any>;
            return styleObject;
        });
        const topSplitterPaneStyle = computed(() => {
            const styleObject = {
                height: `${topPane.value.actualHeight}px`
            } as Record<string, any>;
            return styleObject;
        });
        const bottomSplitterPaneStyle = computed(() => {
            const styleObject = {
                height: `${bottomPane.value.actualHeight}px`
            } as Record<string, any>;
            return styleObject;
        });

        return () => {
            return (
                <div class="f-splitter" style={splitterStyle.value}>
                    {props.direction === 'column' && context.slots.left &&
                        <div class="f-splitter-pane f-splitter-pane-column" style={leftSplitterPaneStyle.value}>
                            {context.slots.left()}
                            <span class="f-splitter-resize-bar f-splitter-resize-bar-e"
                                onMousedown={(payload: MouseEvent) => onClickSplitterHorizontalResizeBar(payload, 'leftPane')}></span>
                        </div>}
                    {props.direction === 'row' && context.slots.top &&
                        <div class="f-splitter-pane f-splitter-pane-row" style={topSplitterPaneStyle.value}>
                            {context.slots.top()}
                            <span class="f-splitter-resize-bar f-splitter-resize-bar-s"
                                onMousedown={(payload: MouseEvent) => onClickSplitterVerticalResizeBar(payload, 'topPane')}></span>
                        </div>}
                    {context.slots.main && <div class="f-splitter-pane f-splitter-pane-main">
                        {context.slots.main()}
                    </div>}
                    {props.direction === 'row' && context.slots.bottom &&
                        <div class="f-splitter-pane f-splitter-pane-row" style={bottomSplitterPaneStyle.value}>
                            {context.slots.bottom()}
                            <span class="f-splitter-resize-bar f-splitter-resize-bar-n"
                                onMousedown={(payload: MouseEvent) => onClickSplitterVerticalResizeBar(payload, 'bottomPane')}></span>
                        </div>}
                    {props.direction === 'column' && context.slots.right &&
                        <div class="f-splitter-pane f-splitter-pane-column" style={rightSplitterPaneStyle.value}>
                            {context.slots.right()}
                            <span class="f-splitter-resize-bar f-splitter-resize-bar-w"
                                onMousedown={(payload: MouseEvent) => onClickSplitterHorizontalResizeBar(payload, 'rightPane')}></span>
                        </div>}
                    <div class="f-splitter-resize-overlay" style={resizeOverlayStyle.value}></div>
                    <div class="f-splitter-horizontal-resize-proxy" style={horizontalResizeHandleStyle.value}></div>
                    <div class="f-splitter-vertical-resize-proxy" style={verticalResizeHandleStyle.value}></div>
                </div>
            );
        };
    }
});
