/* eslint-disable @typescript-eslint/indent */
import { computed, Ref, ref } from 'vue';
import { UseResize } from './types';
import { SplitterPane } from '../splitter.props';

export function useResize(
    leftPane: Ref<SplitterPane>,
    rightPane: Ref<SplitterPane>,
    topPane: Ref<SplitterPane>,
    bottomPane: Ref<SplitterPane>
): UseResize {
    const horizontalResizeBarPosition = ref(-1);
    const verticalResizeBarPosition = ref(-1);
    const horizontalResizeHandleOffset = ref(0);
    const verticalResizeHandleOffset = ref(0);
    const showHorizontalResizeHandle = ref(false);
    const showVerticalResizeHandle = ref(false);

    let splitterPane = '';
    let splitterElement: any;

    const horizontalResizeHandleStyle = computed(() => {
        const styleObject = {
            display: showHorizontalResizeHandle.value ? 'block' : 'none',
            left: `${horizontalResizeHandleOffset.value}px`
        } as Record<string, any>;
        return styleObject;
    });

    const verticalResizeHandleStyle = computed(() => {
        const styleObject = {
            display: showVerticalResizeHandle.value ? 'block' : 'none',
            top: `${verticalResizeHandleOffset.value}px`
        } as Record<string, any>;
        return styleObject;
    });

    const resizeOverlayStyle = computed(() => {
        const styleObject = {
            display: (showVerticalResizeHandle.value || showHorizontalResizeHandle.value) ? 'block' : 'none'
        } as Record<string, any>;
        return styleObject;
    });

    function draggingHorizontalResizeHandle($event: MouseEvent) {
        if (splitterElement) {
            horizontalResizeHandleOffset.value = $event.clientX - (splitterElement as any).offsetLeft;
        }
    }

    function draggingVerticalResizeHandle($event: MouseEvent) {
        if (splitterElement) {
            verticalResizeHandleOffset.value = $event.clientY - (splitterElement as any).offsetTop;
        }
    }

    function releaseMouseMove($event: MouseEvent) {
        let pane;
        switch (splitterPane) {
            case 'leftPane':
                pane = leftPane;
                break;
            case 'rightPane':
                pane = rightPane;
                break;
            case 'topPane':
                pane = topPane;
                break;
            case 'bottomPane':
                pane = bottomPane;
                break;
        }
        // const pane = splitterPane === 'leftPane' ? leftPane : (splitterPane === 'rightPane' ? rightPane : null);
        if (pane && (splitterPane === 'leftPane' || splitterPane === 'rightPane') && splitterElement) {
            const mouseReleasePosition = $event.clientX - (splitterElement as any).offsetLeft;
            if (splitterPane === 'leftPane') {
                pane.value.actualWidth = (pane.value.actualWidth || 0) + (mouseReleasePosition - horizontalResizeBarPosition.value);
            } else {
                pane.value.actualWidth = (pane.value.actualWidth || 0) - (mouseReleasePosition - horizontalResizeBarPosition.value);
            }
        }
        if (pane && (splitterPane === 'topPane' || splitterPane === 'bottomPane') && splitterElement) {
            const mouseReleasePosition = $event.clientY - (splitterElement as any).offsetTop;
            if (splitterPane === 'topPane') {
                pane.value.actualHeight = (pane.value.actualHeight || 0) + (mouseReleasePosition - verticalResizeBarPosition.value);
            } else {
                pane.value.actualHeight = (pane.value.actualHeight || 0) - (mouseReleasePosition - verticalResizeBarPosition.value);
            }
        }
        horizontalResizeHandleOffset.value = 0;
        verticalResizeHandleOffset.value = 0;
        horizontalResizeBarPosition.value = -1;
        verticalResizeBarPosition.value = -1;
        showHorizontalResizeHandle.value = false;
        showVerticalResizeHandle.value = false;
        document.removeEventListener('mousemove', draggingHorizontalResizeHandle);
        document.body.style.userSelect = '';
        splitterPane = '';
        splitterElement = null;
    }

    function onClickSplitterHorizontalResizeBar($event: MouseEvent, splitterPaneName: string) {
        splitterPane = splitterPaneName;
        showHorizontalResizeHandle.value = true;
        const clickElementPath = $event.composedPath();
        splitterElement = clickElementPath.find((element: any) => element.className.split(' ')[0] === 'f-splitter');
        if (splitterElement) {
            horizontalResizeHandleOffset.value = $event.clientX - (splitterElement as any).offsetLeft;
            horizontalResizeBarPosition.value = $event.clientX - (splitterElement as any).offsetLeft;
            document.addEventListener('mousemove', draggingHorizontalResizeHandle);
            document.addEventListener('mouseup', releaseMouseMove);
            document.body.style.userSelect = 'none';
        }
    }

    function onClickSplitterVerticalResizeBar($event: MouseEvent, splitterPaneName: string) {
        splitterPane = splitterPaneName;
        showVerticalResizeHandle.value = true;
        const clickElementPath = $event.composedPath();
        splitterElement = clickElementPath.find((element: any) => element.className.split(' ')[0] === 'f-splitter');
        if (splitterElement) {
            verticalResizeHandleOffset.value = $event.clientY - (splitterElement as any).offsetTop;
            verticalResizeBarPosition.value = $event.clientY - (splitterElement as any).offsetTop;
            document.addEventListener('mousemove', draggingVerticalResizeHandle);
            document.addEventListener('mouseup', releaseMouseMove);
            document.body.style.userSelect = 'none';
        }
    }

    return {
        onClickSplitterHorizontalResizeBar, onClickSplitterVerticalResizeBar,
        horizontalResizeHandleStyle, verticalResizeHandleStyle, resizeOverlayStyle
    };
}
