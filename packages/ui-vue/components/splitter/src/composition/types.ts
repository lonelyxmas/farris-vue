import { Ref } from "vue";

export interface UseResize {
    onClickSplitterHorizontalResizeBar: ($event: MouseEvent, columnField: string) => any;

    onClickSplitterVerticalResizeBar: ($event: MouseEvent, columnField: string) => any;

    horizontalResizeHandleStyle: Ref<Record<string, any>>;

    verticalResizeHandleStyle: Ref<Record<string, any>>;

    resizeOverlayStyle: Ref<Record<string, any>>;
}
