import { ExtractPropTypes, PropType } from "vue";

export interface SplitterPane {
    /** 记录原始定义宽度 */
    width?: number | string;
    /** 实际宽度 */
    actualWidth?: number;
    /** 记录原始定义高度 */
    height?: number | string;
    /** 实际高度 */
    actualHeight?: number;
    /** 面板位置 */
    position?: 'left' | 'center' | 'right' | 'top' | 'bottom';
    /** 是否显示 */
    visible?: boolean | any;
    /** True to allow the pane can be resized. */
    resizable?: boolean;
    /** True to allow the pane can be collapsed. */
    collapsable?: boolean;
}

export type SpliteDirection = 'column' | 'row';

export const splitterProps = {
    direction: { Type: String as PropType<SpliteDirection>, default: 'column' }
};
export type SplitterPropsType = ExtractPropTypes<typeof splitterProps>;
