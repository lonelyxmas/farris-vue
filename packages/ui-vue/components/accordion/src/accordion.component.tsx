/**
 * Copyright (c) 2020 - present, Inspur Genersoft Co., Ltd.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *       http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
import { computed, defineComponent, SetupContext } from 'vue';
import { AccordionProps, accordionProps } from './accordion.props';

import './accordion.css';

export default defineComponent({
    name: 'FAccordion',
    props: accordionProps,
    emits: []  as (string[] & ThisType<void>) | undefined,
    setup(props: AccordionProps, context: SetupContext) {
        const accordionStyle = computed(() => ({
            height: props.height ? `${props.height}px` : '',
            width: props.width ? `${props.width}px` : ''
        }));

        const accordionClass = computed(() => {
            const customClassArray = props.customClass;
            const accordionClassObject = {
                'farris-panel': true,
                accordion: true
            };
            customClassArray.reduce<Record<string, unknown>>((classObject, classString) => {
                classObject[classString] = true;
                return classObject;
            }, accordionClassObject);
            return accordionClassObject;
        });

        return () => {
            return (
                <div class={accordionClass.value} style={accordionStyle.value}>
                    {context.slots.default && context.slots.default()}
                </div>
            );
        };
    }
});
