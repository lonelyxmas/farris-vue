import { ExtractPropTypes } from 'vue';

export interface Tag {
    name: string;
    checked?: boolean;
    closable?: boolean;
    color?: string;
    selectable?: boolean;
    size?: string;
    tagType?: string;
    value?: string;
}

export const tagsProps = {
    activeTag: { Type: String, default: '' },
    addButtonText: { Type: String, default: '' },
    customClass: { Type: String, default: '' },
    customStyle: { Type: String, default: '' },
    data: { Type: Array<Tag>, default: [] },
    enableAddButton: { Type: Boolean, default: false },
    placeholder: { Type: String, default: '' },
    selectable: { Type: Boolean, default: false },
    showAddButton: { Type: Boolean, default: false },
    showClose: { Type: Boolean, default: false },
    showColor: { Type: Boolean, default: false },
    showInput: { Type: Boolean, default: false },
    tagType: { Type: String, default: '' }
};

export type TagsProps = ExtractPropTypes<typeof tagsProps>;
