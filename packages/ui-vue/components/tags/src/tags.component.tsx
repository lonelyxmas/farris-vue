import { SetupContext, computed, defineComponent, ref } from 'vue';
import { Tag, TagsProps, tagsProps } from './tags.props';
import './tags.scss';

export default defineComponent({
    name: 'FTags',
    props: tagsProps,
    emits: [],
    setup(props: TagsProps, context: SetupContext) {
        const selectable = ref(props.selectable);
        const customClass = ref(props.customClass);
        const innerTags = ref(props.data);
        const tagType = ref(props.tagType);
        const showColor = ref(props.showColor);
        const activeTagValue = ref(props.activeTag);
        const customStyle = ref(props.customStyle);
        const showClose = ref(props.showClose);
        const showAddButton = ref(props.showAddButton);
        const addInput = ref(props.showInput);
        const addButtonText = ref(props.addButtonText);
        const enableAddButton = ref(props.enableAddButton);
        const placeholder = ref(props.placeholder);
        const addTagValue = ref('');
        const activeTag = ref<Tag>();

        const tagsClass = computed(() => {
            const classObject = {
                'farris-tags': true,
                'farris-tags-checkable': selectable.value
            } as Record<string, boolean>;
            customClass.value.split(' ').reduce((result: Record<string, boolean>, targetClassName: string) => {
                result[targetClassName] = true;
                return result;
            }, classObject);
            return classObject;
        });

        function tagItemClass(tag: Tag) {
            const classObject = {
                'farris-tag-item': true,
                'farris-tag-item-checked': selectable.value && tag.checked,
                'farris-tag-item-checkable': selectable.value,
                'farris-tag-item-has-color': showColor.value,
                'farris-tag-item-actived': activeTagValue.value === tag.name
            } as Record<string, boolean>;
            if (tagType.value) {
                const tagTypeClassName = `farris-tag-item-${tagType.value}`;
                classObject[tagTypeClassName] = true;
            }
            return classObject;
        }

        const tagItemStyle = computed(() => {
            const styleObject = {} as Record<string, any>;
            if (customStyle.value) {
                customStyle.value.split(';').reduce((result: Record<string, any>, styleString: string) => {
                    const styles = styleString.split(':');
                    result[styles[0]] = styles[1];
                    return result;
                }, styleObject);
            }
            return styleObject;
        });

        function onClickTagItem(e: MouseEvent, currentTag: Tag) {
            if (selectable.value && currentTag.selectable) {
                activeTagValue.value = currentTag.name;
                currentTag.checked = !currentTag.checked;
                innerTags.value.filter((tag: Tag) => tag.name !== currentTag.name)
                    .forEach((otherTag: Tag) => {
                        otherTag.checked = !currentTag.checked;
                    });
            }
        }

        function onClickCloseHandler(e: MouseEvent, tag: any, index: number) { }

        const addButtonClass = computed(() => {
            const classObject = {
                'farris-tag-item': true,
                'farris-tag-add-button': true,
                'farris-tag-add-button-disabled': !enableAddButton.value
            } as Record<string, boolean>;
            return classObject;
        });

        function onClickAddHandler(e: MouseEvent) { }

        function renderAddButton() {
            return (
                <li class={addButtonClass.value} onClick={(payload: MouseEvent) => onClickAddHandler(payload)}>
                    <span class="f-icon f-icon-amplification"></span>
                    <span class="farris-tag-add-text">{addButtonText.value}</span>
                </li>
            );
        }

        function onAddInputBlur(e: FocusEvent) { }

        function renderTagInput() {
            return (
                <li class="farris-tag-input-box">
                    <input
                        type="text"
                        class="form-control"
                        onBlur={(payload: FocusEvent) => onAddInputBlur(payload)}
                        value={addTagValue.value}
                        placeholder={placeholder.value}></input>
                </li>
            );
        }

        function renderTagItems() {
            const tagItems = innerTags.value.map((tag: Tag, index: number) => {
                return (
                    <li
                        class={tagItemClass(tag)}
                        style={tagItemStyle.value}
                        onClick={(payload: MouseEvent) => onClickTagItem(payload, tag)}>
                        <span class="tag-box">{tag.name}</span>
                        {showClose.value && (
                            <span class="tag-delete">
                                <i
                                    class="f-icon f-icon-close"
                                    onClick={(payload: MouseEvent) => onClickCloseHandler(payload, tag, index)}></i>
                            </span>
                        )}
                    </li>
                );
            });
            if (showAddButton.value && !addInput.value) {
                tagItems.push(renderAddButton());
            }
            if (showAddButton.value && addInput.value) {
                tagItems.push(renderTagInput());
            }
            return tagItems;
        }

        return () => {
            return (
                <div class={tagsClass.value}>
                    <ul class="farris-tags-item-container farris-tag-item-capsule">{renderTagItems()}</ul>
                </div>
            );
        };
    }
});
