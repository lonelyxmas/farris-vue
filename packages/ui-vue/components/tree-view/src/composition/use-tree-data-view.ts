/**
 * Copyright (c) 2020 - present, Inspur Genersoft Co., Ltd.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *       http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
import { UseTreeDataView } from './types';
import { computed, ref } from 'vue';
import { TreeViewProps } from '../tree-view.props';

export type DataViewFilter = (dataItem: any) => boolean;

export function useTreeDataView(props: TreeViewProps): UseTreeDataView {
    const collapseMap = new Map<string, boolean>();
    const filtersMap = new Map<string, DataViewFilter>();
    const originalData = ref(props.data);

    function resetDataView() {
        const resetData = originalData.value;

        let rowIndex = 0;
        const dataViewItems: any[] = [];
        if (resetData.length) {
            for (let index = 0; index < resetData.length; index++) {
                const dataItem = resetData[index] as any;

                rowIndex++;
                dataItem.__fv_data_index__ = rowIndex;
                dataItem.__fv_index__ = index;

                dataViewItems.push(dataItem);
            }
        }
        return dataViewItems;
    }
    const dataView = ref(resetDataView());

    function applyFilter(filters: DataViewFilter[], reset = false) {
        const source = reset ? resetDataView() : dataView.value;
        dataView.value = source
            .filter((dataItem: any, index: number) => {
                return filters.reduce((result: boolean, filter: DataViewFilter) => {
                    return result && filter(dataItem);
                }, true);
            })
            .map((dataItem: any, index: number) => {
                dataItem.__fv_index__ = index;
                return dataItem;
            });
        return dataView.value;
    }

    function collapse(collapseField: string, collapseValue: any) {
        const groupedRowId = `group_of_${collapseField}_${collapseValue}`;
        collapseMap.set(groupedRowId, true);
        const collapseFieldFilter = (dataItem: any) => dataItem[collapseField] !== collapseValue;
        filtersMap.set(`collapse_${collapseField}_${collapseValue}`, collapseFieldFilter);
        return applyFilter(Array.from(filtersMap.values()));
    }

    function expand(expandField: string, expandValue: any) {
        const groupedRowId = `group_of_${expandField}_${expandValue}`;
        collapseMap.set(groupedRowId, false);
        filtersMap.delete(`collapse_${expandField}_${expandValue}`);
        return applyFilter(Array.from(filtersMap.values()), true);
    }

    return { collapse, dataView, expand };
}
