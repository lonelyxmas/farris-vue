import { TreeNodeCellMode, UseTreeDataView, UseVisualTreeNode, VisualTreeNode, VisualTreeNodeCell } from './types';
import { TreeViewProps } from '../tree-view.props';

export function useVisualTreeNode(props: TreeViewProps, treeDataViewComposition: UseTreeDataView): UseVisualTreeNode {
    const rowHeight = 28;
    const autoRowHeight = false;

    function renderTreeNode(dataItem: any, preDataItem: any, preMergingRow: VisualTreeNode, rowIndex: number, top: number): VisualTreeNode {
        const dataIndex = dataItem.__fv_data_index__;
        const currentRow: VisualTreeNode = {
            collapse: false,
            index: rowIndex,
            dataIndex,
            top,
            layer: dataItem.layer || 0,
            pre: preDataItem,
            data: {},
            setRef: (vnode) => {
                currentRow.ref = vnode;
            }
        };
        if (!preDataItem) {
            dataItem.__fv_data_position__ = 0;
        }
        if (!autoRowHeight) {
            currentRow.height = rowHeight;
            dataItem.__fv_data_height__ = currentRow.height;
            const preDataItem = currentRow.pre;
            if (preDataItem) {
                const topPosition = (preDataItem.__fv_data_position__ || 0) + (preDataItem.__fv_data_height__ || 0);
                currentRow.top = topPosition;
                dataItem.__fv_data_position__ = topPosition;
            }
        }
        const currentCell: VisualTreeNodeCell = {
            index: 0,
            field: 'name',
            mode: TreeNodeCellMode.readonly,
            data: dataItem.name,
            parent: currentRow,
            getEditor(cell: VisualTreeNodeCell) {},
            setRef: (vnode: any) => {
                if (autoRowHeight && vnode) {
                    const visualData = currentCell.parent as VisualTreeNode;
                    if (currentCell.cellHeight !== vnode.offsetHeight) {
                        currentCell.cellHeight = vnode.offsetHeight;
                    }
                    if (currentCell.cellHeight && currentCell.cellHeight > (visualData.height || 0)) {
                        visualData.height = currentCell.cellHeight;
                        dataItem.__fv_data_height__ = visualData.height;
                    }
                    const preDataItem = visualData.pre;
                    if (preDataItem) {
                        const topPosition = (preDataItem.__fv_data_position__ || 0) + (preDataItem.__fv_data_height__ || 0);
                        visualData.top = topPosition;
                        dataItem.__fv_data_position__ = topPosition;
                    }
                }
            },
            update: (value: any) => {}
        };
        currentRow.data.displayText = currentCell;
        return currentRow;
    }

    function getVisualTreeNode(pre?: any, forceToRefresh?: boolean): VisualTreeNode[] {
        const { dataView } = treeDataViewComposition;
        const visualDatas: VisualTreeNode[] = [];
        if (dataView.value.length > 0) {
            const refreshKey = forceToRefresh ? Date.now().toString() : '';
            for (let rowIndex = 0, visualIndex = 0; rowIndex < dataView.value.length; rowIndex++, visualIndex++) {
                const dataViewItem = dataView.value[rowIndex];
                const preDataItem = dataView.value[rowIndex - 1] || pre;
                const preMergingRow = visualDatas[visualIndex - 1];
                const targetPosition = preDataItem ? (preDataItem.__fv_data_position__ || 0) + (preDataItem.__fv_data_height__ || 0) : 0;
                const currentRow = renderTreeNode(dataViewItem, preDataItem, preMergingRow, rowIndex, targetPosition);
                currentRow.refreshKey = refreshKey;
                visualDatas.push(currentRow);
            }
        }
        return visualDatas;
    }

    return { getVisualTreeNode };
}
