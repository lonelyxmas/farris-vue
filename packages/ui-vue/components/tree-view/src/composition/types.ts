import { ComputedRef, Ref } from 'vue';

export enum TreeNodeCellMode {
    readonly,
    editable,
    editing
}

export interface VisualTreeNodeCell {
    actualHeight?: number;
    cellHeight?: number;
    data: any;
    getEditor: (cell: VisualTreeNodeCell) => any;
    field: string;
    index: number;
    mode: TreeNodeCellMode;
    parent: any;
    ref?: any;
    setRef: (vnode: any) => void;
    update: (value: any) => void;
}

export interface VisualTreeNode {
    collapse: boolean;
    data: Record<string, VisualTreeNodeCell>;
    dataIndex: number;
    details?: VisualTreeNode[];
    height?: number;
    index: number;
    layer: number;
    pre?: any;
    ref?: any;
    refreshKey?: string;
    setRef: (vnode: any) => void;
    top: number;
}

export interface UseVirtualScroll {
    onMouseDownScrollThumb: ($event: MouseEvent, treeContentRef: Ref<any>, thumbType: 'vertical' | 'horizontal') => void;

    horizontalScrollThumbStyle: ComputedRef<Record<string, any>>;

    verticalScrollThumbStyle: ComputedRef<Record<string, any>>;
}

export interface UseTreeDataView {
    collapse: (collapseField: string, collapseValue: any) => any[];

    dataView: Ref<any[]>;

    expand: (expandField: string, expandValue: any) => any[];
}

export interface UseVisualTreeNode {
    getVisualTreeNode: (pre?: VisualTreeNode, forceToRefresh?: boolean) => VisualTreeNode[];
}
