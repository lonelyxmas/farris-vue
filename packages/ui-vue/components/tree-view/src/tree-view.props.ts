import { ExtractPropTypes } from 'vue';

export const treeViewProps = {
    data: { type: Array<object>, default: [] }
};

export type TreeViewProps = ExtractPropTypes<typeof treeViewProps>;
