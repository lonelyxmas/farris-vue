import { Ref, computed } from 'vue';
import { TreeViewProps } from '../../tree-view.props';
import { TreeNodeCellMode, VisualTreeNode, VisualTreeNodeCell } from '../../composition/types';

export default function (props: TreeViewProps, treeContentRef: Ref<any>, visibleDatas: Ref<VisualTreeNode[]>) {
    const treeDataStyle = computed(() => {
        const styleObject = {} as Record<string, any>;
        return styleObject;
    });

    function treeNodeKey(visualTreeNode: VisualTreeNode): string {
        return `r_${visualTreeNode.index}_${visualTreeNode.refreshKey || ''}`;
    }

    function treeNodeCellKey(visualTreeNode: VisualTreeNode, cellIndex: number): string {
        return `r_${visualTreeNode.index}_c_${cellIndex}_${visualTreeNode.refreshKey || ''}`;
    }

    function treeNodeClass(visualTreeNode: VisualTreeNode): Record<string, boolean> {
        const classObject = {
            'fv-tree-node': true
        } as Record<string, boolean>;
        return classObject;
    }

    function treeNodePosition(visualTreeNode: VisualTreeNode): Record<string, any> {
        const rowHeight = visualTreeNode.height ? `${visualTreeNode.height}px` : '';
        const styleObject = {
            top: `${visualTreeNode.top}px`,
            height: `${rowHeight}`
        } as Record<string, any>;
        return styleObject;
    }

    function treeNodeCellPosition(visualTreeNodeCell: VisualTreeNodeCell): Record<string, any> {
        const cellHeight = visualTreeNodeCell.parent.height ? `${visualTreeNodeCell.parent.height}px` : '';
        const styleObject = {
            height: `${cellHeight}`,
            left: `${visualTreeNodeCell.parent.layer * 20}px`
        } as Record<string, any>;
        return styleObject;
    }

    function onClickTreeNode(e: MouseEvent, visualTreeNode: VisualTreeNode) {}

    function onMouseoverTreeNode(e: MouseEvent, visualTreeNode: VisualTreeNode) {}

    function onClickTreeNodeCell(e: MouseEvent, visualTreeNodeCell: VisualTreeNodeCell) {}

    function renderTreeNode(visualTreeNode: VisualTreeNode) {
        return (
            <div
                ref={visualTreeNode.setRef}
                key={treeNodeKey(visualTreeNode)}
                class={treeNodeClass(visualTreeNode)}
                style={treeNodePosition(visualTreeNode)}
                onClick={(payload: MouseEvent) => onClickTreeNode(payload, visualTreeNode)}
                onMouseover={(payload: MouseEvent) => onMouseoverTreeNode(payload, visualTreeNode)}>
                {Object.values(visualTreeNode.data).map((cell: VisualTreeNodeCell) => {
                    return (
                        <>
                            <div
                                ref={cell.setRef}
                                key={treeNodeCellKey(visualTreeNode, cell.index)}
                                class="fv-tree-node-cell"
                                style={treeNodeCellPosition(cell)}
                                onClick={(payload: MouseEvent) => onClickTreeNodeCell(payload, cell)}>
                                <div class="fv-tree-node-toggle"></div>
                                <span>{cell.mode === TreeNodeCellMode.editing ? cell.getEditor(cell) : cell.data}</span>
                            </div>
                        </>
                    );
                })}
            </div>
        );
    }

    function renderTreeData() {
        return visibleDatas.value.map((visualDataRow: VisualTreeNode) => {
            return renderTreeNode(visualDataRow);
        });
    }

    function renderTreeArea() {
        return (
            <div class="fv-tree-data" style={treeDataStyle.value}>
                {renderTreeData()}
            </div>
        );
    }

    return { renderTreeArea };
}
