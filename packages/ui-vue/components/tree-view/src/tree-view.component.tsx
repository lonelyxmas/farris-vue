import { SetupContext, computed, defineComponent, ref } from 'vue';
import { TreeViewProps, treeViewProps } from './tree-view.props';
import { useTreeDataView } from './composition/use-tree-data-view';

import getTreeArea from './components/data/tree-area.component';
import { useVisualTreeNode } from './composition/use-visual-tree-node';
import './tree-view.css';

export default defineComponent({
    name: 'FTreeView',
    props: treeViewProps,
    emits: [],
    setup(props: TreeViewProps, context: SetupContext) {
        const treeContentRef = ref<any>();
        const mouseInContent = ref(false);

        const treeDataView = useTreeDataView(props);

        const useVisualDataComposition = useVisualTreeNode(props, treeDataView);
        const { getVisualTreeNode } = useVisualDataComposition;

        const visibleTreeNodes = ref(getVisualTreeNode());

        const treeClass = computed(() => {
            const classObject = {
                'fv-tree': true
            } as Record<string, boolean>;
            return classObject;
        });

        const treeContentClass = computed(() => {
            const classObject = {
                'fv-tree-content': true
            } as Record<string, boolean>;
            return classObject;
        });

        const { renderTreeArea } = getTreeArea(props, treeContentRef, visibleTreeNodes);

        function onWheel(e: MouseEvent) {}

        function renderHorizontalScrollbar() {
            return [];
        }

        function renderVerticalScrollbar() {
            return [];
        }

        return () => {
            return (
                <div class={treeClass.value} style="height:600px" onWheel={onWheel}>
                    <div
                        ref={treeContentRef}
                        class={treeContentClass.value}
                        onMouseover={() => {
                            mouseInContent.value = true;
                        }}
                        onMouseleave={() => {
                            mouseInContent.value = false;
                        }}>
                        {renderTreeArea()}
                        {renderHorizontalScrollbar()}
                        {renderVerticalScrollbar()}
                    </div>
                </div>
            );
        };
    }
});
