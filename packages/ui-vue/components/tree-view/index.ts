import type { App } from 'vue';
import FTreeView from './src/tree-view.component';

export * from './src/tree-view.props';

export { FTreeView };

export default {
    install(app: App): void {
        app.component(FTreeView.name, FTreeView);
    }
};
