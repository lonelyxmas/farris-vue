import { ExtractPropTypes, PropType } from 'vue';

export type StepDirectionType = 'horizontal' | 'vertical';

export type StepStatus = 'active' | 'finish' | '';

export interface Step {
    disable: boolean;
    status: StepStatus;
    class: string;
    icon: string;
    title: string;
}

export const stepProps = {
    direction: { type: String as PropType<StepDirectionType>, default: 'horizontal' },
    fill: { type: Boolean, default: false },
    height: { type: Number, default: 0 },
    clickable: { type: Boolean, default: true },
    steps: { type: Array<Step>, default: [] }
};
export type StepProps = ExtractPropTypes<typeof stepProps>;
