/**
 * Copyright (c) 2020 - present, Inspur Genersoft Co., Ltd.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *       http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
import { ExtractPropTypes, PropType } from 'vue';

type ButtonType = 'primary' | 'warning' | 'danger' | 'success' | 'link' | 'secondary';
type SizeType = 'small' | 'large';

export const buttonProps = {
    /**
     * 组件标识
     */
    id: String,
    /**
     * 设置按钮类型
     */
    type: { type: String as PropType<ButtonType>, default: 'primary' },
    /**
     * 是否禁用
     */
    disabled: { type: Boolean, default: false },
    /**
     * 按钮尺寸
     */
    size: { type: String as PropType<SizeType>, default: 'small' }
};

export type ButtonProps = ExtractPropTypes<typeof buttonProps>;
