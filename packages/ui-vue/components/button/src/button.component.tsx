/**
 * Copyright (c) 2020 - present, Inspur Genersoft Co., Ltd.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *       http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
import { defineComponent, computed, ref } from 'vue';
import type { SetupContext } from 'vue';
import { buttonProps, ButtonProps } from './button.props';
import { useButton } from './composition/use-button';

export default defineComponent({
    name: 'FButton',
    props: buttonProps,
    emits: ['click'] as (string[] & ThisType<void>) | undefined,
    setup(props: ButtonProps, context: SetupContext): () => JSX.Element {
        const { onClickButton } = useButton(props, context);
        const size = ref(props.size);
        const type = ref(props.type);
        const disabled = ref(props.disabled);

        const buttonClass = computed(() => {
            const classObject = {
                btn: true,
                'btn-lg': size.value === 'large',
                'btn-sm': size.value !== 'large'
            } as Record<string, any>;
            if (type.value) {
                classObject[`btn-${type.value}`] = true;
            }
            return classObject;
        });
        return () => (
            <button class={buttonClass.value} disabled={disabled.value} onClick={(event: MouseEvent) => onClickButton(event)}>
                {context.slots.default && context.slots.default()}
            </button>
        );
    }
});
