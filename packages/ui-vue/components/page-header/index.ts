import { App } from 'vue';
import PageHeader from './src/page-header.component';

export * from './src/page-header.props';
export { PageHeader };

export default {
    install(app: App): void {
        app.component(PageHeader.name, PageHeader);
    }
};
