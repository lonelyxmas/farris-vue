import { ExtractPropTypes } from "vue";

export const pageHeaderProps = {
    icon: { type: String, default: '' },
    title: { type: String, default: '' },
    buttons: { type: Array<any>, default: [] }
};
export type PageHeaderProps = ExtractPropTypes<typeof pageHeaderProps>;
