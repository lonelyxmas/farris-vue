import { mount } from '@vue/test-utils';
import { Avatar } from '..';

describe('avatar', () => {
    describe('properties', () => {});
    describe('render', () => {
        it('should work', () => {
            const wrapper = mount({
                setup(props, ctx) {
                    return () => {
                        return <Avatar></Avatar>;
                    };
                }
            });
            expect(wrapper.find('div').classes('f-avatar')).toBeTruthy();
            expect(wrapper.find('div').classes('f-avatar-circle')).toBeTruthy();
        });
    });
    describe('methods', () => {});
    describe('events', () => {});
    describe('behavior', () => {
        const wrapper = mount({
            setup(props, ctx) {
                return () => {
                    return <Avatar></Avatar>;
                };
            }
        });
        wrapper.find('div').trigger('click');
    });
});
