import { ExtractPropTypes } from 'vue';

export const timePickerProps = {
    /**
     * 组件值
     */
    modelValue: { type: String, default: '' }
};

export type TimePickerProps = ExtractPropTypes<typeof timePickerProps>;
