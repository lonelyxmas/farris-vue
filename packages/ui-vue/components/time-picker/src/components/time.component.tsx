import { SetupContext, computed, defineComponent, ref, watch } from 'vue';
import { TimeProps, timeProps } from './time.props';
import { TimeHolder } from './time-holder';

export default defineComponent({
    name: 'FTimePickerTimeView',
    props: timeProps,
    emits: [],
    setup(props: TimeProps, context: SetupContext) {
        const isShownInDatePicker = ref(false);
        const prefixClass = ref('time-picker-panel');
        const enableColumns = ref(true);
        const panelWidth = ref(0);
        const hourRange = ref<{ index: number; disabled: boolean }[]>([]);
        const minuteRange = ref<{ index: number; disabled: boolean }[]>([]);
        const secondRange = ref<{ index: number; disabled: boolean }[]>([]);
        const use12HoursRange = ref<any>([]);
        const time = new TimeHolder();
        const hourStep = ref(props.hourStep);
        const minuteStep = ref(props.minuteStep);
        const secondStep = ref(props.secondStep);
        const clearText = ref(props.clearText);
        const popupClassName = ref(props.popupClassName);
        const placeholder = ref(props.placeholder);
        const defaultOpenValue = ref(props.defaultOpenValue);

        const format = ref(props.format);
        const isOpen = ref(props.isOpen);
        const use12Hours = ref(props.use12Hours);
        const value = '';
        const hideDisabledOptions = ref(props.hideDisabledOptions);

        watch(
            () => props.use12Hours,
            () => {
                if (value && format && format.value) {
                    format.value = format.value.replace(/H/g, 'h');
                } else if (!value && format.value) {
                    format.value = format.value.replace(/h/g, 'H');
                }

                if (use12Hours.value) {
                    if (!format.value) {
                        format.value = 'hh:mm:ss a';
                    } else if (format.value.indexOf('a') === -1) {
                        format.value += ' a';
                    }
                }
            }
        );

        const timePickerTimeViewClass = computed(() => {
            const classOject = {
                'time-picker-panel': true,
                'time-picker-panel-column-3': true,
                'time-picker-panel-placement-bottomLeft': true,
                'f-area-show': true
            } as Record<string, boolean>;
            return classOject;
        });
        const timePickerContainerClass = computed(() => {
            const classObject = {} as Record<string, boolean>;
            if (isShownInDatePicker.value) {
                const timePickerPanelClassKey = `${prefixClass.value}-panel`;
                classObject[timePickerPanelClassKey] = true;
            }
            return classObject;
        });

        const timePickerInnerContainerClass = computed(() => {
            const timePickerInnerContainerClassKey = `${prefixClass.value}-inner`;
            const classObject = {} as Record<string, boolean>;
            classObject[timePickerInnerContainerClassKey] = true;
            if (isShownInDatePicker.value) {
                const timePickerColumnClassKey = `${prefixClass.value}-column-${enableColumns.value}`;
                classObject[timePickerColumnClassKey] = true;
            }
            return classObject;
        });

        const timePickerComboBoxStyle = computed(() => {
            const styleObject = {
                width: use12Hours.value ? `${panelWidth.value}px` : '100%'
            } as Record<string, any>;
            return styleObject;
        });

        const showHour = computed(() => {
            return true;
        });

        const showMinute = computed(() => {
            return true;
        });

        const showSecond = computed(() => {
            return true;
        });

        function makeRange(length: number, step = 1, start = 0): number[] {
            step = Math.ceil(step);
            return new Array(Math.ceil(length / step)).fill(0).map((_, i) => (i + start) * step);
        }

        function buildHours(): void {
            let hourRanges = 24;
            let startIndex = 0;
            if (use12Hours.value) {
                hourRanges = 12;
                startIndex = 1;
            }
            hourRange.value = makeRange(hourRanges, hourStep.value, startIndex).map((r) => {
                return {
                    index: r,
                    disabled: false
                };
            });
            if (use12Hours.value && hourRange.value[hourRange.value.length - 1].index === 12) {
                const temp = [...hourRange.value];
                temp.unshift(temp[temp.length - 1]);
                temp.splice(temp.length - 1, 1);
                hourRange.value = temp;
            }

            // 移除禁用的值
            hourRange.value = hourRange.value.filter((n) => {
                return !(hideDisabledOptions.value && n.disabled);
            });
        }

        function buildMinutes(): void {
            minuteRange.value = makeRange(60, minuteStep.value).map((r) => {
                return {
                    index: r,
                    disabled: false
                };
            });

            // 移除禁用的值
            minuteRange.value = minuteRange.value.filter((n) => {
                return !(hideDisabledOptions.value && n.disabled);
            });
        }

        function buildSeconds(): void {
            secondRange.value = makeRange(60, secondStep.value).map((r) => {
                return {
                    index: r,
                    disabled: false
                };
            });

            // 移除禁用的值
            secondRange.value = secondRange.value.filter((n) => {
                return !(hideDisabledOptions.value && n.disabled);
            });
        }

        function build12Hours(): void {
            const isUpperForamt = format.value.includes('A');
            use12HoursRange.value = [
                {
                    index: 0,
                    value: isUpperForamt ? 'AM' : 'am'
                },
                {
                    index: 1,
                    value: isUpperForamt ? 'PM' : 'pm'
                }
            ];

            // 移除禁用的值
            use12HoursRange.value = use12HoursRange.value.filter(() => {
                return !hideDisabledOptions.value;
            });
        }

        buildHours();
        buildMinutes();
        buildSeconds();
        build12Hours();

        function getHMSClass(hour: any, format: string) {}
        function selectHour(event: MouseEvent, hour: any) {}
        function select12Hour(event: MouseEvent, hour: any) {}
        function selectMinute(event: MouseEvent, minute: any) {}
        function selectSecond(event: MouseEvent, second: any) {}

        return () => {
            return (
                <div class={timePickerTimeViewClass.value}>
                    <div class={timePickerContainerClass.value}>
                        <div class={timePickerInnerContainerClass.value}>
                            <div class={prefixClass.value + '-combobox'} style={timePickerComboBoxStyle.value}>
                                {showHour.value && (
                                    <div class={prefixClass.value + '-select'}>
                                        <ul>
                                            {hourRange.value.map((hour: any) => {
                                                return (
                                                    <li
                                                        class={getHMSClass(hour, 'h')}
                                                        onClick={(payload: MouseEvent) => selectHour(payload, hour)}>
                                                        {`${hour.index}`}
                                                    </li>
                                                );
                                            })}
                                        </ul>
                                    </div>
                                )}
                                {showMinute.value && (
                                    <div class={prefixClass.value + '-select'}>
                                        <ul>
                                            {minuteRange.value.map((minute: any) => {
                                                return (
                                                    <li
                                                        class={getHMSClass(minute, 'm')}
                                                        onClick={(payload: MouseEvent) => selectMinute(payload, minute)}>
                                                        {`${minute.index}`}
                                                    </li>
                                                );
                                            })}
                                        </ul>
                                    </div>
                                )}
                                {showSecond.value && (
                                    <div class={prefixClass.value + '-select'}>
                                        <ul>
                                            {secondRange.value.map((second: any) => {
                                                return (
                                                    <li
                                                        class={getHMSClass(second, 's')}
                                                        onClick={(payload: MouseEvent) => selectSecond(payload, second)}>
                                                        {`${second.index}`}
                                                    </li>
                                                );
                                            })}
                                        </ul>
                                    </div>
                                )}
                                {use12Hours.value && (
                                    <div class={prefixClass.value + '-select'}>
                                        <ul>
                                            {use12HoursRange.value.map((hour: any) => {
                                                return (
                                                    <li
                                                        class={getHMSClass(hour, 'r')}
                                                        onClick={(payload: MouseEvent) => select12Hour(payload, hour)}>
                                                        {`${hour.index}`}
                                                    </li>
                                                );
                                            })}
                                        </ul>
                                    </div>
                                )}
                            </div>
                        </div>
                    </div>
                </div>
            );
        };
    }
});
