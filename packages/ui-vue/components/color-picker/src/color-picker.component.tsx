/* eslint-disable dot-notation */
/* eslint-disable no-use-before-define */
/* eslint-disable prefer-destructuring */
/**
 * Copyright (c) 2020 - present, Inspur Genersoft Co., Ltd.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *       http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
import { defineComponent, SetupContext, ref, computed, watch,onMounted, toRefs } from 'vue';
import { colorPickerProps, ColorPickerProps } from './color-picker.props';
import { ColorPickerControl, ColorType } from './composition/class/control.class';
import { getValueByType } from './composition/class/helper.functions';
import { Color } from './composition/class/color.class';
import SvPanel from './composition/component/sv-panel';
import Hue from './composition/component/hue';
import Alpha from './composition/component/alpha';
import Preset from './composition/component/preset';
import './composition/component/scss/color-picker.scss';

export default defineComponent({
    name: 'FColorPicker',
    props: colorPickerProps,
    emits: ['valueChanged','activeChange'] as (string[]) | undefined,
    setup(props: ColorPickerProps, context: SetupContext) {
        const { color } = toRefs( props );
        /** new ColorPickerControl('#000000') */
        let control = new ColorPickerControl('#000000');
        /** 用户传入的自定义内置颜色 */
        const presets = ref(props.presets);
        control.setColorPresets(presets.value);
        /** 响应式new Color(props.color) */
        const colorObject = ref(new Color(props.color));
        /** 当前颜色 */
        const colorRef = ref(props.color);
        /** 颜色种类：hex或rgba */
        let _colorFormat: ColorType;
        /** 是否展示颜色选择器panel */
        const isShowPanel = ref(false);
        /** 是否展示下方透明度点击条 */
        const showAlpha = ref(false);
        /** 是否禁用该组件 */
        const disabled = ref(props.disabled);
        /** 上一个颜色 */
        const preColor = ref(props.color);
        /** 展示HEX或RGBA */
        const presetColorTypeText = ref('hex');
        /** 索引值：用来计算当前展示HEX或RGBA */
        let currentColorTypeIndex = -1;

        let inputValue: any;
        const trigger = ref<HTMLDivElement | null>(null);
        const elementRef = ref<HTMLDivElement | null>(null);
        const pickerPanelRef = ref<HTMLDivElement | null>(null);

        /** _colorFormat: hex/rgba */
        const colorFormat = computed(() => {
            if (!_colorFormat) {
                if (control && control.initType) {
                    return control.initType;
                }
            }
            return _colorFormat;
        });

        /** 当前颜色 */
        const currentColor = computed({
            get(){
                if(_colorFormat)
                {
                    findColorType(_colorFormat);
                    return getValueByType(new Color (colorRef.value), ColorType[_colorFormat]);
                }
                return colorRef.value;
            },
            set(newColor: string){
                if(control){
                    control.setValueFrom(newColor);
                }
            }
        });

        const presetsLength = computed(() => {
            if (currentColorTypeIndex > -1) {
                return true;
            }
            return false;
        });

        /** 是否展示下方的自定义颜色preset */
        const ifCurrentColorTypeIndex = computed(() => {
            if (props.presets.length > 0) {
                return true;
            }
            return false;
        });

        const colorTriggerStyle = computed(()=>{
            const styleObject = {
                'background-color':preColor.value,
            } as Record <string,any>;
            return styleObject;
        });

        const ifShowAplha = computed(()=>{
            const hiddenStyle = showAlpha.value && isShowPanel.value? 'visibility:visible' : 'visibility:hidden';
            return hiddenStyle;
        });

        const presetColorType = [
            {
                text: 'HEX',
                type: ColorType.hex
            },
            {
                text: 'RGBA',
                type: ColorType.rgba
            }
        ];

        onMounted(() => {
            if (props.elementConfig.editorParams) {
                colorRef.value = props.elementConfig.editorParams.preColor;
                presets.value = props.elementConfig.editorParams.presets;
            }
            if (!control) {
                presets.value = presets.value || ['#eaecf3'];
                control = new ColorPickerControl(presets);
            }
            if (colorRef.value) {
                control.setValueFrom(colorRef.value);
            }
            if (!control.hasPresets()) {
                control.setColorPresets(presets.value);
            }
            showAlpha.value =  !!((colorFormat.value && colorFormat.value.indexOf('a') > 0
            || control.initType && control.initType.indexOf('a') > 0));
            currentColorTypeIndex = findColorType(control.initType);
            presetColorTypeText.value = presetColorType[currentColorTypeIndex].text; 
            inputValue = currentColor.value;
        });

        watch (presetColorTypeText,()=>{
            control.valueChanges.subscribe(value => {
                const newColorValue = _colorFormat ?
                    getValueByType(value, ColorType[_colorFormat]) :  getValueByType(value, ColorType[control.initType]);
                colorRef.value = newColorValue;
                context.emit("activeChange", newColorValue);
            });
            currentColor.value =  _colorFormat ?
                getValueByType(new Color(colorRef.value), ColorType[_colorFormat]):
                getValueByType(new Color(colorRef.value), ColorType[control.initType]);
            showAlpha.value = !!((colorFormat.value && colorFormat.value.indexOf('a') > 0
             || control.initType && control.initType.indexOf('a') > 0));
        });

        watch(colorRef, (newValue) => {
            if (colorRef.value && control && getValueByType(control.value, control.initType) !== newValue)
            { control.setValueFrom(newValue);}
            currentColorTypeIndex = findColorType(control.initType);
            presetColorTypeText.value = presetColorType[currentColorTypeIndex].text;
        });

        watch(colorObject, (newColor) => {
            const newColorValue = _colorFormat ?
                getValueByType(newColor as Color, ColorType[_colorFormat]) : getValueByType(newColor as Color, ColorType[control.initType]);
            colorRef.value = newColorValue;
            control.setValueFrom(newColorValue);
            context.emit('activeChange', newColorValue);
        });
        watch(color, (newColor) => {
            currentColor.value = newColor;
            inputValue = currentColor.value;
            commit();
        });
        
        function commit(): void {
            trigger.value?.style?.setProperty('background', colorRef.value);
            preColor.value = colorRef.value;
            isShowPanel.value = false;
            const outputValue = {
                elementValue: preColor,
            };
            inputValue = preColor.value;
            context.emit('valueChanged',outputValue.elementValue.value);
        }

        function toggleShowPanel(e: any): void {
            e.stopPropagation();
            isShowPanel.value = true;
            if (disabled.value) { return; }
            const hsva = colorObject.value.getHsva();
        }

        /** 找到颜色对应分类，如：HEX.RGBA */
        function findColorType(typeStr: any) {
            const findTypeIndex = presetColorType.findIndex(item => item.type === typeStr);
            currentColorTypeIndex = findTypeIndex;
            return findTypeIndex;
        }

        /** 点击上下箭头后，切换HEX和RGBA */
        function changeColorFormatByIcon (direction: string) {
            const total = presetColorType.length;
            currentColorTypeIndex = (total + currentColorTypeIndex + (direction === 'up' ? 1 : -1)) % total;
            _colorFormat = presetColorType[currentColorTypeIndex].type;
            presetColorTypeText.value =  presetColorType[currentColorTypeIndex].text;
        };

        function closeColorPanel() {
        }

        return () => {
            return (
                <div class="f-color-picker-component" ref={elementRef}>
                    <div
                        class={['color-picker-panel',
                            { disabled: !isShowPanel.value }
                        ]}
                        id="farris-color-picker-panel"
                        ref={pickerPanelRef}
                        onClick={(event)=>event.stopPropagation}
                        style="top:30px"
                    >
                        <div class="color-picker-main">
                            <Hue hue={control.hue} v-model:color={colorObject.value}></Hue>
                            <SvPanel hue={control.hue} v-model:color={colorObject.value} ></SvPanel>
                        </div>
                        <Alpha v-model:color={colorObject.value} style={ifShowAplha.value}></Alpha>

                        <div class="input-btn" style="display:flex;align-items: center;">
                            <span v-show={ifCurrentColorTypeIndex} style="width:40px;margin-right:10px;">
                                { presetColorTypeText.value }
                            </span>
                            <input type="text" id="farris-color-picker-plus-input" v-model={currentColor.value} style="float:none" />
                            <div class="type-icon-btn-wrapper" style="cursor:pointer" v-show={ifCurrentColorTypeIndex}>
                                <span class="f-icon f-icon-arrow-60-up type-icon-btn"
                                    onClick={()=>changeColorFormatByIcon('up')}></span>
                                <span class="f-icon f-icon-arrow-60-down  type-icon-btn"
                                    onClick={()=>changeColorFormatByIcon('down')}></span>
                            </div >
                            <div class="input-btn">
                                <button id="farris-color-picker-plus-sure" class="btn btn-secondary" onClick={commit}>确定</button>
                            </div>
                        </div >
                        <Preset
                            v-model:color={colorObject.value}
                            colorPresets={control.presets}
                            hue={control.hue}
                            v-show={presetsLength}
                        >
                        </Preset>
                    </div >
                    <div class="color-picker-wrapper">
                        <input
                            type="text"
                            class="color-picker-input form-control"
                            value={ inputValue }
                            readonly={ props.editable || props.disabled }
                            onClick={(event) => event.stopPropagation()}
                            onKeydown={(event: any) => {
                                if (event.key === 'Tab') { closeColorPanel(); }
                            }}
                        />
                        <div class="color-picker-trigger" onClick={toggleShowPanel}>
                            <div class='color-picker-trigger-inner'>
                                <div style={colorTriggerStyle.value}></div>
                            </div>
                        </div>
                    </div>
                </div>
            );
        };
    }
});
