import { ExtractPropTypes } from 'vue';

export const presetProps = {
    color: { type: Object, default: '' },

    hue: { type: Object, default: '' },

    colorPresets:{ type: Object, default: '' },

};
export type PresetProps = ExtractPropTypes<typeof presetProps>;
