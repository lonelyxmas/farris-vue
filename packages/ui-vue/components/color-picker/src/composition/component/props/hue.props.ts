import { ExtractPropTypes } from 'vue';

export const hueProps = {
    color: { type: Object, default: '' },
    hue: { type: Object, default: '' },

};
export type HueProps = ExtractPropTypes<typeof hueProps>;
