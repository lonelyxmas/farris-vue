import { ExtractPropTypes } from 'vue';

export const alphaProps = {
    color: { type: Object, default: '' },
};
export type AlphaProps = ExtractPropTypes<typeof alphaProps>;
