import { ExtractPropTypes } from 'vue';

export const svPanelProps = {
    color: { type: Object, default: '' },

    hue: { type: Object, default: '' },

};
export type SvPanelProps = ExtractPropTypes<typeof svPanelProps>;
