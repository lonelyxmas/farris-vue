/* eslint-disable no-use-before-define */
import { defineComponent, ref, withModifiers } from 'vue';
import { Color } from '../class/color.class';
import './scss/alpha.scss';
import { alphaProps, AlphaProps } from './props/alpha.props';

export default defineComponent({
    name: 'Alpha',
    props: alphaProps,
    emits: ['update:color'] as (string[]) | undefined,
    setup(props: AlphaProps, context) {
        const elementRef = ref<HTMLDivElement | null>(null);
        const thumbRef = ref<HTMLDivElement | null>(null);

        const colorRef = ref(props.color);

        const changePointerPosition = (alpha: number) => {
            const x = Math.max(0, Math.min(alpha * 100, 100));
            const orientation = 'left';
            if (thumbRef.value) {
                const thumbRefValue = thumbRef.value;
                thumbRefValue.style?.setProperty(orientation, `${x}%`);
            }
        };

        class clickValue {
            x = 0;

            y = 0;

            height = 0;

            width = 0;
        }
        function calculateCoordinates(event: MouseEvent | TouchEvent): void {
            if (elementRef.value) {
                const {
                    width: elWidth,
                    height: elHeight,
                    top: elTop,
                    left: elLeft
                } = elementRef.value.getBoundingClientRect();

                const { pageX, pageY } = 'touches' in event ? event.touches[0] : event;

                const x = Math.max(0, Math.min(pageX - (elLeft + window.pageXOffset), elWidth));
                const y = Math.max(0, Math.min(pageY - (elTop + window.pageYOffset), elHeight));

                movePointer({ x, y, height: elHeight, width: elWidth });
            }

        }

        const movePointer = ({ x, width }: clickValue) => {
            const alpha = x / width;
            changePointerPosition(alpha);

            const hsva = colorRef.value.getHsva();
            const newColor = new Color().setHsva(hsva.hue, hsva.saturation, hsva.value, alpha);
            context.emit('update:color', newColor);
        };

        return () => (
            <div class="f-alpha-component" ref={elementRef}
                onMousedown={withModifiers((payload: MouseEvent) => calculateCoordinates(payload), ['prevent'])}
                onTouchstart={withModifiers((payload: MouseEvent) => calculateCoordinates(payload), ['prevent'])}
            >
                <div class="color-alpha-slider__bar" > </div>
                < div class="color-alpha-slider__thumb" ref={thumbRef}> </div>
            </div>
        );
    },
});
