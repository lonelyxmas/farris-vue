/**
 * Copyright (c) 2020 - present, Inspur Genersoft Co., Ltd.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *       http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
import { computed, defineComponent, provide, ref, SetupContext, onMounted, shallowRef, nextTick } from 'vue';
import { TabsProps, tabsProps } from './tabs.props';
import './tabs.css';
import { useTabs } from './composition/use-tabs';
import { TabContext, TabsContext } from './composition/types';
import { TabPageProps } from './components/tab-page.props';

export default defineComponent({
    name: 'FTabs',
    props: tabsProps,
    emits: ['tabChange', 'tabRemove'] as (string[] & ThisType<void>) | undefined,
    setup(props: TabsProps, context: SetupContext) {
        const tabsElement = shallowRef<any>();
        // 标题Ul元素
        const titleUlElement = shallowRef<any>();
        // 显示左右滚动按钮
        const hideButtons = ref(true);
        // 显示下拉面板
        const hideDropDown = ref(true);
        // 存储tab的props数组
        const tabs = ref<TabContext[]>([]);
        // 激活状态的tabId
        const activeId = ref(props.activeId);

        const activeTabSlot = computed(() => {
            const activeTab = tabs.value.find((tab) => tab.props.id === activeId.value);
            return activeTab?.slots;
        });

        const hasInHeadClass = computed(() => {
            const activeTab = tabs.value.find((tab) => tab.props.id === activeId.value);
            const toolbarPosition = activeTab?.props.toolbarPosition;
            if (toolbarPosition === 'inHead') {
                return true;
            }
            return false;
        });

        const menuItemsWidth = ref<string>('atuo');
        // 下拉框后的搜索文本
        const searchTabText = ref<string>('');

        const { setActiveId, getTabStyle, getTabClass, getTabNavLinkClass, getTabTextClass, changeTitleStyle, selectTabByTabId } = useTabs(
            props,
            context,
            activeId
        );
        // 设置页签下拉按钮的显隐状态
        const setHideButtonsStatus = () => {
            const titleUlparentElement = titleUlElement.value?.parentElement;
            if (!titleUlparentElement || !titleUlElement.value) {
                return;
            }
            if (titleUlparentElement.offsetWidth < titleUlElement.value.scrollWidth) {
                hideButtons.value = false;
            } else {
                hideButtons.value = true;
            }
        };
        const stopBubble = ($event: MouseEvent) => {
            $event.preventDefault();
            $event.stopPropagation();
        };
        /**
         * 按照方向，滚动scrollStep距离
         * @param direction 方向
         */
        const scrollTab = (move: number, direction: number) => {
            if (!titleUlElement.value) {
                return;
            }
            const distScrollLeft = titleUlElement.value.scrollLeft;
            const maxScrollLeft = titleUlElement.value.scrollWidth - titleUlElement.value.offsetWidth;
            // 标签页左侧移动
            if (direction > 0) {
                if (titleUlElement.value.scrollLeft >= maxScrollLeft) {
                    return;
                }
                titleUlElement.value.scrollLeft = distScrollLeft + props.scrollStep + move;
            } else if (direction < 0) {
                // 标签页右侧移动
                if (titleUlElement.value.scrollLeft <= 0) {
                    return;
                }
                titleUlElement.value.scrollLeft = distScrollLeft - props.scrollStep - move;
            }
        };
        /**
         * 定位到某个tab
         * @param activeTableIndex 索引
         * @returns
         */
        const scrollToActiveTab = (activeTableIndex: number) => {
            if (hideButtons.value || !titleUlElement.value) {
                return;
            }
            const tabs = titleUlElement.value.querySelectorAll('.nav-item');
            const { parentElement } = titleUlElement.value;
            const scrollTabElement = tabs[activeTableIndex];
            if (scrollTabElement && parentElement) {
                const parentElementLeft = parentElement.getBoundingClientRect().left;
                const parentElementRight = parentElement.getBoundingClientRect().right;
                const activeTabLeft = scrollTabElement.getBoundingClientRect().left;
                const activeTabRight = scrollTabElement.getBoundingClientRect().right;
                if (activeTabLeft < parentElementLeft) {
                    scrollTab(parentElementLeft - activeTabLeft, -1);
                } else if (parentElementRight < activeTabRight) {
                    scrollTab(activeTabRight - parentElementRight, 1);
                }
            }
        };
        // 增加一个tab标题
        const addTab = (tabContext: TabContext) => {
            const index = tabs.value.findIndex((tab) => tab.props.id === tabContext.props.id);
            if (index === -1) {
                tabs.value.push(tabContext);
            }
            nextTick(() => {
                setHideButtonsStatus();
            });
        };
        // 更新tab标题
        const updateTab = (tabContext: TabContext) => {
            const index = tabs.value.findIndex((tab) => tab.props.id === tabContext.props.id);
            if (index === -1) {
                return;
            }
            tabs.value.forEach((tab) => {
                if (tab.props.id === tabContext.props.id) {
                    tab = tabContext;
                }
            });
            nextTick(() => {
                changeTitleStyle(titleUlElement);
                setHideButtonsStatus();
            });
        };
        // 移除tab标题
        function removeTab($event: MouseEvent, targetTabId: string, closeDropdown = false) {
            const index = tabs.value.findIndex((tab) => tab.props.id === targetTabId);
            tabs.value = tabs.value.filter((tab) => tab.props.id !== targetTabId);
            if (activeId.value === targetTabId) {
                activeId.value = '';
                setActiveId(tabs);
            }
            stopBubble($event);
            nextTick(() => {
                changeTitleStyle(titleUlElement);
                setHideButtonsStatus();
                if (closeDropdown) {
                    hideDropDown.value = true;
                }
                const index = tabs.value.findIndex((tab) => tab.props.id === activeId.value);
                scrollToActiveTab(index);
                context.emit('tabRemove', {
                    removeIndex: index,
                    removeId: targetTabId,
                    activeId: activeId.value
                });
            });
        }
        // 提供者tabs，供增加、修改tab标题用
        provide<TabsContext>('tabs', { activeId, addTab, updateTab, tabs });
        // 填充模式
        const shouldShowNavFill = computed(() => {
            return props.fill || props.tabType === 'fill';
        });
        // 药片模式
        const shouldShowNavPills = computed(() => {
            return props.tabType === 'pills';
        });

        const tabsHeaderClass = computed(() => ({
            'farris-tabs-header': true,
            'farris-tabs-inHead': hasInHeadClass.value,
            'farris-tabs-inContent': !hasInHeadClass.value,
            'farris-tabs-nav-fill': shouldShowNavFill.value,
            'farris-tabs-nav-pills': shouldShowNavPills.value
        }));

        const tabsTitleStyle = computed(() => ({
            width: hasInHeadClass.value ? (props.titleWidth ? `${props.titleWidth}%` : '') : ''
        }));

        const leftArrowButtonClass = computed(() => ({
            btn: true,
            'sc-nav-btn': true,
            'px-1': true,
            'sc-nav-lr': true,
            'd-none': hideButtons.value
        }));

        const dropdownTitleContainerCls = computed(() => ({
            'btn-group': true,
            'sc-nav-btn': true,
            dropdown: true,
            'd-none': hideButtons.value
        }));

        const dropdownMenuCls = computed(() => ({
            'dropdown-menu': true,
            'tabs-pt28': props.searchBoxVisible,
            show: !hideDropDown.value
        }));

        const rightArrowButtonClass = computed(() => ({
            btn: true,
            'sc-nav-rg': true,
            'd-none': hideButtons.value
        }));

        const getMenuItemsWidth = () => {
            return { width: menuItemsWidth.value };
        };

        const getDropdownItemCls = (tab: TabContext) => {
            return {
                'dropdown-item': true,
                'text-truncate': true,
                'px-2': true,
                disabled: tab.props.disabled,
                active: tab.props.id === activeId.value,
                'd-none': tab.props.show !== true
            };
        };

        // 选中某个tab
        const _cpSelectTab = ($event: MouseEvent, tab: TabPageProps) => {
            if (tab.disabled) {
                return;
            }
            selectTabByTabId($event, tab.id);
            const index = tabs.value.findIndex((tb) => tb.props.id === tab.id);
            hideDropDown.value = true;
            nextTick(() => {
                setHideButtonsStatus();
                scrollToActiveTab(index);
            });
        };
        // 下拉框使用的tabs，包括搜索
        const cpTabs = computed(() => {
            let _cpTabs: TabContext[] = [];
            if (props.searchBoxVisible) {
                _cpTabs = tabs.value?.filter((tab) => tab.props.title.includes(searchTabText.value));
            } else {
                _cpTabs = tabs.value?.slice();
            }
            return _cpTabs;
        });

        const tabParentClass = computed(() => ({
            spacer: true,
            'f-utils-fill': true,
            // 'spacer-sides': !hideButtons.value && hideDropDown.value,
            'spacer-sides-dropdown': !hideButtons.value
        }));

        const tabContainerClass = computed(() => ({
            nav: true,
            'farris-nav-tabs': true,
            'flex-nowrap': true,
            'nav-fill': props.fill || props.tabType === 'fill',
            'nav-pills': props.tabType === 'pills',
            'flex-row': props.position === 'top' || props.position === 'bottom',
            'flex-column': props.position === 'left' || props.position === 'right'
        }));

        const tabsContainerClass = computed(() => ({
            'farris-tabs': true,
            'flex-column': props.position === 'top',
            'flex-column-reverse': props.position === 'bottom',
            'flex-row': props.position === 'left',
            'flex-row-reverse': props.position === 'right'
        }));

        const selectTab = ($event: MouseEvent, targetTabId: string) => {
            selectTabByTabId($event, targetTabId);
            nextTick(() => {
                setHideButtonsStatus();
            });
        };

        onMounted(() => {
            nextTick(() => {
                changeTitleStyle(titleUlElement);
                // 下拉面板之外空白处点击关闭下拉面板
                window.addEventListener('click', (ev: any) => {
                    if (hideDropDown.value) {
                        return;
                    }
                    if (!tabsElement.value?.contains(ev.target)) {
                        hideDropDown.value = true;
                    }
                });
                window.addEventListener('resize', (ev: any) => {
                    setHideButtonsStatus();
                });
            });
        });

        return () => {
            return (
                <>
                    <div class={tabsContainerClass.value} ref={tabsElement}>
                        <div class={tabsHeaderClass.value}>
                            <div class="farris-tabs-title scroll-tabs" style={tabsTitleStyle.value}>
                                <button
                                    title="left-arrow-button"
                                    type="button"
                                    class={leftArrowButtonClass.value}
                                    onClick={() => {
                                        scrollTab(0, -1);
                                    }}></button>
                                <div class={tabParentClass.value} style="width:100%">
                                    <ul class={tabContainerClass.value} ref={titleUlElement}>
                                        {tabs.value.map((tabPage: any) => {
                                            const tab = tabPage.props;
                                            const titleSlot = tabPage.slots.title;
                                            return (
                                                <li class={getTabClass(tab)} style={getTabStyle(tab)}>
                                                    <a class={getTabNavLinkClass(tab)} onClick={($event) => selectTab($event, tab.id)}>
                                                        {titleSlot ? (
                                                            titleSlot()
                                                        ) : (
                                                            <span class={getTabTextClass(tab)} title={tab.title}>
                                                                {tab.title}
                                                            </span>
                                                        )}
                                                        {tab.removeable && (
                                                            <span class="st-drop-close" onClick={($event) => removeTab($event, tab.id)}>
                                                                <i class="f-icon f-icon-close"></i>
                                                            </span>
                                                        )}
                                                    </a>
                                                </li>
                                            );
                                        })}
                                    </ul>
                                </div>
                                <div class={dropdownTitleContainerCls.value}>
                                    <button
                                        title="right-arrow-button"
                                        type="button"
                                        class={rightArrowButtonClass.value}
                                        onClick={() => {
                                            scrollTab(0, 1);
                                        }}></button>
                                    <button
                                        title="toggle-button"
                                        class="btn dropdown-toggle-split dropdown-toggle"
                                        onClick={() => {
                                            hideDropDown.value = false;
                                        }}></button>
                                    <div class={dropdownMenuCls.value}>
                                        {props.searchBoxVisible && (
                                            <div onClick={($event) => stopBubble($event)} class="pb-1 tabs-li-absolute">
                                                <input
                                                    title="search-box"
                                                    type="text"
                                                    class="form-control k-textbox"
                                                    v-model={searchTabText.value}
                                                />
                                                <span class="f-icon f-icon-page-title-query tabs-icon-search"></span>
                                            </div>
                                        )}
                                        {cpTabs.value.length ? (
                                            <ul class="tab-dropdown-menu--items" style={getMenuItemsWidth()}>
                                                {cpTabs.value.map((tab) => {
                                                    return (
                                                        <li
                                                            class={getDropdownItemCls(tab)}
                                                            onClick={($event) => _cpSelectTab($event, tab.props)}>
                                                            {tab.props.removeable && (
                                                                <span
                                                                    class="float-right st-drop-close"
                                                                    onClick={($event) => removeTab($event, tab.props.id, true)}>
                                                                    <i class="f-icon f-icon-close"></i>
                                                                </span>
                                                            )}
                                                            <a class="dropdown-title">{tab.props.title}</a>
                                                        </li>
                                                    );
                                                })}
                                            </ul>
                                        ) : (
                                            <div class="dropdown-no-data">没有相关数据</div>
                                        )}
                                    </div>
                                </div>
                            </div>
                            {activeTabSlot.value && <div class="farris-tabs-toolbar">{activeTabSlot.value?.toolbarExtra?.()}</div>}
                        </div>
                        <div class="farris-tabs-content">{context.slots.default?.()}</div>
                    </div>
                </>
            );
        };
    }
});
