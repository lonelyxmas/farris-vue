/**
 * Copyright (c) 2020 - present, Inspur Genersoft Co., Ltd.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *       http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
import { Ref, Slots } from 'vue';
import { TabPageProps } from '../components/tab-page.props';

export interface TabContext {
    props: TabPageProps;
    slots: Slots;
}
export interface TabsContext {
    activeId: Ref<string | undefined>;
    addTab(props: TabContext): void;
    updateTab(props: TabContext): void;
    tabs: Ref<TabContext[]>;
}

export type ToolbarPosition = 'inHead' | 'inContent';
export interface ToolbarContentsConf {
    id: string;
    disable: boolean;
    title: string;
    click: () => void;
    appearance: object;
}
