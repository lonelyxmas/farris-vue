/**
 * Copyright (c) 2020 - present, Inspur Genersoft Co., Ltd.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *       http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
import { Ref, SetupContext, ShallowRef } from 'vue';
import { TabsProps } from '../tabs.props';
import { TabPageProps } from '../components/tab-page.props';
import { TabContext } from './types';

export function useTabs(props: TabsProps, context: SetupContext, activeId: Ref<string | undefined>): any {
    function setActiveId(tabs: Ref<TabContext[]>) {
        const index = tabs.value.findIndex((tab) => tab.props.show !== false && !activeId.value && !tab.props.disabled);
        if (!activeId.value && index !== -1) {
            activeId.value = tabs.value[index].props.id;
        }
    }
    function getTabStyle(tab: TabPageProps) {
        return { width: `${tab.tabWidth}px` };
    }
    function getTabClass(tab: TabPageProps) {
        return {
            'nav-item': true,
            'd-none': tab.show !== undefined ? !tab.show : false,
            'f-state-active': tab.id === activeId.value,
            'f-state-disable': tab.disabled
        };
    }
    function getTabNavLinkClass(tab: TabPageProps) {
        return {
            'nav-link': true,
            'tabs-text-truncate': true,
            active: tab.id === activeId.value,
            disabled: tab.disabled
        };
    }
    function getTabTextClass(tab: TabPageProps) {
        return {
            'st-tab-text': true,
            'farris-title-auto': props.autoTitleWidth
        };
    }
    function changeTitleStyle(titleUlElement: ShallowRef<any>) {
        if (props.autoTitleWidth) {
            return;
        }
        const $textEls = titleUlElement.value?.querySelectorAll('.st-tab-text');
        if (!$textEls) {
            return;
        }
        for (let k = 0; k < $textEls.length; k++) {
            const parentEl = $textEls[k].parentNode as any;
            if ($textEls[k].scrollWidth > parentEl.offsetWidth) {
                if (!$textEls[k].classList.contains('farris-title-text-custom')) {
                    $textEls[k].classList.add('farris-title-text-custom');
                }
            } else {
                $textEls[k].classList.remove('farris-title-text-custom');
            }
        }
    }
    function selectTabByTabId($event: MouseEvent, targetTabId: string) {
        const prevId = activeId.value;
        activeId.value = targetTabId;
        context.emit('tabChange', {
            prevId,
            nextId: activeId.value
        });
        $event.preventDefault();
        $event.stopPropagation();
    }

    return {
        setActiveId,
        getTabStyle,
        getTabClass,
        getTabNavLinkClass,
        getTabTextClass,
        changeTitleStyle,
        selectTabByTabId
    };
}
