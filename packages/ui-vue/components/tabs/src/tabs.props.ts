/**
 * Copyright (c) 2020 - present, Inspur Genersoft Co., Ltd.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *       http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
import { ExtractPropTypes, PropType } from 'vue';

export type TabType = 'fill' | 'pills' | 'default';
export type TabPosition = 'left' | 'right' | 'top' | 'bottom';

export const tabsProps = {
    tabType: { type: String as PropType<TabType>, default: 'default' },
    autoTitleWidth: { type: Boolean, default: false },
    titleLength: { type: Number, default: 7 },
    position: { type: String as PropType<TabPosition>, default: 'top' },
    showDropDwon: { type: Boolean, default: false },
    showTooltips: { type: Boolean, default: false },
    scrollStep: { type: Number, default: 10 },
    autoResize: { type: Boolean, default: false },
    closeable: { type: Boolean, default: false },
    selectedTab: { type: String, default: '' },
    width: { type: Number },
    height: { type: Number },
    searchBoxVisible: { type: Boolean, default: true },
    titleWidth: { type: Number, default: 0 },
    customClass: { type: String, default: '' },
    activeId: { type: String },
    fill: { type: Boolean, default: false }
};

export type TabsProps = ExtractPropTypes<typeof tabsProps>;
