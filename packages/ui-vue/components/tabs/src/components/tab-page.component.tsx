/**
 * Copyright (c) 2020 - present, Inspur Genersoft Co., Ltd.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *       http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
import { defineComponent, getCurrentInstance, inject, onMounted, SetupContext, watch, ref, onUnmounted } from 'vue';
import { TabContext, TabsContext } from '../composition/types';
import { TabPageProps, tabPageProps } from './tab-page.props';

export default defineComponent({
    name: 'FTabPage',
    props: tabPageProps,
    emits: [] as (string[] & ThisType<void>) | undefined,
    setup(props: TabPageProps, context: SetupContext) {
        const tabs = inject<TabsContext>('tabs');
        const instance = getCurrentInstance();
        const showContent = ref(true);
        const tableContext: TabContext = {
            slots: context.slots,
            props
        };
        onMounted(() => {
            const index = tabs?.tabs.value.findIndex((tab) => tab.props.id === props.id);
            if (!index || index === -1) {
                tabs?.addTab(tableContext);
            } else if (index > -1) {
                showContent.value = false;
                console.warn(`已经存在id为${props.id}的页签啦`);
            }
        });
        onUnmounted(() => {});
        function getTabPageStyle() {
            return {
                display: props?.id === tabs?.activeId.value ? '' : 'none'
            };
        }
        watch(
            () => props,
            (value) => {
                tabs?.updateTab({
                    props: value,
                    slots: context.slots
                });
            },
            {
                immediate: true,
                deep: true
            }
        );
        return () => {
            const content = context.slots.default?.() as any;
            return showContent.value ? (
                <div class="farris-tab-page" style={getTabPageStyle()}>
                    {content}
                </div>
            ) : null;
        };
    }
});
