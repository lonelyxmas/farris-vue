import { ExtractPropTypes, PropType } from 'vue';

export const verifyDetailProps = {
    /** 追加结果提示模板 */
    resultTipsTmpl: { type: null as unknown as PropType<any>, default: null },

    /** 表单验证默认显示的分组信息 */
    showType: { type: String, default: '' },

    /** 是否默认显示验证信息列表 */
    showList: { type: Boolean, default: false },

    /** 表单验证列表 */
    verifyList: { type: Object },

    verifyType: { type: Object },
};

export type VerifyDetailProps = ExtractPropTypes<typeof verifyDetailProps>;
