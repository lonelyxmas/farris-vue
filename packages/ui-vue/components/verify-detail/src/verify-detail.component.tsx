/* eslint-disable max-len */
import { SetupContext, computed, defineComponent, onBeforeMount, ref } from 'vue';
import { verifyDetailProps, VerifyDetailProps } from './verify-detail.props';
import './verify-detail.scss';

export default defineComponent({
    name: 'FVerifyDetail',
    props: verifyDetailProps,
    emits: ['validatorClick', 'listshow'],
    setup(props: VerifyDetailProps, context: SetupContext) {
        let _errorLists: any[] = [];
        const showList = ref(props.showList);
        // 列表部分最大高度
        const _maxHeight = 60;
        /** 默认传入的表单验证信息数组 */
        const _validatorList = ref(props.verifyList) ||
            [{
                id: '111',
                title: '单据信息[销售组织]',
                msg: '字段值不能为空',
                type: 'empty'
            },
            {
                id: '222',
                title: '单据信息[销售组织]',
                msg: '字段值不能为空',
                type: 'empty'
            }];
        const verifyList = ref(props.verifyList) ||
            [{
                id: '111',
                title: '单据信息[销售组织]',
                msg: '字段值不能为空',
                type: 'empty'
            },
            {
                id: '222',
                title: '单据信息[销售组织]',
                msg: '字段值不能为空',
                type: 'empty'
            }];
        const _tablist = ref(props.verifyType) || [
            {
                id: 'vertifyType1',
                type: 'all',
                title: ''
            },
            {
                id: 'vertifyType2',
                type: 'error',
                title: ''
            },
            {
                id: 'vertifyType3',
                type: 'empty',
                title: ''
            }
        ];
        const validator = computed(() => {
            if (_validatorList.value?.length > 0) {
                return true;
            }
            return false;
        });
        onBeforeMount(() => {
            const errorlist: any[] = [];
            if (_tablist && _tablist.value?.length) {
                _tablist.value?.forEach((item: any) => {
                    item.active = false;
                });

                _tablist.value?.forEach((tab: any) => {
                    const tabType = tab.type;
                    if (tabType === 'all') {
                        // 所有类型
                        const listObj = {
                            'list': verifyList,
                            'show': false,
                            'type': tabType
                        };
                        tab.length = listObj.list?.value?.length;
                        errorlist.push(listObj);
                    }
                    else {
                        const hasType = errorlist.find((item: any) => {
                            return item.type === tabType;
                        });
                        if (hasType) {
                            // 能找到类型
                            const valList = verifyList.value?.filter((error: any) => {
                                return error.type === tabType;
                            });
                            if (valList && valList.length) {
                                hasType.list = valList;
                            }
                        }
                        else {
                            // 找不到类型
                            const listObj = {
                                'list': [],
                                'show': false,
                                'type': tabType
                            };
                            const valList = verifyList.value?.filter((error: any) => {
                                return error.type === tabType;
                            });
                            if (valList && valList.length) {
                                listObj.list = valList;
                            }
                            tab.length = listObj.list.length;
                            errorlist.push(listObj);
                        }
                    }
                });
                const tabObj = _tablist.value.find((item: any) => {
                    return item.type === props.showType;
                });
                if (tabObj) {
                    tabObj.active = true;
                }
            }
            _errorLists = errorlist;
        });

        /** 显示详情 */
        function show() {
            showList.value = !showList.value;
            context.emit('listshow', showList.value);
        }
        /** 点击分组切换*/
        function showListContent(tab: any) {
            if (tab.length <= 0) {
                return;
            }
            _tablist.value?.forEach((item: any) => {
                item.active = false;
            });
            tab.active = true;
            _errorLists.forEach((item: any) => {
                if (item.type === tab.type) {
                    item.show = true;
                }
                else {
                    item.show = false;
                }
            });
        }
        /** 点击关闭按钮 */
        function close() {
            showList.value = false;
            context.emit('listshow', false);
        }
        /** 点击表单验证列表事件 */
        function errorClick(item: any) {
            context.emit('validatorClick', item);
        }

        return () => {
            return (
                <div>{
                    validator.value ? (
                        <div class="f-verify-detail">
                            <div class="f-verify-detail-content">
                                <div class="f-verify-nums" onClick={show}>
                                    <span class="nums-icon f-icon f-icon-warning"></span>
                                    <span class="nums-count">{_validatorList.value?.length}</span>
                                </div >
                                <div class={["f-verify-form-main", { hidden: !showList.value }]}>
                                    <div class="f-verify-form-content">
                                        <div class="f-verify-form-content-arrow"></div>
                                        <div class="f-verify-form-content-list">
                                            <div class="f-verify-forms-title">
                                                <div class="title-tabs">
                                                    <div class="btn-group">
                                                        {_tablist.value?.map((tab: any) => (
                                                            <button class={["btn btn-secondary", { active: tab.active }, { disabled: (tab.length <= 0) }]} onClick={() => showListContent(tab)} > {tab.title}
                                                                <span>{tab.length}</span>
                                                            </button>
                                                        ))}
                                                    </div>
                                                </div>
                                                <div class="f-verify-close" onClick={close}>
                                                    <span class="f-icon f-icon-close"></span>
                                                </div>
                                            </div>
                                            <div class="f-verify-forms-list" style={{ maxHeight: _maxHeight + 'px' }}>
                                                {_errorLists && _errorLists.map((errorlist: any) => (
                                                    <ul class={["f-verify-list-content", { active: errorlist.show }]} >
                                                        {errorlist.list.value && errorlist.list.value.map((item: any) => (
                                                            <li class="f-verify-list" onClick={(item: any) => errorClick(item)}>
                                                                <span class={["f-icon f-icon-close-outline list-icon list-error", { 'list-warning': item.type === 'warn' }]}></span>
                                                                <div class="list-con" >
                                                                    <p class="list-title" title={item.title}>{item.title}</p>
                                                                    <p class="list-msg" title={item.msg}>{item.msg}</p>
                                                                </div>
                                                            </li>
                                                        ))}
                                                    </ul>
                                                ))}
                                            </div>
                                        </div>
                                    </div >
                                </div >
                            </div >
                        </div >) : (null)}
                </div >);
        };
    }
});
