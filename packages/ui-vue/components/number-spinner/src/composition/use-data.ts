/**
 * Copyright (c) 2020 - present, Inspur Genersoft Co., Ltd.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *       http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
import { UseData } from './types';
import { NumberSpinnerProps } from '../number-spinner.props';
import { SetupContext, Ref, ref } from 'vue';
import { BigNumber } from 'bignumber.js';
import { UseUtil } from './use-util';

export function UseData(
    props: NumberSpinnerProps,
    context: SetupContext,
    realValue: Ref<number | string | null>,
    inputValue: Ref,
    isFocus: Ref<boolean>,
    formatOptions: Ref
): UseData {
    /**
     * popValue,isActiveTip为后续接入popover保留字段
     * updateOn为内容修改时的类型，包括change、blur
     */
    const popValue = ref('');
    const isActiveTip = ref(false);

    const { isDisableOfBtn, format, getRealValue, cleanNumString, isEmpty, _modelChanged } = UseUtil(
        props,
        context,
        realValue,
        formatOptions
    );

    /**
     * 手动输入值变化时或失去焦点时的方法
     * @param realVal 输入框中的实际数值
     * @param updateOn 更新类型，分为'change'| 'blur'
     */
    function onModelChange(realVal: any, updateOn = 'change') {
        let _resultValue = realVal;
        if (updateOn === 'change') {
            _resultValue = getRealValue(realVal);
            _modelChanged(_resultValue);
        }
    }

    /**
     * 触发微调事件时，计算结果的方法
     * @param direction 微调类型 up微增 down微减
     */
    function compute(direction: string) {
        if (props.readonly || props.disabled || !props.editable) {
            isFocus.value = false;
            return;
        }
        if (isDisableOfBtn(direction)) {
            let _resultValue;
            const realBigNum = new BigNumber(realValue.value || 0);
            if (direction === 'up') {
                _resultValue = realBigNum.plus(Number(props.step));
            } else {
                _resultValue = realBigNum.minus(Number(props.step));
            }

            const value = _resultValue.toFixed();
            if (!isFocus.value) {
                inputValue.value = format(value);
            } else {
                inputValue.value = value;
            }
            _modelChanged(getRealValue(_resultValue));
        }
    }

    /**
     * 点击微增按钮时或键盘触发 ArrowUp 时执行的方法
     */
    function up(e: Event) {
        compute('up');
        e.stopPropagation();
    }

    /**
     * 点击微减按钮时或键盘触发 ArrowDown 时执行的方法
     */
    function down(e: Event) {
        compute('down');
        e.stopPropagation();
    }

    /**
     * 输入框失焦时执行的方法
     */
    function onBlurTextBox($event: Event) {
        $event.stopPropagation();
        if (props.readonly || props.disabled) {
            return;
        }

        inputValue.value = format(realValue.value);
        isFocus.value = false;
        onModelChange(realValue.value, 'blur');
        context.emit('blur', { event: $event, formatted: inputValue.value, value: realValue.value });
    }

    /**
     * 输入框获取焦点时执行的方法
     */
    function onFocusTextBox($event: Event) {
        $event.stopPropagation();
        if (props.readonly || props.disabled) {
            isFocus.value = false;
            return;
        }

        inputValue.value = isEmpty(realValue.value) ? '' : !props.showZero && realValue.value === '0' ? '' : realValue.value;
        isFocus.value = true;
        context.emit('focus', { event: $event, formatted: inputValue.value, value: realValue.value });
    }

    /**
     * 输入框的input事件
     */
    function onInput($event: Event) {
        $event.stopPropagation();
        if (props.disabled) {
            // context.emit('clickButton', $event);
        }
        inputValue.value = ($event.target as HTMLTextAreaElement)?.value;
        onModelChange(inputValue.value, 'change');
    }

    /**
     * 焦点状态下键盘监听事件
     */
    function onKeyDown(e: KeyboardEvent) {
        if (e.key === 'ArrowDown') {
            e.preventDefault();
            down(e);
        }

        if (e.key === 'ArrowUp') {
            e.preventDefault();
            up(e);
        }

        e.stopPropagation();
    }

    return {
        onModelChange,
        up,
        down,
        onBlurTextBox,
        onFocusTextBox,
        onInput,
        onKeyDown
    };
}
