/**
 * Copyright (c) 2020 - present, Inspur Genersoft Co., Ltd.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *       http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
import { BigNumber } from 'bignumber.js';

export interface UseData {
    onModelChange: (realVal: any, updateOn: string) => void;
    up: (e: Event) => void;
    down: (e: Event) => void;
    onFocusTextBox: ($event: Event) => void;
    onBlurTextBox: ($event: Event) => void;
    onInput: ($event: Event, updateOn: string) => void;
    onKeyDown: ($event: KeyboardEvent) => void;
}

export interface NumberFormatter {
    /** 前置符号 */
    prefix?: string;
    /** 后缀 */
    suffix?: string;
    /** 小数点 */
    decimalSeparator?: string;
    /** 千分位符号 */
    groupSeparator?: string;
    /** 千分位分组 */
    groupSize?: number;
}

export interface UseUtil {
    isEmpty: (value: any) => boolean;
    setDisabledState: (isDisabled: boolean) => void;
    cleanNumString: (value: any) => string;
    getPrecision: () => number;
    toFixed: (value: BigNumber | number) => string;
    _getRealValue: (value: BigNumber) => string | number;
    getRealValue: (value: any) => string | number;
    isDisableOfBtn: (type: string, value?: any) => boolean;
    buildFormatOptions: () => object;
    _modelChanged: (realVal: any) => void;
    validInterval: (bn: BigNumber) => BigNumber;
    format: (value: any) => string;
    _toFormat: (_bgNum: BigNumber, fmt: NumberFormatter) => string;
}
