/**
 * Copyright (c) 2020 - present, Inspur Genersoft Co., Ltd.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *       http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
import { defineComponent, computed, ref, onMounted, onBeforeUpdate, watch } from 'vue';
import type { SetupContext } from 'vue';
import { numberSpinnerProps, NumberSpinnerProps } from './number-spinner.props';
import { UseData } from './composition/use-data';
import { UseUtil } from './composition/use-util';
import './index.scss';

export default defineComponent({
    name: 'FNumberSpinner',
    props: numberSpinnerProps,
    emits: ['valueChange', 'blur', 'focus', 'click', 'input'] as (string[] & ThisType<void>) | undefined,
    setup(props: NumberSpinnerProps, context: SetupContext) {
        // realValue 真实值 开启大数模式时未string类型，普通模式未number类型
        const realValue: any = ref(null);
        const isFocus = ref(false);
        const inputValue = ref('');
        const formatOptions = ref({});
        const { isDisableOfBtn, format, buildFormatOptions, getRealValue } = UseUtil(props, context, realValue, formatOptions);
        const { onFocusTextBox, onBlurTextBox, onInput, up, down, onKeyDown } = UseData(
            props,
            context,
            realValue,
            inputValue,
            isFocus,
            formatOptions
        );

        onMounted(() => {
            const value = getRealValue(props.value);
            realValue.value = value;
            inputValue.value = format(value);
        });

        watch(
            () => [props.precision, props.useThousands, props.prefix, props.suffix],
            ([newPrecision, newUseThousands, newPrefix, newSuffix, newValue]) => {
                formatOptions.value = buildFormatOptions();
                inputValue.value = format(realValue.value);
            }
        );

        watch(
            () => [props.value],
            ([newValue]) => {
                const value = getRealValue(newValue);
                realValue.value = value;
                inputValue.value = format(value);
            }
        )

        return () => (
            <div>
                <div class="input-group  f-state-hover flex-row">
                    <input
                        class="form-control f-utils-fill"
                        style={{ 'text-align': props.textAlign }}
                        type="text"
                        value={inputValue.value}
                        onBlur={onBlurTextBox}
                        onFocus={onFocusTextBox}
                        onInput={(e) => onInput(e, 'change')}
                        disabled={props.disabled}
                        readonly={props.readonly || !props.editable}
                        placeholder={props.disabled || props.readonly || !props.editable ? '' : props.placeholder}
                        onKeydown={(e) => {
                            onKeyDown(e);
                        }}
                    />
                    {!props.disabled && !props.readonly && props.showButton && (
                        <div class="input-group-append btn-group btn-group-number">
                            <button
                                title="upButton"
                                class="btn btn-secondary btn-number-flag"
                                style={{ cursor: isDisableOfBtn('up') ? 'pointer' : 'not-allowed' }}
                                onClick={(e) => {
                                    up(e);
                                }}
                                disabled={!isDisableOfBtn('up')}>
                                <span class="icon k-i-arrow-chevron-up number-arrow-chevron"></span>
                            </button>

                            <button
                                title="downButton"
                                class="btn btn-secondary btn-number-flag"
                                style={{ cursor: isDisableOfBtn('down') ? 'pointer' : 'not-allowed' }}
                                onClick={(e) => {
                                    down(e);
                                }}
                                disabled={!isDisableOfBtn('down')}>
                                <span class="icon k-i-arrow-chevron-down number-arrow-chevron"></span>
                            </button>
                        </div>
                    )}
                </div>
            </div>
        );
    }
});
