/**
 * Copyright (c) 2020 - present, Inspur Genersoft Co., Ltd.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *       http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
import { Ref, ref } from 'vue';
import { DataGridColumn, DataGridProps } from '../data-grid.props';
import { UseCommandColumn } from './types';

export function useCommandColumn(props: DataGridProps): UseCommandColumn {
    const defaultColumnWidth = 120;
    const enableCommands = ref(props.enableCommands);
    const commands = ref(props.commands);

    function applyCommands(columns: Ref<DataGridColumn[]>) {
        if (enableCommands.value) {
            const commandColumn = {
                field: '__commands__',
                title: '操作',
                width: defaultColumnWidth,
                fixed: 'right',
                dataType: 'commands',
                commands: commands.value
            } as DataGridColumn;
            columns.value.push(commandColumn as DataGridColumn);
        }
    }

    return { applyCommands };
}
