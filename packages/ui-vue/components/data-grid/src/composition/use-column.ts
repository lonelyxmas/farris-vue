/**
 * Copyright (c) 2020 - present, Inspur Genersoft Co., Ltd.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *       http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
import { ref, Ref, computed } from 'vue';
import { DataGridColumn, DataGridProps } from '../data-grid.props';
import { DataGridHeaderCell, UseColumn } from './types';

export interface ColumnRenderContext {
    defaultColumnWidth: number;
    headerDepth: number;
    leftColumns: DataGridColumn[];
    leftColumnsWidth: number;
    leftHeaderColumns: DataGridHeaderCell[];
    primaryColumns: DataGridColumn[];
    primaryColumnsWidth: number;
    primaryHeaderColumns: DataGridHeaderCell[];
    rightColumns: DataGridColumn[];
    rightColumnsWidth: number;
    rightHeaderColumns: DataGridHeaderCell[];
    summaryColumns: DataGridColumn[];
}

export function useColumn(props: DataGridProps): UseColumn {
    const defaultColumnWidth = 120;

    const columnContext: Ref<ColumnRenderContext> = ref({
        defaultColumnWidth,
        headerDepth: 1,
        leftColumns: [],
        leftColumnsWidth: 0,
        leftHeaderColumns: [],
        primaryColumns: [],
        primaryColumnsWidth: 0,
        primaryHeaderColumns: [],
        rightColumns: [],
        rightColumnsWidth: 0,
        rightHeaderColumns: [],
        summaryColumns: []
    });

    const summaryOptions = ref(props.summary);

    const summaryFields = computed(() => {
        const options = summaryOptions.value;
        return options?.groupFields || [];
    });

    function updateColumnRenderContext(columns: DataGridColumn[]) {
        const summaryColumnsMap = new Map<string, boolean>();
        if (summaryFields.value) {
            summaryFields.value.reduce((result: Map<string, boolean>, summaryField: string) => {
                result.set(summaryField, true);
                return result;
            }, summaryColumnsMap);
        }
        columns.reduce((result, column) => {
            if (column.fixed === 'left') {
                columnContext.value.leftColumns.push(column);
                columnContext.value.leftColumnsWidth += column.actualWidth || defaultColumnWidth;
            } else if (column.fixed === 'right') {
                columnContext.value.rightColumns.push(column);
                columnContext.value.rightColumnsWidth += column.actualWidth || defaultColumnWidth;
            } else {
                columnContext.value.primaryColumns.push(column);
                columnContext.value.primaryColumnsWidth += column.actualWidth || defaultColumnWidth;
            }
            if (summaryColumnsMap.has(column.field)) {
                columnContext.value.summaryColumns.push(column);
            }
            return result;
        }, columnContext);
        return columnContext;
    }

    updateColumnRenderContext(props.columns);

    const hasLeftFixedColumn = computed(() => {
        return columnContext.value.leftColumns.length > 0;
    });

    const hasRightFixedColumn = computed(() => {
        return columnContext.value.rightColumns.length > 0;
    });

    return { columnContext, hasLeftFixedColumn, hasRightFixedColumn };
}
