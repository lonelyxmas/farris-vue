import { DataGridHeaderCell } from "./types";
import booleanFitler from '../components/filter/boolean-filter-editor.component';
import dateFilter from '../components/filter/date-filter-editor.component';
import numericFilter from '../components/filter/numeric-filter-editor.component';
import textFiler from '../components/filter/text-filter-editor.component';
import enumFilter from '../components/filter/enum-filter-editor.component';
import listFiler from '../components/filter/list-filter-editor.component';

export function useFilter() {

    const typeToEditorName = new Map<string, string>([
        ['boolean', 'boolean-filter'],
        ['date', 'date-filter'],
        ['datetime', 'date-filter'],
        ['number', 'numeric-filter'],
        ['string', 'text-filter'],
        ['text', 'text-filter'],
        ['enum', 'enum-filter'],
        ['reference', 'list-filter']
    ]);

    const editorMap = new Map<string, (headerCell: DataGridHeaderCell) => any>([
        ['boolean-filter', booleanFitler],
        ['date-filter', dateFilter],
        ['enum-filter', enumFilter],
        ['list-filter', listFiler],
        ['numeric-filter', numericFilter],
        ['text-filter', textFiler]
    ]);

    function getFilterEitor(headerCell: DataGridHeaderCell) {
        const fieldType = headerCell.column?.dataType || 'string';
        const editorName = headerCell.column?.filter || typeToEditorName.get(fieldType) || 'text-editor';

        const editor = editorMap.get(editorName);
        if (editor) {
            return editor(headerCell);
        }
    }

    return { getFilterEitor };
}
