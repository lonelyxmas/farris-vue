/**
 * Copyright (c) 2020 - present, Inspur Genersoft Co., Ltd.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *       http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
import { UseDataView, UseSelection } from './types';
import { computed, ref } from 'vue';
import { DataGridProps } from '../data-grid.props';
import { useGroupData } from './use-group-data';

export type DataViewFilter = (dataItem: any) => boolean;

export function useDataView(
    props: DataGridProps,
    selectionComposition: UseSelection
): UseDataView {
    const collapseMap = new Map<string, boolean>();
    const filtersMap = new Map<string, DataViewFilter>();
    const { getSelectionRow } = selectionComposition;
    const getNewDataItem = ref(props.newDataItem);
    const groupFields = ref(props.group?.groupFields || []);
    const shouldGroupingData = ref(props.group?.enable || false);
    const originalData = ref(props.data);
    const useGroupDataComposition = useGroupData(props);
    const summaryOptions = ref(props.summary);
    const groupSummaryFields = computed(() => {
        const options = summaryOptions.value;
        return options?.groupFields || [];
    });
    const summaries = groupSummaryFields.value.reduce((sumaries: Map<string, number>, summaryField: string) => {
        sumaries.set(summaryField, 0);
        return sumaries;
    }, new Map<string, number>());

    const totalItems = computed<number>(() => {
        return originalData.value.length;
    });

    const pagination = ref(props.pagination);
    const pageSize = computed<number>(() => {
        return pagination.value?.size || originalData.value.length;
    });
    const pageIndex = ref(1);

    function resetDataView() {
        let resetData = originalData.value;
        if (shouldGroupingData.value) {
            resetData = useGroupDataComposition.renderGroupedData(groupFields.value, originalData.value);
        }
        const startRowIndex = (pageIndex.value - 1) * pageSize.value;
        const endRowIndex = Math.min(startRowIndex + pageSize.value, totalItems.value);
        let rowIndex = 0;
        const dataViewItems = [];
        if (resetData.length) {
            for (let index = startRowIndex; index < endRowIndex; index++) {
                const dataItem = resetData[index] as any;
                if (dataItem.__fv_data_grid_group_row__) {
                    rowIndex = 0;
                    dataItem.__fv_data_index__ = '';
                } else if (dataItem.__fv_data_grid_group_summary__) {
                    dataItem.__fv_data_index__ = '';
                } else {
                    rowIndex++;
                    dataItem.__fv_data_index__ = rowIndex;
                }
                dataItem.__fv_index__ = index;
                groupSummaryFields.value.forEach((summaryField: string) => {
                    const summaryFieldValue = summaries.get(summaryField) || 0;
                    summaries.set(summaryField, summaryFieldValue + dataItem[summaryField]);
                });
                dataViewItems.push(dataItem);
            }
        }
        return dataViewItems;
    }
    const dataView = ref(resetDataView());

    function applyFilter(filters: DataViewFilter[], reset = false) {
        const source = reset ? resetDataView() : dataView.value;
        dataView.value = source
            .filter((dataItem: any, index: number) => {
                return filters.reduce((result: boolean, filter: DataViewFilter) => {
                    return result && filter(dataItem);
                }, true);
            })
            .map((dataItem: any, index: number) => {
                if (dataItem.__fv_data_grid_group_row__) {
                    const groupedRowId = `group_of_${dataItem.__fv_data_grid_group_field__}_${dataItem.__fv_data_grid_group_value__}`;
                    dataItem.__fv_data_grid_group_collapse__ = !!collapseMap.get(groupedRowId);
                }
                dataItem.__fv_index__ = index;
                return dataItem;
            });
        return dataView.value;
    }

    function collapse(collapseField: string, collapseValue: any) {
        const groupedRowId = `group_of_${collapseField}_${collapseValue}`;
        collapseMap.set(groupedRowId, true);
        const collapseFieldFilter = (dataItem: any) => dataItem[collapseField] !== collapseValue;
        filtersMap.set(`collapse_${collapseField}_${collapseValue}`, collapseFieldFilter);
        return applyFilter(Array.from(filtersMap.values()));
    }

    function expand(expandField: string, expandValue: any) {
        const groupedRowId = `group_of_${expandField}_${expandValue}`;
        collapseMap.set(groupedRowId, false);
        filtersMap.delete(`collapse_${expandField}_${expandValue}`);
        return applyFilter(Array.from(filtersMap.values()), true);
    }

    function getRange(filters: DataViewFilter[], start: number, end: number) {
        const filteredData =
            filters && filters.length
                ? dataView.value.filter((dataItem: any, index: number) => {
                    return filters.reduce((result: boolean, filter: DataViewFilter) => {
                        return result && filter(dataItem);
                    }, true);
                })
                : dataView.value;
        return filteredData.slice(start, end);
    }

    function navigatePageTo(targetPageIndex: number) {
        const shouldNavigating = targetPageIndex * pageSize.value <= totalItems.value;
        if (shouldNavigating) {
            pageIndex.value = targetPageIndex;
            const newPageData = resetDataView();
            dataView.value = newPageData;
        }
    }

    function insertNewDataItem() {
        const newDataItem = getNewDataItem.value();
        const currentSelectionRow = getSelectionRow();
        const targetIndex = currentSelectionRow ? currentSelectionRow.dataIndex - 1 : 0;
        originalData.value.splice(targetIndex, 0, newDataItem);
        const newPageData = resetDataView();
        dataView.value = newPageData;
    }

    function removeDataItem(dataIndex: number) {
        const targetIndex = dataIndex ? dataIndex - 1 : 0;
        originalData.value.splice(targetIndex, 1);
        const newPageData = resetDataView();
        dataView.value = newPageData;
    }

    function recalculatePageIndexByPageSize(currentPageIndex: number, originalPageSize: number, newPageSize: number) {
        const currentVisibleRecordsCount = currentPageIndex * originalPageSize;
        const latestPageIndex = Math.ceil(currentVisibleRecordsCount / newPageSize);
        return latestPageIndex;
    }

    function changePageSizeTo(newPageSize: number) {
        if (pagination.value) {
            // const latestPageIndex = recalculatePageIndexByPageSize(pageIndex.value, pagination.value.size, newPageSize);
            // pageIndex.value = latestPageIndex;
            pagination.value.size = newPageSize;
            const newPageData = resetDataView();
            dataView.value = newPageData;
        }
    }

    return {
        changePageSizeTo,
        collapse,
        dataView,
        expand,
        getRange,
        navigatePageTo,
        summaries,
        totalItems,
        insertNewDataItem,
        removeDataItem
    };
}
