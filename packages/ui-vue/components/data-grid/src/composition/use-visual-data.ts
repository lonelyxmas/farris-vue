/**
 * Copyright (c) 2020 - present, Inspur Genersoft Co., Ltd.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *       http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
import { DataGridColumn, DataGridProps } from '../data-grid.props';
import { Ref, ref } from 'vue';
import {
    CellMode, UseDataView, UseEdit, UseGroupData, UseVisualData,
    VisualData, VisualDataCell, VisualDataStatus, VisualDataType
} from './types';

export function useVisualData(
    props: DataGridProps,
    columns: Ref<DataGridColumn[]>,
    useGroupData: UseGroupData,
    dataViewComposition: UseDataView,
    visibleCapacity: Ref<number>,
    preloadCount: number,
    useEditComposition: UseEdit
): UseVisualData {
    const mergingCell = ref(props.mergeCell);
    const rowOption = ref(props.rowOption);
    const rowHeight = rowOption.value?.height || 28;
    const autoRowHeight = rowOption.value?.wrapContent || false;
    const maxVisibleRowIndex = ref(visibleCapacity.value - 1 + preloadCount);
    const minVisibleRowIndex = ref(0);
    const identifyField = ref(props.idField);

    function merginSameValueInColumn(currentCell: VisualDataCell, preDataItem: VisualData, columnField: string) {
        const preRowMerginSpan = preDataItem && preDataItem.data[columnField];
        const hasBeenSpanned = preRowMerginSpan && preRowMerginSpan.data === currentCell.data;
        if (hasBeenSpanned) {
            const spannedBy: VisualDataCell = (preRowMerginSpan && preRowMerginSpan.spannedBy) || preRowMerginSpan;
            spannedBy.rowSpan++;
            spannedBy.spanned = spannedBy.spanned || [];
            spannedBy.spanned.push(currentCell);
            currentCell.colSpan = 0;
            currentCell.rowSpan = 0;
            currentCell.spannedBy = spannedBy;
        }
    }

    function renderDataRow(
        dataItem: any,
        preDataItem: any,
        preMergingRow: VisualData,
        rowIndex: number,
        top: number,
        columns: DataGridColumn[]
    ): VisualData {
        const dataIndex = dataItem.__fv_data_index__;
        const dataIdentify = dataItem[identifyField.value];
        const editingSnapshot = useEditComposition.getEditingSnapshot(dataIdentify) as VisualData;
        let currentRow: VisualData;
        let shouldWrapCells = true;
        if (editingSnapshot) {
            shouldWrapCells = false;
            currentRow = editingSnapshot;
            currentRow.index = rowIndex;
            currentRow.dataIndex = dataIndex;
            currentRow.top = top;
            currentRow.pre = preDataItem;
        } else {
            currentRow = {
                index: rowIndex,
                dataIndex,
                top,
                layer: -1,
                type: VisualDataType.data,
                pre: preDataItem,
                data: {},
                setRef: (vnode) => {
                    currentRow.ref = vnode;
                },
                status: VisualDataStatus.initial
            };
        }
        if (!preDataItem) {
            dataItem.__fv_data_position__ = 0;
        }
        if (!autoRowHeight) {
            currentRow.height = rowHeight;
            dataItem.__fv_data_height__ = currentRow.height;
            const preDataItem = currentRow.pre;
            if (preDataItem) {
                const topPosition = (preDataItem.__fv_data_position__ || 0) + (preDataItem.__fv_data_height__ || 0);
                currentRow.top = topPosition;
                dataItem.__fv_data_position__ = topPosition;
            }
        }
        if (shouldWrapCells) {
            columns.forEach((column: DataGridColumn, columnIndex: number) => {
                const currentCell: VisualDataCell = {
                    colSpan: 1,
                    rowSpan: 1,
                    index: columnIndex,
                    field: column.field,
                    mode: column.readonly ? CellMode.readonly : CellMode.editable,
                    data: dataItem[column.field],
                    editingData: dataItem[column.field],
                    parent: currentRow,
                    getEditor(cell: VisualDataCell) {
                        return useEditComposition.getEditor(cell, column, cell.parent);
                    },
                    setRef: (vnode: any) => {
                        if (autoRowHeight && vnode) {
                            const visualData = currentCell.parent as VisualData;
                            if (currentCell.cellHeight !== vnode.offsetHeight) {
                                currentCell.cellHeight = vnode.offsetHeight;
                            }
                            if (currentCell.cellHeight && currentCell.cellHeight > (visualData.height || 0)) {
                                visualData.height = currentCell.cellHeight;
                                dataItem.__fv_data_height__ = visualData.height;
                            }
                            const preDataItem = visualData.pre;
                            if (preDataItem) {
                                const topPosition = (preDataItem.__fv_data_position__ || 0) + (preDataItem.__fv_data_height__ || 0);
                                visualData.top = topPosition;
                                dataItem.__fv_data_position__ = topPosition;
                            }
                        }
                    },
                    update: (value: any) => {
                        if (value !== undefined) {
                            currentCell.editingData = value;
                        }
                    },
                    accept: () => {
                        dataItem[column.field] = currentCell.editingData;
                        currentCell.data = dataItem[column.field];
                    },
                    cancel: () => {
                        currentCell.editingData = currentCell.data;
                    }
                };
                if (column.dataType === 'commands') {
                    currentCell.formatter = (cell: VisualDataCell, visualDataRow: VisualData) => {
                        return useEditComposition.getEditor(cell, column, visualDataRow);
                    };
                }
                if (mergingCell.value) {
                    merginSameValueInColumn(currentCell, preMergingRow, column.field);
                }
                currentRow.data[column.field] = currentCell;
            });
        }
        return currentRow;
    }

    function getVisualData(start: number, end: number, pre?: any, forceToRefresh?: boolean): VisualData[] {
        const { dataView } = dataViewComposition;
        const visualDatas: VisualData[] = [];
        if (dataView.value.length > 0) {
            const refreshKey = forceToRefresh ? Date.now().toString() : '';
            for (let rowIndex = start, visualIndex = 0; rowIndex <= end; rowIndex++, visualIndex++) {
                const dataViewItem = dataView.value[rowIndex];
                const preDataItem = dataView.value[rowIndex - 1] || pre;
                const preMergingRow = visualDatas[visualIndex - 1];
                const targetPosition = preDataItem ? (preDataItem.__fv_data_position__ || 0) + (preDataItem.__fv_data_height__ || 0) : 0;
                const currentRow = dataViewItem.__fv_data_grid_group_row__
                    ? useGroupData.renderGroupRow(dataViewItem, preDataItem, rowIndex, targetPosition, columns.value)
                    : dataViewItem.__fv_data_grid_group_summary__
                        ? useGroupData.renderSummaryRow(dataViewItem, preDataItem, rowIndex, targetPosition, columns.value)
                        : renderDataRow(dataViewItem, preDataItem, preMergingRow, rowIndex, targetPosition, columns.value);
                currentRow.refreshKey = refreshKey;
                visualDatas.push(currentRow);
            }
        }
        minVisibleRowIndex.value = visualDatas.length > 0 ? visualDatas[0].index : 0;
        maxVisibleRowIndex.value = visualDatas.length > 0 ? visualDatas[visualDatas.length - 1].index : 0;
        return visualDatas;
    }

    function toggleGroupRow(status: 'collapse' | 'expand', groupRow: VisualData, visibleDatas: VisualData[]): VisualData[] {
        const groupField = groupRow.groupField || '';
        const { groupValue } = groupRow;
        dataViewComposition[status](groupField, groupValue);
        const { dataView } = dataViewComposition;
        const start = visibleDatas[0].index;
        const end = Math.min(start + visibleCapacity.value + preloadCount + 1, dataView.value.length);
        const visualDatas: VisualData[] = getVisualData(start, end);
        return visualDatas;
    }

    return { getVisualData, maxVisibleRowIndex, minVisibleRowIndex, toggleGroupRow };
}
