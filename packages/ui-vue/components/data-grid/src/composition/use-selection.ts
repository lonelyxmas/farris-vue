/**
 * Copyright (c) 2020 - present, Inspur Genersoft Co., Ltd.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *       http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
import { Ref, ref } from 'vue';
import { DataGridProps } from '../data-grid.props';
import { UseSelection, VisualData } from './types';

export function useSelection(props: DataGridProps): UseSelection {
    const identifyField = ref(props.idField);
    const currentSelectedDataId = ref('');
    let visibleDatasRef: Ref<VisualData[]>;

    function getSelectionRow(): VisualData {
        return visibleDatasRef?.value.find((dataItem: VisualData) => {
            return dataItem.data[identifyField.value].data === currentSelectedDataId.value;
        }) as VisualData;
    }

    function setSelectionRow(visibleDatas: Ref<VisualData[]>, selectedDataId: string) {
        visibleDatasRef = visibleDatas;
        currentSelectedDataId.value = selectedDataId;
    }

    return { getSelectionRow, setSelectionRow };
}
