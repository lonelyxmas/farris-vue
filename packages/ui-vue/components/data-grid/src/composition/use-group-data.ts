/**
 * Copyright (c) 2020 - present, Inspur Genersoft Co., Ltd.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *       http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
import { computed, ref } from 'vue';
import { DataGridColumn, DataGridProps } from '../data-grid.props';
import { CellMode, UseGroupData, VisualData, VisualDataCell, VisualDataStatus, VisualDataType } from './types';

interface GroupingItem {
    field: string;
    value: any;
    details: any[];
    layer: number;
    nestGroup?: Map<any, GroupingItem>;
    groupSummaries: Map<string, number>;
}

export function useGroupData(props: DataGridProps): UseGroupData {
    const identifyField = ref(props.idField);

    const summaryOptions = ref(props.summary);

    const groupOptions = ref(props.group);

    const groupSummaryFields = computed(() => {
        const options = summaryOptions.value;
        return options?.groupFields || [];
    });

    const shouldShowColumnSummary = computed(() => {
        const options = summaryOptions.value;
        return options && options.enable && options.groupFields && options.groupFields.length > 0;
    });

    const shouldShowSummary = computed(() => {
        const options = groupOptions.value;
        return options && options.showSummary;
    });

    const groupedData = new Map<any, GroupingItem>();
    let flatGroupedData: any[] = [];
    let groupedPath = '';

    function processGroupTo(groupObject: Map<any, GroupingItem>, rawData: any[], groupLayer: number, groupFields: string[]) {
        const groupField = groupFields[groupLayer];
        if (groupField) {
            rawData.reduce((groupResult: Map<any, GroupingItem>, rawDataItem: any, index: number) => {
                const groupingValue = rawDataItem[groupField] as any;
                let groupingItem = groupResult.get(groupingValue);
                if (!groupingItem) {
                    const groupSummaries = groupSummaryFields.value.reduce((sumaries: Map<string, number>, summaryField: string) => {
                        sumaries.set(summaryField, 0);
                        return sumaries;
                    }, new Map<string, number>());
                    groupingItem = { field: groupField, value: groupingValue, layer: groupLayer, details: [], groupSummaries };
                    groupResult.set(groupingValue, groupingItem);
                }
                if (groupSummaryFields.value) {
                    groupSummaryFields.value.forEach((summaryField: string) => {
                        if (groupingItem) {
                            const summaryFieldValue = groupingItem.groupSummaries.get(summaryField) || 0;
                            groupingItem.groupSummaries.set(summaryField, summaryFieldValue + rawDataItem[summaryField]);
                        }
                    });
                }
                groupingItem.details.push(rawDataItem);
                return groupResult;
            }, groupObject);
            if (groupLayer < groupFields.length - 1) {
                groupObject.forEach((groupingItem: GroupingItem, groupingValue: any) => {
                    groupingItem.nestGroup = new Map<any, GroupingItem>();
                    processGroupTo(groupingItem.nestGroup, groupingItem.details, groupLayer + 1, groupFields);
                    groupingItem.details = [];
                });
            }
        }
    }

    function toFlattenGroupedObject(targetGroupingObject: Map<any, GroupingItem>) {
        const groupingData: any[] = [];
        targetGroupingObject.forEach((groupingItem: GroupingItem) => {
            const virtualDataItem = {
                __fv_data_grid_group_collapse__: false,
                __fv_data_grid_group_field__: groupingItem.field,
                __fv_data_grid_group_layer__: groupingItem.layer,
                __fv_data_grid_group_row__: true,
                __fv_data_grid_group_value__: groupingItem.value
            } as Record<string, any>;
            let total = 0;
            groupingData.push(virtualDataItem);
            if (groupingItem.nestGroup) {
                const nestGroupingData = toFlattenGroupedObject(groupingItem.nestGroup);
                groupingData.push(...nestGroupingData);
                total += nestGroupingData.length;
            }
            if (groupingItem.details && groupingItem.details.length) {
                groupingData.push(...groupingItem.details);
                total += groupingItem.details.length;
            }
            if (shouldShowSummary.value) {
                const virtualSummaryDataItem = {
                    __fv_data_grid_group_field__: groupingItem.field,
                    __fv_data_grid_group_layer__: groupingItem.layer,
                    __fv_data_grid_group_summary__: true
                } as Record<string, any>;
                virtualSummaryDataItem[identifyField.value] = `summary_of_${groupingItem.field}_${groupingItem.value}`;
                virtualSummaryDataItem[groupingItem.field] = Array.from(groupingItem.groupSummaries.entries()).reduce(
                    (summaryResult: string, [summaryField, summaryValue]) => {
                        return `${summaryResult} ${summaryField} total:${summaryValue} `;
                    },
                    ''
                );
                groupingData.push(virtualSummaryDataItem);
            }
            if (groupingItem.groupSummaries && groupingItem.groupSummaries.size) {
                groupingItem.groupSummaries.forEach((summaryValue: number, summaryField: string) => {
                    virtualDataItem[summaryField] = summaryValue;
                });
            }
            virtualDataItem[identifyField.value] = `group_of_${groupingItem.field}_${groupingItem.value}`;
            virtualDataItem[groupingItem.field] = `${groupingItem.field}:${groupingItem.value} (${total})`;
        });
        groupingData.forEach((dataItem: any, index: number) => {
            dataItem.__fv_data_index__ = index;
        });
        return groupingData;
    }

    function renderGroupedData(groupFields: string[], rawData: any[]): any[] {
        if (!groupFields || groupFields.length === 0) {
            return rawData;
        }

        const fieldsPath = groupFields.join(',');
        const hasGroupingPathChanged = fieldsPath !== groupedPath;
        if (hasGroupingPathChanged) {
            groupedData.clear();
            flatGroupedData = [];
            groupedPath = fieldsPath;
            processGroupTo(groupedData, rawData, 0, groupFields);
            flatGroupedData = toFlattenGroupedObject(groupedData);
        }

        return flatGroupedData;
    }

    function renderGroupRow(dataItem: any, preDataItem: any, rowIndex: number, top: number, columns: DataGridColumn[]): VisualData {
        const collapse = dataItem.__fv_data_grid_group_collapse__;
        const groupField = dataItem.__fv_data_grid_group_field__;
        const groupValue = dataItem.__fv_data_grid_group_value__;
        const dataIndex = dataItem.__fv_data_index__;
        const layer = dataItem.__fv_data_grid_group_layer__;
        const currentRow: VisualData = {
            collapse,
            data: {},
            dataIndex,
            groupField,
            groupValue,
            layer,
            index: rowIndex,
            pre: preDataItem,
            top,
            type: VisualDataType.group,
            setRef: (vnode) => {
                currentRow.ref = vnode;
            },
            status: VisualDataStatus.initial
        };

        const identifyFieldCell: VisualDataCell = {
            colSpan: 0,
            rowSpan: 0,
            index: -1,
            field: identifyField.value,
            data: dataItem[identifyField.value],
            editingData: dataItem[identifyField.value],
            parent: currentRow,
            mode: CellMode.readonly,
            getEditor: (cell) => cell.data,
            setRef: (vnode: any) => {
                if (vnode) {
                    const visualData = identifyFieldCell.parent as VisualData;
                    if (identifyFieldCell.cellHeight !== vnode.offsetHeight) {
                        identifyFieldCell.cellHeight = vnode.offsetHeight;
                    }
                    if (identifyFieldCell.cellHeight && identifyFieldCell.cellHeight > (visualData.height || 0)) {
                        visualData.height = identifyFieldCell.cellHeight;
                        dataItem.__fv_data_height__ = visualData.height;
                    }
                    const preDataItem = visualData.pre;
                    if (preDataItem) {
                        const topPosition = (preDataItem.__fv_data_position__ || 0) + (preDataItem.__fv_data_height__ || 0);
                        visualData.top = topPosition;
                        dataItem.__fv_data_position__ = topPosition;
                    }
                }
            },
            update: () => { },
            accept: () => { },
            cancel: () => { }
        };
        currentRow.data[identifyField.value] = identifyFieldCell;
        const groupFieldCell: VisualDataCell = {
            colSpan: columns.length,
            rowSpan: 1,
            index: 1,
            field: groupField,
            data: dataItem[groupField],
            editingData: dataItem[groupField],
            parent: currentRow,
            mode: CellMode.readonly,
            getEditor: (cell) => cell.data,
            setRef: (vnode: any) => {
                if (vnode) {
                    const visualData = groupFieldCell.parent as VisualData;
                    if (groupFieldCell.cellHeight !== vnode.offsetHeight) {
                        groupFieldCell.cellHeight = vnode.offsetHeight;
                    }
                    if (groupFieldCell.cellHeight && groupFieldCell.cellHeight > (visualData.height || 0)) {
                        visualData.height = groupFieldCell.cellHeight;
                        dataItem.__fv_data_height__ = visualData.height;
                    }
                    const preDataItem = visualData.pre;
                    if (preDataItem) {
                        const topPosition = (preDataItem.__fv_data_position__ || 0) + (preDataItem.__fv_data_height__ || 0);
                        visualData.top = topPosition;
                        dataItem.__fv_data_position__ = topPosition;
                    }
                }
            },
            update: () => { },
            accept: () => { },
            cancel: () => { }
        };
        currentRow.data[groupField] = groupFieldCell;

        if (shouldShowColumnSummary.value) {
            let groupCellColumnSpanCount = groupFieldCell.colSpan;
            const groupSummaryMap = groupSummaryFields.value.reduce((mapResult: Map<string, boolean>, groupField) => {
                mapResult.set(groupField, true);
                return mapResult;
            }, new Map<string, boolean>());
            columns.reduce((groupRow: VisualData, column: DataGridColumn, columnIndex: number) => {
                if (groupSummaryMap.has(column.field)) {
                    const currentCell: VisualDataCell = {
                        colSpan: 1,
                        rowSpan: 1,
                        index: columnIndex,
                        field: column.field,
                        data: dataItem[column.field],
                        editingData: dataItem[column.field],
                        parent: currentRow,
                        mode: CellMode.readonly,
                        getEditor: (cell) => cell.data,
                        setRef: (vnode: any) => {
                            if (vnode) {
                                const visualData = currentCell.parent as VisualData;
                                if (currentCell.cellHeight !== vnode.offsetHeight) {
                                    currentCell.cellHeight = vnode.offsetHeight;
                                }
                                if (currentCell.cellHeight && currentCell.cellHeight > (visualData.height || 0)) {
                                    visualData.height = currentCell.cellHeight;
                                    dataItem.__fv_data_height__ = visualData.height;
                                }
                                const preDataItem = visualData.pre;
                                if (preDataItem) {
                                    const topPosition = (preDataItem.__fv_data_position__ || 0) + (preDataItem.__fv_data_height__ || 0);
                                    visualData.top = topPosition;
                                    dataItem.__fv_data_position__ = topPosition;
                                }
                            }
                        },
                        update: () => { },
                        accept: () => { },
                        cancel: () => { }
                    };
                    currentRow.data[column.field] = currentCell;
                    if (groupCellColumnSpanCount - 1 > columnIndex) {
                        groupCellColumnSpanCount = columnIndex;
                    }
                }
                return groupRow;
            }, currentRow);
            groupFieldCell.colSpan = groupCellColumnSpanCount;
        }
        return currentRow;
    }

    function renderSummaryRow(dataItem: any, preDataItem: any, rowIndex: number, top: number, columns: DataGridColumn[]): VisualData {
        const groupField = dataItem.__fv_data_grid_group_field__;
        const dataIndex = dataItem.__fv_data_index__;
        const layer = dataItem.__fv_data_grid_group_layer__;
        const currentRow: VisualData = {
            data: {},
            dataIndex,
            groupField,
            groupValue: null,
            layer,
            index: rowIndex,
            pre: preDataItem,
            top,
            type: VisualDataType.summary,
            setRef: (vnode) => {
                currentRow.ref = vnode;
            },
            status: VisualDataStatus.initial
        };
        const identifyCell: VisualDataCell = {
            colSpan: 0,
            rowSpan: 0,
            index: -1,
            field: identifyField.value,
            data: dataItem[identifyField.value],
            editingData: dataItem[identifyField.value],
            parent: currentRow,
            mode: CellMode.readonly,
            getEditor: (cell) => cell.data,
            setRef: (vnode) => {
                if (vnode) {
                    const visualData = identifyCell.parent as VisualData;
                    if (identifyCell.cellHeight !== vnode.offsetHeight) {
                        identifyCell.cellHeight = vnode.offsetHeight;
                    }
                    if (identifyCell.cellHeight && identifyCell.cellHeight > (visualData.height || 0)) {
                        visualData.height = identifyCell.cellHeight;
                        dataItem.__fv_data_height__ = visualData.height;
                    }
                    const preDataItem = visualData.pre;
                    if (preDataItem) {
                        const topPosition = (preDataItem.__fv_data_position__ || 0) + (preDataItem.__fv_data_height__ || 0);
                        visualData.top = topPosition;
                        dataItem.__fv_data_position__ = topPosition;
                    }
                }
            },
            update: () => { },
            accept: () => { },
            cancel: () => { }
        };
        currentRow.data[identifyField.value] = identifyCell;
        const summaryCell: VisualDataCell = {
            colSpan: columns.length,
            rowSpan: 1,
            index: 1,
            field: groupField,
            data: dataItem[groupField],
            editingData: dataItem[groupField],
            parent: currentRow,
            mode: CellMode.readonly,
            getEditor: (cell) => cell.data,
            setRef: (vnode) => {
                if (vnode) {
                    const visualData = summaryCell.parent as VisualData;
                    if (summaryCell.cellHeight !== vnode.offsetHeight) {
                        summaryCell.cellHeight = vnode.offsetHeight;
                    }
                    if (summaryCell.cellHeight && summaryCell.cellHeight > (visualData.height || 0)) {
                        visualData.height = summaryCell.cellHeight;
                        dataItem.__fv_data_height__ = visualData.height;
                    }
                    const preDataItem = visualData.pre;
                    if (preDataItem) {
                        const topPosition = (preDataItem.__fv_data_position__ || 0) + (preDataItem.__fv_data_height__ || 0);
                        visualData.top = topPosition;
                        dataItem.__fv_data_position__ = topPosition;
                    }
                }
            },
            update: () => { },
            accept: () => { },
            cancel: () => { }
        };
        currentRow.data[groupField] = summaryCell;
        return currentRow;
    }

    function collpaseGroupIconClass(groupRow: VisualData) {
        const classObject = {
            'f-icon': true,
            'f-icon-arrow-chevron-down': true,
            'fv-grid-group-row-icon-collapse': groupRow.collapse
        } as Record<string, boolean>;
        return classObject;
    }

    return { collpaseGroupIconClass, renderGroupedData, renderGroupRow, renderSummaryRow };
}
