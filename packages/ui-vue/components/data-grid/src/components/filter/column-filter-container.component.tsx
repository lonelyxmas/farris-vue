/* eslint-disable @typescript-eslint/indent */
/**
 * Copyright (c) 2020 - present, Inspur Genersoft Co., Ltd.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *       http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

import { ref } from "vue";
import { CapsuleItem } from "../../../../capsule/src/capsule.props";
import { DataGridHeaderCell, UseFilter } from "../../composition/types";

export default function (
    useFilterComposition: UseFilter,
) {
    const items: CapsuleItem[] = [
        { name: '升序', value: 'asc', icon: 'f-icon f-icon-col-ascendingorder' },
        { name: '降序', value: 'desc', icon: 'f-icon f-icon-col-descendingorder' }
    ];
    const activeValue = ref('desc');
    const filterBoxPlaceholder = ref('请输入关键词');
    const searchIconContent = ref('<i class="f-icon f-icon-search"></i>');

    function onClear($event: MouseEvent, headerCell: DataGridHeaderCell) {
        // headerCell.showPopover = false;
    }

    function onConfirm($event: MouseEvent, headerCell: DataGridHeaderCell) {
        headerCell.showPopover = false;
    }

    function onCancel($event: MouseEvent, headerCell: DataGridHeaderCell) {
        headerCell.showPopover = false;
    }

    function renderFilterContainer(headerCell: DataGridHeaderCell) {
        return (
            <div class="fv-column-sort-filter-container">
                <div class="fv-column-sort-section">
                    <f-capsule items={items} v-model={activeValue}></f-capsule>
                </div>
                <div class="fv-column-filter-section">
                    <div class="fv-column-filter-section-title">筛选</div>
                    <div class="fv-column-filter-section-editor">
                        {useFilterComposition.getFilterEitor(headerCell)}
                    </div>
                </div>
                <div class="fv-column-sort-filter-footer">
                    <div class="fv-column-clear-section">
                        <f-button style="margin: 5px 0;padding-left: 0;"
                            type="link" onClick={(payload: MouseEvent) => onClear(payload, headerCell)}>
                            清空
                        </f-button>
                    </div>
                    <div class="fv-column-confirm-section">
                        <f-button style="margin: 5px;width: 48px;" onClick={(payload: MouseEvent) => onConfirm(payload, headerCell)}>
                            确定
                        </f-button>
                        <f-button style="margin: 5px;width: 48px;" type="secondary"
                            onClick={(payload: MouseEvent) => onCancel(payload, headerCell)}>
                            取消
                        </f-button>
                    </div>
                </div>
            </div>
        );
    }

    return { renderFilterContainer };
}
