import { ref } from "vue";
import { DataGridHeaderCell } from "../../composition/types";

export default function (headerCell: DataGridHeaderCell) {
    const filterBoxPlaceholder = ref('请输入关键词');
    const searchIconContent = ref('<i class="f-icon f-icon-search"></i>');
    return <f-button-edit
        button-content={searchIconContent.value}
        placeholder={filterBoxPlaceholder.value}
        enable-clear={true}
    ></f-button-edit>;
}
