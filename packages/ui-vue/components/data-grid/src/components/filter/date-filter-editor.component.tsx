import { ref } from "vue";
import { DataGridHeaderCell } from "../../composition/types";

export default function (headerCell: DataGridHeaderCell) {
    const filterBoxPlaceholder = ref('请输入关键词');
    const searchIconContent = ref('<i class="f-icon f-icon-search"></i>');
    const status = ref(false);
    const tags = [
        {
            name: '七天', selectable: true
        },
        {
            name: '一个月', selectable: true
        },
        {
            name: '三个月', selectable: true
        },
        {
            name: '半年', selectable: true
        }
    ];
    return <div style="display:flex;flex-direction:column;">
        <div style="display:flex;margin-bottom:6px;">
            <label style="margin-right:8px">按区间筛选</label>
            <f-switch v-model={status.value} size="small"></f-switch>
        </div>
        <f-date-picker></f-date-picker>
        <f-tags style="margin-top:10px" data={tags} selectable={true}></f-tags>
    </div>;
}
