import { ref } from "vue";
import { DataGridHeaderCell } from "../../composition/types";
import { Checkbox } from "../../../../checkbox/src/composition/types";

export default function (headerCell: DataGridHeaderCell) {

    const enumData: Checkbox[] = [
        { name: '是', value: true },
        { name: '否', value: false }
    ];

    return <div>
        <f-checkbox-group enum-data={enumData}></f-checkbox-group>
    </div>;
}
