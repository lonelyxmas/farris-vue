import { inject } from "vue";
import {  IUploadServer, TGetService, TNotifyService } from "../composition/type";
import { FFileUploadDefaultService } from "./default-service";
import { FFileUploaderService } from "./uploader-service";

export const getService = (): TGetService => {
    /**
     * 获取提示服务
     */
    const getNotifyService = ():TNotifyService => {
        const NotifyService = inject('NotifyService') as TNotifyService;
        return NotifyService;
    }
    /**
     * 获取提示服务
     */
    const getAPIService = (): FFileUploaderService => {
        let realUploadService = null;
        if(inject('IUploadServer',null)){
            let uploadServiceFromUser = inject('IUploadServer') as IUploadServer;
            if (uploadServiceFromUser&&uploadServiceFromUser()) {
                realUploadService = uploadServiceFromUser();
            } 
        }
        if(!realUploadService){
                realUploadService = new FFileUploadDefaultService()
        }
        // 获取注入的上传服务
       
        const uploadService = new FFileUploaderService(realUploadService);
        return uploadService;
    }
    return { getNotifyService, getAPIService }
}