/**
 * Copyright (c) 2020 - present, Inspur Genersoft Co., Ltd.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *       http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
import { computed, inject, ref, watch } from 'vue';
import { COMBO_LIST_TOKEN, PANEL_EVENTS } from '../const';
import { UsePanelEventOptions, UsePanelEvent, UsePanelOptions, UsePanel, Position, Placement } from '../types';

export function usePanel(options: UsePanelOptions): UsePanel {
    const { isPanelVisible, panelRef, context } = options;
    const comboListContext = inject(COMBO_LIST_TOKEN, null);
    const maxHeight = ref(comboListContext?.comboListProps.maxHeight || 350);
    const valueField = comboListContext?.comboListProps.valueField || 'id';
    const textField = comboListContext?.comboListProps.textField || 'label';
    const placement = comboListContext?.comboListProps.placement || Placement.bottom;
    const items = ref(comboListContext?.dataSource);
    const originalData = ref(comboListContext?.dataSource);

    /**
     * 搜索关键字
     */
    const keyword = ref('');
    /**
     * 下拉数据源
     */
    // const items = ref(options.props.data);
    /**
     * 是否启用搜索
     */
    const enableSearch = computed(() => {
        return comboListContext?.comboListProps.enableSearch || false;
    });
    /**
     * 面板样式
     */
    const panelStyle = computed(() => ({
        'z-index': 99999,
        position: 'absolute',
        background: '#fff',
        border: 'solid 1px #ededed'
    }));
    /**
     * 卡片样式
     */
    const cardStyle = computed(() => ({
        'max-height': enableSearch.value ? maxHeight.value - 36 + 'px' : maxHeight.value + 'px',
        overflow: 'auto',
        border: 'none'
    }));
    // 下拉面板样式
    const panelClass = computed(() => ({
        comboPanel: true,
        'f-area-hide': !isPanelVisible.value,
        'f-area-show': isPanelVisible.value
    }));
    // update element position
    const updatePanelPosition = (panelEl: any, relativePosition: Position, position: Placement = Placement.bottom) => {
        const panelHeight = panelEl?.getBoundingClientRect()?.height || 350;
        const relativeBottom = relativePosition.bottom || 0;
        const bodyHeight = document.body.offsetHeight;
        const relativeTop = relativePosition?.top || 0;
        const relativeHeight = relativePosition?.height || 0;

        // left、width不区分上下位置
        panelEl.style.left = relativePosition?.left + 'px';
        panelEl.style.width = relativePosition?.width + 'px';
        // 底部展示场景
        if (position === Placement.bottom || position === Placement.auto) {
            // 计算top
            panelEl.style.top = relativeTop + relativeHeight + 'px';
            // 计算下拉面板的可用高度
            const avaliableHeight = bodyHeight - relativeBottom;
            const computedHeight = panelHeight <= avaliableHeight ? panelHeight : avaliableHeight;
            maxHeight.value = computedHeight;
        } else if (position === Placement.top) {
            // 上部展示场景
            const avaliableHeight = relativeTop;
            const computedHeight = panelHeight <= avaliableHeight ? panelHeight : avaliableHeight;
            panelEl.style.top = relativeTop - computedHeight + 'px';
            maxHeight.value = computedHeight;
        }
    };
    watch([panelRef, isPanelVisible], ([panelEl, isPanelVisibleValue]) => {
        if (isPanelVisibleValue && panelEl) {
            const comboListRef = comboListContext?.comboListRef;
            const inputElement: Element = (comboListRef as any)?.$el?.parentNode?.querySelector('.input-group');
            // 启用搜索时搜索框自动获取焦点
            if (enableSearch.value) {
                const searchInput: any = panelEl.querySelector('input');
                if (searchInput) {
                    searchInput.focus();
                }
            }
            const location = inputElement?.getBoundingClientRect();
            updatePanelPosition(panelEl, location, placement);
            // 触发面板展示事件
            context.emit(PANEL_EVENTS.panelShow);
        }
    });
    /**
     * 监听搜索关键字
     */
    watch([keyword], ([keywordValue]) => {
        debounce(() => {
            if (!keywordValue) {
                return (items.value = originalData.value);
            }
            items.value = originalData?.value?.filter(
                (item) =>
                    item[valueField]?.startsWith(keywordValue) ||
                    item[valueField].toString().startsWith(keywordValue) ||
                    item[textField]?.startsWith(keywordValue)
            );
        }, 500)();
    });
    return {
        panelStyle,
        panelClass,
        cardStyle,
        enableSearch,
        keyword,
        items
    };
}
/**
 * 下拉列表事件处理钩子
 * @param options 上下文
 */
export function usePanelEvent(options: UsePanelEventOptions): UsePanelEvent {
    // 监听输入框下拉按钮事件
    const { context } = options;
    // #region events
    /**
     * 下拉列表容器点击事件处理器
     * @param $event event
     */
    function onPanelContainerClick($event: any) {
        context.emit(PANEL_EVENTS.panelHidden);
    }
    function onPanelItemClick($event: any) {
        context.emit(PANEL_EVENTS.panelHidden);
    }
    /**
     * 搜索事件
     * @param event
     */
    function onSearchClick(event: any) {}
    /**
     * 面板点击事件
     * @param event
     */
    function onPanelClick(event: any) {}
    // #endregion
    return {
        onPanelContainerClick,
        onPanelItemClick,
        onSearchClick,
        onPanelClick
    };
}
export function debounce(task: (...args: any) => any, timeout = 300) {
    let timer: number;
    return (...args: any) => {
        clearTimeout(timer);
        timer = window.setTimeout(() => {
            task(args);
        }, timeout);
    };
}
