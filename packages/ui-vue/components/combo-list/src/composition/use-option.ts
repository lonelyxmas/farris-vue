/**
 * Copyright (c) 2020 - present, Inspur Genersoft Co., Ltd.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *       http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
import { computed, inject, ref, SetupContext } from 'vue';
import { UseOption } from '../types';
import { OptionProps } from '../combo-list.props';
import { COMBO_LIST_PANEL_TOKEN, COMBO_LIST_TOKEN } from '../const';

export function useOption(props: OptionProps, context: SetupContext): UseOption {
    const comboListContext = inject(COMBO_LIST_TOKEN, null);
    const comboListPanelContext = inject(COMBO_LIST_PANEL_TOKEN, null);
    const dataSource = ref(comboListContext?.dataSource);
    const valueField = comboListContext?.comboListProps.valueField || 'id';
    const textField = comboListContext?.comboListProps.textField || 'label';
    const separator = comboListContext?.comboListProps.separator || ',';
    const isChecked = computed(() => {
        if (comboListContext?.modelValue) {
            return props.value !== undefined && comboListContext?.modelValue?.toString().split(separator).includes(props.value.toString());
        }
        return false;
    });
    // 是否支持多选
    const multiSelect = computed(() => {
        return comboListContext?.comboListProps.multiSelect || false;
    });
    const name = computed(() => {
        return props.name || props.value;
    });
    const defaultOptionSlot = comboListContext?.defaultOptionSlot;
    const onOptionClick = ($event: Event) => {
        if (!props.disabled && props.value !== undefined) {
            if (multiSelect.value) {
                // check the checkbox
                const modelValue = comboListContext?.modelValue || null;
                const modelValues = modelValue?.toString().split(separator) || [];
                const index = modelValues?.indexOf(props.value.toString());
                // collect values
                if (index !== -1) {
                    modelValues.splice(index, 1);
                } else {
                    modelValues.push(props.value.toString());
                }
                const items = dataSource.value?.filter((item) => item[valueField] && modelValues.includes(item[valueField].toString()));
                const values = items?.map((item) => item[valueField]).join(separator);
                const labels = items?.map((item) => item[textField]).join(separator);
                // update value
                comboListContext?.onValueChange({
                    value: values,
                    name: labels
                });
            } else {
                // update value
                comboListContext?.onValueChange({
                    value: props.value,
                    name: props.name
                });
                // close panel
                comboListPanelContext?.onPanelItemClick?.($event);
            }
        }
    };
    const isOptionSelected = computed(() => {
        if (!multiSelect.value) {
            return comboListContext?.modelValue === props.value;
        }
        return isChecked.value;
    });
    const optionClass = computed(() => ({
        active: isOptionSelected.value,
        'list-group-item': true,
        'list-group-item-action': true
    }));
    const rowStyle = computed(() => ({
        cursor: props.disabled ? 'not-allowed' : 'default'
    }));
    return {
        name,
        optionClass,
        isChecked,
        rowStyle,
        multiSelect,
        onOptionClick,
        defaultOptionSlot
    };
}
