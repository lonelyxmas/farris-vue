/**
 * Copyright (c) 2020 - present, Inspur Genersoft Co., Ltd.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *       http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
import { ref, SetupContext, watch } from 'vue';
import { ComboListProps } from '../combo-list.props';
import { EVENTS } from '../const';
import { Option, UseComboListEventOptions, UseComboListEvent } from '../types';

export function useComboList(props: ComboListProps, context: SetupContext) {
    const displayText = ref<string | undefined>('');
    const modelValue = ref(props.modelValue);
    const isPanelVisible = ref(false);
    const dataSource = ref();

    watch([dataSource], ([dataSourceValue]) => {
        if (props.modelValue) {
            const currentItem = dataSourceValue.find((item: any) => item[props.valueField] === props.modelValue);
            if (currentItem) {
                displayText.value = currentItem[props.textField];
            }
        }
    });
    // 有remote时优先使用远端数据
    if (props.remote) {
        const { url, method = 'GET', headers = {}, body = null } = props.remote;
        const requestInfo = { method, headers, body };
        fetch(url, requestInfo)
            .then((response) => {
                if (response.status === 200) {
                    return response.json();
                }
                const error = new Error(response.statusText);
                throw error;
            })
            .then((data) => {
                dataSource.value = data;
            })
            .catch((err) => {
                console.error(err);
            });
    } else {
        dataSource.value = props.data;
    }
    /**
     * 值变化事件处理器
     * @param item item
     */
    function onValueChange(item: Option) {
        if (modelValue.value) {
            modelValue.value = item.value;
        }
        displayText.value = item.name;
        context.emit('update:modelValue', item.value);
    }
    return {
        displayText,
        modelValue,
        isPanelVisible,
        dataSource,
        onValueChange
    };
}
/**
 * 下拉列表事件处理钩子
 * @param options 上下文
 */
export function useComboListEvent(options: UseComboListEventOptions): UseComboListEvent {
    // 监听输入框下拉按钮事件
    const { isPanelVisible, context, modelValue, props } = options;
    /**
     * window窗口大小变化事件处理器
     * @param event
     */
    const windowResizeEventHandler = (event: UIEvent) => {
        if (isPanelVisible.value !== false) {
            isPanelVisible.value = false;
        }
    };
    /**
     * window窗口滚动条滚动事件处理器
     * @param event
     */
    const windowScrollEventHandler = (event: Event) => {
        if (isPanelVisible.value !== false) {
            isPanelVisible.value = false;
        }
    };
    watch([isPanelVisible], ([isPanelVisibleValue]) => {
        if (isPanelVisibleValue) {
            window.addEventListener('resize', windowResizeEventHandler);
            window.addEventListener('scroll', windowScrollEventHandler);
        } else {
            window.removeEventListener('resize', windowResizeEventHandler);
            window.removeEventListener('scroll', windowScrollEventHandler);
        }
    });

    // #region events
    /**
     * 清空事件
     * @param $event event
     */
    function onClear($event: Event) {
        modelValue.value = '';
        if (props.hidePanelOnClear) {
            isPanelVisible.value = false;
        }
        context.emit(EVENTS.update, '');
        context.emit(EVENTS.clear);
    }
    /**
     * 下拉按钮点击
     * @param $event event
     */
    function onButtonClick($event: any) {
        isPanelVisible.value = !isPanelVisible.value;
    }
    /**
     * 下拉面板收起事件处理器
     * @param $event
     */
    function onPanelHidden($event: any) {
        isPanelVisible.value = false;
    }
    /**
     * 输入框点击事件
     * @param $event
     */
    function onClick($event: any) {
        if (props.editable === false) {
            isPanelVisible.value = true;
        }
    }
    // #endregion

    return {
        onClear,
        onClick,
        onButtonClick,
        onPanelHidden
    };
}
