/**
 * Copyright (c) 2020 - present, Inspur Genersoft Co., Ltd.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *       http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
import { ComputedRef, Ref, SetupContext, Slot } from 'vue';
import { ComboListProps, PanelProps } from './combo-list.props';

export type ModelValue = number | string | undefined | Array<number | string | undefined>;
/**
 * 数据展现方式
 */
export enum ViewType {
    Text = 'text',
    Tag = 'tag'
}
/**
 * 展示位置
 */
export const enum Placement {
    /**
     * 在控件的上方展示
     */
    top = 'top',
    /**
     * 在控件底部展示
     */
    bottom = 'bottom',
    /**
     * 根据控件的位置自动确认展示位置
     */
    auto = 'auto'
}
export interface UseOption {
    name: ComputedRef<string | number | undefined>;
    /**
     * 下拉列表行样式
     */
    rowStyle: ComputedRef<any>;
    onOptionClick: ($event: Event) => void;
    optionClass: ComputedRef<object>;
    multiSelect: ComputedRef<boolean>;
    isChecked: ComputedRef<boolean>;
    defaultOptionSlot: any;
}

export interface Option {
    disabled: boolean;
    [prop: string]: any;
}

export type Options = Array<Option>;

export interface ComboListContext {
    readonly isPanelVisible: boolean;
    readonly comboListProps: ComboListProps;
    readonly comboListRef: Ref<any>;
    readonly modelValue: string | number | undefined;
    dataSource: Ref<Options>;
    readonly defaultOptionSlot: Slot | undefined;
    readonly [prop: string]: any;
}

export interface ComboListPanelContext {
    readonly onPanelItemClick: (event: any) => void;
    readonly data: Options;
}
/**
 * combo-list事件钩子参数约定
 */
export interface UseComboListEventOptions {
    /**
     * is combo-list panel visible(opend)
     */
    isPanelVisible: Ref<boolean | unknown>;
    /**
     * combo-list props
     */
    props: ComboListProps;

    /**
     * setup context
     */
    context: SetupContext;
    /**
     * modelvalue
     */
    modelValue: Ref<string | number | undefined>;
}
export interface UseComboListEvent {
    /**
     * clear event handler
     */
    onClear: ($event: Event) => void;
    /**
     * button click event handler
     */
    onButtonClick: ($event: any) => void;
    /**
     * button click event handler
     */
    onPanelHidden: ($event: any) => void;
    /**
     * text box click event handler
     * @param $event
     * @returns
     */
    onClick: ($event: any) => void;
}
export interface UsePanelOptions {
    panelRef: Ref<any>;
    /**
     * combo-list props
     */
    props: PanelProps;
    /**
     * is combo-list panel visible(opend)
     */
    isPanelVisible: Ref<boolean>;
    /**
     * setup context
     */
    context: SetupContext;
}
export interface UsePanel {
    /**
     * panel class
     */
    panelClass: ComputedRef<object>;
    /**
     * panel styles
     */
    panelStyle: ComputedRef<any>;
    /**
     * panel card styles
     */
    cardStyle: ComputedRef<any>;
    enableSearch: ComputedRef<boolean>;
    keyword: Ref<string>;
    items: Ref<any>;
}
export interface UsePanelEventOptions {
    /**
     * is combo-list panel visible(opend)
     */
    isPanelVisible: Ref<boolean>;
    panelRef: Ref<any>;
    /**
     * setup context
     */
    context: SetupContext;
}
export interface UsePanelEvent {
    onPanelContainerClick: ($event: any) => void;
    onPanelItemClick: ($event: any) => void;
    onSearchClick: ($event: any) => void;
    onPanelClick: ($event: any) => void;
}

export interface UseOptions {
    idField: string | undefined;
    valueField: string | undefined;
    textField: string | undefined;
}
/**
 * position
 */
export interface Position {
    top: number;
    left: number;
    width: number;
    height: number;
    bottom: number;
}
/**
 * remote info
 */
export interface Remote {
    url: string;
    method?: 'GET' | 'POST' | 'PUT';
    headers?: any;
    body?: any;
}
