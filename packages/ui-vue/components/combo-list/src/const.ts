/**
 * Copyright (c) 2020 - present, Inspur Genersoft Co., Ltd.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *       http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
import { InjectionKey } from 'vue';
import { ComboListContext, ComboListPanelContext } from './types';

export const groupIcon = '<span class="f-icon f-icon-arrow-60-down"></span>';
/**
 * combo list injector token
 */
export const COMBO_LIST_TOKEN: InjectionKey<Readonly<ComboListContext>> = Symbol('fComboList');
/**
 * combo-list panel injector token
 */
export const COMBO_LIST_PANEL_TOKEN: InjectionKey<Readonly<ComboListPanelContext>> = Symbol('fComboListPanel');
/**
 * combo-list component events
 */
export const enum EVENTS {
    clear = 'clear',
    update = 'update:modelValue'
}
/**
 * panel component events
 */
export const enum PANEL_EVENTS {
    panelShow = 'panelShow',
    panelHidden = 'panelHidden'
}
