/**
 * Copyright (c) 2020 - present, Inspur Genersoft Co., Ltd.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *       http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
import { ExtractPropTypes, PropType } from 'vue';
import { Options, Placement, Remote, ViewType } from './types';

/**
 * 下拉列表属性
 */
export const comboListProps = {
    /**
     * 组件标识
     */
    id: { type: String },
    /**
     * 可选，是否可编辑
     * 默认`false`
     */
    editable: { default: false, type: Boolean },
    /**
     * 可选，是否禁用
     * 默认为`false`
     */
    disabled: { default: false, type: Boolean },
    /**
     * 可选，是否只读
     * 默认为`false`
     */
    readonly: { default: false, type: Boolean },
    /**
     * 最大输入长度
     */
    maxLength: { type: Number },
    /**
     * 占位符
     */
    placeholder: { type: String },
    /**
     * 可选，强制显示占位符
     * 默认`false`
     */
    forcePlaceholder: { default: false, type: Boolean },
    /**
     * 可选，是否启用清空
     * 默认启用
     */
    enableClear: { default: true, type: Boolean },
    /**
     * 可选，鼠标悬停时是否显示控件值
     * 默认显示
     */
    enableTitle: { default: true, type: Boolean },
    /**
     * 可选，下拉列表值展示方式
     * 支持text | tag，即文本或标签，默认为`ViewType.Text`，即文本方式`text`
     */
    viewType: { default: ViewType.Text, type: String as PropType<ViewType> },
    /**
     * 可选，字段映射
     */
    mapFields: { type: Object },
    /**
     * 下拉数据源
     */
    data: { type: Array as PropType<Options> },
    /**
     * 可选，数据源id字段
     * 默认为`id`
     */
    idField: { default: 'id', type: String },
    /**
     * 可选，数据源值字段
     * 默认为`id`
     */
    valueField: { default: 'id', type: String },
    /**
     * 可选，数据源显示字段
     * 默认为`label`
     */
    textField: { default: 'label', type: String },
    /**
     * 可选，是否支持多选
     * 默认`false`
     */
    multiSelect: { default: false, type: Boolean },
    /**
     * 远端数据源信息
     */
    remote: { default: null, type: Object as PropType<Remote> },
    /**
     * 可选，最大高度
     * 默认`350`
     */
    maxHeight: { default: 350, type: Number },
    /**
     * 可选，是否支持远端过滤
     * 默认`false`
     */
    remoteSearch: { default: false, type: Boolean },
    /**
     * 可选，清空值时隐藏面板
     * 默认`true`
     */
    hidePanelOnClear: { default: true, type: Boolean },
    /**
     * 可选，分隔符
     * 默认`,`
     */
    separator: { default: ',', type: String },
    /**
     * 可选，展示文本
     * 默认为空字符串
     */
    displayText: { type: String, default: '' },
    /**
     * 绑定值
     */
    modelValue: { type: [String, Number] },
    /**
     * 可选，下拉图标
     * 默认为'<span class="f-icon f-icon-arrow-60-down"></span>'
     */
    dropDownIcon: { type: String, default: '<span class="f-icon f-icon-arrow-60-down"></span>' },
    /**
     * tabIndex
     */
    tabIndex: { type: Number, default: -1 },
    /**
     * 可选，启用搜索
     * 默认为`false`
     */
    enableSearch: { type: Boolean, default: false },
    /**
     * 可选，下拉面板展示位置
     * 默认为`auto`
     */
    placement: { type: String as PropType<Placement>, default: Placement.auto }
};
export type ComboListProps = ExtractPropTypes<typeof comboListProps>;

/**
 * option 属性
 */
export const optionProps = {
    /**
     * 必须，值
     */
    value: { required: true, type: [String, Number] as PropType<string | number> },
    /**
     * 必须，名称
     */
    name: { required: true, type: [String, Number] as PropType<string | number> },
    /**
     * 可选，是否禁用
     * 默认`false`
     */
    disabled: { default: false, type: Boolean },
    /**
     * 是否已经勾选
     */
    checked: { default: false, type: Boolean }
};
export type OptionProps = ExtractPropTypes<typeof optionProps>;

/**
 * panel 属性
 */
export const panelProps = {
    /**
     * 下拉面板是否可见
     */
    isPanelVisible: { type: Boolean, default: false },
    /**
     * 可选，下拉列表展示位置
     * 默认为`bottom`
     */
    position: { type: String as PropType<Placement>, default: Placement.bottom }
};
export type PanelProps = ExtractPropTypes<typeof panelProps>;

/**
 * options 属性
 */
export const optionsProps = {
    options: {
        type: Array as PropType<Options>
    }
};
export type OptionsProps = ExtractPropTypes<typeof optionsProps>;
