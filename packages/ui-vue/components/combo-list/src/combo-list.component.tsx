/**
 * Copyright (c) 2020 - present, Inspur Genersoft Co., Ltd.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *       http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
import { defineComponent, provide, reactive, Ref, ref, SetupContext } from 'vue';
import { comboListProps, ComboListProps } from './combo-list.props';
import { COMBO_LIST_TOKEN, EVENTS } from './const';
import { useComboList, useComboListEvent } from './composition/use-combo-list';
import FPanel from './components/panel.component';

export default defineComponent({
    name: 'FComboList',
    props: comboListProps,
    emits: [EVENTS.clear, EVENTS.update]  as (string[] & ThisType<void>) | undefined,
    setup(props: ComboListProps, context: SetupContext) {
        const comboListRef: Ref<any> = ref();
        // component logic
        const { onValueChange, displayText, modelValue, isPanelVisible, dataSource } = useComboList(props, context);
        // event logic
        const { onClear, onButtonClick, onPanelHidden, onClick } = useComboListEvent({ props, context, isPanelVisible, modelValue });
        // combo-list context provider
        const comboListContext = reactive({
            comboListRef,
            isPanelVisible,
            comboListProps: props,
            onValueChange,
            modelValue,
            dataSource,
            defaultOptionSlot: context.slots.default
        });
        /**
         * provider
         */
        provide(COMBO_LIST_TOKEN, comboListContext);
        return () => {
            return (
                <>
                    {/** main component */}
                    <f-button-edit
                        ref={comboListRef}
                        id={props.id}
                        class={{ active: isPanelVisible }}
                        {...context.attrs}
                        disable={props.disabled}
                        readonly={props.readonly}
                        forcePlaceholder={props.forcePlaceholder}
                        editable={props.editable}
                        buttonContent={props.dropDownIcon}
                        placeholder={props.placeholder}
                        enableClear={props.enableClear}
                        maxLength={props.maxLength}
                        tabIndex={props.tabIndex}
                        enableTitle={props.enableTitle}
                        style="display:block"
                        v-model={displayText.value}
                        onClear={onClear}
                        onClick={onClick}
                        onClickButton={onButtonClick}></f-button-edit>
                    {isPanelVisible.value && <FPanel isPanelVisible={isPanelVisible.value} onPanelHidden={onPanelHidden} />}
                </>
            );
        };
    }
});
