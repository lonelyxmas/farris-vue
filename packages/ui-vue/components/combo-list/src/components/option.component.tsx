/**
 * Copyright (c) 2020 - present, Inspur Genersoft Co., Ltd.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *       http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
import { defineComponent, SetupContext, withModifiers } from 'vue';
import { optionProps, OptionProps } from '../combo-list.props';
import { useOption } from '../composition/use-option';

export default defineComponent({
    name: 'FOption',
    props: optionProps,
    emits: [] as (string[] & ThisType<void>) | undefined,
    setup(props: OptionProps, context: SetupContext) {
        const { name, optionClass, rowStyle, multiSelect, onOptionClick, isChecked, defaultOptionSlot } = useOption(props, context);
        return () => {
            return (
                <li style={rowStyle.value} class={optionClass.value} onClick={withModifiers(onOptionClick, ['prevent', 'stop'])}>
                    {multiSelect.value && (
                        <div
                            class="custom-control custom-checkbox  custom-control-inline"
                            style="margin:0;padding-right:0;float:none;vertical-align: top;">
                            <input
                                class="custom-control-input"
                                type="checkbox"
                                title="custom-input"
                                disabled={props.disabled}
                                v-model={isChecked.value}
                            />
                            <label class="custom-control-label"></label>
                        </div>
                    )}
                    <span class="f-datalist-item-text">{defaultOptionSlot ? defaultOptionSlot({ name: name.value }) : name.value}</span>
                </li>
            );
        };
    }
});
