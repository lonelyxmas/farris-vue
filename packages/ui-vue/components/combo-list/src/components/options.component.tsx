/**
 * Copyright (c) 2020 - present, Inspur Genersoft Co., Ltd.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *       http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
import { defineComponent, SetupContext } from 'vue';
import { optionsProps, OptionsProps } from '../combo-list.props';
import { useOptions } from '../composition/use-options';
import { Option } from '../types';
import FOption from './option.component';

export default defineComponent({
    name: 'FOptions',
    props: optionsProps,
    emits: [] as (string[] & ThisType<void>) | undefined,
    setup(props: OptionsProps, context: SetupContext) {
        const { valueField = 'id', textField = 'label' } = useOptions();
        return () => {
            return (
                <ul class="list-group list-group-flush">
                    {props?.options?.map((option: Option) => (
                        <FOption value={option[valueField]} name={option[textField]} disabled={option.disabled}></FOption>
                    ))}
                </ul>
            );
        };
    }
});
