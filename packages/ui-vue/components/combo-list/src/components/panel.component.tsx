/**
 * Copyright (c) 2020 - present, Inspur Genersoft Co., Ltd.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *       http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
import { defineComponent, provide, reactive, ref, SetupContext, toRefs, withModifiers } from 'vue';
import { PanelProps, panelProps } from '../combo-list.props';
import FOverlay from '../../../overlay/src/overlay.component';
import FOptions from './options.component';
import { usePanel, usePanelEvent } from '../composition/use-panel';
import { COMBO_LIST_PANEL_TOKEN, PANEL_EVENTS } from '../const';

export default defineComponent({
    name: 'FPanel',
    props: panelProps,
    emits: [PANEL_EVENTS.panelHidden, PANEL_EVENTS.panelShow] as (string[] & ThisType<void>) | undefined,
    setup(props: PanelProps, context: SetupContext) {
        const panelRef = ref<any>();
        const { isPanelVisible } = toRefs(props);
        const { panelStyle, panelClass, cardStyle, enableSearch, keyword, items } = usePanel({ isPanelVisible, props, panelRef, context });
        const { onPanelContainerClick, onPanelItemClick, onSearchClick, onPanelClick } = usePanelEvent({
            panelRef,
            isPanelVisible,
            context
        });
        // provider
        const providers = reactive({
            onPanelItemClick
        });
        provide(COMBO_LIST_PANEL_TOKEN, providers);
        return () => {
            return (
                isPanelVisible.value && (
                    <FOverlay onClick={onPanelContainerClick}>
                        <div
                            class={panelClass.value}
                            ref={panelRef}
                            style={panelStyle.value}
                            onClick={withModifiers(onPanelClick, ['prevent', 'stop'])}>
                            {enableSearch.value && (
                                <f-button-edit
                                    enableClear={true}
                                    style="padding:5px 10px"
                                    buttonContent="<i class='f-icon f-icon-search'></i>"
                                    v-model={keyword.value}
                                    onClickButton={onSearchClick}
                                />
                            )}
                            <div style="position: relative;" class="f-datalist">
                                <div class="card" style={cardStyle.value}>
                                    <FOptions options={items.value}></FOptions>
                                </div>
                            </div>
                        </div>
                    </FOverlay>
                )
            );
        };
    }
});
