/**
 * Copyright (c) 2020 - present, Inspur Genersoft Co., Ltd.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *       http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
import { computed, defineComponent, nextTick, ref, SetupContext, Teleport } from 'vue';
import { PopoverProps, popoverProps } from './popover.props';

import './popover.scss';

export default defineComponent({
    name: 'FPopover',
    props: popoverProps,
    emits: [] as (string[] & ThisType<void>) | undefined,
    setup(props: PopoverProps, context: SetupContext) {
        let scrollLeft = 0;

        let scrollTop = 0;

        if (typeof window !== 'undefined') {
            // eslint-disable-next-line prefer-destructuring
            scrollLeft = document.documentElement.scrollLeft;
            // eslint-disable-next-line prefer-destructuring
            scrollTop = document.documentElement.scrollTop;
        }

        const arrowRef = ref<any>();

        const popoverRef = ref<any>();

        const position = ref(props.placement);

        const popoverLeftPosition = ref('0px');

        const popoverTopPosition = ref('0px');

        const shouldShowTitle = computed(() => !!props.title);

        const displayPopover = ref(props.visible);

        const popoverClass = computed(() => {
            const originPopover = `popover in popover-${position.value}`;
            const bsPopover = `bs-popover-${position.value}`;
            const popoverClassObject = {} as any;
            popoverClassObject[originPopover] = true;
            popoverClassObject[bsPopover] = true;
            const customClassArray = (props.class || '').split(' ');
            customClassArray.reduce<Record<string, any>>((classObject, classString) => {
                classObject[classString] = true;
                return classObject;
            }, popoverClassObject);
            return popoverClassObject;
        });

        const popoverContainerClass = computed(() => ({
            'popover-content': true,
            'popover-body': true
        }));

        const popoverStyle = computed(() => {
            const styleObject = {
                left: popoverLeftPosition.value,
                top: popoverTopPosition.value
            };
            return styleObject;
        });

        async function show(reference: any) {
            if (popoverRef.value && arrowRef.value) {
                displayPopover.value = true;
                await nextTick();
                const hostRect = reference.getBoundingClientRect();
                const arrowRect = arrowRef.value.getBoundingClientRect();
                const popoverRect = popoverRef.value?.getBoundingClientRect();
                if (position.value === 'top') {
                    popoverLeftPosition.value = `${hostRect.left + hostRect.width / 2 + scrollLeft}px`;
                    popoverTopPosition.value = `${hostRect.top - (popoverRect.height + arrowRect.height) + scrollTop}px`;
                }
                if (position.value === 'bottom') {
                    popoverLeftPosition.value = `${(hostRect.left + hostRect.width / 2) -
                        (arrowRect.width / 2 - popoverRect.left) + scrollLeft}px`;
                    popoverTopPosition.value = `${hostRect.top + (hostRect.height + arrowRect.height) + scrollTop}px`;
                }

            }
        }

        function hide() {
            displayPopover.value = false;
        }

        const shown = computed(() => displayPopover.value);

        context.expose({ hide, show, shown });

        return () => {
            return (
                <>
                    <Teleport to="body">
                        <div ref={popoverRef} class={popoverClass.value} style={popoverStyle.value} v-show={displayPopover.value}>
                            <div ref={arrowRef} class="popover-arrow arrow"></div>
                            {shouldShowTitle.value && <h3 class="popover-title popover-header">{props.title}</h3>}
                            <div class={popoverContainerClass.value}>{context.slots.default && context.slots?.default()}</div>
                        </div>
                    </Teleport>
                </>
            );
        };
    }
});
