/**
 * Copyright (c) 2020 - present, Inspur Genersoft Co., Ltd.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *       http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
import { ExtractPropTypes, PropType } from 'vue';

export interface ButtonAppearance {
    class: string;
}

export interface ButtonConfig {
    id: string;
    disable: boolean;
    title: string;
    click: any;
    appearance: ButtonAppearance;
    visible?: boolean;
}

export interface ToolbarConfig {
    position: string;
    contents: ButtonConfig[];
}

export const sectionProps = {
    contentClass: { type: String, default: '' },
    maxStatus: { type: Boolean, default: false },
    enableAccording: { type: Boolean, default: false },
    enableCollapse: { type: Boolean, default: true },
    mainTitle: { type: String, default: '' },
    subTitle: { type: String, default: '' },
    showHeader: { type: Boolean, default: true },
    enableMaximize: { type: Boolean, default: false },
    fill: { type: Boolean, default: false },
    expandStatus: { type: Boolean, default: true },
    cotext: { type: Object },
    index: { type: Number },
    toolbarPosition: { type: String, default: '' },
    toolbarButtons: { type: Array<object>, default: [] },
    toolbar: { type: Object as PropType<ToolbarConfig>, default: {} },
    showToolbarMoreButton: { type: Boolean, default: true },
    clickThrottleTime: { type: Number, default: 350 },
    headerClass: { type: String, default: '' }
};
export type SectionProps = ExtractPropTypes<typeof sectionProps>;
