import { Ref } from "vue";

export interface UseItem {

    getKey(item: any, index: number): any;

    listViewItemClass(item: any): Record<string, any>;

    onCheckItem($event: MouseEvent, item: any, index: number): any;

    onClickItem($event: MouseEvent, item: any, index: number): any;

    onMouseoverItem($event: MouseEvent, item: any): any;
}

export interface UseSelection {
    clearSelection(): void;

    findIndexInSelectedItems(item: any): number;

    toggleSelectItem(item: any, index: number): void;
}
