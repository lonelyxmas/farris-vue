import { Ref, SetupContext, computed, ref } from "vue";
import { ListViewProps, MultiSelectMode } from "../list-view.props";
import { UseSelection } from "./types";

export function useSelection(props: ListViewProps, context: SetupContext): UseSelection {
    const identifyField = ref(props.idField);
    const enableMultiSelect = ref(props.multiSelect);
    const selectedItems: Ref<any[]> = ref([]);
    const multiSelectMode: Ref<MultiSelectMode> = ref(props.multiSelectMode as MultiSelectMode);

    const shouldClearSelection = computed(() => {
        return !enableMultiSelect.value;
    });

    function getKey(item: any) {
        return item[identifyField.value] || '';
    }

    function findIndexInSelectedItems(item: any): number {
        const selectItemIndex = selectedItems.value.findIndex((selectedItem: any) => {
            return getKey(selectedItem) === getKey(item);
        });
        return selectItemIndex;
    }

    function clearSelection() {
        selectedItems.value.forEach((selectionItem: any) => { selectionItem.checked = false; });
        selectedItems.value = [];
    }

    function toggleSelectItem(item: any, index: number) {
        if (shouldClearSelection.value) {
            clearSelection();
        }

        const currentItemIndexInSelection = findIndexInSelectedItems(item);
        const hasSelectedCurrentItem = currentItemIndexInSelection > -1;

        if (hasSelectedCurrentItem) {
            selectedItems.value.splice(currentItemIndexInSelection, 1);
            item.checked = false;
        } else {
            selectedItems.value.unshift(item);
            item.checked = true;
        }
        context.emit('selectChange', { item, index });
    }

    return { clearSelection, findIndexInSelectedItems, toggleSelectItem };
}
