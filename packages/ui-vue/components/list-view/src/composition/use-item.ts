import { Ref, SetupContext, computed, ref } from "vue";
import { ListViewProps, MultiSelectMode } from "../list-view.props";
import { UseItem, UseSelection } from "./types";

export function useItem(props: ListViewProps, useSelectionCompostion: UseSelection, context: SetupContext): UseItem {
    const identifyField = ref(props.idField);
    const draggable = ref(props.draggable);
    const listClassName = ref('');
    const hoverIndex = ref(-1);
    const focusedItemId = ref('');
    const enableMultiSelect = ref(props.multiSelect);
    const multiSelectMode: Ref<MultiSelectMode> = ref(props.multiSelectMode as MultiSelectMode);
    const keepSelect = ref(true);
    const selectedItems: Ref<any[]> = ref([]);
    const { clearSelection, toggleSelectItem } = useSelectionCompostion;

    function listViewItemClass(item: any) {
        const classObject = {
            'f-list-view-group-item': true,
            'f-list-view-draggable-item': draggable.value,
            'f-un-click': !!item.unClick,
            'f-un-select': !!item.selectDisable,
            'f-listview-active': item.index === hoverIndex.value
        } as Record<string, boolean>;
        const customClassArray = listClassName.value.split(' ');
        customClassArray.reduce((result, className) => {
            result[className] = true;
            return result;
        }, classObject);
        return classObject;
    };

    function getKey(item: any, index: number) {
        return item[identifyField.value] || '';
    }

    const shouldFocusedItemOnCheck = computed(() => {
        return !enableMultiSelect.value;
    });

    function onCheckItem($event: MouseEvent, item: any, index: number) {
        if (item.unClick || item.selectDisable) {
            return;
        }
        if (shouldFocusedItemOnCheck.value) {
            focusedItemId.value = item[identifyField.value];
        }
        toggleSelectItem(item, index);
    }

    const shouldClearSelectionOnClick = computed(() => {
        return enableMultiSelect.value && multiSelectMode.value === 'OnCheckClearByClick';
    });

    const shouldSelectItemOnClick = computed(() => {
        return !enableMultiSelect.value ||
            (
                enableMultiSelect.value &&
                (multiSelectMode.value === 'OnCheckAndClick' || multiSelectMode.value === 'OnClick')
            );
    });

    function onClickItem($event: MouseEvent, item: any, index: number) {
        if (item.unClick || item.selectDisable) {
            return;
        }
        focusedItemId.value = item[identifyField.value];
        if (shouldClearSelectionOnClick.value) {
            clearSelection();
        }
        if (shouldSelectItemOnClick.value) {
            toggleSelectItem(item, index);
        }
        context.emit('clickItem', { data: selectedItems.value, index });
    }

    function onMouseoverItem($event: MouseEvent, item: any) {
        hoverIndex.value = item.index;
    }

    return { getKey, listViewItemClass, onCheckItem, onClickItem, onMouseoverItem };
}
