import { UseItem } from "../../composition/types";
import { ListViewProps } from "../../list-view.props";
import FListViewCheckBox from '../../components/list-view-checkbox.component';
import { SetupContext, ref } from "vue";

export default function (
    props: ListViewProps,
    context: SetupContext,
    useItemCompostion: UseItem
) {
    const enableMultiSelect = ref(props.multiSelect);
    const { getKey, listViewItemClass, onCheckItem, onClickItem, onMouseoverItem } = useItemCompostion;
    let dragIndex = 0;

    function dragstart(e: any, index: number) {
        e.stopPropagation();
        dragIndex = index;
        setTimeout(() => {
            e?.target?.classList?.add('moveing');
        }, 0);
    }
    function dragenter(e: any, index: number) {
        e.preventDefault();
        // 拖拽到原位置时不触发
        if (dragIndex !== index) {
            // const source = drag.list[dragIndex];
            // drag.list.splice(dragIndex, 1);
            // drag.list.splice(index, 0, source);

            // 更新节点位置
            dragIndex = index;
        }
    }
    function dragover(e: any) {
        e.preventDefault();
        e.dataTransfer.dropEffect = 'move';
    }
    function dragend(e: any) {
        e?.target?.classList?.remove('moveing');
    }

    function renderItem(item: any, index: number, clickItem: any) {
        return (
            <li
                class={listViewItemClass(item)}
                id={getKey(item, index)}
                onClick={(payload: MouseEvent) => onClickItem(payload, item, index)}
                onMouseover={(payload: MouseEvent) => onMouseoverItem(payload, item)}
                draggable="true"
                onDragstart={(payload: DragEvent) => dragstart(payload, index)}
                onDragenter={(payload: DragEvent) => dragenter(payload, index)}
                onDragend={(payload: DragEvent) => dragend(payload)}
                onDragover={(payload: DragEvent) => dragover(payload)}>
                {enableMultiSelect.value && (
                    <div class="f-list-select" onClick={(payload: MouseEvent) => payload.stopPropagation()}>
                        <FListViewCheckBox
                            id={'list-' + getKey(item, index)}
                            disabled={item.selectDisable || item.unClick}
                            checked={item.checked}
                            onChange={($event: any) => onCheckItem($event, item, index)}></FListViewCheckBox>
                    </div>
                )}
                <div class="f-list-content">
                    <div style="margin: 10px 0;">{item.name}</div>
                </div>
                <div class="f-list-remove"><div><i class="f-icon f-icon-remove_face"></i></div></div>
                <div class="f-list-handle"><div><i class="f-icon f-icon-drag-vertical"></i></div></div>
            </li>
        );
    }

    return { renderItem };
}
