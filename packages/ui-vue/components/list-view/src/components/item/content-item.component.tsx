import { UseItem } from "../../composition/types";
import { ListViewProps } from "../../list-view.props";
import FListViewCheckBox from '../../components/list-view-checkbox.component';
import { SetupContext, ref } from "vue";

export default function (
    props: ListViewProps,
    context: SetupContext,
    useItemCompostion: UseItem
) {
    const enableMultiSelect = ref(props.multiSelect);
    const { getKey, listViewItemClass, onCheckItem, onClickItem, onMouseoverItem } = useItemCompostion;

    function renderListViewItemContent(item: any, index: number, selectedItem: any) {
        if (context.slots.content) {
            return <>{context.slots.content && context.slots.content({ item, index, selectedItem })}</>;
        }
        return <div style="margin: 10px 0;">{item.name}</div>;
    }

    function renderItem(item: any, index: number, clickItem: any) {
        return (
            <li
                class={listViewItemClass(item)}
                id={getKey(item, index)}
                onClick={(payload: MouseEvent) => onClickItem(payload, item, index)}
                onMouseover={(payload: MouseEvent) => onMouseoverItem(payload, item)}>
                {enableMultiSelect.value && (
                    <div class="f-list-select" onClick={(payload: MouseEvent) => payload.stopPropagation()}>
                        <FListViewCheckBox
                            id={'list-' + getKey(item, index)}
                            disabled={item.selectDisable || item.unClick}
                            checked={item.checked}
                            onChange={($event: any) => onCheckItem($event, item, index)}></FListViewCheckBox>
                    </div>
                )}
                <div class="f-list-content">
                    {renderListViewItemContent(item, index, clickItem)}
                </div>
            </li>
        );
    }

    return { renderItem };
}
