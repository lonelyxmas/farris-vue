/**
 * Copyright (c) 2020 - present, Inspur Genersoft Co., Ltd.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *       http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
import { defineComponent, ref, SetupContext } from 'vue';
import { listViewCheckBoxProps, ListViewCheckBoxProps } from './list-view-checkbox.props';

export default defineComponent({
    name: 'FListViewCheckBox',
    props: listViewCheckBoxProps,
    emits: ['change'] as (string[] & ThisType<void>) | undefined,
    setup(props: ListViewCheckBoxProps, context: SetupContext) {
        const checked = ref(props.checked);
        const disabled = ref(props.disabled);
        const id = ref(props.id);

        function handleClick(event: MouseEvent) {
            event.stopPropagation();
            checked.value = !checked.value;
            if (!disabled.value) {
                context.emit('change', { originalEvent: event, checked: !checked.value });
            }
        }
        return () => {
            return (
                <div class="custom-control custom-checkbox  custom-control-inline listview-checkbox">
                    <input title={id.value} type="checkbox" class="custom-control-input" v-model={checked.value}></input>
                    <label class="custom-control-label" onClick={(payload: MouseEvent) => handleClick(payload)}></label>
                </div>
            );
        };
    }
});
