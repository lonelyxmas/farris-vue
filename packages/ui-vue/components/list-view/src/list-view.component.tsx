/**
 * Copyright (c) 2020 - present, Inspur Genersoft Co., Ltd.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *       http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
import { computed, defineComponent, Ref, ref, SetupContext, watch } from 'vue';
import { ListViewProps, listViewProps } from './list-view.props';
import FListViewCheckBox from './components/list-view-checkbox.component';
import { useItem } from './composition/use-item';

import './list-view.scss';
import { useSelection } from './composition/use-selection';
import getSingleItem from './components/item/single-item.component';
import getContentItem from './components/item/content-item.component';
import getDraggableItem from './components/item/draggable-item.component';

export default defineComponent({
    name: 'FListView',
    props: listViewProps,
    emits: ['checkValuesChange', 'clickItem', 'selectChange'] as (string[] & ThisType<void>) | undefined,
    setup(props: ListViewProps, context: SetupContext) {
        const enableMultiSelect = ref(props.multiSelect);
        const cardView = ref(props.cardView);
        const enablePagination = ref(true);
        const clickItem = ref<any>({});
        const emptyMessage = ref('暂无数据');
        const keepSelect = ref(true);
        const enableMorePageSelect = ref(false);
        const selectDataListWithOtherPager: any[] = [];
        const listidName = ref('id');

        let selectDataList: any[] = [];
        const currentData = ref<any[]>(props.data.map((origin: any, index: number) => {
            if (typeof origin !== 'object') {
                return { index, name: origin };
            }
            if (!Object.hasOwn(origin, 'index')) {
                return Object.assign({ index }, origin);
            }
            return origin;
        }));
        const data = ref<any[]>(props.data);

        const listViewClass = computed(() => {
            const classObject = {
                'f-list-view': true,
                'f-list-view-multiple': enableMultiSelect.value
            } as Record<string, boolean>;
            return classObject;
        });

        const listViewGroupClass = computed(() => {
            const classObject = {
                'f-list-view-group': true,
                'd-flex': cardView.value,
                'flex-wrap': cardView.value
            } as Record<string, boolean>;
            return classObject;
        });

        const shouldShowListViewGroupItem = computed(() => !!currentData.value && currentData.value.length > 0);

        const useSelectionComposition = useSelection(props, context);

        const useItemCompostion = useItem(props, useSelectionComposition, context);
        const { listViewItemClass, onCheckItem, onClickItem, onMouseoverItem } = useItemCompostion;

        const showEmpty = computed(() => currentData.value.length === 0);

        const shouldShowEmptyTemplate = computed(() => {
            return showEmpty.value && !context.slots.empty;
        });

        const shouldShowFooter = computed(() => {
            return !!context.slots.footer || enablePagination.value;
        });

        function getKey(listItem: any, index: number) {
            return '';
        }

        function updateDataListForMorePager(active: 'remove' | 'append' | '', listItem?: any, emit = true) { }

        function onCheckValuesChange(ids: any[]) {
            context.emit('checkValuesChange', ids);
        }

        function selectListViewItem($event: Event, listItem: any, index: number, checkChangeEvent = false) {
            const isClickCheckbox = typeof event === 'boolean';

            if (listItem.unClick || listItem.selectDisable) {
                return;
            }
            context.emit('selectChange', { listdata: listItem, index });
            if (enableMultiSelect.value) {
                if (listItem.checked) {
                    if (!keepSelect.value || isClickCheckbox) {
                        // 支持多选 删除该选中行
                        const selectItemIndex = selectDataList.findIndex((item, i) => {
                            return getKey(item, i) === getKey(listItem, index);
                        });
                        selectDataList.splice(selectItemIndex, 1);
                        updateDataListForMorePager('remove', listItem);
                        listItem.checked = false;
                    }
                } else {
                    // 支持多选 插入该选中行
                    const result = selectDataList.find((value, i) => {
                        return getKey(value, i) === getKey(listItem, index);
                    });
                    if (!result) {
                        selectDataList.unshift(listItem);
                        updateDataListForMorePager('append', listItem);
                        listItem.checked = true;
                    }
                }
                // listItem['checked'] = !listItem['checked'];
            } else {
                // 不支持多选 替换当前选中行
                selectDataList = [];
                selectDataList.push(listItem);
            }
            if (!checkChangeEvent) {
                clickItem.value = listItem;
            }
            context.emit('clickItem', { data: selectDataList, index, checkChangeEvent });

            if (enableMultiSelect.value) {
                let ids = data.value.filter((item) => !!item.checked).map((item) => item[listidName.value]);
                if (enableMorePageSelect.value) {
                    ids = selectDataListWithOtherPager.map((item) => item[listidName.value]);
                }
                onCheckValuesChange(ids);
            }
        }

        function getSelect(event: any, changeData: any, i: number) {
            const state = event.checked;
            selectListViewItem(state, changeData, i, true);
        }

        function renderListViewItemContent(item: any, index: number, selectedItem: any) {
            if (context.slots.content) {
                return <>{context.slots.content && context.slots.content({ item, index, selectedItem: clickItem })}</>;
            }
            return <div style="margin: 10px 0;">{item.name}</div>;
        }

        function getRenderFactory() {
            if (context.slots.content) {
                return getContentItem;
            }
            return getDraggableItem;
        }

        const renderFactory = getRenderFactory();

        const { renderItem } = renderFactory(props, context, useItemCompostion);

        return () => {
            return (
                <div class={listViewClass.value}>
                    {context.slots.header && <div class="f-list-view-header">{context.slots.header()}</div>}
                    <div class="f-list-view-content">
                        <ul class={listViewGroupClass.value} style="list-style: none;">
                            {shouldShowListViewGroupItem.value &&
                                currentData.value.map((item, index) => renderItem(item, index, clickItem))}
                            {shouldShowEmptyTemplate.value && (
                                <div class="f-list-view-emptydata">
                                    <p class="f-empty-title">{emptyMessage.value}</p>
                                </div>
                            )}
                            {context.slots.empty && context.slots.empty()}
                        </ul>
                    </div>
                    {shouldShowFooter.value && <div class="f-list-view-footer">{context.slots.footer && context.slots.footer()}</div>}
                </div>
            );
        };
    }
});
