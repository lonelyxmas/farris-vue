/* eslint-disable no-use-before-define */
/**
 * Copyright (c) 2020 - present, Inspur Genersoft Co., Ltd.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *       http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
import { computed, defineComponent, onBeforeMount, onMounted, ref, SetupContext } from 'vue';
import { listNavProps, ListNavProps } from './list-nav.props';

import './list-nav.scss';

export default defineComponent({
    name: 'FListNav',
    props: listNavProps,
    emits: ['navState', 'rzStart', 'rzResizing', 'rzStop'] as (string[] & ThisType<void>) | undefined,
    setup(props: ListNavProps, context: SetupContext) {
        /** 拖拽元素 */
        const draggableWrapper = ref<HTMLDivElement | null>(null);
        /** 初始收起状态 */
        let initCollapse = false;
        /** 处理关联变动中，另外一处有iframe，iframe影响拖拽的性能 */
        let _relatedIframeParent: string | any;
        /** 记录真实的iframe父容器 */
        let _relatedContainer: any = null;
        /** 标记iframe父容器的样式属性position的值 */
        let _relatedContainerPosition = "";
        /** 记录iframe在拖拽时的遮罩层 */
        const _relatedIframeOverlay = ref<HTMLDivElement | null>(null);
        /** 标记iframe遮罩层是否已显示 */
        let _iframeOverlayShownFlag = false;
        /** collapse的动画样式名 */
        const animateCls = "splitter-pane-collapse-animate";
        /** 标记是否需要更新变量 */
        let updateParamsFlag = false;
        /** 标记拖拽时上次的大小 */
        let resizeDistance = -1;
        /** 初始时，是否隐藏Nav */
        const hideNav = computed({
            set(value: any) {
                if (value !== undefined && value !== null) {
                    if (initCollapse !== value) {
                        initCollapse = value;
                    }
                }
            },
            get() {
                return initCollapse;
            }
        });
        const relatedIframeParent = computed({
            set(value: string | any) {
                _relatedIframeParent = value;
                getRelatedIframeParent(value);
            },
            get() {
                return _relatedContainer;
            }
        });
        const headerTemplate = (template: any) => {
            return template;
        };

        const navContentTemplate = (template: any) => {
            return template;
        };

        const navFooterTemplate = (template: any) => {
            return template;
        };

        onBeforeMount(() => {
            if (document.documentElement) {
                compatibleEl();
            }
        });
        onMounted(() => {
            if (!document.documentElement) {
                compatibleEl();
            }
        });
        /** 兼容旧表单写法 */
        function compatibleEl() {
            // 设置了iframe容器，但是没有创建遮罩
            if (_relatedIframeParent && !_relatedIframeOverlay.value) {
                getRelatedIframeParent(_relatedIframeParent);
            }
        }
        /**
         * 点击収折
         * @param event
         */
        function toggle(event: any) {
            event && event.stopPropagation();
            if (props.disabled) {
                return;
            }
            // 初始没有加上这个样式，是避免初始设置收起的时候，看到有収折的动画
            const targetElement = document.documentElement?.querySelector('.f-list-nav-in');
            if (targetElement) {
                targetElement.classList.add(animateCls);
            }
            initCollapse = !initCollapse;
            context.emit('navState', { 'showNav': !hideNav.value });
        }
        /** 拖拽的变化中事件，如果超过阈值，不处理 */
        function onRZResizing(pos: any) {
            if (pos && pos.size) {
                const changeProp = getStyleProp();
                let resizeDirection = "";
                if (resizeDistance >= 0) {
                    // 开始记录后, 此次值比上次记录大，说明在expand
                    resizeDirection = pos.size[changeProp] > resizeDistance ? "expand" : "collapse";
                }
                resizeDistance = pos.size[changeProp];
                if (resizeDirection === "expand" && initCollapse) { initCollapse = false; }
                // 拖拽速度快的时候，会漏掉计算的时机
                if (pos.size[changeProp] > props.resizeCollapseThreshold || !resizeDirection) {
                    return;
                }
                if (props.resizeCollapseThreshold > 0) {
                    // 设置了自动収折阈值
                    if (!initCollapse && resizeDirection === "collapse") {
                        // 处理拖拽収折过程中，自动收起；不处理展开过程中的，自动展开
                        if (
                            pos.actions &&
                            'stopResize' in pos.actions
                        ) {
                            pos.actions.stopResize();
                        }
                        toggle(null);
                    }
                }
                else if (resizeDirection === "collapse" && pos.size[changeProp] < 3 && !initCollapse) {
                    initCollapse = true;
                }
            }
            context.emit('rzResizing', pos);
        }
        // 拖拽的开始的事件
        function onRZStart(pos: any) {
            // 禁用収折
            if (draggableWrapper?.value?.classList.contains(animateCls)) {
                document.documentElement.classList.remove(animateCls);
            }
            // 如果有iframe遮罩并且没有显示
            if (_relatedIframeOverlay.value && !_iframeOverlayShownFlag) {
                if (["relative", "absolute"].indexOf(_relatedContainerPosition) < 0) {
                    const _relatedContainer = ref<HTMLDivElement | null>(null);
                    const _relatedContainerRefValue = _relatedContainer.value;
                    _relatedContainerRefValue?.style?.setProperty('position', 'relative');
                }
                const _relatedIframeOverlay = ref<HTMLDivElement | null>(null);
                const _relatedIframeOverlayRefValue = _relatedIframeOverlay.value;
                _relatedIframeOverlayRefValue?.style?.setProperty('display', 'block');
                _iframeOverlayShownFlag = true;
            }
            if (!updateParamsFlag && pos.actions && "updateParams" in pos.actions) {
                pos.actions.updateParams({ fixedEdge: true });
                updateParamsFlag = true;
            }
            context.emit('rzStart', pos);
        }
        /** 拖拽的停止事件 */
        function onRZStop(pos: any) {
            resizeDistance = -1;
            if (_relatedIframeOverlay.value && _iframeOverlayShownFlag) {
                if (["relative", "absolute"].indexOf(_relatedContainerPosition) < 0) {
                    const _relatedContainer = ref<HTMLDivElement | null>(null);
                    const _relatedContainerRefValue = _relatedContainer.value;
                    _relatedContainerRefValue?.style?.setProperty('position', _relatedContainerPosition);
                }
                const _relatedIframeOverlay = ref<HTMLDivElement | null>(null);
                const _relatedIframeOverlayRefValue = _relatedIframeOverlay.value;
                _relatedIframeOverlayRefValue?.style?.setProperty('display', 'none');
                _iframeOverlayShownFlag = false;
            }
            context.emit('rzStop', pos);
        }
        /**
        * 布局排列不一样，影响到是设置宽度还是高度
        */
        function getStyleProp() {
            const hori = ["left", "right"].findIndex(item => item === props.position);
            return hori > -1 ? "width" : "height";
        }

        /**
         * 规范获取iframe所在的父容器
         */
        function getRelatedIframeParent(content: any) {
            if (!content || !document.documentElement) {
                _relatedContainer = null;
            } else if (content === document.documentElement) {
                _relatedContainer = content;
            } else if (typeof content === "string") {
                _relatedContainer = document.documentElement?.parentElement?.querySelector(content);
            } else {
                _relatedContainer = content;
            }
            if (_relatedContainer) {
                _relatedContainerPosition = _relatedContainer.style.position;
                // 构造遮罩层
                const _relatedIframeOverlayRefValue = document.createElement("div");
                _relatedIframeOverlayRefValue.className = "f-utils-absolute-all";
                _relatedIframeOverlayRefValue.style.setProperty('display', 'none');
                if (_relatedIframeOverlayRefValue) {
                    document.body.appendChild(_relatedIframeOverlayRefValue);
                }
            }

        }

        return () => {
            return (
                <div>
                    <div class={`f-list-nav f-list-nav-${props.position}`}>
                        {!props.enableDraggable && (
                            <div
                                class="f-list-nav-in"
                                style={{
                                    width: ['left', 'right'].includes(props.position)
                                        ? !hideNav.value
                                            ? `${props.listNavWidth}px` : '0px'
                                        : '100%',
                                    height: ['top', 'bottom'].includes(props.position)
                                        ? !hideNav.value
                                            ? `${props.listNavWidth}px` : '0px'
                                        : '100%',
                                }}
                            >
                                <div>
                                    <div class="f-list-nav-main">
                                        {context.slots.navHeader && <div class="f-list-nav-header">{context.slots.navHeader()}</div>}
                                        {!context.slots.navHeader && props.title && (
                                            <div class="f-list-nav-header" >
                                                <div class="f-list-nav-title">
                                                    {props.title}
                                                </div>
                                            </div>
                                        )}
                                    </div>
                                    <div>
                                        {context.slots.navContent && <div class="f-list-nav-content">{context.slots.navContent()}</div>}
                                    </div>
                                    <div>
                                        {context.slots.navFooter && <div class="f-list-nav-footer">{context.slots.navFooter()}</div>}
                                    </div>
                                </div>
                            </div>
                        )}
                        {props.enableDraggable && (
                            <div
                                class="f-list-nav-in"
                                ref={draggableWrapper}
                                style={{
                                    width: ['left', 'right'].includes(props.position)
                                        ? !hideNav.value
                                            ? `${props.listNavWidth}px` : '0px'
                                        : '100%',
                                    height: ['top', 'bottom'].includes(props.position)
                                        ? !hideNav.value
                                            ? `${props.listNavWidth}px` : '0px'
                                        : '100%',
                                }}
                            >
                                <div>
                                    <div class="f-list-nav-main">
                                        {context.slots.navHeader && <div class="f-list-nav-header">{context.slots.navHeader()}</div>}
                                        {!context.slots.navHeader && props.title && (
                                            <div class="f-list-nav-header" >
                                                <div class="f-list-nav-title">
                                                    {props.title}
                                                </div>
                                            </div>
                                        )}
                                    </div>
                                    <div>
                                        {context.slots.navContent && <div class="f-list-nav-content">{context.slots.navContent()}</div>}
                                    </div>
                                    <div>
                                        {context.slots.navFooter && <div class="f-list-nav-footer">{context.slots.navFooter()}</div>}
                                    </div>
                                </div>
                            </div>
                        )}
                    </div>
                </div >
            );
        };
    }
});
